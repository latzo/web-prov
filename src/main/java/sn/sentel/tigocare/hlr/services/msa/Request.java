package sn.sentel.tigocare.hlr.services.msa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String requestId;
    private String featureId;
    private String timeStamp;
    private String languageId;
    Data DataObject;
    private String channelId;
}
