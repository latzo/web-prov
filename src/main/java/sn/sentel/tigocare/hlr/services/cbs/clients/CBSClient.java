package sn.sentel.tigocare.hlr.services.cbs.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.WSAHeader;
import sn.sentel.tigocare.jaxb2.cbs.*;
import sn.sentel.tigocare.repository.MainProductsCbsRepository;

import java.util.UUID;

@Slf4j
@Component
public class CBSClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:CBS_CLIENT]";
    @Autowired private MainProductsCbsRepository cbsRepository;
    private ObjectFactory objectFactory = new ObjectFactory();

    public QueryBasicInfoResultMsg queryBasicInfo(String msisdn){
        QueryBasicInfoRequestMsg req = this.objectFactory.createQueryBasicInfoRequestMsg();
        req.setRequestHeader(this.buildQueryBasicInfoRequestHeader());

        QueryBasicInfoRequest request = this.objectFactory.createQueryBasicInfoRequest();
        request.setSubscriberNo(msisdn);
        request.setQueryType(4);

        req.setQueryBasicInfoRequest(request);
        return (QueryBasicInfoResultMsg) this.getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("QueryBasicInfo"));
    }

    public NewSubscriberResultMsg createNewSubscriber(String msisdn, String imsi, String mainProductId ){
        NewSubscriberRequestMsg req = this.objectFactory.createNewSubscriberRequestMsg();
        req.setRequestHeader(buildRequestHeader());

        NewSubscriberRequest subscriberRequest = this.objectFactory.createNewSubscriberRequest();
        subscriberRequest.setSubscriberNo(msisdn);

        NewSubscriberRequest.Subscriber subs = new NewSubscriberRequest.Subscriber();
        subs.setLang("1");
        subs.setPaidMode("0");
        Integer initialCredit = 0;
        subs.setInitialCredit(this.objectFactory.createSubscriberBasicInitialCredit(initialCredit));
        subs.setMainProductID(mainProductId);
        subs.setIMSI(this.objectFactory.createSubscriberIMSI(imsi));
        subscriberRequest.setSubscriber(subs);

        req.setNewSubscriberRequest(subscriberRequest);

        return (NewSubscriberResultMsg) this.getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("NewSubscriber"));
     }

    public DeleteSubscriberResultMsg deleteSubscriber(String msisdn){
        DeleteSubscriberRequestMsg req = this.objectFactory.createDeleteSubscriberRequestMsg();
        req.setRequestHeader(buildDelRequestHeader());
        DeleteSubscriberRequest del = this.objectFactory.createDeleteSubscriberRequest();
        Common common = this.objectFactory.createCommon();
        common.setSubscriberNo(msisdn);
        del.setSubscriberNo(common);
        req.setDeleteSubscriberRequest(del);
        return (DeleteSubscriberResultMsg) getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("DeleteSubscriber"));
    }

    public ChangeSIMResultMsg changeSIM(String msisdn, String oldImsi, String newImsi){
        ChangeSIMRequestMsg req = this.objectFactory.createChangeSIMRequestMsg();
        req.setRequestHeader(buildChangeSIMReqHeader());
        ChangeSIMRequest changeSIM = this.objectFactory.createChangeSIMRequest();
        changeSIM.setSubscriberNo(msisdn);
        changeSIM.setOldIMSI(oldImsi);
        changeSIM.setNewIMSI(newImsi);
        req.setChangeSIMRequest(changeSIM);
        return (ChangeSIMResultMsg) getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("ChangeSIM"));
    }

    private RequestHeader buildChangeSIMReqHeader() {
        RequestHeader header = this.objectFactory.createRequestHeader();
        header.setCommandId("ChangeSIM");
        header.setVersion("1");
        header.setTransactionId("Null");
        header.setSequenceId("1");
        header.setRequestType("Event");
        header.setSerialNo(UUID.randomUUID().toString().substring(0, 5));
        return header;
    }

    private RequestHeader buildDelRequestHeader() {
        RequestHeader header = this.objectFactory.createRequestHeader();
        header.setCommandId("DeleteSubscriber");
        header.setVersion("1");
        header.setTransactionId("Null");
        header.setSequenceId("1");
        header.setRequestType("Event");
        header.setSerialNo(UUID.randomUUID().toString().substring(0, 5));
        return header;
    }

    private String getThePaidMode(String mainProductId) {
        final String profileId = this.cbsRepository.findByMainProductId(mainProductId).orElseThrow(IllegalArgumentException::new).getHlrProfilId();
        switch (profileId){
            case "11":
            case "20":
                return "2";
            case "7":
            case "16":
                return "1";
            case "19": return "3";
        }
        throw new IllegalArgumentException();
    }

    private RequestHeader buildRequestHeader() {
        RequestHeader header = this.objectFactory.createRequestHeader();
        header.setCommandId("NewSubscriber");
        header.setVersion("1");
        header.setTransactionId("1");
        header.setSequenceId("1");
        header.setRequestType("Event");
        header.setSerialNo(UUID.randomUUID().toString().substring(0, 5));
        header.setSessionEntity(buildSessionEntity());
        return header;
    }

    private RequestHeader buildQueryBasicInfoRequestHeader() {
        RequestHeader header = this.objectFactory.createRequestHeader();
        header.setCommandId("QueryBasicInfo");
        header.setVersion("1");
        header.setTransactionId("1");
        header.setSequenceId("1");
        header.setRequestType("Event");
        header.setSerialNo(UUID.randomUUID().toString().substring(0,6));
        header.setSessionEntity(buildSessionEntity());
        return header;
    }

    private SessionEntityType buildSessionEntity() {
        SessionEntityType sessionEntityType = new SessionEntityType();
        sessionEntityType.setName("THBS");
        sessionEntityType.setPassword("************");
        sessionEntityType.setRemoteAddress("10.100.1.20");
        return null;
    }
}
