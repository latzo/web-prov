package sn.sentel.tigocare.hlr.services.cbs.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBSClient;

@Slf4j
@Configuration
public class CBSConfig {

    @Value("${app.ws.cbs56bc.endpoint}")
    private String endpoint;

    @Value("${app.ws.cbs56bc.host}")
    private String host;

    @Value("${app.ws.cbs56bc.package}")
    private String contextPath;

    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public CBSClient cbsClient() {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
            new javax.net.ssl.HostnameVerifier(){
                public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                    return true;
                }
            }
            );
        final CBSClient cbsCLient = new CBSClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();
        String uri = this.host + this.endpoint;
        cbsCLient.setDefaultUri(uri);
        cbsCLient.setMarshaller(marshaller);
        cbsCLient.setUnmarshaller(marshaller);
        return cbsCLient;
    }
}
