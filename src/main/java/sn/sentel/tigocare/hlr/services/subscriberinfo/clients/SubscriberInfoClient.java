package sn.sentel.tigocare.hlr.services.subscriberinfo.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.subscriberinfo.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Component
public class SubscriberInfoClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:SUBSCRIBER INFO CLIENT]";
    private final ObjectFactory objectFactory = new ObjectFactory();

    public GetResponse getSubscriberInfo(String msisdn, String session) {

        Get request = this.objectFactory.createGet();
        request.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");
        Get.MOId moId = new Get.MOId();
        moId.setMsisdn(msisdn);
        request.setMOId(moId);

        log.info("{} Request for get subscriber info with {}", LOG_HEADER, request);
        return (GetResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public GetResponse getSubscriberInfoImsi(String imsi, String session) {

        Get request = this.objectFactory.createGet();
        request.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");
        Get.MOId moId = new Get.MOId();
        moId.setImsi(imsi);
        request.setMOId(moId);

        log.info("{} Request for get subscriber info with IMSI {}", LOG_HEADER, request);
        return (GetResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public CreateResponse createSubscriber(String msisdn, String imsi, String session, Integer profileId) {

        boolean epinMode = profileId == 0;
        log.info("EPIN MODE IS " + epinMode);

        Create request = this.objectFactory.createCreate();
        request.setMOId(this.objectFactory.createCreateMOId());
        request.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");
        request.setMOAttributes(this.objectFactory.createCreateMOAttributes());
        request.getMOAttributes().setCreateSubscription(new CreateSubscriptionType());


        //RSA 0
        request.getMOAttributes().getCreateSubscription().setRsa(0);

        Create.MOId moId = request.getMOId();
        moId.setMsisdn(msisdn);
        moId.setImsi(imsi);


        CreateSubscriptionType createSubscription = request.getMOAttributes().getCreateSubscription();
        createSubscription.setProfileId(profileId);
        createSubscription.setMsisdnAttr(msisdn);
        createSubscription.setImsiAttr(imsi);
        createSubscription.setImsi(imsi);
        createSubscription.setMsisdn(msisdn);


        //EPIN

        if(epinMode){
            log.info("This is an epin number ");
            createSubscription.setProfileId(11);
            createSubscription.setOick(15);
            createSubscription.setObi((short) 1);
            createSubscription.setObo((short) 1);
        }

        log.info("{} Request for creating subscriber with {}", LOG_HEADER, request);
        return (CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public DeleteResponse deleteSubscriber(String imsi, String msisdn, String session) {
        Delete req = this.objectFactory.createDelete();
        req.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");

        Delete.MOId moid = new Delete.MOId();
        moid.getMsisdnOrImsi().add(this.objectFactory.createImsi(imsi));
        //moid.getMsisdnOrImsi().add(objectFactory.createMsisdn(msisdn));
        req.setMOId(moid);

        return (DeleteResponse) getWebServiceTemplate().marshalSendAndReceive(req, new LoginCallBack(session));
    }


    //CHANGEMENT DE SERVICES

    public SetResponse changeServiceCallWaiting(String msisdn, OperationEnum operation, String session) {

        CawSupplementaryServiceType caw = new CawSupplementaryServiceType();
        caw.setActivationState(operation.getVal());
//        caw.setProvisionState(operation.getVal());

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCaw(caw);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
        //return this.changeServiceAddApnId(msisdn, operation, session);
    }

    public SetResponse changeServiceCLIP(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setClip(operation.getVal());

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
        //return this.changeServiceAddPdcp(msisdn, operation, session);
    }

    public SetResponse changeServiceBarringIncoming(String msisdn, OperationEnum operation, String session) {


        BarringSupplementaryServiceType baic = new BarringSupplementaryServiceType();
        baic.setTs10(this.objectFactory.createSimpleActivationStateType());
        baic.getTs10().setActivationState(operation.getVal());
//        baic.setActivationState(operation.getVal());
//        baic.setProvisionState(operation.getVal());

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setBaic(baic);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    //NOT GOOD
    public SetResponse changeServiceBarringOutgoing(String msisdn, OperationEnum operation, String session) {


        BarringSupplementaryServiceType baic = new BarringSupplementaryServiceType();
        baic.setTs10(this.objectFactory.createSimpleActivationStateType());
        baic.getTs10().setActivationState(operation.getVal());
//        baic.setProvisionState(operation.getVal());

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setBaoc(baic);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceBarringALL(String msisdn, OperationEnum operation, String session) {
        this.changeServiceBarringIncoming(msisdn, operation, session);
        return this.changeServiceBarringOutgoing(msisdn, operation, session);
    }

    public SetResponse changeServiceFullSuspension(String msisdn, OperationEnum operation, String session) {
        //Calls
        this.changeServiceBarringALL(msisdn, operation, session);
        Set set = getSetRequestForService(msisdn);
        //sms
        set.getMOAttributes().getSetSubscription().setTs21((short) (operation == OperationEnum.ACTIVATE ? 0 : 1));
        set.getMOAttributes().getSetSubscription().setTs22((short) (operation == OperationEnum.ACTIVATE ? 0 : 1));
        //data
        NamType nam = this.objectFactory.createNamType();
        nam.setProv((short) (operation == OperationEnum.ACTIVATE ? 1 : 0));
        set.getMOAttributes().getSetSubscription().setNam(nam);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceListRougeNormal(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setClir(operation.getVal());
        set.getMOAttributes().getSetSubscription().setSoclir((short) (operation == OperationEnum.ACTIVATE ? 2 : 0));

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceListRougePermanent(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setClir(operation.getVal());
        set.getMOAttributes().getSetSubscription().setSoclir(operation.getVal());

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }


    public SetResponse changeServiceConfCall(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setMpty(operation.getVal());

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceVMS(String msisdn, OperationEnum operation, String session) {

        CfCfuCfbCfnrcSupplementaryServiceType cfu = new CfCfuCfbCfnrcSupplementaryServiceType();
        cfu.setActivationState(operation.getVal());
//        cfu.setProvisionState(operation.getVal());
        cfu.setFnum("221766747777");
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCfnrc(cfu);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }


    public SetResponse changeServiceCallFowarding(String msisdn, OperationEnum operation, String session) {

        CfCfuCfbCfnrcSupplementaryServiceType callFowarding = new CfCfuCfbCfnrcSupplementaryServiceType();
        callFowarding.setActivationState(operation.getVal());
//        callFowarding.setProvisionState(operation.getVal());
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCfu(callFowarding);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse doCallForwardingToOtherMsisdn(String msisdn, String msisdnDest, OperationEnum operationEnum, String session){
        CfCfuCfbCfnrcSupplementaryServiceType cfu = new CfCfuCfbCfnrcSupplementaryServiceType();
        cfu.setProvisionState(operationEnum.getVal());
        cfu.setActivationState(operationEnum.getVal());
        if(operationEnum == OperationEnum.ACTIVATE) {
            cfu.setFnum(msisdnDest);
        }
        if(operationEnum == OperationEnum.DEACTIVATE){
            cfu.setProvisionState((short) 1);
        }
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCfu(cfu);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceGPRS(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        NamType nam = this.objectFactory.createNamType();
        nam.setProv((short) (operation == OperationEnum.ACTIVATE ? 0 : 1));
        set.getMOAttributes().getSetSubscription().setNam(nam);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceRBT(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setPrbt(operation.getVal());

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeToPrepaid(String msisdn, OperationEnum operation, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCsp(1);
        set.getMOAttributes().getSetSubscription().setProfileId(operation == OperationEnum.ACTIVATE ? 11 : 7);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeToPostpaid(String msisdn, OperationEnum operation, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCsp(3);
        set.getMOAttributes().getSetSubscription().setProfileId(operation == OperationEnum.ACTIVATE ? 7 : 11);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeToEPIN(String msisdn, OperationEnum operation, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setProfileId(operation == OperationEnum.ACTIVATE ? 19 : 11);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeAPN(String msisdn, OperationEnum operation, String apnId, String profileId, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCsp(1);
        List<GprsType> gprs = Optional.ofNullable(set.getMOAttributes().getSetSubscription().getGprs())
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .collect(Collectors.toList());
        if (operation == OperationEnum.ACTIVATE) {
            GprsType gprsConfig = new GprsType();
            gprsConfig.setApnid(apnId);
            gprsConfig.setPdpid(apnId);
            gprsConfig.setEqosid(1);
            gprsConfig.setPdpty("IPV4");
            gprsConfig.setApnidAttr(apnId);
//      gprsConfig.setPdpid("1");
//      gprsConfig.setPdpidAttr("1");
            gprs.add(gprsConfig);
        } else {
            if (!gprs.removeIf(g -> g.getApnid().equals(apnId))) {
                throw new IllegalArgumentException("APN not deleted");
            }
        }
        set.getMOAttributes().getSetSubscription().getGprs().size();
        set.getMOAttributes().getSetSubscription().getGprs().addAll(gprs);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }


    /***
     *
     * Here are codes for roaming services
     * First part are post paid
     * Second part are pre paid
     *
     * **/

    public void roamingPostVoiceData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(2);
        set.getMOAttributes().getSetSubscription().setObo((short)0);
        set.getMOAttributes().getSetSubscription().setObi((short)0);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public void roamingPostVoiceNoData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(76);
        set.getMOAttributes().getSetSubscription().setObo((short)0);
        set.getMOAttributes().getSetSubscription().setObi((short)0);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public void roamingPostNoVoiceData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(2);
        set.getMOAttributes().getSetSubscription().setObo((short) 4);
        set.getMOAttributes().getSetSubscription().setObi((short) 2);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    public void roamingPostNoVoiceNoData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(76);
        set.getMOAttributes().getSetSubscription().setObo((short) 4);
        set.getMOAttributes().getSetSubscription().setObi((short) 2);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

//--- PrePaid

    public void roamingPreVoiceData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(0);
        set.getMOAttributes().getSetSubscription().setObo((short)0);
        set.getMOAttributes().getSetSubscription().setObi((short)0);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    public void roamingPreVoiceNoData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(65);
        set.getMOAttributes().getSetSubscription().setObo((short)0);
        set.getMOAttributes().getSetSubscription().setObi((short)0);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    public void roamingPreNoVoiceData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(0);
        set.getMOAttributes().getSetSubscription().setObo((short) 4);
        set.getMOAttributes().getSetSubscription().setObi((short) 2);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));


    }

    public void roamingPreNoVoiceNoData(String msisdn, String session) {
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setRsa(65);
        set.getMOAttributes().getSetSubscription().setObo((short) 4);
        set.getMOAttributes().getSetSubscription().setObi((short) 2);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    public void cancelVLR(String msisdn, String session){
        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setTeardown((short)1);
        getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceAddApnId(String msisdn, OperationEnum operation, String session) {

        Set set = getSetRequestForService(msisdn);
        set.getMOAttributes().getSetSubscription().setCsp(1);
        List<GprsType> gprs = Optional.ofNullable(set.getMOAttributes().getSetSubscription().getGprs())
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .collect(Collectors.toList());
        GprsType gprsConfig = new GprsType();
        gprsConfig.setApnid("46");
        gprsConfig.setEqosid(1);
        gprsConfig.setPdpty("IPV4");
        gprsConfig.setApnidAttr("46");
//      gprsConfig.setPdpid("1");
//      gprsConfig.setPdpidAttr("1");
        gprs.add(gprsConfig);

        set.getMOAttributes().getSetSubscription().getGprs().size();
        set.getMOAttributes().getSetSubscription().getGprs().addAll(gprs);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }

    public SetResponse changeServiceAddPdcp(String msisdn, OperationEnum operation, String session) {

        Set set = this.objectFactory.createSet();
        set.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");
        set.setMOId(this.objectFactory.createSetMOId());
        set.setMOAttributes(this.objectFactory.createSetMOAttributes());
        set.getMOAttributes().setSetSubscription(new SetSubscriptionType());
        Set.MOId moId = set.getMOId();
        moId.setMsisdn(msisdn);


        SetSubscriptionType setSubscription = set.getMOAttributes().getSetSubscription();
        setSubscription.setMsisdnAttr(msisdn);
        set.getMOAttributes().getSetSubscription().setPdpcp(3);

        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    private Set getSetRequestForService(String msisdn) {
        Set set = this.objectFactory.createSet();
        set.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");
        set.setMOId(this.objectFactory.createSetMOId());
        set.setMOAttributes(this.objectFactory.createSetMOAttributes());
        set.getMOAttributes().setSetSubscription(new SetSubscriptionType());
        Set.MOId moId = set.getMOId();
        moId.setMsisdn(msisdn);
        SetSubscriptionType setSubscription = set.getMOAttributes().getSetSubscription();
        setSubscription.setMsisdnAttr(msisdn);
        return set;
    }

}
