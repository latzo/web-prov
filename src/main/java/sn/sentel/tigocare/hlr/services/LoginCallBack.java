package sn.sentel.tigocare.hlr.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import sn.sentel.tigocare.jaxb2.simchange.ObjectFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Data
@AllArgsConstructor
public class LoginCallBack implements WebServiceMessageCallback {

    private String session;

    @Override
    public void doWithMessage(WebServiceMessage webServiceMessage) throws IOException, TransformerException {
        try {
            SOAPMessage soapMessage = ((SaajSoapMessage) webServiceMessage).getSaajMessage();
            SOAPHeader soapHeader = soapMessage.getSOAPPart().getEnvelope().getHeader();
            ObjectFactory objectFactory = new ObjectFactory();

            JAXBElement<String> sessionId = objectFactory.createSessionId(this.session);
            SOAPHeaderElement sessionIdElement = soapHeader.addHeaderElement(sessionId.getName());
            sessionIdElement.setTextContent(sessionId.getValue());

            JAXBElement<BigInteger> sequenceId = objectFactory.createSequenceId(new BigInteger(32, new Random()));
            SOAPHeaderElement sequenceIdElement = soapHeader.addHeaderElement(sequenceId.getName());
            sequenceIdElement.setTextContent(sequenceId.getValue().toString());

            JAXBElement<String> transactionId = objectFactory.createTransactionId(UUID.randomUUID().toString());
            SOAPHeaderElement txIdElement = soapHeader.addHeaderElement(transactionId.getName());
            txIdElement.setTextContent(transactionId.getValue());

            soapMessage.saveChanges();
        } catch (SOAPException se) {
            log.error(se.getMessage());
        }
    }
}
