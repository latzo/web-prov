package sn.sentel.tigocare.hlr.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapMessage;

@Slf4j
@Data
@AllArgsConstructor
public class WSAHeader implements WebServiceMessageCallback {

    private String action;

    @Override
    public void doWithMessage(WebServiceMessage webServiceMessage){
           ((SoapMessage) webServiceMessage).setSoapAction(action);
    }

}
