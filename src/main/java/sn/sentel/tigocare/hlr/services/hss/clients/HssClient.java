package sn.sentel.tigocare.hlr.services.hss.clients;


import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.hss.*;

@Slf4j
public class HssClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:HSS CLIENT]";

    private final ObjectFactory objectFactory = new ObjectFactory();

    public CreateResponse createSubscriber(String imsi, String msisdn, String epsProfileId, String session){
        Create request = this.objectFactory.createCreate();
        request.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        //Moid
        Create.MOId moid  = this.objectFactory.createCreateMOId();
        moid.setImsi(imsi);
        request.setMOId(moid);
        //Moattributes
        Create.MOAttributes moAttributes = this.objectFactory.createCreateMOAttributes();
        CreateEPSMultiSC epsMultiSC = this.objectFactory.createCreateEPSMultiSC();
        epsMultiSC.setImsiAttr(imsi);
        epsMultiSC.setImsi(imsi);
        epsMultiSC.setMsisdn(msisdn);
        epsMultiSC.setEpsProfileId(epsProfileId);
        epsMultiSC.setEpsRoamingAllowed(true);
        epsMultiSC.setEpsOdb(EpsOdbType.NONE);
        moAttributes.setCreateEPSMultiSC(epsMultiSC);
        request.setMOAttributes(moAttributes);
        //do request
        log.info("{} Request for creating subscriber with {}", LOG_HEADER, request);
        return (CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public DeleteResponse deleteSubscriber(String imsi, String session){
        Delete req = this.objectFactory.createDelete();
        req.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");

        req.setMOId(objectFactory.createDeleteMOId());
        req.getMOId().setImsi(imsi);

        return (DeleteResponse) this.getWebServiceTemplate().marshalSendAndReceive(req, new LoginCallBack(session));
    }

    public GetResponse getSubscriberInfoMsisdn(String msisdn, String session) {

        Get request = this.objectFactory.createGet();
        request.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        Get.MOId moId = new Get.MOId();
        moId.setMsisdn(msisdn);
        request.setMOId(moId);

        log.info("{} Request for get subscriber info with {}", LOG_HEADER, request);
        return (GetResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public GetResponse getSubscriberInfo(String imsi, String session) {

        Get request = this.objectFactory.createGet();
        request.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        Get.MOId moId = new Get.MOId();
        moId.setImsi(imsi);
        request.setMOId(moId);

        log.info("{} Request for get subscriber info with {}", LOG_HEADER, request);
        return (GetResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }

    public SetResponse changeServiceAddEsmProfile(String imsi, OperationEnum operation, String session) {

        sn.sentel.tigocare.jaxb2.hss.Set set = this.objectFactory.createSet();
        set.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        set.setMOId(this.objectFactory.createSetMOId());
        sn.sentel.tigocare.jaxb2.hss.Set.MOId moId = set.getMOId();
        moId.setImsi(imsi);
        set.setMOAttributes(this.objectFactory.createSetMOAttributes());
        set.getMOAttributes().setSetEPSMultiSC(new SetEPSMultiSC());
        SetEPSMultiSC setEPSMultiSC = set.getMOAttributes().getSetEPSMultiSC();
        setEPSMultiSC.setImsiAttr(imsi);
        setEPSMultiSC.setEpsProfileId("EsmProfile21");
        //setEPSMultiSC.setEpsRoamingAllowed(true);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));

    }

    public SetResponse changeServiceActivateRoaming4g(String imsi, String session) {

        sn.sentel.tigocare.jaxb2.hss.Set set = this.objectFactory.createSet();
        set.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        set.setMOId(this.objectFactory.createSetMOId());
        sn.sentel.tigocare.jaxb2.hss.Set.MOId moId = set.getMOId();
        moId.setImsi(imsi);
        set.setMOAttributes(this.objectFactory.createSetMOAttributes());
        set.getMOAttributes().setSetEPSMultiSC(new SetEPSMultiSC());
        SetEPSMultiSC setEPSMultiSC = set.getMOAttributes().getSetEPSMultiSC();
        setEPSMultiSC.setImsiAttr(imsi);
        //setEPSMultiSC.setEpsProfileId("EsmProfile21");
        setEPSMultiSC.setEpsRoamingAllowed(true);
        setEPSMultiSC.setEpsOdb(EpsOdbType.NONE);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }



    public SetResponse changeServiceBarreRoaming4g(String imsi, String session) {

        sn.sentel.tigocare.jaxb2.hss.Set set = this.objectFactory.createSet();
        set.setMOType("EPSMultiSC@http://schemas.ericsson.com/ma/HSS/");
        set.setMOId(this.objectFactory.createSetMOId());
        sn.sentel.tigocare.jaxb2.hss.Set.MOId moId = set.getMOId();
        moId.setImsi(imsi);
        set.setMOAttributes(this.objectFactory.createSetMOAttributes());
        set.getMOAttributes().setSetEPSMultiSC(new SetEPSMultiSC());
        SetEPSMultiSC setEPSMultiSC = set.getMOAttributes().getSetEPSMultiSC();
        setEPSMultiSC.setImsiAttr(imsi);
        //setEPSMultiSC.setEpsProfileId("EsmProfile21");
        setEPSMultiSC.setEpsRoamingAllowed(true);
        setEPSMultiSC.setEpsOdb(EpsOdbType.ODB_HPLMN_APN);
        return (SetResponse) getWebServiceTemplate().marshalSendAndReceive(set, new LoginCallBack(session));
    }
}
