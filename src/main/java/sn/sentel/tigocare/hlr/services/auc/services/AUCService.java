package sn.sentel.tigocare.hlr.services.auc.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.Pinpuk;
import sn.sentel.tigocare.hlr.services.auc.clients.AUCClient;
import sn.sentel.tigocare.hlr.services.exceptions.SubscriberCreationFailedException;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.jaxb2.auc.CreateResponse;
import sn.sentel.tigocare.jaxb2.auc.DeleteResponse;
import sn.sentel.tigocare.jaxb2.auc.GetResponse;
import sn.sentel.tigocare.service.PinpukService;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Slf4j
@Service
public class AUCService {

    private final AUCClient aucClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;
    private final PinpukService pinpukService;


//    @PostConstruct
//    public void testGetKi(){
//        log.info("Testing get ki");
//        log.info("Response got ki : " + this.getKi("608026401114109"));
//    }

    public AUCService(AUCClient aucClient, SessionControl sessionControl, PinpukService pinpukService) {
        this.aucClient = aucClient;
        this.sessionControl = sessionControl;
        this.pinpukService = pinpukService;
    }

    public CreateResponse createSIM(String imsi) throws SubscriberCreationFailedException {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        String ki= this.getKi(imsi);
        if(ki == null) throw new SubscriberCreationFailedException("KI not found");

        CreateResponse res;
        try {
            res = this.aucClient.createIMSI(imsi, ki, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

    public DeleteResponse deleteSIM(String imsi) throws SubscriberCreationFailedException {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        DeleteResponse res;
        try {
             res = this.aucClient.deleteIMSI(imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

    public GetResponse getImsi(String imsi) throws SubscriberCreationFailedException {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        GetResponse res;
        try {
            res = this.aucClient.getImsi(imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

    private String getKi(String imsi){
        return this.pinpukService.findFirstByImsi(imsi)
            .map(Pinpuk::getKi)
            .orElse(null);
    }
}
