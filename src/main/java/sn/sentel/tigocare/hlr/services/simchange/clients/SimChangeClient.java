package sn.sentel.tigocare.hlr.services.simchange.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.simchange.Create;
import sn.sentel.tigocare.jaxb2.simchange.CreateImsiChangeoverType;
import sn.sentel.tigocare.jaxb2.simchange.CreateResponse;
import sn.sentel.tigocare.jaxb2.simchange.ObjectFactory;

@Slf4j
@Component
public class SimChangeClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:SIM CHANGE CLIENT]";

    private final ObjectFactory objectFactory = new ObjectFactory();

    public CreateResponse createSimChangeOver(String msisdn, String newImsi, String oldImsi, String session) {
        Create createRequest = this.objectFactory.createCreate();

        createRequest.setMOType("ImsiChangeover@http://schemas.ericsson.com/ema/UserProvisioning/GsmHlr/");

        Create.MOId moid = new Create.MOId();
        moid.setMsisdn(msisdn);
        moid.setNimsi(newImsi);
        createRequest.setMOId(moid);

        Create.MOAttributes moAttr = new Create.MOAttributes();
        CreateImsiChangeoverType imsiChange = new CreateImsiChangeoverType();
        imsiChange.setMsisdn(msisdn);
        imsiChange.setMsisdnAttr(msisdn);
        imsiChange.setNimsi(newImsi);
        imsiChange.setNimsiAttr(newImsi);
        moAttr.setCreateImsiChangeover(imsiChange);
        createRequest.setMOAttributes(moAttr);
        log.info("{} Request for a sim change over with {}", LOG_HEADER, createRequest);
        return (CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(createRequest, new LoginCallBack(session));
    }
}
