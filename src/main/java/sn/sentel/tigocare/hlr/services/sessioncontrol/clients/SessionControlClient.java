package sn.sentel.tigocare.hlr.services.sessioncontrol.clients;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.authentication.*;

@Slf4j
@Component
public class SessionControlClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:SESSION CONTROL CLIENT]";

    private final ObjectFactory objectFactory = new ObjectFactory();

    public LoginResponse login(String username, String password) {

        Login loginData = this.objectFactory.createLogin();
        loginData.setUserId(username);
        loginData.setPwd(password);
        LoginResponse res;
        log.debug("[{}] login on {} with {}", LOG_HEADER, this.getWebServiceTemplate().getDefaultUri(), loginData);
        try {
            res = (LoginResponse) this.getWebServiceTemplate().marshalSendAndReceive(loginData);
        } catch (SoapFaultClientException sfce) {
            log.error(sfce.getMessage());
            return new LoginResponse();
        }
        log.debug("[{}] response => {}", LOG_HEADER, res);
        return res;
    }

    public LogoutResponse logout(String sessionId){
        Logout logout = this.objectFactory.createLogout();
        logout.setSessionId(sessionId);
        try {
            return (LogoutResponse) this.getWebServiceTemplate().marshalSendAndReceive(logout, new LoginCallBack(sessionId));
        }catch (SoapFaultClientException sfce){
            log.error("{} log out failed {}", LOG_HEADER, sfce);
            return null;
        }
    }
}
