package sn.sentel.tigocare.hlr.services.auc.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.auc.*;
import sn.sentel.tigocare.service.UtilsService;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class AUCClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:AUC-NODE CLIENT]";
    private ObjectFactory objectFactory = new ObjectFactory();
    @Autowired
    private UtilsService utilsService;

    public AUCClient(UtilsService utilsService) {
        super();
        this.utilsService = utilsService;
    }

    public CreateResponse createIMSI(String imsi, String ki, String sessionId){
        Create createReq = this.objectFactory.createCreate();
        createReq.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmAuc/");


        Create.MOId moid = new Create.MOId();
        moid.setImsi(imsi);
        createReq.setMOId(moid);

        Create.MOAttributes moAttr = new Create.MOAttributes();
        CreateSubscriptionType createSubs = new CreateSubscriptionType();
        createSubs.setElementImsi(imsi);
        createSubs.setImsi(imsi);
        createSubs.setKi(ki);
        if(this.utilsService.is4gNumber(imsi)){
            log.info("{} IS 4G NUMBER - CREATING WITH 2-2-2", imsi);
            createSubs.setAdkey(2);
            createSubs.setFsetind(new Short("2"));
            createSubs.setA4Ind(new Short("2"));
        }else{
            log.warn("{} IS NOT 4G NUMBER - IS4GNUMBER CHECK {}", imsi, utilsService.is4gNumber(imsi));
            createSubs.setAdkey(1);
            createSubs.setA4Ind(new Short("0"));
            createSubs.setA38(new Short("2"));
        }
        moAttr.setCreateSubscription(createSubs);
        createReq.setMOAttributes(moAttr);
        log.info("{} AUC CREATION REQUEST {}", imsi, createReq);
        log.info("{} uri to call {}", LOG_HEADER, getDefaultUri());
        return (CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(createReq, new LoginCallBack(sessionId));
    }


//    @PostConstruct
//    public void testAucCreation(){
//        this.createIMSI("608026406206857", "E349DE8F0A6962781860336F5CCEDE86", "ae87fd876cec4c4db1158f7735ac162a");
//
//    }

    public DeleteResponse deleteIMSI(String imsi, String sessionId){
        Delete req = this.objectFactory.createDelete();
        req.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmAuc/");

        Delete.MOId moid = new Delete.MOId();
        moid.setImsi(imsi);
        req.setMOId(moid);

        return (DeleteResponse) getWebServiceTemplate().marshalSendAndReceive(req, new LoginCallBack(sessionId));

    }

    public GetResponse getImsi(String imsi, String sessionId){
        Get req = this.objectFactory.createGet();
        req.setMOType("Subscription@http://schemas.ericsson.com/ema/UserProvisioning/GsmAuc/");

        Get.MOId moid = new Get.MOId();
        moid.setImsi(imsi);
        req.setMOId(moid);

        log.info("{} AUC DISPLAY IMSI REQUEST {}", imsi, req);
        log.info("{} uri to call {}", LOG_HEADER, getDefaultUri());
        return (GetResponse) getWebServiceTemplate().marshalSendAndReceive(req, new LoginCallBack(sessionId));

    }

}
