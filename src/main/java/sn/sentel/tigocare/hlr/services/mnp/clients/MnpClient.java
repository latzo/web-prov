package sn.sentel.tigocare.hlr.services.mnp.clients;


import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.mnp.Create;
import sn.sentel.tigocare.jaxb2.mnp.CreateNumberPortability;
import sn.sentel.tigocare.jaxb2.mnp.CreateResponse;
import sn.sentel.tigocare.jaxb2.mnp.ObjectFactory;

@Slf4j
public class MnpClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:MNP CLIENT]";

    private final ObjectFactory objectFactory = new ObjectFactory();

    public CreateResponse createPorting(String msisdn, String prefix, String session){
        Create request = this.objectFactory.createCreate();
        //MoType
        request.setMOType("NumberPortability@http://schemas.ericsson.com/ema/UserProvisioning/GsmFnr/");
        //Moid
        Create.MOId moid  = this.objectFactory.createCreateMOId();
        moid.setMsisdn(msisdn);
        request.setMOId(moid);
        //Moattributes
        Create.MOAttributes moAttributes = this.objectFactory.createCreateMOAttributes();
        CreateNumberPortability gsm = this.objectFactory.createCreateNumberPortability();
        gsm.setElementMsisdn(msisdn);
        gsm.setMsisdn(msisdn);
        gsm.setNprefix(prefix);
        moAttributes.setCreateNumberPortability(gsm);
        request.setMOAttributes(moAttributes);
        //do request
        log.info("{} Request for creating number portability with {}", LOG_HEADER, request);
        return (CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(request, new LoginCallBack(session));
    }
}
