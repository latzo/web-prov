package sn.sentel.tigocare.hlr.services.sessioncontrol.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.sessioncontrol.clients.SessionControlClient;

@Configuration
public class SessionControlConfig {

    @Value("${app.ws.session-control.endpoint}")
    private String endpoint;

    @Value("${app.ws.session-control.host}")
    private String host;

    @Value("${app.ws.session-control.package}")
    private String contextPath;


    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public SessionControlClient sessionControlClient() {
        final SessionControlClient sessionControlClient = new SessionControlClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();

        sessionControlClient.setDefaultUri(this.host + this.endpoint);
        sessionControlClient.setMarshaller(marshaller);
        sessionControlClient.setUnmarshaller(marshaller);

        return sessionControlClient;

    }
}
