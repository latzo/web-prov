package sn.sentel.tigocare.hlr.services.hss.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.hss.clients.HssClient;

@Configuration
public class HssConfig {
    @Value("${app.ws.hss.endpoint}")
    private String endpoint;

    @Value("${app.ws.hss.host}")
    private String host;

    @Value("${app.ws.hss.package}")
    private String contextPath;


    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public HssClient hssClient() {
        final HssClient hssClient = new HssClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();
        hssClient.setDefaultUri(this.host + this.endpoint);
        hssClient.setMarshaller(marshaller);
        hssClient.setUnmarshaller(marshaller);
        return hssClient;
    }
}
