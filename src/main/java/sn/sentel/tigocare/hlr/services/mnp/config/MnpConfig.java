package sn.sentel.tigocare.hlr.services.mnp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.mnp.clients.MnpClient;

@Configuration
public class MnpConfig {

    @Value("${app.ws.mnp.endpoint}")
    private String endpoint;

    @Value("${app.ws.mnp.host}")
    private String host;

    @Value("${app.ws.mnp.package}")
    private String contextPath;

    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public MnpClient mnpClient() {
        final MnpClient mnpClient = new MnpClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();
        mnpClient.setDefaultUri(this.host + this.endpoint);
        mnpClient.setMarshaller(marshaller);
        mnpClient.setUnmarshaller(marshaller);
        return mnpClient;
    }
}
