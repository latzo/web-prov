package sn.sentel.tigocare.hlr.services.exceptions;

public class SubscriberCreationFailedException extends RuntimeException {
    public SubscriberCreationFailedException(String message) {
        super(message);
    }
}
