package sn.sentel.tigocare.hlr.services.msa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Param {
    private String name;
    private String value;

}
