package sn.sentel.tigocare.hlr.services.sessioncontrol.services;

import org.springframework.stereotype.Service;
import sn.sentel.tigocare.hlr.services.sessioncontrol.clients.SessionControlClient;
import sn.sentel.tigocare.jaxb2.authentication.LoginResponse;

//Gadio 766752343
@Service
public class SessionControl {


    private final SessionControlClient sessionControlClient;

    public SessionControl(SessionControlClient sessionControlClient) {
        this.sessionControlClient = sessionControlClient;
    }

//    @PostConstruct
//    public void testLogin(){
//        login("prov","Provisioning@123");
//    }

    /**
     * Authenticate a user on the HLR platform
     *
     * @param username login
     * @param password pwd
     * @return the sessionId
     */
    public String login(String username, String password) {
        LoginResponse loginResponse = this.sessionControlClient.login(username, password);
        if (loginResponse.getSessionId() != null) {
            return loginResponse.getSessionId();
        }
        return null;
    }

    public void logout(String sessionId){
        sessionControlClient.logout(sessionId);
    }
}
