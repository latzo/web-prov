package sn.sentel.tigocare.hlr.services.mnp.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sn.sentel.tigocare.hlr.services.mnp.clients.MnpClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.jaxb2.mnp.CreateResponse;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class MnpService {

    private final MnpClient mnpClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;

    public MnpService(SessionControl sessionControl, MnpClient mnpClient) {
        this.sessionControl = sessionControl;
        this.mnpClient = mnpClient;
    }

//    pmbodj

    public CreateResponse createNumberPortability(String msisdn, String prefix){
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        CreateResponse res;
        try {
            res = this.mnpClient.createPorting(msisdn, prefix, sessionId);
        }catch (Exception e ) {
            //log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }
}
