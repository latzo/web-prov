package sn.sentel.tigocare.hlr.services.auc.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.auc.clients.AUCClient;
import sn.sentel.tigocare.service.UtilsService;

@Slf4j
@Configuration
public class AUCConfig {

    @Value("${app.ws.auc.endpoint}")
    private String endpoint;

    @Value("${app.ws.auc.host}")
    private String host;

    @Value("${app.ws.auc.package}")
    private String contextPath;

    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public AUCClient aucClient() {
        final UtilsService utilsService = new UtilsService();
        final AUCClient aucClient = new AUCClient(utilsService);
        final Jaxb2Marshaller marshaller = this.buildMarshaller();
        String uri = this.host + this.endpoint;
        aucClient.setDefaultUri(uri);
        aucClient.setMarshaller(marshaller);
        aucClient.setUnmarshaller(marshaller);
        return aucClient;

    }
}
