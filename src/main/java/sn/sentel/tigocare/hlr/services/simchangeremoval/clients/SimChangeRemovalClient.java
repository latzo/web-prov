package sn.sentel.tigocare.hlr.services.simchangeremoval.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.simchangeremoval.CreateIMSIChangeoverRemoval;

@Slf4j
@Component
public class SimChangeRemovalClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:SIM CHANGE REMOVAL CLIENT]";

    private final sn.sentel.tigocare.jaxb2.simchangeremoval.ObjectFactory objectFactoryRemoval = new sn.sentel.tigocare.jaxb2.simchangeremoval.ObjectFactory();


    public sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse createSimChangeOverRemoval(String msisdn, String session){
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create createRequest = this.objectFactoryRemoval.createCreate();
        //Motype
        createRequest.setMOType("IMSIChangeoverRemoval@http://schemas.ericsson.com/pg/cudb/1.0/");
        //Moid
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOId moId = new sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOId();
        moId.setMsisdn(msisdn);
        createRequest.setMOId(moId);
        //MoAttributes
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOAttributes moAttributes = new sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOAttributes();
        CreateIMSIChangeoverRemoval createIMSIChangeoverRemoval = new CreateIMSIChangeoverRemoval();
        createIMSIChangeoverRemoval.setMsisdnAttr(msisdn);
        createIMSIChangeoverRemoval.setMsisdn(msisdn);
        moAttributes.setCreateIMSIChangeoverRemoval(createIMSIChangeoverRemoval);
        createRequest.setMOAttributes(moAttributes);
        log.info("{} Request for a sim change over  removal with {}", LOG_HEADER, createRequest);
        return (sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(createRequest, new LoginCallBack(session));
    }

    public sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse createSimChangeOverRemovalWithImsi(String imsi, String session){
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create createRequest = this.objectFactoryRemoval.createCreate();
        //Motype
        createRequest.setMOType("IMSIChangeoverRemoval@http://schemas.ericsson.com/pg/cudb/1.0/");
        //Moid
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOId moId = new sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOId();
        moId.setImsi(imsi);
        createRequest.setMOId(moId);
        //MoAttributes
        sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOAttributes moAttributes = new sn.sentel.tigocare.jaxb2.simchangeremoval.Create.MOAttributes();
        CreateIMSIChangeoverRemoval createIMSIChangeoverRemoval = new CreateIMSIChangeoverRemoval();
        createIMSIChangeoverRemoval.setImsiAttr(imsi);
        createIMSIChangeoverRemoval.setImsi(imsi);
        moAttributes.setCreateIMSIChangeoverRemoval(createIMSIChangeoverRemoval);
        createRequest.setMOAttributes(moAttributes);
        log.info("{} Request for a sim change over  removal old imsi with {}", LOG_HEADER, createRequest);
        return (sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse) this.getWebServiceTemplate().marshalSendAndReceive(createRequest, new LoginCallBack(session));
    }
}
