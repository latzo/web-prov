package sn.sentel.tigocare.hlr.services.msa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Data {
    ArrayList< Param > param;
}
