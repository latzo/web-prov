package sn.sentel.tigocare.hlr.services.simchangeremoval.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.simchangeremoval.clients.SimChangeRemovalClient;

@Slf4j
@Service
public class SimChangeRemovalService {


    private final SimChangeRemovalClient simChangeRemovalClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;


    public SimChangeRemovalService(SessionControl sessionControl, SimChangeRemovalClient simChangeRemovalClient) {
        this.sessionControl = sessionControl;
        this.simChangeRemovalClient = simChangeRemovalClient;
    }

//    @PostConstruct
//    public void testLogin() {
//        try {
//            this.changeSIM("234567", "456789", "23456789");
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//
//    }


    public sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse simRemoval(String msisdn) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse res;
        try {
            res = this.simChangeRemovalClient.createSimChangeOverRemoval(msisdn, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse simRemovalImsi(String imsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        sn.sentel.tigocare.jaxb2.simchangeremoval.CreateResponse res;
        try {
            res = this.simChangeRemovalClient.createSimChangeOverRemovalWithImsi(imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

}
