package sn.sentel.tigocare.hlr.services.cbs.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.WSAHeader;
import sn.sentel.tigocare.jaxb2.cbs56.bc.*;
import sn.sentel.tigocare.repository.MainProductsCbsRepository;

import javax.xml.bind.JAXBElement;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

public class CBS56BCClient extends WebServiceGatewaySupport {
    private static final String LOG_HEADER = "[WS:CBS56_CLIENT]";
    @Autowired
    private MainProductsCbsRepository cbsRepository;
    private ObjectFactory objectFactory = new ObjectFactory();

    public QueryCustomerInfoResultMsg queryBasicInfo(String msisdn){
        QueryCustomerInfoRequestMsg req = this.objectFactory.createQueryCustomerInfoRequestMsg();

        req.setRequestHeader(this.buildCbsQueryRequestHeader());
        req.getRequestHeader().setBusinessCode("Tertio_QueryCustomerInfo");
        QueryCustomerInfoRequest request = this.objectFactory.createQueryCustomerInfoRequest();
        sn.sentel.tigocare.jaxb2.cbs56.bc.QueryCustomerInfoRequest.QueryObj queryObj = this.objectFactory.createQueryCustomerInfoRequestQueryObj();
        QueryCustomerInfoRequest.QueryObj.SubAccessCode subAccessCode = this.objectFactory.createQueryCustomerInfoRequestQueryObjSubAccessCode();
        subAccessCode.setPrimaryIdentity(msisdn);
        queryObj.setSubAccessCode(subAccessCode);
        request.setQueryObj(queryObj);
        request.setQueryMode(2);
        request.setCustomerMask("11");
        request.setAccountMask("11");
        request.setSubscriberMask("11111111");
        req.setQueryCustomerInfoRequest(request);

        return (QueryCustomerInfoResultMsg) this.getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("QueryCustomerInfo"));
    }

    public CreateSubscriberResultMsg createNewSubscriber(String msisdn, String imsi, String mainProductId ){
        CreateSubscriberRequestMsg req = buildCreateSubscriberRequestMsg(msisdn, imsi, mainProductId, false);
        logger.info("Here the request " +req);
        return (CreateSubscriberResultMsg) this.getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("CreateSubscriber"));
    }

    public CreateSubscriberResultMsg createPortedSubscriber(String msisdn, String imsi, String mainProductId ){
        CreateSubscriberRequestMsg req = buildCreateSubscriberRequestMsg(msisdn, imsi, mainProductId, true);
        logger.info("Here the request " +req);
        return (CreateSubscriberResultMsg) this.getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("CreateSubscriber"));
    }



    private CreateSubscriberRequestMsg buildCreateSubscriberRequestMsg(String msisdn, String imsi, String mainProductId, boolean welcomePack) {
        CreateSubscriberRequestMsg req = this.objectFactory.createCreateSubscriberRequestMsg();

        req.setRequestHeader(this.buildCbsQueryRequestHeader());

        CreateSubscriberRequest subscriberRequest = this.objectFactory.createCreateSubscriberRequest();
        String currentTimeStampMillis = String.valueOf(System.currentTimeMillis());

        //RegisterCustomer
        CreateSubscriberRequest.RegisterCustomer registerCustomer = this.objectFactory.createCreateSubscriberRequestRegisterCustomer();
        //Assignments
        registerCustomer.setOpType("1");
        registerCustomer.setCustKey("CUST_"+currentTimeStampMillis);
        CustInfo custInfo = this.objectFactory.createCustInfo();
        custInfo.setCustType("1");
        custInfo.setCustNodeType("1");
        custInfo.setCustCode(registerCustomer.getCustKey());
        CustBasicInfo custBasicInfo = this.objectFactory.createCustBasicInfo();
        custBasicInfo.setDFTWrittenLang("2016");
        custBasicInfo.setDFTIVRLang("2016");
        custBasicInfo.setDFTBillCycleType("1");
        JAXBElement<String> custBasicInfoCustLevel = this.objectFactory.createCustBasicInfoCustLevel("1");
        custBasicInfo.setCustLevel(custBasicInfoCustLevel);
        SimpleProperty e = new SimpleProperty();
        e.setCode("C_REG_FLAG");
        e.setValue("Y");
        custBasicInfo.getCustProperty().add(e);
        custInfo.setCustBasicInfo(custBasicInfo);
        registerCustomer.setCustInfo(custInfo);
        subscriberRequest.setRegisterCustomer(registerCustomer);

        //Account
        CreateSubscriberRequest.Account account = this.objectFactory.createCreateSubscriberRequestAccount();
        //Assignments
        account.setAcctKey("ACT_"+currentTimeStampMillis);
        AccountInfo accountInfo = this.objectFactory.createAccountInfo();
        accountInfo.setAcctCode(account.getAcctKey());
        accountInfo.setUserCustomerKey(registerCustomer.getCustKey());
        accountInfo.setBillCycleType("1");
        accountInfo.setAcctType("1");
        accountInfo.setPaymentType("0");
        accountInfo.setAcctClass("1");
        accountInfo.setAcctPayMethod("1");
        account.setAcctInfo(accountInfo);
        subscriberRequest.getAccount().add(account);

        //Subscriber
        CreateSubscriberRequest.Subscriber subscriber = this.objectFactory.createCreateSubscriberRequestSubscriber();
        //Assignments
        subscriber.setSubscriberKey("SUBS_"+currentTimeStampMillis);
        Subscriber subscriberInfo = this.objectFactory.createSubscriber();
        SubBasicInfo subBasicInfo = this.objectFactory.createSubBasicInfo();
        subBasicInfo.setWrittenLang("2016");
        subBasicInfo.setIVRLang("2016");
        subscriberInfo.setSubBasicInfo(subBasicInfo);
        subscriberInfo.setUserCustomerKey(registerCustomer.getCustKey());
        SubIdentity subIdentity1 = this.objectFactory.createSubIdentity();
        subIdentity1.setSubIdentityType("1");
        subIdentity1.setSubIdentity(msisdn);
        subIdentity1.setPrimaryFlag("1");
        subscriberInfo.getSubIdentity().add(subIdentity1);
        SubIdentity subIdentity2 = this.objectFactory.createSubIdentity();
        subIdentity2.setSubIdentityType("2");
        subIdentity2.setSubIdentity(imsi);
        subIdentity2.setPrimaryFlag("2");
        subscriberInfo.getSubIdentity().add(subIdentity2);
        subscriberInfo.setStatus("1");
        subscriber.setSubscriberInfo(subscriberInfo);
        CreateSubscriberRequest.Subscriber.SubPaymentMode subPaymentMode = this.objectFactory.createCreateSubscriberRequestSubscriberSubPaymentMode();
        subPaymentMode.setPaymentMode("0");
        subPaymentMode.setPayRelationKey("PPR_"+currentTimeStampMillis);
        subPaymentMode.setAcctKey("ACT_"+currentTimeStampMillis);
        subscriber.setSubPaymentMode(subPaymentMode);
        subscriberRequest.setSubscriber(subscriber);


        //PrimaryOffering
        CreateSubscriberRequest.PrimaryOffering primaryOffering = this.objectFactory.createCreateSubscriberRequestPrimaryOffering();
        //Assignments
        OfferingKey offeringKey = this.objectFactory.createOfferingKey();
        JAXBElement<BigInteger> offeringID = this.objectFactory.createOfferingKeyOfferingID(new BigInteger(mainProductId));
        offeringKey.setOfferingID(offeringID);
        primaryOffering.setOfferingKey(offeringKey);
        primaryOffering.setStatus("1");
        subscriberRequest.setPrimaryOffering(primaryOffering);

        if(welcomePack) {
            subscriberRequest.getSupplementaryOffering().clear();
            CreateSubscriberRequest.SupplementaryOffering supOffering = new CreateSubscriberRequest.SupplementaryOffering();
            offeringKey = this.objectFactory.createOfferingKey();
            //old welcome pack
            //offeringKey.setOfferingID(this.objectFactory.createOfferingKeyOfferingID(new BigInteger("33011")));
            //new welcome pack
            offeringKey.setOfferingID(this.objectFactory.createOfferingKeyOfferingID(new BigInteger("60063")));
            supOffering.setOfferingKey(offeringKey);
            EffectMode effectMode = objectFactory.createEffectMode();
            effectMode.setMode("I");
            supOffering.setEffectiveTime(effectMode);
            subscriberRequest.getSupplementaryOffering().add(supOffering);
        }

        //Full Assignation
        req.setCreateSubscriberRequest(subscriberRequest);
        return req;
    }

    public SubDeactivationResultMsg deleteSubscriber(String msisdn){
        SubDeactivationRequestMsg req = this.objectFactory.createSubDeactivationRequestMsg();
        req.setRequestHeader(this.buildCbsQueryRequestHeader());
        req.getRequestHeader().setBusinessCode("Tertio_DeleteSubscriber");
        SubDeactivationRequest request = this.objectFactory.createSubDeactivationRequest();
        SubAccessCode subAccessCode = new SubAccessCode();
        subAccessCode.setPrimaryIdentity(msisdn);
        request.setSubAccessCode(subAccessCode);
        request.setOpType("3");
        req.setSubDeactivationRequest(request);
        return (SubDeactivationResultMsg) getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("SubDeactivation"));
    }


    public ChangeSubIdentityResultMsg changeSIM(String msisdn, String oldImsi, String newImsi){
        ChangeSubIdentityRequestMsg req = this.objectFactory.createChangeSubIdentityRequestMsg();
        req.setRequestHeader(this.buildCbsQueryRequestHeader());
        req.getRequestHeader().setBusinessCode("Tertio_ChangeIMSI");
        ChangeSubIdentityRequest request = this.objectFactory.createChangeSubIdentityRequest();
        SubAccessCode subAccessCode = this.objectFactory.createSubAccessCode();
        subAccessCode.setPrimaryIdentity(msisdn);
        request.setSubAccessCode(subAccessCode);
        ChangeSubIdentityRequest.ModifySubIdentity modifySubIdentity = this.objectFactory.createChangeSubIdentityRequestModifySubIdentity();
        modifySubIdentity.setOldSubIdentity(oldImsi);
        modifySubIdentity.setOldSubIdentityType("2");
        modifySubIdentity.setNewSubIdentity(newImsi);
        request.getModifySubIdentity().add(modifySubIdentity);
        request.setHandlingChargeFlag("0");
        req.setChangeSubIdentityRequest(request);
        return (ChangeSubIdentityResultMsg) getWebServiceTemplate().marshalSendAndReceive(req, new WSAHeader("ChangeSubIdentity"));
    }

    private RequestHeader buildCbsQueryRequestHeader(){
        RequestHeader requestHeader = this.objectFactory.createRequestHeader();
        requestHeader.setVersion("1");
        requestHeader.setBusinessCode("Tertio_Request");
        requestHeader.setMessageSeq(new java.text.SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + (int) (Math.random()*1000));
        OwnershipInfo ownershipInfo = this.objectFactory.createOwnershipInfo();
        ownershipInfo.setBEID(BigInteger.valueOf(101));
        ownershipInfo.setBRID(101L);
        requestHeader.setOwnershipInfo(ownershipInfo);
        SecurityInfo accessSecurity = this.objectFactory.createSecurityInfo();
        accessSecurity.setLoginSystemCode("102");
        accessSecurity.setPassword("SQHpd9ICXX/SVOLzCRXZZB6lAzrxa9Z9DqknK28EiB40/zqbKdzqnv2xNy1SE1ST");
        accessSecurity.setRemoteIP("10.22.4.19");
        requestHeader.setAccessSecurity(accessSecurity);
        requestHeader.setAccessMode("3");
        RequestHeader.TimeFormat timeFormat = this.objectFactory.createRequestHeaderTimeFormat();
        timeFormat.setTimeType("1");
        requestHeader.setTimeFormat(timeFormat);
        return requestHeader;
    }
}
