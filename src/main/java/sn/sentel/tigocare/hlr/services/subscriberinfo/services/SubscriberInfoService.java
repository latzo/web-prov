package sn.sentel.tigocare.hlr.services.subscriberinfo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.hlr.services.hss.clients.HssClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;
import sn.sentel.tigocare.jaxb2.subscriberinfo.CreateResponse;
import sn.sentel.tigocare.jaxb2.subscriberinfo.DeleteResponse;
import sn.sentel.tigocare.jaxb2.subscriberinfo.GetResponse;
import sn.sentel.tigocare.jaxb2.subscriberinfo.SetResponse;

@Slf4j
@Service
public class SubscriberInfoService {

    private final SubscriberInfoClient subscriberInfoClient;
    private final SessionControl sessionControl;
    private final HssClient hssClient;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;


    public SubscriberInfoService(SubscriberInfoClient subscriberInfoClient, SessionControl sessionControl, HssClient hssClient) {
        this.subscriberInfoClient = subscriberInfoClient;
        this.sessionControl = sessionControl;
        this.hssClient = hssClient;
    }

//    @PostConstruct
//    public void testSubscriberInfo() {
//        try {
//            this.getSubscriberInfo("762865365");
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//        try {
//            this.getSubscriberInfoImsi("608026401114101");
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//
//    }

    public GetResponse getSubscriberInfo(String msisdn) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        GetResponse res;
        try {
            res = this.subscriberInfoClient.getSubscriberInfo(msisdn, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public GetResponse getSubscriberInfoImsi(String imsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        GetResponse res;
        try {
            res = this.subscriberInfoClient.getSubscriberInfoImsi(imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public sn.sentel.tigocare.jaxb2.hss.GetResponse getSubscriberInfoHss(String imsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        sn.sentel.tigocare.jaxb2.hss.GetResponse res;
        try {
            res = this.hssClient.getSubscriberInfo(imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            return new sn.sentel.tigocare.jaxb2.hss.GetResponse();
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public CreateResponse createSubscriber(String msisdn, String imsi, Integer profileId) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        CreateResponse res;
        try {
            res =  this.subscriberInfoClient.createSubscriber(msisdn, imsi, sessionId, profileId);
            this.subscriberInfoClient.changeServiceCallWaiting(msisdn, OperationEnum.ACTIVATE, sessionId);
            this.subscriberInfoClient.changeServiceConfCall(msisdn, OperationEnum.ACTIVATE, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public DeleteResponse deleteSubscriber(String imsi, String msisdn){
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        DeleteResponse res;
        try {
            res = this.subscriberInfoClient.deleteSubscriber(imsi, msisdn, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }

    public void cancelVLR(String msisdn) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return;
        }
        try {
            this.subscriberInfoClient.cancelVLR(msisdn, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
    }

    public SetResponse doTransfertAppelVersAutreNum(String msisdn, String msisdnDest, OperationEnum operationEnum){
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        SetResponse response;
        try{
            response = this.subscriberInfoClient.doCallForwardingToOtherMsisdn(msisdn, msisdnDest, operationEnum, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return response;
    }
}
