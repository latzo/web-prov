package sn.sentel.tigocare.hlr.services.imsichangeovercudb.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.hlr.services.imsichangeovercudb.clients.ImsiChangeOverCudbClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.jaxb2.imsichangeovercudb.GetResponse;
import javax.annotation.PostConstruct;

@Service
@Slf4j
public class ImsiChangeOverCudbService {

    @Autowired
    private SessionControl sessionControl;
    @Autowired
    private ImsiChangeOverCudbClient imsiChangeOverCudbClient;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;

    public ImsiChangeOverCudbService(SessionControl sessionControl, ImsiChangeOverCudbClient imsiChangeOverCudbClient){
        this.imsiChangeOverCudbClient = imsiChangeOverCudbClient;
        this.sessionControl = sessionControl;
    }

    public GetResponse getImsiChangeOverData(String nimsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        GetResponse res = new GetResponse();
        try {
            res = this.imsiChangeOverCudbClient.getImsiChangeOverData(nimsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
        }finally {
            sessionControl.logout(sessionId);
            return res;
        }
    }

//    @PostConstruct
//    public void testGetImsiChangeOverData() {
//        try {
//            GetResponse getResponse = this.getImsiChangeOverData("608026404443953");
//            log.info("msisdn get imsi changeoverdata msisdn " + getResponse.getMOAttributes());
//            //log.info("msisdn get imsi changeoverdata msisdn" + getResponse.getMOAttributes().getGetIMSIChangeover().getIMSIChangeoverData().getMsisdn());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
