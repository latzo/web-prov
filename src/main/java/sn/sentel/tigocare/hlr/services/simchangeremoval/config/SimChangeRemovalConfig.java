package sn.sentel.tigocare.hlr.services.simchangeremoval.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.simchange.clients.SimChangeClient;
import sn.sentel.tigocare.hlr.services.simchangeremoval.clients.SimChangeRemovalClient;

@Configuration
public class SimChangeRemovalConfig {

    @Value("${app.ws.sim-change.endpoint}")
    private String endpoint;

    @Value("${app.ws.sim-change.host}")
    private String host;

    @Value("${app.ws.sim-change.packageremoval}")
    private String contextPathRemoval;


    private Jaxb2Marshaller buildMarshallerRemoval() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPathRemoval);
        return marshaller;
    }


    @Bean
    public SimChangeRemovalClient simChangeRemovalClient() {
        final SimChangeRemovalClient simChangeRemovalClient = new SimChangeRemovalClient();
        final Jaxb2Marshaller marshaller = this.buildMarshallerRemoval();
        simChangeRemovalClient.setDefaultUri(this.host + this.endpoint);
        simChangeRemovalClient.setMarshaller(marshaller);
        simChangeRemovalClient.setUnmarshaller(marshaller);

        return simChangeRemovalClient;

    }
}
