package sn.sentel.tigocare.hlr.services.subscriberinfo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;
import sn.sentel.tigocare.hlr.services.simchange.clients.SimChangeClient;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;

@Configuration
public class SubsciberInfoConfig {

    @Value("${app.ws.subscriber-info.endpoint}")
    private String endpoint;

    @Value("${app.ws.subscriber-info.host}")
    private String host;

    @Value("${app.ws.subscriber-info.package}")
    private String contextPath;

    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }

    @Bean
    public SubscriberInfoClient subscriberInfoClient() {
        final SubscriberInfoClient subscriberInfoClient = new SubscriberInfoClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();

        subscriberInfoClient.setDefaultUri(this.host + this.endpoint);
        subscriberInfoClient.setMarshaller(marshaller);
        subscriberInfoClient.setUnmarshaller(marshaller);

        return subscriberInfoClient;

    }
}
