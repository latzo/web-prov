package sn.sentel.tigocare.hlr.services.hss.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.hlr.services.hss.clients.HssClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.jaxb2.hss.CreateResponse;
import sn.sentel.tigocare.jaxb2.hss.DeleteResponse;
import sn.sentel.tigocare.jaxb2.hss.SetResponse;
import sn.sentel.tigocare.service.UtilsService;

@Slf4j
@Service
public class HssService {

    private final HssClient hssClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;

    @Autowired
    private final UtilsService utilsService;

    public HssService(SessionControl sessionControl, HssClient hssClient, UtilsService utilsService) {
        this.sessionControl = sessionControl;
        this.hssClient = hssClient;
        this.utilsService = utilsService;
    }

//    @PostConstruct
//    public void testCreateSubscriber() {
//        try {
//            this.deleteSubscriber("608026401155743");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public CreateResponse createSubscriber(String imsi, String msisdn, String epsProfileId) throws Exception {
        CreateResponse res = null;
        log.info("MSISDN IS " + msisdn);

        if(this.utilsService.is4gNumber(imsi)){
            String sessionId = this.sessionControl.login(this.userId, this.pwd);
            if (sessionId == null) return null;
            try {
                res = this.hssClient.createSubscriber(imsi, msisdn,epsProfileId, sessionId);
            }catch (SoapFaultClientException e ) {
                //log.error(e.getMessage());
                throw e;
            }finally {
                sessionControl.logout(sessionId);
            }
        }
        return res;
    }

    public DeleteResponse deleteSubscriber(String imsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        DeleteResponse res;
        try {
            res =  this.hssClient.deleteSubscriber(imsi,sessionId);
        }catch (Exception e ) {
            //log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

    public SetResponse changeServiceAddEsmProfile(String imsi, OperationEnum operation) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        SetResponse res;
        try {
            res =  this.hssClient.changeServiceAddEsmProfile(imsi ,operation, sessionId);
        }catch (Exception e ) {
            //log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

    public sn.sentel.tigocare.jaxb2.hss.GetResponse getSubscriberInfoHssByMsisdn(String msisdn) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return null;
        }
        sn.sentel.tigocare.jaxb2.hss.GetResponse res;
        try {
            res = this.hssClient.getSubscriberInfoMsisdn(msisdn, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            return new sn.sentel.tigocare.jaxb2.hss.GetResponse();
        }finally {
            this.sessionControl.logout(sessionId);
        }
        return res;
    }
}
