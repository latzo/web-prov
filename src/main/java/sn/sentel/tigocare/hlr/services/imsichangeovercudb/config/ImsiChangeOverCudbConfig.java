package sn.sentel.tigocare.hlr.services.imsichangeovercudb.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.imsichangeovercudb.clients.ImsiChangeOverCudbClient;

@Configuration
public class ImsiChangeOverCudbConfig {

    @Value("${app.ws.sim-change-cudb.endpoint}")
    private String endpoint;

    @Value("${app.ws.sim-change-cudb.host}")
    private String host;

    @Value("${app.ws.sim-change-cudb.package}")
    private String contextPath;

    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }


    @Bean
    public ImsiChangeOverCudbClient imsiChangeOverCudbClient() {
        final ImsiChangeOverCudbClient imsiChangeOverCudbClient = new ImsiChangeOverCudbClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();

        imsiChangeOverCudbClient.setDefaultUri(this.host + this.endpoint);
        imsiChangeOverCudbClient.setMarshaller(marshaller);
        imsiChangeOverCudbClient.setUnmarshaller(marshaller);

        return imsiChangeOverCudbClient;

    }
}
