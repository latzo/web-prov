package sn.sentel.tigocare.hlr.services.simchange.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.simchange.clients.SimChangeClient;
import sn.sentel.tigocare.jaxb2.simchange.CreateResponse;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class SimChangeService {


    private final SimChangeClient simChangeClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;

    @Value("${app.ws.password}")
    private String pwd;


    public SimChangeService(SimChangeClient simChangeClient, SessionControl sessionControl) {
        this.simChangeClient = simChangeClient;
        this.sessionControl = sessionControl;
    }

//    @PostConstruct
//    public void testLogin() {
//        try {
//            this.changeSIM("234567", "456789", "23456789");
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//
//    }

    public CreateResponse changeSIM(String msisdn, String nimsi, String imsi) {
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) return null;
        CreateResponse res;
        try {
            res = this.simChangeClient.createSimChangeOver(msisdn, nimsi, imsi, sessionId);
        }catch (SoapFaultClientException e ) {
            log.error(e.getMessage());
            throw e;
        }finally {
            sessionControl.logout(sessionId);
        }
        return res;
    }

}
