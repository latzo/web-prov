package sn.sentel.tigocare.hlr.services.imsichangeovercudb.clients;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.jaxb2.imsichangeovercudb.Get;
import sn.sentel.tigocare.jaxb2.imsichangeovercudb.GetResponse;
import sn.sentel.tigocare.jaxb2.imsichangeovercudb.ObjectFactory;

@Component
@Slf4j
public class ImsiChangeOverCudbClient extends WebServiceGatewaySupport {

    private static final String LOG_HEADER = "[WS:IMSI CHANGE OVER CUDB CLIENT]";
    private final ObjectFactory objectFactory = new sn.sentel.tigocare.jaxb2.imsichangeovercudb.ObjectFactory();

    public GetResponse getImsiChangeOverData(String newImsi, String session){
        Get getRequest = this.objectFactory.createGet();

        getRequest.setMOType("IMSIChangeover@http://schemas.ericsson.com/pg/cudb/1.0/");

        Get.MOId moid = new Get.MOId();
        moid.setNimsi(newImsi);
        getRequest.setMOId(moid);
        log.info("{} Request for a sim change over with {}", LOG_HEADER, getRequest);

        return (GetResponse) this.getWebServiceTemplate().marshalSendAndReceive(getRequest, new LoginCallBack(session));
    }
}
