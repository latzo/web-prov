package sn.sentel.tigocare.hlr.services.msa;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.sentel.tigocare.domain.Archivage;
import sn.sentel.tigocare.repository.ArchivageRepository;

import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ArchivageService {
    private final ArchivageRepository archivageRepository;

    @Value("${app.talend.host}")
    private String host;

    public ArchivageService(ArchivageRepository archivageRepository) {
        this.archivageRepository = archivageRepository;
    }



    public ResponseEntity archiver(String requestId, String featureId, String timeStamp, String languageId, String channelId, String msisdn, String apiName, String apiType) {
/*
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);

        ArrayList < Param > param = new ArrayList< Param >();
        param.add(Param.builder().name("Msisdn").value(msisdn).build());
        param.add(Param.builder().name("ApiName").value(apiName).build());
        param.add(Param.builder().name("ApiType").value(apiType).build());
        HttpEntity<Request> entity = new HttpEntity<>(Request.builder()
            .requestId(requestId)
            .featureId(featureId)
            .languageId(languageId)
            .channelId(channelId)
            .DataObject(Data.builder()
                .param(param)
                .build())
            .build(), headers);
        ResponseEntity<Request> requestResponseEntity = restTemplate.postForEntity("http://192.168.41.165:8040/services/sentinel/archivage" , entity, Request.class);
         return   requestResponseEntity;
*/
        String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?><request>\n" +
            "    <requestId>"+Instant.now().toEpochMilli()+"</requestId>\n" +
            "    <featureId>engrafi.middleware.delete.subscriber</featureId>\n" +
            "    <timeStamp>"+Instant.now().toEpochMilli()+"</timeStamp>\n" +
            "    <languageId>1</languageId> \n" +
            "    <data>\n" +
            "        <param>\n" +
            "            <name>Msisdn</name>\n" +
            "            <value>"+msisdn+"</value>\n" +
            "        </param>\n" +
            "       <param>\n" +
            "            <name>ApiName</name>\n" +
            "            <value>NMS</value>\n" +
            "        </param>\n" +
            "        <param>\n" +
            "            <name>ApiType</name>\n" +
            "            <value>TERTIO-ARCHIVING</value>\n" +
            "        </param>\n" +
            "    </data>\n" +
            "    <channelId>5</channelId>\n" +
            "</request>\n";



        RestTemplate restTemplate =  new RestTemplate();
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = null;

            sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //Create a list for the message converters
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        //Add the String Message converter
        messageConverters.add(new StringHttpMessageConverter());
        //Add the message converters to the restTemplate
        restTemplate.setMessageConverters(messageConverters);


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

        final ResponseEntity<String> response = restTemplate.postForEntity(this.host+"/services/sentinel/archivage", request, String.class);
        Archivage archivage = new Archivage();
        archivage.setStatus(response.getBody().toString());
        archivage.setMsisdn(msisdn);
        archivageRepository.save(archivage);
        System.out.println(xmlString);
        return response;
    }

}
