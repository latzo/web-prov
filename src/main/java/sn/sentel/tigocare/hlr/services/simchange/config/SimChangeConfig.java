package sn.sentel.tigocare.hlr.services.simchange.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import sn.sentel.tigocare.hlr.services.simchange.clients.SimChangeClient;

@Configuration
public class SimChangeConfig {

    @Value("${app.ws.sim-change.endpoint}")
    private String endpoint;

    @Value("${app.ws.sim-change.host}")
    private String host;

    @Value("${app.ws.sim-change.package}")
    private String contextPath;


    private Jaxb2Marshaller buildMarshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(this.contextPath);
        return marshaller;
    }


    @Bean
    public SimChangeClient simChangeClient() {
        final SimChangeClient simChangeClient = new SimChangeClient();
        final Jaxb2Marshaller marshaller = this.buildMarshaller();

        simChangeClient.setDefaultUri(this.host + this.endpoint);
        simChangeClient.setMarshaller(marshaller);
        simChangeClient.setUnmarshaller(marshaller);

        return simChangeClient;

    }

}
