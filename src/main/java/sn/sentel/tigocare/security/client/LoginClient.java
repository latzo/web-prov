//package sn.sentel.tigocare.security.client;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import javax.annotation.PostConstruct;
//import java.io.IOException;
//
//@Service
//@Slf4j
//public class LoginClient {
//    @Value("${app.apiLogin.login}")
//    private String login;
//    @Value("${app.apiLogin.password}")
//    private String password;
//    private ObjectMapper objectMapper = new ObjectMapper();
//
//    public String doAdminLogin() throws JSONException, IOException {
//        RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        JSONObject loginObject = new JSONObject();
//        loginObject.put("username",login);
//        loginObject.put("password", password);
//        HttpEntity<String> request = new HttpEntity<String>(loginObject.toString(), headers);
//        String loginResultAsJsonStr = restTemplate.postForObject("http://localhost:6060/api/authenticate", request, String.class);
//        if(loginResultAsJsonStr != null){
//            JsonNode root = objectMapper.readTree(loginResultAsJsonStr);
//            return root.path("id_token").asText();
//        }
//        return "";
//    }
//
////    @PostConstruct
////    public void testCreateSubscriber() {
////        try {
////            log.info("Login with admin creds");
////            String bearer = this.doAdminLogin();
////            log.info("Bearer : " + bearer);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
//}
