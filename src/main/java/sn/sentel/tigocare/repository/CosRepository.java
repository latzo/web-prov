package sn.sentel.tigocare.repository;

import sn.sentel.tigocare.domain.Cos;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Cos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CosRepository extends JpaRepository<Cos, Long> {

}
