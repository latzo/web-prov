package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.MnpPortout;
import sn.sentel.tigocare.domain.Pinpuk;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Pinpuk entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PinpukRepository extends JpaRepository<Pinpuk, Long> {

    Optional<Pinpuk> findOneByImsi(String imsi);

    Optional<Pinpuk> findFirstByImsi(String imsi);
}
