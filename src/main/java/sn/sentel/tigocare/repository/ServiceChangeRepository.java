package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.RoamingService;
import sn.sentel.tigocare.domain.ServiceChange;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the ServiceChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceChangeRepository extends JpaRepository<ServiceChange, Long> {

    List<ServiceChange> findAllByProcessingStatus(ProcessingStatus submitted);

    void deleteByCreatedDateBefore(Instant before);

    Page<ServiceChange> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

    Page<ServiceChange> findAllByTransactionId(String transactionId, Pageable pageable);
}
