package sn.sentel.tigocare.repository;

import sn.sentel.tigocare.domain.MainProductsCbs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the MainProductsCbs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MainProductsCbsRepository extends JpaRepository<MainProductsCbs, Long> {
    Optional<MainProductsCbs> findByMainProductId(String mainProductId);
}
