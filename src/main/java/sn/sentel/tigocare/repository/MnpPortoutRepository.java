package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.MnpPorting;
import sn.sentel.tigocare.domain.MnpPortout;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the MnpPortout entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MnpPortoutRepository extends JpaRepository<MnpPortout, Long> {
    void deleteByCreatedDateBefore(Instant before);
    Page<MnpPortout> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
    List<MnpPortout> findAllByProcessingStatus(ProcessingStatus status);
    //Page<MnpPortout> findAllByTransactionId(String transactionId, Pageable pageable);
}
