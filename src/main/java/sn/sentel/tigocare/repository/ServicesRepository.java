package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import sn.sentel.tigocare.domain.RoamingService;
import sn.sentel.tigocare.domain.Services;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Services entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServicesRepository extends JpaRepository<Services, Long> {

    Optional<Services> findOneByCode(String serviceId);
    //Page<Services> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
}
