package sn.sentel.tigocare.repository;
import sn.sentel.tigocare.domain.Roaming4g;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Roaming4g entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Roaming4gRepository extends JpaRepository<Roaming4g, Long> {

}
