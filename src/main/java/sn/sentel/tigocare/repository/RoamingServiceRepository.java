package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.MnpPorting;
import sn.sentel.tigocare.domain.ResetLocation;
import sn.sentel.tigocare.domain.RoamingService;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.util.List;


/**
 * Spring Data  repository for the RoamingServiceEnum entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoamingServiceRepository extends JpaRepository<RoamingService, Long> {
    Page<RoamingService> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
    List<RoamingService> findAllByProcessingStatus(ProcessingStatus status);
    //Page<RoamingService> findAllByTransactionId(String transactionId, Pageable pageable);
}
