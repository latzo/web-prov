package sn.sentel.tigocare.repository;
import sn.sentel.tigocare.domain.Archivage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Archivage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArchivageRepository extends JpaRepository<Archivage, Long> {

}
