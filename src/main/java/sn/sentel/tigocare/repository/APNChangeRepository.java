package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.SimSwap;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the APNChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface APNChangeRepository extends JpaRepository<APNChange, Long> {

    List<APNChange> findAllByProcessingStatus(ProcessingStatus status);
    Page<APNChange> findAllByTransactionId(String transactionId, Pageable pageable);
    void deleteByCreatedDateBefore(Instant before);
    Page<APNChange> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

}
