package sn.sentel.tigocare.repository;

import sn.sentel.tigocare.domain.FullPortin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FullPortin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FullPortinRepository extends JpaRepository<FullPortin, Long> {

}
