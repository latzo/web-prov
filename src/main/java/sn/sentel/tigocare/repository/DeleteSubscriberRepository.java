package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the DeleteSubscriber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeleteSubscriberRepository extends JpaRepository<DeleteSubscriber, Long> {

    List<DeleteSubscriber> findAllByProcessingStatus(ProcessingStatus processingStatus);

    Page<DeleteSubscriber> findAllByTransactionId(String transactionId, Pageable pageable);

    void deleteByCreatedDateBefore(Instant before);

    Page<DeleteSubscriber> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
}
