package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the CreateSubscriber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CreateSubscriberRepository extends JpaRepository<CreateSubscriber, Long> {

    List<CreateSubscriber> findAllByProcessingStatus(ProcessingStatus p);
    Page<CreateSubscriber> findAllByTransactionId(String transactionId, Pageable pageable);

    void deleteByCreatedDateBefore(Instant before);

    Page<CreateSubscriber> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

    Page<CreateSubscriber> findAllByCreationTypeAndCreatedByIn(SIMCreationType creationType, List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

}
