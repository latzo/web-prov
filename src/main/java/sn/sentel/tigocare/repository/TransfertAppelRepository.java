package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.domain.TransfertAppel;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.util.List;


/**
 * Spring Data  repository for the TransfertAppel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransfertAppelRepository extends JpaRepository<TransfertAppel, Long> {

    List<TransfertAppel> findAllByProcessingStatus(ProcessingStatus processingStatus);
    Page<TransfertAppel> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
    Page<TransfertAppel> findAllByTransactionId(String transactionId, Pageable pageable);
}
