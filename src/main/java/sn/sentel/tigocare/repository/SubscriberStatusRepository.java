package sn.sentel.tigocare.repository;

import sn.sentel.tigocare.domain.SubscriberStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SubscriberStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubscriberStatusRepository extends JpaRepository<SubscriberStatus, Long> {

}
