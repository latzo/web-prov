package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.MnpPorting;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the MnpPorting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MnpPortingRepository extends JpaRepository<MnpPorting, Long> {

    void deleteByCreatedDateBefore(Instant before);

    Page<MnpPorting> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

    List<MnpPorting> findAllByProcessingStatus(ProcessingStatus status);

    //Page<MnpPorting> findAllByTransactionId(String transactionId, Pageable pageable);

}
