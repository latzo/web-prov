package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.AffichageInformationsAbonnes;
import sn.sentel.tigocare.domain.User;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the AffichageInformationsAbonnes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AffichageInformationsAbonnesRepository extends JpaRepository<AffichageInformationsAbonnes, Long> {

    void deleteByCreatedDateBefore(Instant before);

    Page<AffichageInformationsAbonnes> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
}
