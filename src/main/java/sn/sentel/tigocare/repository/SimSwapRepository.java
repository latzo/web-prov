package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.RoamingService;
import sn.sentel.tigocare.domain.SimSwap;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the SimSwap entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SimSwapRepository extends JpaRepository<SimSwap, Long> {

    List<SimSwap> findAllByProcessingStatus(ProcessingStatus processingStatus);
    Page<SimSwap> findAllByTransactionId(String transactionId,  Pageable pageable);
    void deleteByCreatedDateBefore(Instant before);
    Page<SimSwap> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);

}
