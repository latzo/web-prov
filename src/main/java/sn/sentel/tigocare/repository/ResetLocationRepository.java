package sn.sentel.tigocare.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.domain.MnpPorting;
import sn.sentel.tigocare.domain.MnpPortout;
import sn.sentel.tigocare.domain.ResetLocation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.util.List;


/**
 * Spring Data  repository for the ResetLocation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResetLocationRepository extends JpaRepository<ResetLocation, Long> {
    Page<ResetLocation> findByCreatedByIn(List<String> loginOfUsersOfTheSameGroup, Pageable pageable);
    List<ResetLocation> findAllByProcessingStatus(ProcessingStatus status);
    //Page<ResetLocation> findAllByTransactionId(String transactionId, Pageable pageable);
}
