package sn.sentel.tigocare.domain;

import lombok.*;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A DeleteSubscriber.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "delete_subscriber")
public class DeleteSubscriber extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "delete_on_auc")
    private Boolean deleteOnAuc;

    @Column(name = "delete_on_hlr")
    private Boolean deleteOnHlr;

    @Column(name = "delete_on_hss")
    private Boolean deleteOnHss;

    @Column(name = "delete_on_cbs")
    private Boolean deleteOnCbs;

    @Enumerated(EnumType.STRING)
    @Column(name = "auc_deleted_status")
    private ProcessingStatus aucDeletedStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "hlr_deleted_status")
    private ProcessingStatus hlrDeletedStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "hss_deleted_status")
    private ProcessingStatus hssDeletedStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "cbs_deleted_status")
    private ProcessingStatus cbsDeletedStatus;

    @Column(name = "error_message")
    private String errorMessage;


    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    @Column(name = "transaction_id")
    private String transactionId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
