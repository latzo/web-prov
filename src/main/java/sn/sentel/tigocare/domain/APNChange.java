package sn.sentel.tigocare.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A APNChange.
 */
@Entity
@Table(name = "apnchange")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class APNChange extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "imsi", nullable = false)
    private String imsi;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation", nullable = false)
    private OperationEnum operation;

    @NotNull
    @Column(name = "apn", nullable = false)
    private String apn;

    @NotNull
    @Column(name = "profil", nullable = false)
    private String profil;

    @Column(name = "processing_status")
    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "transaction_id")
    private String transactionId;

}
