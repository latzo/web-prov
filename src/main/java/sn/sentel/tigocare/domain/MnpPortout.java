package sn.sentel.tigocare.domain;

import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A MnpPortout.
 */
@Entity
@Table(name = "mnp_portout")
public class MnpPortout extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "operateur")
    private String operateur;

    @Column(name = "processing_status")
    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public MnpPortout msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public String getOperateur() {
        return this.operateur;
    }

    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    public MnpPortout operateur(String operateur) {
        this.operateur = operateur;
        return this;
    }

    public ProcessingStatus getProcessingStatus() {
        return this.processingStatus;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    public MnpPortout processingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MnpPortout)) {
            return false;
        }
        return this.id != null && this.id.equals(((MnpPortout) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MnpPortout{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", operateur='" + getOperateur() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            "}";
    }
}
