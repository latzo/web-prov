package sn.sentel.tigocare.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;

/**
 * A CreateSubscriber.
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "create_subscriber")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateSubscriber extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "imsi", nullable = false)
    private String imsi;

    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    @Column(name = "statut")
    private String statut;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "backend_resp")
    private String backendResp;

    @Column(name = "cos")
    private String cos;

    @Column(name = "initial_balance")
    private String initialBalance;

    @Enumerated(EnumType.STRING)
    @Column(name = "creation_type")
    private SIMCreationType creationType;

    @Column(name = "name")
    private String name;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "dob")
    private String dob;

    @Column(name = "city")
    private String city;

    @Column(name = "puk")
    private String puk;

    @Column(name = "tigo_cash_status")
    private String tigoCashStatus;

    @Column(name = "gender")
    private String gender;

    @Column(name = "alt_contact_num")
    private String altContactNum;

    @Column(name = "district")
    private String district;

    @Column(name = "document_reference_type")
    private String documentReferenceType;

    @Column(name = "proof_number")
    private String proofNumber;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_date")
    private String createDate;

}
