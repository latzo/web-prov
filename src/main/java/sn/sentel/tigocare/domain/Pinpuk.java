package sn.sentel.tigocare.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Pinpuk.
 */
@Entity
@Table(name = "PINPUK")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pinpuk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ICCID")
    private String iccid;

    @Column(name = "IMSI")
    private String imsi;

    @Column(name = "PIN1")
    private Long pin1;

    @Column(name = "PUK1")
    private Long puk1;

    @Column(name = "PIN2")
    private Long pin2;

    @Column(name = "PUK2")
    private Long puk2;

    @Column(name = "KI")
    private String ki;

    @Column(name = "ADM1")
    private String adm1;

    @Column(name = "NOMFICHIER")
    private String nomfichier;

    @Column(name = "SUPPLIER")
    private String supplier;

    @Column(name = "LOADDATE")
    private LocalDate loaddate;

}
