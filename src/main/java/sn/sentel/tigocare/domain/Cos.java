package sn.sentel.tigocare.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Cos.
 */
@Entity
@Table(name = "cos")
public class Cos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "class_id_cbs")
    private String classIdCbs;

    @Column(name = "label")
    private String label;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassIdCbs() {
        return classIdCbs;
    }

    public Cos classIdCbs(String classIdCbs) {
        this.classIdCbs = classIdCbs;
        return this;
    }

    public void setClassIdCbs(String classIdCbs) {
        this.classIdCbs = classIdCbs;
    }

    public String getLabel() {
        return label;
    }

    public Cos label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cos)) {
            return false;
        }
        return id != null && id.equals(((Cos) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cos{" +
            "id=" + getId() +
            ", classIdCbs='" + getClassIdCbs() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
