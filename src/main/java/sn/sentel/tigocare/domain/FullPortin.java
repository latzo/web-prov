package sn.sentel.tigocare.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A FullPortin.
 */
@Entity
@Table(name = "full_portin")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullPortin extends CreateSubscriber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
