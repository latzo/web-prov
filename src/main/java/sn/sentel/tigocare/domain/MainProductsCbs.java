package sn.sentel.tigocare.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A MainProductsCbs.
 */
@Entity
@Table(name = "main_products_cbs")
public class MainProductsCbs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "main_product_name")
    private String mainProductName;

    @Column(name = "main_product_id")
    private String mainProductId;

    @Column(name = "hlr_profil_id")
    private String hlrProfilId;

    @Column(name = "profile_class")
    private String profileClass;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMainProductName() {
        return mainProductName;
    }

    public MainProductsCbs mainProductName(String mainProductName) {
        this.mainProductName = mainProductName;
        return this;
    }

    public void setMainProductName(String mainProductName) {
        this.mainProductName = mainProductName;
    }

    public String getMainProductId() {
        return mainProductId;
    }

    public MainProductsCbs mainProductId(String mainProductId) {
        this.mainProductId = mainProductId;
        return this;
    }

    public void setMainProductId(String mainProductId) {
        this.mainProductId = mainProductId;
    }

    public String getHlrProfilId() {
        return hlrProfilId;
    }

    public MainProductsCbs hlrProfilId(String hlrProfilId) {
        this.hlrProfilId = hlrProfilId;
        return this;
    }

    public void setHlrProfilId(String hlrProfilId) {
        this.hlrProfilId = hlrProfilId;
    }

    public String getProfileClass() {
        return profileClass;
    }

    public MainProductsCbs profileClass(String profileClass) {
        this.profileClass = profileClass;
        return this;
    }

    public void setProfileClass(String profileClass) {
        this.profileClass = profileClass;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MainProductsCbs)) {
            return false;
        }
        return id != null && id.equals(((MainProductsCbs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MainProductsCbs{" +
            "id=" + getId() +
            ", mainProductName='" + getMainProductName() + "'" +
            ", mainProductId='" + getMainProductId() + "'" +
            ", hlrProfilId='" + getHlrProfilId() + "'" +
            ", profileClass='" + getProfileClass() + "'" +
            "}";
    }
}
