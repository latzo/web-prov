package sn.sentel.tigocare.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import sn.sentel.tigocare.domain.enumeration.Roaming4gEnum;

import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

/**
 * A Roaming4g.
 */
@Entity
@Table(name = "roaming4g")
public class Roaming4g extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "imsi", nullable = false)
    private String imsi;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "services", nullable = false)
    private Roaming4gEnum services;

    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImsi() {
        return imsi;
    }

    public Roaming4g imsi(String imsi) {
        this.imsi = imsi;
        return this;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Roaming4gEnum getServices() {
        return services;
    }

    public Roaming4g services(Roaming4gEnum services) {
        this.services = services;
        return this;
    }

    public void setServices(Roaming4gEnum services) {
        this.services = services;
    }

    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public Roaming4g processingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
        return this;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Roaming4g)) {
            return false;
        }
        return id != null && id.equals(((Roaming4g) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Roaming4g{" +
            "id=" + getId() +
            ", imsi='" + getImsi() + "'" +
            ", services='" + getServices() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            "}";
    }
}
