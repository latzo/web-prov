package sn.sentel.tigocare.domain.enumeration;

/**
 * The Roaming4gEnum enumeration.
 */
public enum Roaming4gEnum {
    F4G_ROAMING, NO_4G_ROAMING
}
