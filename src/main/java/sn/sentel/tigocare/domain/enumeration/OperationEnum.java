package sn.sentel.tigocare.domain.enumeration;

import lombok.Getter;

/**
 * The OperationEnum enumeration.
 */
@Getter
public enum OperationEnum {
    ACTIVATE(1), DEACTIVATE(0);

    short val;

    OperationEnum(int val) {
        this.val = (short) val;
    }
}
