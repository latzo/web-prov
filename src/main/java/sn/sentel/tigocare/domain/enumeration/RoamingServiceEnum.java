package sn.sentel.tigocare.domain.enumeration;

/**
 * The RoamingServiceEnum enumeration.
 */
public enum RoamingServiceEnum {
    VOICE_DATA, VOICE_NODATA, NOVOICE_DATA, NOVOICE_NODATA
}
