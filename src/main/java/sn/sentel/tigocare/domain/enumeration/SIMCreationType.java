package sn.sentel.tigocare.domain.enumeration;

/**
 * The SIMCreationType enumeration.
 */
public enum SIMCreationType {
    IN, SWITCH, IN_SWITCH, AUC, HSS, IN_SWITCH_HSS,FULL_PORTIN, PORTIN
}
