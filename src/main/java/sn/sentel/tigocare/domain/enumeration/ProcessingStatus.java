package sn.sentel.tigocare.domain.enumeration;

/**
 * The ProcessingStatus enumeration.
 */
public enum ProcessingStatus {
    SUBMITTED, SUCCEEDED, FAILED
}
