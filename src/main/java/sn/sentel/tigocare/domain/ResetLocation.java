package sn.sentel.tigocare.domain;

import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A ResetLocation.
 */
@Entity
@Table(name = "reset_location")
public class ResetLocation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "processing_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public ResetLocation msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResetLocation)) {
            return false;
        }
        return this.id != null && this.id.equals(((ResetLocation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ResetLocation{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            "}";
    }

    public ProcessingStatus getProcessingStatus() {
        return this.processingStatus;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }
}
