package sn.sentel.tigocare.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

/**
 * A TransfertAppel.
 */
@Entity
@Table(name = "transfert_appel")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransfertAppel extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn_source", nullable = false)
    private String msisdnSource;

    @Column(name = "msisdn_dest")
    private String msisdnDest;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation", nullable = false)
    private OperationEnum operation;

    @Enumerated(EnumType.STRING)
    @Column(name = "processing_status")
    private ProcessingStatus processingStatus;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "backend_resp")
    private String backendResp;

}
