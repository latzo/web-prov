package sn.sentel.tigocare.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A AffichageInformationsAbonnes.
 */
@Entity
@Table(name = "affichage_informations_abonnes")
public class AffichageInformationsAbonnes extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "imsi_hlr")
    private String imsiHlr;

    @Column(name = "imsi_cbs")
    private String imsiCbs;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public AffichageInformationsAbonnes msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public AffichageInformationsAbonnes imsi(String imsi) {
        this.imsi = imsi;
        return this;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getImsiHlr() {
        return imsiHlr;
    }

    public AffichageInformationsAbonnes imsiHlr(String imsiHlr) {
        this.imsiHlr = imsiHlr;
        return this;
    }

    public void setImsiHlr(String imsiHlr) {
        this.imsiHlr = imsiHlr;
    }

    public String getImsiCbs() {
        return imsiCbs;
    }

    public AffichageInformationsAbonnes imsiCbs(String imsiCbs) {
        this.imsiCbs = imsiCbs;
        return this;
    }

    public void setImsiCbs(String imsiCbs) {
        this.imsiCbs = imsiCbs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AffichageInformationsAbonnes)) {
            return false;
        }
        return id != null && id.equals(((AffichageInformationsAbonnes) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AffichageInformationsAbonnes{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", imsiHlr='" + getImsiHlr() + "'" +
            ", imsiCbs='" + getImsiCbs() + "'" +
            "}";
    }
}
