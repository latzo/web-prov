package sn.sentel.tigocare.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A SimSwap.
 */
@Entity
@Table(name = "sim_swap")
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimSwap extends AbstractAuditingEntity implements Serializable, ProcessingAction {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn")
    private String msisdn;

    @NotNull
    @Column(name = "old_imsi", nullable = false)
    private String oldIMSI;

    @NotNull
    @Column(name = "new_imsi", nullable = false)
    private String newIMSI;

    @NotNull
    @Column(name = "processing_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "backend_resp")
    private String backendResp;
}
