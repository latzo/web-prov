package sn.sentel.tigocare.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Archivage.
 */
@Entity
@Table(name = "archivage")
public class Archivage extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "status")
    private String status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Archivage msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }


    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Archivage)) {
            return false;
        }
        return id != null && id.equals(((Archivage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Archivage{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            "}";
    }
}
