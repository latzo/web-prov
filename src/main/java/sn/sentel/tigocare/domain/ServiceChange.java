package sn.sentel.tigocare.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

/**
 * A ServiceChange.
 */
@Entity
@Table(name = "service_change")
public class ServiceChange extends AbstractAuditingEntity implements Serializable, ProcessingAction {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "imsi", nullable = false)
    private String imsi;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation", nullable = false)
    private OperationEnum operation;

    @NotNull
    @Column(name = "service_id", nullable = false)
    private String serviceId;

    @NotNull
    @Column(name = "processing_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProcessingStatus processingStatus;

    @Column(name = "transaction_id")
    private String transactionId;

    @Getter
    @Setter
    @Column(name = "backend_resp")
    private String backendResp;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public ServiceChange msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public ServiceChange imsi(String imsi) {
        this.imsi = imsi;
        return this;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public ServiceChange operation(OperationEnum operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(OperationEnum operation) {
        this.operation = operation;
    }

    public String getServiceId() {
        return serviceId;
    }

    public ServiceChange serviceId(String serviceId) {
        this.serviceId = serviceId;
        return this;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceChange)) {
            return false;
        }
        return id != null && id.equals(((ServiceChange) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ServiceChange{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", operation='" + getOperation() + "'" +
            ", serviceId='" + getServiceId() + "'" +
            "}";
    }

    @Override
    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    @Override
    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    @Override
    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public void setTransactionId(String txId) {
        this.transactionId = txId;
    }
}
