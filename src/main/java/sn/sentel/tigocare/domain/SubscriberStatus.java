package sn.sentel.tigocare.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A SubscriberStatus.
 */
@Entity
@Table(name = "subscriber_status")
public class SubscriberStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status_id")
    private String statusId;

    @Column(name = "label")
    private String label;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusId() {
        return statusId;
    }

    public SubscriberStatus statusId(String statusId) {
        this.statusId = statusId;
        return this;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getLabel() {
        return label;
    }

    public SubscriberStatus label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubscriberStatus)) {
            return false;
        }
        return id != null && id.equals(((SubscriberStatus) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SubscriberStatus{" +
            "id=" + getId() +
            ", statusId='" + getStatusId() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
