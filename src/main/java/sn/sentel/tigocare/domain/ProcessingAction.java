package sn.sentel.tigocare.domain;

import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

public interface ProcessingAction {

    ProcessingStatus getProcessingStatus();

    void setProcessingStatus(ProcessingStatus processingStatus);

    String getTransactionId();

    void setTransactionId(String txId);
}

