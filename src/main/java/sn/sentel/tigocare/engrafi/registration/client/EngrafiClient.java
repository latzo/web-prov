package sn.sentel.tigocare.engrafi.registration.client;

import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.sentel.tigocare.jaxb2.engrafi.Acknowledgment;
import sn.sentel.tigocare.jaxb2.engrafi.ObjectFactory;
import sn.sentel.tigocare.jaxb2.engrafi.Request;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


@Service
@Slf4j
public class EngrafiClient {

    private ObjectFactory objectFactory = new ObjectFactory();
    private SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyy");//("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
    @Value("${app.ws.engrafi.endpoint}")
    private String endpoint;
    @Value("${app.ws.engrafi.host}")
    private String host;
    @Value("${app.ws.engrafi.channelId}")
    private String channelId;
    @Value("${app.ws.engrafi.username}")
    private String userName;
    @Value("${app.ws.engrafi.password}")
    private String password;
    @Value("${app.ws.engrafi.createUser}")
    private String createUSer;




    public String doIdentification(String msisdn, String name, String firstName, String dob, String city, String puk, String tigoCashStatus,
                                    String gender, String altContactNum, String district, String documentReferenceType, String proofNumber,
                                    String createUser, String createDate   ){

        Request request = this.objectFactory.createRequest();
        request.setRequestId(Integer.valueOf(new SimpleDateFormat("yyyyMMHHss").format(new Date())));
        request.setFeatureId("middleware.reg.sim");
        request.setTimeStamp(Integer.valueOf(this.sdfDate.format(new Date())));
        request.setLanguageId((byte) 2);
        request.setChannelId(Byte.parseByte(this.channelId));
        request.setUsername(userName);
        request.setPassword(password);
        Request.Data data = this.objectFactory.createRequestData();
        request.setData(data);
        //CUSTOMER_MSISDN
        Request.Data.Param param1 = this.objectFactory.createRequestDataParam();
        param1.setName("CUSTOMER_MSISDN");
        param1.setValue(msisdn);
        request.getData().getParam().add(param1);
        //NAME
        Request.Data.Param param2 = this.objectFactory.createRequestDataParam();
        param2.setName("NAME");
        param2.setValue(name);
        request.getData().getParam().add(param2);
        //FIRST_NAME
        Request.Data.Param param3 = this.objectFactory.createRequestDataParam();
        param3.setName("FIRST_NAME");
        param3.setValue(firstName);
        request.getData().getParam().add(param3);
        //DOB
        Request.Data.Param param4 = this.objectFactory.createRequestDataParam();
        param4.setName("DOB");
        param4.setValue(dob);
        request.getData().getParam().add(param4);
        //CITY
        Request.Data.Param param5 = this.objectFactory.createRequestDataParam();
        param5.setName("CITY");
        param5.setValue(city);
        request.getData().getParam().add(param5);
        //PUK
        Request.Data.Param param6 = this.objectFactory.createRequestDataParam();
        param6.setName("PUK");
        param6.setValue(puk);
        request.getData().getParam().add(param6);
        //TIGO_CASH_STATUS
        Request.Data.Param param7 = this.objectFactory.createRequestDataParam();
        param7.setName("TIGO_CASH_STATUS");
        param7.setValue(tigoCashStatus);
        request.getData().getParam().add(param7);
        //GENDER
        Request.Data.Param param8 = this.objectFactory.createRequestDataParam();
        param8.setName("GENDER");
        param8.setValue(gender);
        request.getData().getParam().add(param8);
        //ALT_CONTACT_NUM
        Request.Data.Param param9 = this.objectFactory.createRequestDataParam();
        param9.setName("ALT_CONTACT_NUM");
        param9.setValue(altContactNum);
        request.getData().getParam().add(param9);
        //DISTRICT
        Request.Data.Param param10 = this.objectFactory.createRequestDataParam();
        param10.setName("DISTRICT");
        param10.setValue(district);
        request.getData().getParam().add(param10);
        //DOCUMENT_REFERENCE_TYPE
        Request.Data.Param param11 = this.objectFactory.createRequestDataParam();
        param11.setName("DOCUMENT_REFERENCE_TYPE");
        param11.setValue(documentReferenceType);
        request.getData().getParam().add(param11);
        //PROOF_NUMBER
        Request.Data.Param param12 = this.objectFactory.createRequestDataParam();
        param12.setName("PROOF_NUMBER");
        param12.setValue(proofNumber);
        request.getData().getParam().add(param12);
        //CREATE_USER
        Request.Data.Param param13 = this.objectFactory.createRequestDataParam();
        createUser = this.createUSer;
        param13.setName("CREATE_USER");
        param13.setValue(createUser);
        request.getData().getParam().add(param13);
        //CREATE_DATE
        Request.Data.Param param14 = this.objectFactory.createRequestDataParam();
        createDate = new SimpleDateFormat("ddMMyyyy").format(new Date());
        param14.setName("CREATE_DATE");
        param14.setValue(createDate);
        request.getData().getParam().add(param14);
        log.info("Rest request to engrafi : " + jaxbObjectToXML(request));
        RestTemplate restTemplate = new RestTemplate();
        Acknowledgment response = restTemplate.postForObject(this.host+this.endpoint, request, Acknowledgment.class);
        log.info("Rest response from engrafi : " + jaxbObjectToXML(response));
        return response.getStatus().getCode();
    }

    private String jaxbObjectToXML(Request request)
    {
        try
        {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(Request.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(request, sw);

            //Verify XML Content
            String xmlContent = sw.toString();
            return xmlContent;

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String jaxbObjectToXML(Acknowledgment acknowledgment)
    {
        try
        {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(Acknowledgment.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(acknowledgment, sw);

            //Verify XML Content
            String xmlContent = sw.toString();
            return xmlContent;

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }
}
