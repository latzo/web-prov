package sn.sentel.tigocare.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.service.APNChangeService;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.APNChangeDtoVTalend;
import sn.sentel.tigocare.service.dto.AffichageInformationsAbonnesDTO;
import sn.sentel.tigocare.service.dto.SimSwapDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.APNChange}.
 */
@RestController
@RequestMapping("/api")
public class APNChangeResource {

    private static final String ENTITY_NAME = "aPNChange";
    private final Logger log = LoggerFactory.getLogger(APNChangeResource.class);
    private final APNChangeService aPNChangeService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public APNChangeResource(APNChangeService aPNChangeService) {
        this.aPNChangeService = aPNChangeService;
    }

    /**
     * {@code POST  /apn-changes} : Create a new aPNChange.
     *
     * @param aPNChangeDTO the aPNChangeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aPNChangeDTO, or with status {@code 400 (Bad Request)} if the aPNChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/apn-changes")
    public ResponseEntity<APNChangeDTO> createAPNChange(@Valid @RequestBody APNChangeDTO aPNChangeDTO) throws URISyntaxException {
        this.log.debug("REST request to save APNChange : {}", aPNChangeDTO);
        if (aPNChangeDTO.getId() != null) {
            throw new BadRequestAlertException("A new aPNChange cannot already have an ID", ENTITY_NAME, "idexists");
        }
        aPNChangeDTO.setMsisdn("221"+aPNChangeDTO.getMsisdn());
        aPNChangeDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        APNChangeDTO result = this.aPNChangeService.save(aPNChangeDTO);
        return ResponseEntity.created(new URI("/api/apn-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /apn-changes} : Updates an existing aPNChange.
     *
     * @param aPNChangeDTO the aPNChangeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aPNChangeDTO,
     * or with status {@code 400 (Bad Request)} if the aPNChangeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aPNChangeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/apn-changes")
    public ResponseEntity<APNChangeDTO> updateAPNChange(@Valid @RequestBody APNChangeDTO aPNChangeDTO) throws URISyntaxException {
        this.log.debug("REST request to update APNChange : {}", aPNChangeDTO);
        if (aPNChangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        APNChangeDTO result = this.aPNChangeService.save(aPNChangeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(this.applicationName, false, ENTITY_NAME, aPNChangeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /apn-changes} : get all the aPNChanges.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aPNChanges in body.
     */
    @GetMapping("/apn-changes")
    public ResponseEntity<List<APNChangeDTO>> getAllAPNChanges(Pageable pageable) {
        this.log.debug("REST request to get all APNChanges");
        Page<APNChangeDTO> page = this.aPNChangeService.findAll(pageable);
        return ResponseEntity.ok().body(page.getContent());
    }

    @GetMapping("/apn-changes/bulks/{transactionId}")
    public ResponseEntity<List<APNChangeDTO>> getAllApnChangesOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<APNChangeDTO> page = this.aPNChangeService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /apn-changes/:id} : get the "id" aPNChange.
     *
     * @param id the id of the aPNChangeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aPNChangeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/apn-changes/{id}")
    public ResponseEntity<APNChangeDTO> getAPNChange(@PathVariable Long id) {
        this.log.debug("REST request to get APNChange : {}", id);
        Optional<APNChangeDTO> aPNChangeDTO = this.aPNChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aPNChangeDTO);
    }

    /**
     * {@code DELETE  /apn-changes/:id} : delete the "id" aPNChange.
     *
     * @param id the id of the aPNChangeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/apn-changes/{id}")
    public ResponseEntity<Void> deleteAPNChange(@PathVariable Long id) {
        this.log.debug("REST request to delete APNChange : {}", id);
        this.aPNChangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(this.applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/apn-changes/upload")
    public ResponseEntity bulk(@RequestParam("msisdn_bulk") MultipartFile file,
                               @RequestParam(value = "operation_bulk", required = false, defaultValue = "ACTIVATE") String operation,
                               @RequestParam(value = "apn_bulk", required = false, defaultValue = "ACTIVATE") String apn,
                               @RequestParam(value = "profil_bulk", required = false, defaultValue = "ACTIVATE") String profil) throws IOException, URISyntaxException {

        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        OperationEnum op = OperationEnum.ACTIVATE;
        this.aPNChangeService.bulk(file.getInputStream(), apn, profil, op, txId);
        return ResponseEntity.created(new URI("/api/apn-changes/bulks/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }

    /**
     * {@code POST  /apn-changes} : Create a new aPNChange.
     *
     * @param aPNChangeDTO the aPNChangeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aPNChangeDTO, or with status {@code 400 (Bad Request)} if the aPNChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/apn-changes/vTalend")
    public ResponseEntity<APNChangeDTO> changeApnThroughTalend(@Valid @RequestBody APNChangeDtoVTalend aPNChangeDTO) throws URISyntaxException {

        this.log.debug("REST request to save APNChange : {}", aPNChangeDTO);
        APNChangeDTO result = this.aPNChangeService.saveApnThroughTalend(aPNChangeDTO);

        return ResponseEntity.created(new URI("/api/apn-changes/vTalend/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/apn-changes/bulk/vTalend")
    public ResponseEntity changeApnThroughTalend(@RequestParam("msisdn_bulk") MultipartFile file,
                               @Valid @RequestParam(value = "apn_bulk") String apn,
                               @Valid @RequestParam(value = "technology_bulk") String technology,
                               @Valid @RequestParam(value = "operation") String operation) throws IOException, URISyntaxException {

        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        this.aPNChangeService.saveApnThroughTalend(file.getInputStream(), apn, technology, operation);
        return ResponseEntity.created(new URI("/api/apn-changes/bulk/vTalend" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }
}
