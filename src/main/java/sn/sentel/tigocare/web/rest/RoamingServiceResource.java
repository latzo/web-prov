package sn.sentel.tigocare.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import sn.sentel.tigocare.service.RoamingServiceService;
import sn.sentel.tigocare.service.dto.RoamingServiceDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.RoamingService}.
 */
@RestController
@RequestMapping("/api")
public class RoamingServiceResource {

    private static final String ENTITY_NAME = "roamingService";
    private final Logger log = LoggerFactory.getLogger(RoamingServiceResource.class);
    private final RoamingServiceService roamingServiceService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public RoamingServiceResource(RoamingServiceService roamingServiceService) {
        this.roamingServiceService = roamingServiceService;
    }

    /**
     * {@code POST  /roaming-services} : Create a new roamingService.
     *
     * @param roamingServiceDTO the roamingServiceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new roamingServiceDTO, or with status {@code 400 (Bad Request)} if the roamingService has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/roaming-services")
    public ResponseEntity<RoamingServiceDTO> createRoamingService(@Valid @RequestBody RoamingServiceDTO roamingServiceDTO) throws URISyntaxException {
        this.log.debug("REST request to save RoamingServiceEnum : {}", roamingServiceDTO);
        if (roamingServiceDTO.getId() != null) {
            throw new BadRequestAlertException("A new roamingService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        roamingServiceDTO.setMsisdn("221"+roamingServiceDTO.getMsisdn());
        RoamingServiceDTO result = this.roamingServiceService.save(roamingServiceDTO);
        return ResponseEntity.created(new URI("/api/roaming-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /roaming-services} : Updates an existing roamingService.
     *
     * @param roamingServiceDTO the roamingServiceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roamingServiceDTO,
     * or with status {@code 400 (Bad Request)} if the roamingServiceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the roamingServiceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/roaming-services")
    public ResponseEntity<RoamingServiceDTO> updateRoamingService(@Valid @RequestBody RoamingServiceDTO roamingServiceDTO) throws URISyntaxException {
        this.log.debug("REST request to update RoamingServiceEnum : {}", roamingServiceDTO);
        if (roamingServiceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RoamingServiceDTO result = this.roamingServiceService.save(roamingServiceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(this.applicationName, false, ENTITY_NAME, roamingServiceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /roaming-services} : get all the roamingServices.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of roamingServices in body.
     */
    @GetMapping("/roaming-services")
    public ResponseEntity<List<RoamingServiceDTO>> getAllRoamingServices(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        this.log.debug("REST request to get a page of RoamingServices");
        Page<RoamingServiceDTO> page = this.roamingServiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /roaming-services/:id} : get the "id" roamingService.
     *
     * @param id the id of the roamingServiceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the roamingServiceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/roaming-services/{id}")
    public ResponseEntity<RoamingServiceDTO> getRoamingService(@PathVariable Long id) {
        this.log.debug("REST request to get RoamingServiceEnum : {}", id);
        Optional<RoamingServiceDTO> roamingServiceDTO = this.roamingServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roamingServiceDTO);
    }

    /**
     * {@code DELETE  /roaming-services/:id} : delete the "id" roamingService.
     *
     * @param id the id of the roamingServiceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/roaming-services/{id}")
    public ResponseEntity<Void> deleteRoamingService(@PathVariable Long id) {
        this.log.debug("REST request to delete RoamingServiceEnum : {}", id);
        this.roamingServiceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(this.applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/roaming-services/upload")
    public ResponseEntity bulk(@RequestParam("msisdn_bulk") MultipartFile file,
                               @RequestParam("service_bulk") String serviceBulk) throws URISyntaxException, IOException {
        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        this.roamingServiceService.bulk(file.getInputStream(), serviceBulk, txId);
        return ResponseEntity.created(new URI("/api/roaming-services/" ))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();

    }
}
