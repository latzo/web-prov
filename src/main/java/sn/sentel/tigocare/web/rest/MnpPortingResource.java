package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.service.MnpPortingService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.MnpPortingDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.MnpPorting}.
 */
@RestController
@RequestMapping("/api")
public class MnpPortingResource {

    private final Logger log = LoggerFactory.getLogger(MnpPortingResource.class);

    private static final String ENTITY_NAME = "mnpPorting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MnpPortingService mnpPortingService;

    public MnpPortingResource(MnpPortingService mnpPortingService) {
        this.mnpPortingService = mnpPortingService;
    }

    /**
     * {@code POST  /mnp-portings} : Create a new mnpPorting.
     *
     * @param mnpPortingDTO the mnpPortingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mnpPortingDTO, or with status {@code 400 (Bad Request)} if the mnpPorting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mnp-portings")
    public ResponseEntity<MnpPortingDTO> createMnpPorting(@RequestBody MnpPortingDTO mnpPortingDTO) throws URISyntaxException {
        log.debug("REST request to save MnpPorting : {}", mnpPortingDTO);
        if (mnpPortingDTO.getId() != null) {
            throw new BadRequestAlertException("A new mnpPorting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MnpPortingDTO result = mnpPortingService.save(mnpPortingDTO);
        return ResponseEntity.created(new URI("/api/mnp-portings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mnp-portings} : Updates an existing mnpPorting.
     *
     * @param mnpPortingDTO the mnpPortingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mnpPortingDTO,
     * or with status {@code 400 (Bad Request)} if the mnpPortingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mnpPortingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mnp-portings")
    public ResponseEntity<MnpPortingDTO> updateMnpPorting(@RequestBody MnpPortingDTO mnpPortingDTO) throws URISyntaxException {
        log.debug("REST request to update MnpPorting : {}", mnpPortingDTO);
        if (mnpPortingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MnpPortingDTO result = mnpPortingService.save(mnpPortingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, mnpPortingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mnp-portings} : get all the mnpPortings.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mnpPortings in body.
     */
    @GetMapping("/mnp-portings")
    public ResponseEntity<List<MnpPortingDTO>> getAllMnpPortings(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MnpPortings");
        Page<MnpPortingDTO> page = mnpPortingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /mnp-portings/:id} : get the "id" mnpPorting.
     *
     * @param id the id of the mnpPortingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mnpPortingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mnp-portings/{id}")
    public ResponseEntity<MnpPortingDTO> getMnpPorting(@PathVariable Long id) {
        log.debug("REST request to get MnpPorting : {}", id);
        Optional<MnpPortingDTO> mnpPortingDTO = mnpPortingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mnpPortingDTO);
    }

    /**
     * {@code DELETE  /mnp-portings/:id} : delete the "id" mnpPorting.
     *
     * @param id the id of the mnpPortingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mnp-portings/{id}")
    public ResponseEntity<Void> deleteMnpPorting(@PathVariable Long id) {
        log.debug("REST request to delete MnpPorting : {}", id);
        mnpPortingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
