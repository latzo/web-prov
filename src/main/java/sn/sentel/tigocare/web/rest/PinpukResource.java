package sn.sentel.tigocare.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import sn.sentel.tigocare.service.PinpukService;
import sn.sentel.tigocare.service.mapper.PinpukMapper;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.PinpukDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.Pinpuk}.
 */
@RestController
@RequestMapping("/api")
public class PinpukResource {

    private final Logger log = LoggerFactory.getLogger(PinpukResource.class);

    private static final String ENTITY_NAME = "pinpuk";

    private final PinpukMapper pinpukMapper;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PinpukService pinpukService;

    public PinpukResource(PinpukService pinpukService, PinpukMapper pinpukMapper) {
        this.pinpukService = pinpukService;
        this.pinpukMapper = pinpukMapper;
    }

    /**
     * {@code POST  /pinpuks} : Create a new pinpuk.
     *
     * @param pinpukDTO the pinpukDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pinpukDTO, or with status {@code 400 (Bad Request)} if the pinpuk has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pinpuks")
    public ResponseEntity<PinpukDTO> createPinpuk(@RequestBody PinpukDTO pinpukDTO) throws URISyntaxException {
        log.debug("REST request to save Pinpuk : {}", pinpukDTO);
        if (pinpukDTO.getId() != null) {
            throw new BadRequestAlertException("A new pinpuk cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PinpukDTO result = pinpukService.save(pinpukDTO);
        return ResponseEntity.created(new URI("/api/pinpuks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pinpuks} : Updates an existing pinpuk.
     *
     * @param pinpukDTO the pinpukDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pinpukDTO,
     * or with status {@code 400 (Bad Request)} if the pinpukDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pinpukDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pinpuks")
    public ResponseEntity<PinpukDTO> updatePinpuk(@RequestBody PinpukDTO pinpukDTO) throws URISyntaxException {
        log.debug("REST request to update Pinpuk : {}", pinpukDTO);
        if (pinpukDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PinpukDTO result = pinpukService.save(pinpukDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pinpukDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pinpuks} : get all the pinpuks.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pinpuks in body.
     */
    @GetMapping("/pinpuks")
    public ResponseEntity<List<PinpukDTO>> getAllPinpuks(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Pinpuks");
        Page<PinpukDTO> page = pinpukService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pinpuks/:id} : get the "id" pinpuk.
     *
     * @param id the id of the pinpukDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pinpukDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pinpuks/{id}")
    public ResponseEntity<PinpukDTO> getPinpuk(@PathVariable Long id) {
        log.debug("REST request to get Pinpuk : {}", id);
        Optional<PinpukDTO> pinpukDTO = pinpukService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pinpukDTO);
    }

    @GetMapping("/pinpuks/imsi/{imsi}")
    public ResponseEntity<PinpukDTO> getPinpukByMsisdn(@PathVariable String imsi) {
        log.debug("REST request to get Pinpuk : {}", imsi);
        Optional<PinpukDTO> pinpukDTO = pinpukService.findFirstByImsi(imsi)
            .map(pinpukMapper::toDto);
        return ResponseUtil.wrapOrNotFound(pinpukDTO);
    }

    /**
     * {@code DELETE  /pinpuks/:id} : delete the "id" pinpuk.
     *
     * @param id the id of the pinpukDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pinpuks/{id}")
    public ResponseEntity<Void> deletePinpuk(@PathVariable Long id) {
        log.debug("REST request to delete Pinpuk : {}", id);
        pinpukService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
