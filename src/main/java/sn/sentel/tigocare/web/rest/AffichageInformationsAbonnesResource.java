package sn.sentel.tigocare.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import sn.sentel.tigocare.hlr.services.LoginCallBack;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.jaxb2.subscriberinfo.CreateResponse;
import sn.sentel.tigocare.jaxb2.subscriberinfo.GetResponse;
import sn.sentel.tigocare.service.AffichageInformationsAbonnesService;
import sn.sentel.tigocare.service.dto.AffichageInformationsAbonnesDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.AffichageInformationsAbonnes}.
 */
@RestController
@RequestMapping("/api")
public class AffichageInformationsAbonnesResource {

    private static final String ENTITY_NAME = "affichageInformationsAbonnes";
    private final Logger log = LoggerFactory.getLogger(AffichageInformationsAbonnesResource.class);
    private final AffichageInformationsAbonnesService affichageInformationsAbonnesService;
    private final SubscriberInfoService subscriberInfoService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public AffichageInformationsAbonnesResource(AffichageInformationsAbonnesService affichageInformationsAbonnesService, SubscriberInfoService subscriberInfoService) {
        this.affichageInformationsAbonnesService = affichageInformationsAbonnesService;
        this.subscriberInfoService = subscriberInfoService;
    }


    @GetMapping("/affichage-informations-abonnes/xml/{msisdn}")
    public String getAffichageInformationsAbonnes(@PathVariable String msisdn) {
        this.log.debug("REST request to get AffichageInformationsAbonnes XML : {}", msisdn);
        GetResponse subscriberInfo = this.subscriberInfoService.getSubscriberInfo(msisdn);
        String response = "";
        /* init jaxb marshaler */
        try{
            JAXBContext jaxbContext = JAXBContext.newInstance( GetResponse.class );
            javax.xml.bind.Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true );
            StringWriter writer = new StringWriter();
            jaxbMarshaller.marshal( subscriberInfo,writer);
            response = writer.toString();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    @GetMapping("/affichage-informations-abonnes/subscriberinfo/{imsi}")
    public String getInformationsAbonnes(@PathVariable String imsi) {
        this.log.debug("REST request to get AffichageInformationsAbonnes CBS : {}", imsi);
        GetResponse subscriberInfo = this.subscriberInfoService.getSubscriberInfoImsi(imsi);
        String jsonString = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonString = mapper.writeValueAsString(subscriberInfo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    @GetMapping("/affichage-informations-abonnes/subscriberinfohss/{imsi}")
    public String getInformationsAbonnesHSS(@PathVariable String imsi) {
        this.log.debug("REST request to get AffichageInformationsAbonnes HSSS : {}", imsi);
        sn.sentel.tigocare.jaxb2.hss.GetResponse subscriberInfo = this.subscriberInfoService.getSubscriberInfoHss(imsi);
        String jsonString = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonString = mapper.writeValueAsString(subscriberInfo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

//    @GetMapping("/affichage-informations-abonnes/servicestates/{msisdn}")
//    public String getAffichageServicesStates(@PathVariable String msisdn) {
//        ObjectMapper mapper = new ObjectMapper();
//        this.log.debug("REST request to get AffichageInformationsAbonnes XML : {}", msisdn);
//        GetResponse subscriberInfo = this.subscriberInfoService.getSubscriberInfo(msisdn);
//
//    }

    /**
     * {@code POST  /affichage-informations-abonnes} : Create a new affichageInformationsAbonnes.
     *
     * @param affichageInformationsAbonnesDTO the affichageInformationsAbonnesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new affichageInformationsAbonnesDTO, or with status {@code 400 (Bad Request)} if the affichageInformationsAbonnes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/affichage-informations-abonnes")
    public ResponseEntity<AffichageInformationsAbonnesDTO> createAffichageInformationsAbonnes(@RequestBody AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO) throws URISyntaxException {
        this.log.debug("REST request to save AffichageInformationsAbonnes : {}", affichageInformationsAbonnesDTO);
        if (affichageInformationsAbonnesDTO.getId() != null) {
            throw new BadRequestAlertException("A new affichageInformationsAbonnes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AffichageInformationsAbonnesDTO result = this.affichageInformationsAbonnesService.save(affichageInformationsAbonnesDTO);
        return ResponseEntity.created(new URI("/api/affichage-informations-abonnes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /affichage-informations-abonnes} : Updates an existing affichageInformationsAbonnes.
     *
     * @param affichageInformationsAbonnesDTO the affichageInformationsAbonnesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated affichageInformationsAbonnesDTO,
     * or with status {@code 400 (Bad Request)} if the affichageInformationsAbonnesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the affichageInformationsAbonnesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/affichage-informations-abonnes")
    public ResponseEntity<AffichageInformationsAbonnesDTO> updateAffichageInformationsAbonnes(@RequestBody AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO) throws URISyntaxException {
        this.log.debug("REST request to update AffichageInformationsAbonnes : {}", affichageInformationsAbonnesDTO);
        if (affichageInformationsAbonnesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AffichageInformationsAbonnesDTO result = this.affichageInformationsAbonnesService.save(affichageInformationsAbonnesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(this.applicationName, false, ENTITY_NAME, affichageInformationsAbonnesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /affichage-informations-abonnes} : get all the affichageInformationsAbonnes.
     *
     * @param pageable    the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder  a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of affichageInformationsAbonnes in body.
     */
    @GetMapping("/affichage-informations-abonnes")
    public ResponseEntity<List<AffichageInformationsAbonnesDTO>> getAllAffichageInformationsAbonnes(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        this.log.debug("REST request to get a page of AffichageInformationsAbonnes");
        Page<AffichageInformationsAbonnesDTO> page = this.affichageInformationsAbonnesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /affichage-informations-abonnes/:id} : get the "id" affichageInformationsAbonnes.
     *
     * @param id the id of the affichageInformationsAbonnesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the affichageInformationsAbonnesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/affichage-informations-abonnes/{id}")
    public ResponseEntity<AffichageInformationsAbonnesDTO> getAffichageInformationsAbonnes(@PathVariable Long id) {
        this.log.debug("REST request to get AffichageInformationsAbonnes : {}", id);
        Optional<AffichageInformationsAbonnesDTO> affichageInformationsAbonnesDTO = this.affichageInformationsAbonnesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(affichageInformationsAbonnesDTO);
    }

    /**
     * {@code DELETE  /affichage-informations-abonnes/:id} : delete the "id" affichageInformationsAbonnes.
     *
     * @param id the id of the affichageInformationsAbonnesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/affichage-informations-abonnes/{id}")
    public ResponseEntity<Void> deleteAffichageInformationsAbonnes(@PathVariable Long id) {
        this.log.debug("REST request to delete AffichageInformationsAbonnes : {}", id);
        this.affichageInformationsAbonnesService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(this.applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
