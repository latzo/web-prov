package sn.sentel.tigocare.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.sentel.tigocare.hlr.services.msa.ArchivageService;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api")
public class ArchivageResource {
    private final Logger log = LoggerFactory.getLogger(ArchivageResource.class);

    private final ArchivageService archivageService;

    public ArchivageResource(ArchivageService archivageService) {
        this.archivageService = archivageService;
    }

    /**
     * {@code GET  /archivages/:msisdn} : .
     *
     * @param msisdn the id of the createSubscriberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body , or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/archivages/{msisdn}")
    public ResponseEntity<Void> archive(@PathVariable String msisdn) {
        log.debug("REST request archive msisdn : {}", msisdn);
     return   archivageService.archiver((new java.text.SimpleDateFormat("yyyyMMddHHmmss")).format(new Date())+(int)(Math.random()*1000),"engrafi.middleware.delete.subscriber",(new java.text.SimpleDateFormat("yyyyMMddHHmmss")).format(new Date()),"1","5",msisdn,"NMS","TERTIO-ARCHIVING");

    }

}
