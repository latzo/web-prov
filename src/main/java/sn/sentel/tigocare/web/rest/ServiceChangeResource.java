package sn.sentel.tigocare.web.rest;

import org.springframework.web.multipart.MultipartFile;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.service.ServiceChangeService;
import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.ServiceChangeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.ServiceChange}.
 */
@RestController
@RequestMapping("/api")
public class ServiceChangeResource {

    private final Logger log = LoggerFactory.getLogger(ServiceChangeResource.class);

    private static final String ENTITY_NAME = "serviceChange";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ServiceChangeService serviceChangeService;

    public ServiceChangeResource(ServiceChangeService serviceChangeService) {
        this.serviceChangeService = serviceChangeService;
    }

    /**
     * {@code POST  /service-changes} : Create a new serviceChange.
     *
     * @param serviceChangeDTO the serviceChangeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new serviceChangeDTO, or with status {@code 400 (Bad Request)} if the serviceChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/service-changes")
    public ResponseEntity<ServiceChangeDTO> createServiceChange(@Valid @RequestBody ServiceChangeDTO serviceChangeDTO) throws URISyntaxException {
        log.debug("REST request to save ServiceChange : {}", serviceChangeDTO);
        if (serviceChangeDTO.getId() != null) {
            throw new BadRequestAlertException("A new serviceChange cannot already have an ID", ENTITY_NAME, "idexists");
        }
        serviceChangeDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        ServiceChangeDTO result = serviceChangeService.save(serviceChangeDTO);
        return ResponseEntity.created(new URI("/api/service-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /service-changes} : Updates an existing serviceChange.
     *
     * @param serviceChangeDTO the serviceChangeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceChangeDTO,
     * or with status {@code 400 (Bad Request)} if the serviceChangeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the serviceChangeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/service-changes")
    public ResponseEntity<ServiceChangeDTO> updateServiceChange(@Valid @RequestBody ServiceChangeDTO serviceChangeDTO) throws URISyntaxException {
        log.debug("REST request to update ServiceChange : {}", serviceChangeDTO);
        if (serviceChangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ServiceChangeDTO result = serviceChangeService.save(serviceChangeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceChangeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /service-changes} : get all the serviceChanges.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of serviceChanges in body.
     */
    @GetMapping("/service-changes")
    public ResponseEntity<List<ServiceChangeDTO>> getAllServiceChanges(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ServiceChanges");
        Page<ServiceChangeDTO> page = serviceChangeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/service-changes/bulks/{transactionId}")
    public ResponseEntity<List<ServiceChangeDTO>> getAllServiceChangesOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<ServiceChangeDTO> page = this.serviceChangeService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /service-changes/:id} : get the "id" serviceChange.
     *
     * @param id the id of the serviceChangeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the serviceChangeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/service-changes/{id}")
    public ResponseEntity<ServiceChangeDTO> getServiceChange(@PathVariable Long id) {
        log.debug("REST request to get ServiceChange : {}", id);
        Optional<ServiceChangeDTO> serviceChangeDTO = serviceChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(serviceChangeDTO);
    }

    /**
     * {@code DELETE  /service-changes/:id} : delete the "id" serviceChange.
     *
     * @param id the id of the serviceChangeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/service-changes/{id}")
    public ResponseEntity<Void> deleteServiceChange(@PathVariable Long id) {
        log.debug("REST request to delete ServiceChange : {}", id);
        serviceChangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/service-changes/upload")
    public ResponseEntity bulkSimSwap(@RequestParam("file") MultipartFile file, @RequestParam("operation") String operation, @RequestParam("serviceId") String serviceId) throws IOException, URISyntaxException {
        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        serviceChangeService.bulk(file.getInputStream(), txId, OperationEnum.valueOf(operation), serviceId);
        return ResponseEntity.created(new URI("/api/service-changes/transaction/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }

}
