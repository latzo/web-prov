package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.service.MnpPortoutService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.MnpPortoutDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.MnpPortout}.
 */
@RestController
@RequestMapping("/api")
public class MnpPortoutResource {

    private final Logger log = LoggerFactory.getLogger(MnpPortoutResource.class);

    private static final String ENTITY_NAME = "mnpPortout";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MnpPortoutService mnpPortoutService;

    public MnpPortoutResource(MnpPortoutService mnpPortoutService) {
        this.mnpPortoutService = mnpPortoutService;
    }

    /**
     * {@code POST  /mnp-portouts} : Create a new mnpPortout.
     *
     * @param mnpPortoutDTO the mnpPortoutDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mnpPortoutDTO, or with status {@code 400 (Bad Request)} if the mnpPortout has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mnp-portouts")
    public ResponseEntity<MnpPortoutDTO> createMnpPortout(@RequestBody MnpPortoutDTO mnpPortoutDTO) throws URISyntaxException {
        log.debug("REST request to save MnpPortout : {}", mnpPortoutDTO);
        if (mnpPortoutDTO.getId() != null) {
            throw new BadRequestAlertException("A new mnpPortout cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MnpPortoutDTO result = mnpPortoutService.save(mnpPortoutDTO);
        return ResponseEntity.created(new URI("/api/mnp-portouts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mnp-portouts} : Updates an existing mnpPortout.
     *
     * @param mnpPortoutDTO the mnpPortoutDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mnpPortoutDTO,
     * or with status {@code 400 (Bad Request)} if the mnpPortoutDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mnpPortoutDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mnp-portouts")
    public ResponseEntity<MnpPortoutDTO> updateMnpPortout(@RequestBody MnpPortoutDTO mnpPortoutDTO) throws URISyntaxException {
        log.debug("REST request to update MnpPortout : {}", mnpPortoutDTO);
        if (mnpPortoutDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MnpPortoutDTO result = mnpPortoutService.save(mnpPortoutDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, mnpPortoutDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mnp-portouts} : get all the mnpPortouts.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mnpPortouts in body.
     */
    @GetMapping("/mnp-portouts")
    public ResponseEntity<List<MnpPortoutDTO>> getAllMnpPortouts(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MnpPortouts");
        Page<MnpPortoutDTO> page = mnpPortoutService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /mnp-portouts/:id} : get the "id" mnpPortout.
     *
     * @param id the id of the mnpPortoutDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mnpPortoutDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mnp-portouts/{id}")
    public ResponseEntity<MnpPortoutDTO> getMnpPortout(@PathVariable Long id) {
        log.debug("REST request to get MnpPortout : {}", id);
        Optional<MnpPortoutDTO> mnpPortoutDTO = mnpPortoutService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mnpPortoutDTO);
    }

    /**
     * {@code DELETE  /mnp-portouts/:id} : delete the "id" mnpPortout.
     *
     * @param id the id of the mnpPortoutDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mnp-portouts/{id}")
    public ResponseEntity<Void> deleteMnpPortout(@PathVariable Long id) {
        log.debug("REST request to delete MnpPortout : {}", id);
        mnpPortoutService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
