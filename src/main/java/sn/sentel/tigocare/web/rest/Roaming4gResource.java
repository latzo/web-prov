package sn.sentel.tigocare.web.rest;

import org.springframework.web.multipart.MultipartFile;
import sn.sentel.tigocare.service.Roaming4gService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.Roaming4gDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.Roaming4g}.
 */
@RestController
@RequestMapping("/api")
public class Roaming4gResource {

    private final Logger log = LoggerFactory.getLogger(Roaming4gResource.class);

    private static final String ENTITY_NAME = "roaming4g";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final Roaming4gService roaming4gService;

    public Roaming4gResource(Roaming4gService roaming4gService) {
        this.roaming4gService = roaming4gService;
    }

    /**
     * {@code POST  /roaming-4-gs} : Create a new roaming4g.
     *
     * @param roaming4gDTO the roaming4gDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new roaming4gDTO, or with status {@code 400 (Bad Request)} if the roaming4g has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/roaming-4-gs")
    public ResponseEntity<Roaming4gDTO> createRoaming4g(@Valid @RequestBody Roaming4gDTO roaming4gDTO) throws URISyntaxException {
        log.debug("REST request to save Roaming4g : {}", roaming4gDTO);
        if (roaming4gDTO.getId() != null) {
            throw new BadRequestAlertException("A new roaming4g cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Roaming4gDTO result = roaming4gService.save(roaming4gDTO);
        return ResponseEntity.created(new URI("/api/roaming-4-gs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /roaming-4-gs} : Updates an existing roaming4g.
     *
     * @param roaming4gDTO the roaming4gDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated roaming4gDTO,
     * or with status {@code 400 (Bad Request)} if the roaming4gDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the roaming4gDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/roaming-4-gs")
    public ResponseEntity<Roaming4gDTO> updateRoaming4g(@Valid @RequestBody Roaming4gDTO roaming4gDTO) throws URISyntaxException {
        log.debug("REST request to update Roaming4g : {}", roaming4gDTO);
        if (roaming4gDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Roaming4gDTO result = roaming4gService.save(roaming4gDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, roaming4gDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /roaming-4-gs} : get all the roaming4gs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of roaming4gs in body.
     */
    @GetMapping("/roaming-4-gs")
    public ResponseEntity<List<Roaming4gDTO>> getAllRoaming4gs(Pageable pageable) {
        log.debug("REST request to get a page of Roaming4gs");
        Page<Roaming4gDTO> page = roaming4gService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /roaming-4-gs/:id} : get the "id" roaming4g.
     *
     * @param id the id of the roaming4gDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the roaming4gDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/roaming-4-gs/{id}")
    public ResponseEntity<Roaming4gDTO> getRoaming4g(@PathVariable Long id) {
        log.debug("REST request to get Roaming4g : {}", id);
        Optional<Roaming4gDTO> roaming4gDTO = roaming4gService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roaming4gDTO);
    }

    /**
     * {@code DELETE  /roaming-4-gs/:id} : delete the "id" roaming4g.
     *
     * @param id the id of the roaming4gDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/roaming-4-gs/{id}")
    public ResponseEntity<Void> deleteRoaming4g(@PathVariable Long id) {
        log.debug("REST request to delete Roaming4g : {}", id);
        roaming4gService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/roaming-4-gs/upload")
    public ResponseEntity bulk(@RequestParam("imsi_bulk") MultipartFile file,
                               @RequestParam("service_bulk") String serviceBulk) throws URISyntaxException, IOException {
        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        this.roaming4gService.bulk(file.getInputStream(), serviceBulk, txId);
        return ResponseEntity.created(new URI("/api/roaming-services/" ))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }
}
