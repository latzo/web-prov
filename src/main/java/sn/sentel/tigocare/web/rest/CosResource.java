package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.service.CosService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.CosDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.Cos}.
 */
@RestController
@RequestMapping("/api")
public class CosResource {

    private final Logger log = LoggerFactory.getLogger(CosResource.class);

    private static final String ENTITY_NAME = "cos";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CosService cosService;

    public CosResource(CosService cosService) {
        this.cosService = cosService;
    }

    /**
     * {@code POST  /cos} : Create a new cos.
     *
     * @param cosDTO the cosDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cosDTO, or with status {@code 400 (Bad Request)} if the cos has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cos")
    public ResponseEntity<CosDTO> createCos(@RequestBody CosDTO cosDTO) throws URISyntaxException {
        log.debug("REST request to save Cos : {}", cosDTO);
        if (cosDTO.getId() != null) {
            throw new BadRequestAlertException("A new cos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CosDTO result = cosService.save(cosDTO);
        return ResponseEntity.created(new URI("/api/cos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cos} : Updates an existing cos.
     *
     * @param cosDTO the cosDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cosDTO,
     * or with status {@code 400 (Bad Request)} if the cosDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cosDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cos")
    public ResponseEntity<CosDTO> updateCos(@RequestBody CosDTO cosDTO) throws URISyntaxException {
        log.debug("REST request to update Cos : {}", cosDTO);
        if (cosDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CosDTO result = cosService.save(cosDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cosDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cos} : get all the cos.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cos in body.
     */
    @GetMapping("/cos")
    public ResponseEntity<List<CosDTO>> getAllCos(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Cos");
        Page<CosDTO> page = cosService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cos/:id} : get the "id" cos.
     *
     * @param id the id of the cosDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cosDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cos/{id}")
    public ResponseEntity<CosDTO> getCos(@PathVariable Long id) {
        log.debug("REST request to get Cos : {}", id);
        Optional<CosDTO> cosDTO = cosService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cosDTO);
    }

    /**
     * {@code DELETE  /cos/:id} : delete the "id" cos.
     *
     * @param id the id of the cosDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cos/{id}")
    public ResponseEntity<Void> deleteCos(@PathVariable Long id) {
        log.debug("REST request to delete Cos : {}", id);
        cosService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
