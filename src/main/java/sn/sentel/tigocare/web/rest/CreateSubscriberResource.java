package sn.sentel.tigocare.web.rest;

import org.springframework.web.multipart.MultipartFile;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
import sn.sentel.tigocare.service.CreateSubscriberService;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.CreateSubscriber}.
 */
@RestController
@RequestMapping("/api")
public class CreateSubscriberResource {

    private final Logger log = LoggerFactory.getLogger(CreateSubscriberResource.class);

    private static final String ENTITY_NAME = "createSubscriber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CreateSubscriberService createSubscriberService;

    public CreateSubscriberResource(CreateSubscriberService createSubscriberService) {
        this.createSubscriberService = createSubscriberService;
    }

    /**
     * {@code POST  /create-subscribers} : Create a new createSubscriber.
     *
     * @param createSubscriberDTO the createSubscriberDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new createSubscriberDTO, or with status {@code 400 (Bad Request)} if the createSubscriber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/create-subscribers")
    public ResponseEntity<CreateSubscriberDTO> createCreateSubscriber(@Valid @RequestBody CreateSubscriberDTO createSubscriberDTO) throws URISyntaxException {
        log.debug("REST request to save CreateSubscriber : {}", createSubscriberDTO);
        if (createSubscriberDTO.getId() != null) {
            throw new BadRequestAlertException("A new createSubscriber cannot already have an ID", ENTITY_NAME, "idexists");
        }
        createSubscriberDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        CreateSubscriberDTO result = createSubscriberService.save(createSubscriberDTO);
        return ResponseEntity.created(new URI("/api/create-subscribers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PostMapping("/create-subscribers/sync")
    public ResponseEntity<List<CreateSubscriberDTO>> createCreateSubscriberSync(@Valid @RequestBody List<CreateSubscriberDTO> createSubscriberDTOList) throws URISyntaxException {
        this.log.debug("REST request to fullportin subscriber list : {}", createSubscriberDTOList);
        if (createSubscriberDTOList.size() == 0) {
            throw new BadRequestAlertException("A new createSubscriber cannot be emty list", ENTITY_NAME, "empty list");
        }
        List<CreateSubscriberDTO> result = this.createSubscriberService.fullPortinListOfSubscribers(createSubscriberDTOList);

        return ResponseEntity
            .ok(result);
    }

    /**
     * {@code PUT  /create-subscribers} : Updates an existing createSubscriber.
     *
     * @param createSubscriberDTO the createSubscriberDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated createSubscriberDTO,
     * or with status {@code 400 (Bad Request)} if the createSubscriberDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the createSubscriberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/create-subscribers")
    public ResponseEntity<CreateSubscriberDTO> updateCreateSubscriber(@Valid @RequestBody CreateSubscriberDTO createSubscriberDTO) throws URISyntaxException {
        log.debug("REST request to update CreateSubscriber : {}", createSubscriberDTO);
        if (createSubscriberDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CreateSubscriberDTO result = createSubscriberService.save(createSubscriberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, createSubscriberDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /create-subscribers} : get all the createSubscribers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of createSubscribers in body.
     */
    @GetMapping("/create-subscribers")
    public ResponseEntity<List<CreateSubscriberDTO>> getAllCreateSubscribers(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of CreateSubscribers");
        Page<CreateSubscriberDTO> page = createSubscriberService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/create-subscribers/full-portin")
    public ResponseEntity<List<CreateSubscriberDTO>> getAllCreateSubscribersFullPortin(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of CreateSubscribers");
        Page<CreateSubscriberDTO> page = createSubscriberService.findAllFullPortin(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/create-subscribers/bulks/{transactionId}")
    public ResponseEntity<List<CreateSubscriberDTO>> getAllCreateSubscribersOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<CreateSubscriberDTO> page = this.createSubscriberService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /create-subscribers/:id} : get the "id" createSubscriber.
     *
     * @param id the id of the createSubscriberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the createSubscriberDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/create-subscribers/{id}")
    public ResponseEntity<CreateSubscriberDTO> getCreateSubscriber(@PathVariable Long id) {
        log.debug("REST request to get CreateSubscriber : {}", id);
        Optional<CreateSubscriberDTO> createSubscriberDTO = createSubscriberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(createSubscriberDTO);
    }

    /**
     * {@code DELETE  /create-subscribers/:id} : delete the "id" createSubscriber.
     *
     * @param id the id of the createSubscriberDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/create-subscribers/{id}")
    public ResponseEntity<Void> deleteCreateSubscriber(@PathVariable Long id) {
        log.debug("REST request to delete CreateSubscriber : {}", id);
        createSubscriberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/create-subscribers/upload")
    public ResponseEntity bulk(@RequestParam("file") MultipartFile file, @RequestParam("creationType") SIMCreationType creationType,
                               @RequestParam("statut") String statut,  @RequestParam("cos") String cos) throws IOException, URISyntaxException {
        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        createSubscriberService.bulk(file.getInputStream(), creationType, statut, cos, txId);
        return ResponseEntity.created(new URI("/api/create-subscribers/bulks/transaction/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }

    @GetMapping("/create-subs-lte/{msisdn}/{imsi}")
    public ResponseEntity<String> createLTE(@PathVariable("msisdn") String msisdn, @PathVariable("imsi") String imsi){
        String res = this.createSubscriberService.createLTE(msisdn, imsi);
        if(res == null){
            return ResponseEntity.ok("success");
        }else{
            return ResponseEntity.unprocessableEntity().body("FAILED - "+ res);
        }
    }
}
