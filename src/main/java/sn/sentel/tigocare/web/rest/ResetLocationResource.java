package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.service.ResetLocationService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.ResetLocationDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.ResetLocation}.
 */
@RestController
@RequestMapping("/api")
public class ResetLocationResource {

    private final Logger log = LoggerFactory.getLogger(ResetLocationResource.class);

    private static final String ENTITY_NAME = "resetLocation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResetLocationService resetLocationService;

    public ResetLocationResource(ResetLocationService resetLocationService) {
        this.resetLocationService = resetLocationService;
    }

    /**
     * {@code POST  /reset-locations} : Create a new resetLocation.
     *
     * @param resetLocationDTO the resetLocationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new resetLocationDTO, or with status {@code 400 (Bad Request)} if the resetLocation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/reset-locations")
    public ResponseEntity<ResetLocationDTO> createResetLocation(@Valid @RequestBody ResetLocationDTO resetLocationDTO) throws URISyntaxException {
        log.debug("REST request to save ResetLocation : {}", resetLocationDTO);
        if (resetLocationDTO.getId() != null) {
            throw new BadRequestAlertException("A new resetLocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResetLocationDTO result = resetLocationService.save(resetLocationDTO);
        return ResponseEntity.created(new URI("/api/reset-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /reset-locations} : Updates an existing resetLocation.
     *
     * @param resetLocationDTO the resetLocationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated resetLocationDTO,
     * or with status {@code 400 (Bad Request)} if the resetLocationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the resetLocationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/reset-locations")
    public ResponseEntity<ResetLocationDTO> updateResetLocation(@Valid @RequestBody ResetLocationDTO resetLocationDTO) throws URISyntaxException {
        log.debug("REST request to update ResetLocation : {}", resetLocationDTO);
        if (resetLocationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResetLocationDTO result = resetLocationService.save(resetLocationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, resetLocationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /reset-locations} : get all the resetLocations.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of resetLocations in body.
     */
    @GetMapping("/reset-locations")
    public ResponseEntity<List<ResetLocationDTO>> getAllResetLocations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ResetLocations");
        Page<ResetLocationDTO> page = resetLocationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /reset-locations/:id} : get the "id" resetLocation.
     *
     * @param id the id of the resetLocationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the resetLocationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/reset-locations/{id}")
    public ResponseEntity<ResetLocationDTO> getResetLocation(@PathVariable Long id) {
        log.debug("REST request to get ResetLocation : {}", id);
        Optional<ResetLocationDTO> resetLocationDTO = resetLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(resetLocationDTO);
    }

    /**
     * {@code DELETE  /reset-locations/:id} : delete the "id" resetLocation.
     *
     * @param id the id of the resetLocationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/reset-locations/{id}")
    public ResponseEntity<Void> deleteResetLocation(@PathVariable Long id) {
        log.debug("REST request to delete ResetLocation : {}", id);
        resetLocationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
