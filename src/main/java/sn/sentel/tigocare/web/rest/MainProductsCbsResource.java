package sn.sentel.tigocare.web.rest;

import org.springframework.data.domain.PageRequest;
import sn.sentel.tigocare.service.MainProductsCbsService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.MainProductsCbsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.MainProductsCbs}.
 */
@RestController
@RequestMapping("/api")
public class MainProductsCbsResource {

    private final Logger log = LoggerFactory.getLogger(MainProductsCbsResource.class);

    private static final String ENTITY_NAME = "mainProductsCbs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MainProductsCbsService mainProductsCbsService;

    public MainProductsCbsResource(MainProductsCbsService mainProductsCbsService) {
        this.mainProductsCbsService = mainProductsCbsService;
    }

    /**
     * {@code POST  /main-products-cbs} : Create a new mainProductsCbs.
     *
     * @param mainProductsCbsDTO the mainProductsCbsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mainProductsCbsDTO, or with status {@code 400 (Bad Request)} if the mainProductsCbs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/main-products-cbs")
    public ResponseEntity<MainProductsCbsDTO> createMainProductsCbs(@RequestBody MainProductsCbsDTO mainProductsCbsDTO) throws URISyntaxException {
        log.debug("REST request to save MainProductsCbs : {}", mainProductsCbsDTO);
        if (mainProductsCbsDTO.getId() != null) {
            throw new BadRequestAlertException("A new mainProductsCbs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MainProductsCbsDTO result = mainProductsCbsService.save(mainProductsCbsDTO);
        return ResponseEntity.created(new URI("/api/main-products-cbs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /main-products-cbs} : Updates an existing mainProductsCbs.
     *
     * @param mainProductsCbsDTO the mainProductsCbsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mainProductsCbsDTO,
     * or with status {@code 400 (Bad Request)} if the mainProductsCbsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mainProductsCbsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/main-products-cbs")
    public ResponseEntity<MainProductsCbsDTO> updateMainProductsCbs(@RequestBody MainProductsCbsDTO mainProductsCbsDTO) throws URISyntaxException {
        log.debug("REST request to update MainProductsCbs : {}", mainProductsCbsDTO);
        if (mainProductsCbsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MainProductsCbsDTO result = mainProductsCbsService.save(mainProductsCbsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, mainProductsCbsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /main-products-cbs} : get all the mainProductsCbs.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mainProductsCbs in body.
     */
    @GetMapping("/main-products-cbs")
    public ResponseEntity<List<MainProductsCbsDTO>> getAllMainProductsCbs(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        pageable = new PageRequest(0,Integer.MAX_VALUE);
        log.debug("REST request to get a page of MainProductsCbs");
        Page<MainProductsCbsDTO> page = mainProductsCbsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /main-products-cbs/:id} : get the "id" mainProductsCbs.
     *
     * @param id the id of the mainProductsCbsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mainProductsCbsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/main-products-cbs/{id}")
    public ResponseEntity<MainProductsCbsDTO> getMainProductsCbs(@PathVariable Long id) {
        log.debug("REST request to get MainProductsCbs : {}", id);
        Optional<MainProductsCbsDTO> mainProductsCbsDTO = mainProductsCbsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mainProductsCbsDTO);
    }

    /**
     * {@code DELETE  /main-products-cbs/:id} : delete the "id" mainProductsCbs.
     *
     * @param id the id of the mainProductsCbsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/main-products-cbs/{id}")
    public ResponseEntity<Void> deleteMainProductsCbs(@PathVariable Long id) {
        log.debug("REST request to delete MainProductsCbs : {}", id);
        mainProductsCbsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
