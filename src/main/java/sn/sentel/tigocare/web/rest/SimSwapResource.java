package sn.sentel.tigocare.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.service.SimSwapService;
import sn.sentel.tigocare.service.dto.SimSwapDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.SimSwap}.
 */
@RestController
@RequestMapping("/api")
public class SimSwapResource {

    private static final String ENTITY_NAME = "simSwap";
    private final Logger log = LoggerFactory.getLogger(SimSwapResource.class);
    private final SimSwapService simSwapService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public SimSwapResource(SimSwapService simSwapService) {
        this.simSwapService = simSwapService;
    }

    /**
     * {@code POST  /sim-swaps} : Create a new simSwap.
     *
     * @param simSwapDTO the simSwapDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new simSwapDTO, or with status {@code 400 (Bad Request)} if the simSwap has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sim-swaps")
    public ResponseEntity<SimSwapDTO> createSimSwap(@Valid @RequestBody SimSwapDTO simSwapDTO) throws URISyntaxException {
        this.log.debug("REST request to save SimSwap : {}", simSwapDTO);
        if (simSwapDTO.getId() != null) {
            throw new BadRequestAlertException("A new simSwap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        simSwapDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        SimSwapDTO result = this.simSwapService.save(simSwapDTO);
        return ResponseEntity.created(new URI("/api/sim-swaps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, "TransactionId = "+result.getId().toString()))
            .body(result);
    }

    @PostMapping("/sim-swaps/sync")
    public ResponseEntity<SimSwapDTO> createSimSwapSync(@Valid @RequestBody SimSwapDTO simSwapDTO) throws URISyntaxException {
        this.log.debug("REST request to save SimSwap : {}", simSwapDTO);
        if (simSwapDTO.getId() != null) {
            throw new BadRequestAlertException("A new simSwap cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SimSwapDTO result = this.simSwapService.doSimswap(simSwapDTO);
        return ResponseEntity.created(new URI("/api/sim-swaps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, "TransactionId = "+result.getId().toString()))
            .body(result);
    }

    @PostMapping("/sim-swaps/upload")
    public ResponseEntity bulkSimSwap(@RequestParam("file")MultipartFile file) throws IOException, URISyntaxException {
        String txId = UUID.randomUUID().toString();
        this.log.debug(" bulking this file {} with transactionID {}", file, txId);
        this.simSwapService.bulk(file.getInputStream(), txId);
        return ResponseEntity.created(new URI("/api/sim-swaps/transaction/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(this.applicationName, false, ENTITY_NAME, txId))
            .build();
    }

    /**
     * {@code PUT  /sim-swaps} : Updates an existing simSwap.
     *
     * @param simSwapDTO the simSwapDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated simSwapDTO,
     * or with status {@code 400 (Bad Request)} if the simSwapDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the simSwapDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sim-swaps")
    public ResponseEntity<SimSwapDTO> updateSimSwap(@Valid @RequestBody SimSwapDTO simSwapDTO) throws URISyntaxException {
        this.log.debug("REST request to update SimSwap : {}", simSwapDTO);
        if (simSwapDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SimSwapDTO result = this.simSwapService.save(simSwapDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(this.applicationName, false, ENTITY_NAME, simSwapDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sim-swaps} : get all the simSwaps.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of simSwaps in body.
     */
    @GetMapping("/sim-swaps")
    public ResponseEntity<List<SimSwapDTO>> getAllSimSwaps(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        this.log.debug("REST request to get a page of SimSwaps");
        Page<SimSwapDTO> page = this.simSwapService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/sim-swaps/bulks/{transactionId}")
    public ResponseEntity<List<SimSwapDTO>> getAllSimSwapsOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<SimSwapDTO> page = this.simSwapService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sim-swaps/:id} : get the "id" simSwap.
     *
     * @param id the id of the simSwapDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the simSwapDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sim-swaps/{id}")
    public ResponseEntity<SimSwapDTO> getSimSwap(@PathVariable Long id) {
        this.log.debug("REST request to get SimSwap : {}", id);
        Optional<SimSwapDTO> simSwapDTO = this.simSwapService.findOne(id);
        return ResponseUtil.wrapOrNotFound(simSwapDTO);
    }

    /**
     * {@code DELETE  /sim-swaps/:id} : delete the "id" simSwap.
     *
     * @param id the id of the simSwapDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sim-swaps/{id}")
    public ResponseEntity<Void> deleteSimSwap(@PathVariable Long id) {
        this.log.debug("REST request to delete SimSwap : {}", id);
        this.simSwapService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(this.applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
