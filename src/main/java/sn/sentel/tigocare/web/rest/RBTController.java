package sn.sentel.tigocare.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;

@Slf4j
@RestController
@RequestMapping("/RBTService.aspx")
public class RBTController {

    private final SubscriberInfoClient subscriberInfoClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;
    @Value("${app.ws.password}")
    private String pwd;

    public RBTController(SubscriberInfoClient subscriberInfoClient, SessionControl sessionControl) {
        this.subscriberInfoClient = subscriberInfoClient;
        this.sessionControl = sessionControl;
    }

    // http://10.0.4.100:8089/RBTService.aspx?msisdn=766752355&action=1
    @GetMapping
    ResponseEntity<String> setRBT(@RequestParam String msisdn, @RequestParam String action) {
        if (!(msisdn.length() == 12 && msisdn.startsWith("221"))) {
            msisdn = "221"+msisdn;
        }

        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        if (sessionId == null) {
            return ResponseEntity.ok("failed");
        }
        try {
            this.subscriberInfoClient.changeServiceRBT(msisdn, action.equals("1") ? OperationEnum.ACTIVATE : OperationEnum.DEACTIVATE, sessionId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.ok("failure");
        } finally {
            this.sessionControl.logout(sessionId);
        }
        return ResponseEntity.ok("success");
    }
}
