package sn.sentel.tigocare.web.rest;

import org.springframework.web.multipart.MultipartFile;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
import sn.sentel.tigocare.service.DeleteSubscriberService;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.DeleteSubscriber}.
 */
@RestController
@RequestMapping("/api")
public class DeleteSubscriberResource {

    private final Logger log = LoggerFactory.getLogger(DeleteSubscriberResource.class);

    private static final String ENTITY_NAME = "deleteSubscriber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeleteSubscriberService deleteSubscriberService;

    public DeleteSubscriberResource(DeleteSubscriberService deleteSubscriberService) {
        this.deleteSubscriberService = deleteSubscriberService;
    }

    /**
     * {@code POST  /delete-subscribers} : Create a new deleteSubscriber.
     *
     * @param deleteSubscriberDTO the deleteSubscriberDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deleteSubscriberDTO, or with status {@code 400 (Bad Request)} if the deleteSubscriber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delete-subscribers")
    public ResponseEntity<DeleteSubscriberDTO> createDeleteSubscriber(@RequestBody DeleteSubscriberDTO deleteSubscriberDTO) throws URISyntaxException {
        log.debug("REST request to save DeleteSubscriber : {}", deleteSubscriberDTO);
        if (deleteSubscriberDTO.getId() != null) {
            throw new BadRequestAlertException("A new deleteSubscriber cannot already have an ID", ENTITY_NAME, "idexists");
        }
        deleteSubscriberDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        DeleteSubscriberDTO result = deleteSubscriberService.save(deleteSubscriberDTO);
        return ResponseEntity.created(new URI("/api/delete-subscribers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delete-subscribers} : Updates an existing deleteSubscriber.
     *
     * @param deleteSubscriberDTO the deleteSubscriberDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deleteSubscriberDTO,
     * or with status {@code 400 (Bad Request)} if the deleteSubscriberDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deleteSubscriberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delete-subscribers")
    public ResponseEntity<DeleteSubscriberDTO> updateDeleteSubscriber(@RequestBody DeleteSubscriberDTO deleteSubscriberDTO) throws URISyntaxException {
        log.debug("REST request to update DeleteSubscriber : {}", deleteSubscriberDTO);
        if (deleteSubscriberDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        deleteSubscriberDTO.setProcessingStatus(ProcessingStatus.SUBMITTED);
        DeleteSubscriberDTO result = deleteSubscriberService.save(deleteSubscriberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deleteSubscriberDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delete-subscribers} : get all the deleteSubscribers.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deleteSubscribers in body.
     */
    @GetMapping("/delete-subscribers")
    public ResponseEntity<List<DeleteSubscriberDTO>> getAllDeleteSubscribers(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of DeleteSubscribers");
        Page<DeleteSubscriberDTO> page = deleteSubscriberService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/delete-subscribers/bulks/{transactionId}")
    public ResponseEntity<List<DeleteSubscriberDTO>> getAllDeleteSubscribersOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<DeleteSubscriberDTO> page = this.deleteSubscriberService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /delete-subscribers/:id} : get the "id" deleteSubscriber.
     *
     * @param id the id of the deleteSubscriberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deleteSubscriberDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delete-subscribers/{id}")
    public ResponseEntity<DeleteSubscriberDTO> getDeleteSubscriber(@PathVariable Long id) {
        log.debug("REST request to get DeleteSubscriber : {}", id);
        Optional<DeleteSubscriberDTO> deleteSubscriberDTO = deleteSubscriberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deleteSubscriberDTO);
    }

    /**
     * {@code DELETE  /delete-subscribers/:id} : delete the "id" deleteSubscriber.
     *
     * @param id the id of the deleteSubscriberDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delete-subscribers/{id}")
    public ResponseEntity<Void> deleteDeleteSubscriber(@PathVariable Long id) {
        log.debug("REST request to delete DeleteSubscriber : {}", id);
        deleteSubscriberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/delete-subscribers/bulk")
    public ResponseEntity bulk(@RequestParam("file") MultipartFile file,
                               @RequestParam("deleteOnAuc") String deleteOnAuc,
                               @RequestParam("deleteOnHlr") String deleteOnHlr,
                               @RequestParam("deleteOnHss") String deleteOnHss,
                               @RequestParam("deleteOnCbs") String deleteOnCbs
                              ) throws IOException, URISyntaxException {
        String txId = UUID.randomUUID().toString();
        deleteSubscriberService.bulk(file.getInputStream(), getVal(deleteOnAuc),getVal(deleteOnHlr),getVal( deleteOnHss),getVal( deleteOnCbs), txId);
        return ResponseEntity.created(new URI("/api/sim-swaps/transaction/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }

    private boolean getVal(String deleteOnAuc) {
        return deleteOnAuc.equals("true");
    }
}
