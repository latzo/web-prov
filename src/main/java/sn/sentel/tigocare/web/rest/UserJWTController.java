package sn.sentel.tigocare.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.sentel.tigocare.security.TertioRoles;
import sn.sentel.tigocare.security.jwt.JWTFilter;
import sn.sentel.tigocare.security.jwt.TokenProvider;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.UserDTO;
import sn.sentel.tigocare.web.rest.vm.LoginVM;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller to authenticate users.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final ActiveDirectoryLdapAuthenticationProvider adAuthProvider;
    private final UserService userService;
    private final UserDetailsService userDetailsService;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManager, ActiveDirectoryLdapAuthenticationProvider adAuthProvider, UserService userService, UserDetailsService userDetailsService) throws Exception {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManager;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.adAuthProvider = adAuthProvider;

        adAuthProvider.setSearchFilter("(&(objectClass=user)(userPrincipalName={0}))");

        this.authenticationManagerBuilder.userDetailsService(userDetailsService);
        this.authenticationManagerBuilder.authenticationProvider(adAuthProvider);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = this.authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        if (authentication != null && !this.userService.getUserByLogin(loginVM.getUsername()).isPresent()) {
            LdapUserDetails userDetails = (LdapUserDetails) authentication.getPrincipal();
            final List<TertioRoles> roles = TertioRoles.allRoles.stream()
                .filter(r -> userDetails.getAuthorities().contains(r.getAuthority()))
                .collect(Collectors.toList());
            if(roles.isEmpty()) {
                log.error("{} filtered roles => {}", "JWT CONTROLLER", roles);
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            if(roles.contains(TertioRoles.ADMIN)){
                roles.add(TertioRoles.B2B);
                roles.add(TertioRoles.COMPLAIN);
            }
            roles.add(TertioRoles.USER);
            this.userService.createUser(UserDTO.builder()
                .login(userDetails.getUsername())
                .firstName(userDetails.getDn()
                    .split(",")[0]
                    .replace("CN=", ""))
                .authorities(new HashSet<>(roles.stream()
                    .map(TertioRoles::getRole)
                    .collect(Collectors.toList())))
                .build());
        }
        String jwt = this.tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return this.idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
