/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.sentel.tigocare.web.rest.vm;
