package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.service.FullPortinService;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;
import sn.sentel.tigocare.service.dto.FullPortinDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.FullPortin}.
 */
@RestController
@RequestMapping("/api")
public class FullPortinResource {

    private final Logger log = LoggerFactory.getLogger(FullPortinResource.class);

    private static final String ENTITY_NAME = "fullPortin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FullPortinService fullPortinService;

    public FullPortinResource(FullPortinService fullPortinService) {
        this.fullPortinService = fullPortinService;
    }

    /**
     * {@code POST  /full-portins} : Create a new fullPortin.
     *
     * @param fullPortinDTO the fullPortinDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fullPortinDTO, or with status {@code 400 (Bad Request)} if the fullPortin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/full-portins")
    public ResponseEntity<FullPortinDTO> createFullPortin(@Valid @RequestBody FullPortinDTO fullPortinDTO) throws URISyntaxException {
        log.debug("REST request to save FullPortin : {}", fullPortinDTO);
        if (fullPortinDTO.getId() != null) {
            throw new BadRequestAlertException("A new fullPortin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FullPortinDTO result = fullPortinService.save(fullPortinDTO);
        return ResponseEntity.created(new URI("/api/full-portins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /full-portins} : Updates an existing fullPortin.
     *
     * @param fullPortinDTO the fullPortinDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fullPortinDTO,
     * or with status {@code 400 (Bad Request)} if the fullPortinDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fullPortinDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/full-portins")
    public ResponseEntity<FullPortinDTO> updateFullPortin(@Valid @RequestBody FullPortinDTO fullPortinDTO) throws URISyntaxException {
        log.debug("REST request to update FullPortin : {}", fullPortinDTO);
        if (fullPortinDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FullPortinDTO result = fullPortinService.save(fullPortinDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, fullPortinDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /full-portins} : get all the fullPortins.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fullPortins in body.
     */
    @GetMapping("/full-portins")
    public ResponseEntity<List<FullPortinDTO>> getAllFullPortins(Pageable pageable) {
        log.debug("REST request to get a page of FullPortins");
        Page<FullPortinDTO> page = fullPortinService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /full-portins/:id} : get the "id" fullPortin.
     *
     * @param id the id of the fullPortinDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fullPortinDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/full-portins/{id}")
    public ResponseEntity<FullPortinDTO> getFullPortin(@PathVariable Long id) {
        log.debug("REST request to get FullPortin : {}", id);
        Optional<FullPortinDTO> fullPortinDTO = fullPortinService.findOne(id);
        return ResponseUtil.wrapOrNotFound(fullPortinDTO);
    }

    /**
     * {@code DELETE  /full-portins/:id} : delete the "id" fullPortin.
     *
     * @param id the id of the fullPortinDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/full-portins/{id}")
    public ResponseEntity<Void> deleteFullPortin(@PathVariable Long id) {
        log.debug("REST request to delete FullPortin : {}", id);
        fullPortinService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
