package sn.sentel.tigocare.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.service.TransfertAppelService;
import sn.sentel.tigocare.service.dto.TransfertAppelDTO;
import sn.sentel.tigocare.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link sn.sentel.tigocare.domain.TransfertAppel}.
 */
@RestController
@RequestMapping("/api")
public class TransfertAppelResource {

    private static final String ENTITY_NAME = "transfertAppel";
    private final Logger log = LoggerFactory.getLogger(TransfertAppelResource.class);
    private final TransfertAppelService transfertAppelService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public TransfertAppelResource(TransfertAppelService transfertAppelService) {
        this.transfertAppelService = transfertAppelService;
    }

    /**
     * {@code POST  /transfert-appels} : Create a new transfertAppel.
     *
     * @param transfertAppelDTO the transfertAppelDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transfertAppelDTO, or with status {@code 400 (Bad Request)} if the transfertAppel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transfert-appels")
    public ResponseEntity<TransfertAppelDTO> createTransfertAppel(@Valid @RequestBody TransfertAppelDTO transfertAppelDTO) throws URISyntaxException {
        log.debug("REST request to save TransfertAppel : {}", transfertAppelDTO);
        if (transfertAppelDTO.getId() != null) {
            throw new BadRequestAlertException("A new transfertAppel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TransfertAppelDTO result = transfertAppelService.save(transfertAppelDTO);
        return ResponseEntity.created(new URI("/api/transfert-appels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transfert-appels} : Updates an existing transfertAppel.
     *
     * @param transfertAppelDTO the transfertAppelDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transfertAppelDTO,
     * or with status {@code 400 (Bad Request)} if the transfertAppelDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transfertAppelDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transfert-appels")
    public ResponseEntity<TransfertAppelDTO> updateTransfertAppel(@Valid @RequestBody TransfertAppelDTO transfertAppelDTO) throws URISyntaxException {
        log.debug("REST request to update TransfertAppel : {}", transfertAppelDTO);
        if (transfertAppelDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TransfertAppelDTO result = transfertAppelService.save(transfertAppelDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transfertAppelDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transfert-appels} : get all the transfertAppels.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transfertAppels in body.
     */
    @GetMapping("/transfert-appels")
    public ResponseEntity<List<TransfertAppelDTO>> getAllTransfertAppels(Pageable pageable) {
        log.debug("REST request to get a page of TransfertAppels");
        Page<TransfertAppelDTO> page = transfertAppelService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transfert-appels/:id} : get the "id" transfertAppel.
     *
     * @param id the id of the transfertAppelDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transfertAppelDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transfert-appels/{id}")
    public ResponseEntity<TransfertAppelDTO> getTransfertAppel(@PathVariable Long id) {
        log.debug("REST request to get TransfertAppel : {}", id);
        Optional<TransfertAppelDTO> transfertAppelDTO = transfertAppelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transfertAppelDTO);
    }

    /**
     * {@code DELETE  /transfert-appels/:id} : delete the "id" transfertAppel.
     *
     * @param id the id of the transfertAppelDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transfert-appels/{id}")
    public ResponseEntity<Void> deleteTransfertAppel(@PathVariable Long id) {
        log.debug("REST request to delete TransfertAppel : {}", id);
        transfertAppelService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/transfert-appels/upload")
    public ResponseEntity bulk(@RequestParam("file") MultipartFile file, @RequestParam(value = "operation", required = false, defaultValue = "ACTIVATE") String operationType) throws IOException, URISyntaxException {

        String txId = UUID.randomUUID().toString();
        log.debug(" bulking this file {} with transactionID {}", file, txId);
        OperationEnum op = OperationEnum.ACTIVATE;
        this.transfertAppelService.bulk(file.getInputStream(), op, txId);
        return ResponseEntity.created(new URI("/api/transfert-appels/bulks/" + txId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, txId))
            .build();
    }

    @GetMapping("/transfert-appels/bulks/{transactionId}")
    public ResponseEntity<List<TransfertAppelDTO>> getAllTransfertsAppelsOfABulk(@PathVariable String transactionId, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<TransfertAppelDTO> page = this.transfertAppelService.findAllByTransactionId(transactionId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
