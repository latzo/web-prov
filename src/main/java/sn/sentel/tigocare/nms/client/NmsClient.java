package sn.sentel.tigocare.nms.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.sentel.tigocare.jaxb2.nms.DoNmsLinkRequest;
import sn.sentel.tigocare.jaxb2.nms.DoNmsLinkResult;
import sn.sentel.tigocare.jaxb2.nms.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

@Service
@Slf4j
public class NmsClient {
    @Value("${app.ws.nms.endpoint}")
    private String endpoint;
    @Value("${app.ws.nms.host}")
    private String host;
    private ObjectFactory objectFactory = new ObjectFactory();

    public DoNmsLinkResult doNMSLink(String msisdn, String imsi){
        DoNmsLinkRequest request = this.objectFactory.createDoNmsLinkRequest();
        request.setMsisdn(msisdn);
        request.setImsi(imsi);
        try {
            log.info("Rest request to nms : " + jaxbObjectToXML(request));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        RestTemplate restTemplate = new RestTemplate();
        DoNmsLinkResult response = restTemplate.postForObject(this.host+this.endpoint, request, DoNmsLinkResult.class);
        log.info("Rest response from nms : " + jaxbObjectToXML(response));
        return response;
    }

    private String jaxbObjectToXML(DoNmsLinkRequest request) throws JAXBException {
        try
        {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(DoNmsLinkRequest.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(request, sw);

            //Verify XML Content
            String xmlContent = sw.toString();
            return xmlContent;

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String jaxbObjectToXML(DoNmsLinkResult result)
    {
        try
        {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(DoNmsLinkResult.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(result, sw);

            //Verify XML Content
            String xmlContent = sw.toString();
            return xmlContent;

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }
}
