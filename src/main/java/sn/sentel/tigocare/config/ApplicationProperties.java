package sn.sentel.tigocare.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Tigocare.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = true)
public class ApplicationProperties {

}
