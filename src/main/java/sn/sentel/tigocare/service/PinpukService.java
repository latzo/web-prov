package sn.sentel.tigocare.service;

import sn.sentel.tigocare.domain.Pinpuk;
import sn.sentel.tigocare.service.dto.PinpukDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.Pinpuk}.
 */
public interface PinpukService {

    /**
     * Save a pinpuk.
     *
     * @param pinpukDTO the entity to save.
     * @return the persisted entity.
     */
    PinpukDTO save(PinpukDTO pinpukDTO);

    /**
     * Get all the pinpuks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PinpukDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pinpuk.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PinpukDTO> findOne(Long id);

    /**
     * Delete the "id" pinpuk.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<Pinpuk> findOneByImsi(String imsi);

    Optional<Pinpuk> findFirstByImsi(String imsi);
}
