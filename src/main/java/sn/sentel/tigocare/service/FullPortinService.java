package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.FullPortinDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.FullPortin}.
 */
public interface FullPortinService {

    /**
     * Save a fullPortin.
     *
     * @param fullPortinDTO the entity to save.
     * @return the persisted entity.
     */
    FullPortinDTO save(FullPortinDTO fullPortinDTO);

    /**
     * Get all the fullPortins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FullPortinDTO> findAll(Pageable pageable);


    /**
     * Get the "id" fullPortin.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FullPortinDTO> findOne(Long id);

    /**
     * Delete the "id" fullPortin.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
