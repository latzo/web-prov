package sn.sentel.tigocare.service;

import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.CreateSubscriber}.
 */
public interface CreateSubscriberService {

    /**
     * Save a createSubscriber.
     *
     * @param createSubscriberDTO the entity to save.
     * @return the persisted entity.
     */
    CreateSubscriberDTO save(CreateSubscriberDTO createSubscriberDTO);

    /**
     * Get all the createSubscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CreateSubscriberDTO> findAll(Pageable pageable);

    Page<CreateSubscriberDTO> findAllFullPortin(Pageable pageable);

    Page<CreateSubscriberDTO> findAllByTransactionId(String transactionId, Pageable pageable);


    /**
     * Get the "id" createSubscriber.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CreateSubscriberDTO> findOne(Long id);

    /**
     * Delete the "id" createSubscriber.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, SIMCreationType creationType, String statut, String cos, String txId) throws IOException;

    List<CreateSubscriberDTO> fullPortinListOfSubscribers (List<CreateSubscriberDTO> createSubscriberList);

    String createLTE(String msisdn, String imsi);

}
