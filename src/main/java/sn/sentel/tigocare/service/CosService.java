package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.CosDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.Cos}.
 */
public interface CosService {

    /**
     * Save a cos.
     *
     * @param cosDTO the entity to save.
     * @return the persisted entity.
     */
    CosDTO save(CosDTO cosDTO);

    /**
     * Get all the cos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CosDTO> findAll(Pageable pageable);


    /**
     * Get the "id" cos.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CosDTO> findOne(Long id);

    /**
     * Delete the "id" cos.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
