package sn.sentel.tigocare.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.APNChangeDtoVTalend;
import sn.sentel.tigocare.service.dto.SimSwapDTO;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.APNChange}.
 */
public interface APNChangeService {

    /**
     * Save a aPNChange.
     *
     * @param aPNChangeDTO the entity to save.
     * @return the persisted entity.
     */
    APNChangeDTO save(APNChangeDTO aPNChangeDTO);

    /**
     * Get all the aPNChanges.
     *
     * @return the list of entities.
     */
    Page<APNChangeDTO> findAll(Pageable pageable);

    Page<APNChangeDTO> findAllByTransactionId(String transactionId, Pageable pageable);


    /**
     * Get the "id" aPNChange.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<APNChangeDTO> findOne(Long id);

    /**
     * Delete the "id" aPNChange.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, String apn, String profile, OperationEnum operation, String txId) throws IOException;

    APNChangeDTO saveApnThroughTalend(APNChangeDtoVTalend aPNChangeDTO);

    void saveApnThroughTalend(InputStream inputStream, String apn, String technology, String operation) throws IOException;
}
