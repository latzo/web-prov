package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.MainProductsCbsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.MainProductsCbs}.
 */
public interface MainProductsCbsService {

    /**
     * Save a mainProductsCbs.
     *
     * @param mainProductsCbsDTO the entity to save.
     * @return the persisted entity.
     */
    MainProductsCbsDTO save(MainProductsCbsDTO mainProductsCbsDTO);

    /**
     * Get all the mainProductsCbs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MainProductsCbsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" mainProductsCbs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MainProductsCbsDTO> findOne(Long id);

    /**
     * Delete the "id" mainProductsCbs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
