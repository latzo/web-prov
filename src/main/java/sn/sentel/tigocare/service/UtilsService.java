package sn.sentel.tigocare.service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Service
@Transactional
public class UtilsService {

    @Value("${app.imsi4gPrefixes}")
    private String[] imsi4gPrefixes;

    public boolean is4gNumber(String imsi){
        boolean res = Arrays.stream(imsi4gPrefixes).anyMatch( p -> imsi.startsWith(p));
        System.out.println(imsi + " IS 4G NUMBER :" + res);
        return res;
    }

    @PostConstruct
    public void testis4gNumber(){
        Arrays.stream(imsi4gPrefixes).forEach(p -> System.out.println(p));
        is4gNumber("608027000000085");
        is4gNumber("6080264XXXXXXXX");
        is4gNumber("6080263XXXXXXXX");
    }
}
