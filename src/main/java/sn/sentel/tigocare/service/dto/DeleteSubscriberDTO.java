package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.DeleteSubscriber} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeleteSubscriberDTO implements Serializable {

    private Long id;

    private String imsi;

    private String msisdn;

    private Boolean deleteOnAuc;

    private Boolean deleteOnHlr;

    private Boolean deleteOnHss;

    private Boolean deleteOnCbs;

    private ProcessingStatus processingStatus;

    private ProcessingStatus aucDeletedStatus;

    private ProcessingStatus hlrDeletedStatus;

    private ProcessingStatus hssDeletedStatus;

    private ProcessingStatus cbsDeletedStatus;

    private String errorMessage;

    private String transanctionId;

    private String createdBy;

    private Instant createdDate;
}
