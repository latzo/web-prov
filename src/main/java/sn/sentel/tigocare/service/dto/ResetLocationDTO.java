package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.ResetLocation} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResetLocationDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    private String createdBy;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResetLocationDTO resetLocationDTO = (ResetLocationDTO) o;
        if (resetLocationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resetLocationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ResetLocationDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            "}";
    }
}
