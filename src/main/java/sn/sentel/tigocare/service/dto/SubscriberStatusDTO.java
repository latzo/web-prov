package sn.sentel.tigocare.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.SubscriberStatus} entity.
 */
public class SubscriberStatusDTO implements Serializable {

    private Long id;

    private String statusId;

    private String label;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubscriberStatusDTO subscriberStatusDTO = (SubscriberStatusDTO) o;
        if (subscriberStatusDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subscriberStatusDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubscriberStatusDTO{" +
            "id=" + getId() +
            ", statusId='" + getStatusId() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
