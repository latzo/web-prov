package sn.sentel.tigocare.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.RoamingServiceEnum;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.RoamingService} entity.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoamingServiceDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private RoamingServiceEnum services;

    private ProcessingStatus processingStatus;

    private String createdBy;

    private Instant createdDate;
}
