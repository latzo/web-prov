package sn.sentel.tigocare.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.FullPortin} entity.
 */
public class FullPortinDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String imsi;

    private String processingStatus;

    private String statut;

    private String transactionId;

    private String backendResp;

    private String cos;

    private SIMCreationType creationType;

    private String name;

    private String firstName;

    private String dob;

    private String city;

    private String puk;

    private String tigoCashStatus;

    private String gender;

    private String altContactNum;

    private String district;

    private String documentReferenceType;

    private String proofNumber;

    private String createUser;

    private String createDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBackendResp() {
        return backendResp;
    }

    public void setBackendResp(String backendResp) {
        this.backendResp = backendResp;
    }

    public String getCos() {
        return cos;
    }

    public void setCos(String cos) {
        this.cos = cos;
    }

    public SIMCreationType getCreationType() {
        return creationType;
    }

    public void setCreationType(SIMCreationType creationType) {
        this.creationType = creationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPuk() {
        return puk;
    }

    public void setPuk(String puk) {
        this.puk = puk;
    }

    public String getTigoCashStatus() {
        return tigoCashStatus;
    }

    public void setTigoCashStatus(String tigoCashStatus) {
        this.tigoCashStatus = tigoCashStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAltContactNum() {
        return altContactNum;
    }

    public void setAltContactNum(String altContactNum) {
        this.altContactNum = altContactNum;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDocumentReferenceType() {
        return documentReferenceType;
    }

    public void setDocumentReferenceType(String documentReferenceType) {
        this.documentReferenceType = documentReferenceType;
    }

    public String getProofNumber() {
        return proofNumber;
    }

    public void setProofNumber(String proofNumber) {
        this.proofNumber = proofNumber;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FullPortinDTO fullPortinDTO = (FullPortinDTO) o;
        if (fullPortinDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fullPortinDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FullPortinDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            ", statut='" + getStatut() + "'" +
            ", transactionId='" + getTransactionId() + "'" +
            ", backendResp='" + getBackendResp() + "'" +
            ", cos='" + getCos() + "'" +
            ", creationType='" + getCreationType() + "'" +
            ", name='" + getName() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", dob='" + getDob() + "'" +
            ", city='" + getCity() + "'" +
            ", puk='" + getPuk() + "'" +
            ", tigoCashStatus='" + getTigoCashStatus() + "'" +
            ", gender='" + getGender() + "'" +
            ", altContactNum='" + getAltContactNum() + "'" +
            ", district='" + getDistrict() + "'" +
            ", documentReferenceType='" + getDocumentReferenceType() + "'" +
            ", proofNumber='" + getProofNumber() + "'" +
            ", createUser='" + getCreateUser() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
