package sn.sentel.tigocare.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.Cos} entity.
 */
public class CosDTO implements Serializable {

    private Long id;

    private String classIdCbs;

    private String label;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassIdCbs() {
        return classIdCbs;
    }

    public void setClassIdCbs(String classIdCbs) {
        this.classIdCbs = classIdCbs;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CosDTO cosDTO = (CosDTO) o;
        if (cosDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cosDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CosDTO{" +
            "id=" + getId() +
            ", classIdCbs='" + getClassIdCbs() + "'" +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
