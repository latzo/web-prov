package sn.sentel.tigocare.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.Roaming4gEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.Roaming4g} entity.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Roaming4gDTO implements Serializable {

    private Long id;

    @NotNull
    private String imsi;

    @NotNull
    private Roaming4gEnum services;

    private ProcessingStatus processingStatus;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Roaming4gEnum getServices() {
        return services;
    }

    public void setServices(Roaming4gEnum services) {
        this.services = services;
    }

    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Roaming4gDTO roaming4gDTO = (Roaming4gDTO) o;
        if (roaming4gDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roaming4gDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Roaming4gDTO{" +
            "id=" + getId() +
            ", imsi='" + getImsi() + "'" +
            ", services='" + getServices() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            "}";
    }
}
