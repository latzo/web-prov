package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.Pinpuk} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PinpukDTO implements Serializable {

    private Long id;

    private String iccid;

    private String imsi;

    private Long pin1;

    private Long puk1;

    private Long pin2;

    private Long puk2;

    private String ki;

    private String adm1;

    private String nomfichier;

    private String supplier;

    private LocalDate loaddate;

}
