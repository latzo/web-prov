package sn.sentel.tigocare.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.TransfertAppel} entity.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransfertAppelDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdnSource;

    private String msisdnDest;

    @NotNull
    private OperationEnum operation;

    private String processingStatus;

    private String transactionId;

    private String backendResp;
}
