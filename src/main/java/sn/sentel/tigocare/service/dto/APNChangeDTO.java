package sn.sentel.tigocare.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.APNChange} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class APNChangeDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String imsi;

    @NotNull
    private OperationEnum operation;

    @NotNull
    private String apn;

    @NotNull
    private String profil;

    private ProcessingStatus processingStatus;

    private String errorMessage;

    private String transactionId;

    private String createdBy;

    private Instant createdDate;
}
