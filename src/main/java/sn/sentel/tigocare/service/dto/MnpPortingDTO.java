package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.MnpPorting} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MnpPortingDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String processingStatus;

    private String createdBy;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MnpPortingDTO mnpPortingDTO = (MnpPortingDTO) o;
        if (mnpPortingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mnpPortingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MnpPortingDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            "}";
    }
}
