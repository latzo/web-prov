package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.AffichageInformationsAbonnes} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AffichageInformationsAbonnesDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String imsi;

    private String imsiHlr;

    private String imsiCbs;

    private String createdBy;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO = (AffichageInformationsAbonnesDTO) o;
        if (affichageInformationsAbonnesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), affichageInformationsAbonnesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AffichageInformationsAbonnesDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", imsiHlr='" + getImsiHlr() + "'" +
            ", imsiCbs='" + getImsiCbs() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
