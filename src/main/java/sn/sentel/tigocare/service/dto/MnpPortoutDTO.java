package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.MnpPortout} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MnpPortoutDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String operateur;

    private String processingStatus;

    private String createdBy;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MnpPortoutDTO mnpPortoutDTO = (MnpPortoutDTO) o;
        if (mnpPortoutDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mnpPortoutDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MnpPortoutDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", operateur='" + getOperateur() + "'" +
            ", processingStatus='" + getProcessingStatus() + "'" +
            "}";
    }
}
