package sn.sentel.tigocare.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.ServiceChange} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceChangeDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String imsi;

    @NotNull
    private OperationEnum operation;

    @NotNull
    private String serviceId;

    private ProcessingStatus processingStatus;

    private String transactionId;

    private String createdBy;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ServiceChangeDTO serviceChangeDTO = (ServiceChangeDTO) o;
        if (serviceChangeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceChangeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiceChangeDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", imsi='" + getImsi() + "'" +
            ", operation='" + getOperation() + "'" +
            ", serviceId='" + getServiceId() + "'" +
            "}";
    }
}
