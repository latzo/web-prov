package sn.sentel.tigocare.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.APNChange} entity.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class APNChangeDtoVTalend extends APNChangeDTO implements Serializable {

    private String ipAddr;

    private String technology;
}
