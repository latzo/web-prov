package sn.sentel.tigocare.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.MainProductsCbs} entity.
 */
public class MainProductsCbsDTO implements Serializable {

    private Long id;

    private String mainProductName;

    private String mainProductId;

    private String hlrProfilId;

    private String profileClass;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMainProductName() {
        return mainProductName;
    }

    public void setMainProductName(String mainProductName) {
        this.mainProductName = mainProductName;
    }

    public String getMainProductId() {
        return mainProductId;
    }

    public void setMainProductId(String mainProductId) {
        this.mainProductId = mainProductId;
    }

    public String getHlrProfilId() {
        return hlrProfilId;
    }

    public void setHlrProfilId(String hlrProfilId) {
        this.hlrProfilId = hlrProfilId;
    }

    public String getProfileClass() {
        return profileClass;
    }

    public void setProfileClass(String profileClass) {
        this.profileClass = profileClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MainProductsCbsDTO mainProductsCbsDTO = (MainProductsCbsDTO) o;
        if (mainProductsCbsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mainProductsCbsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MainProductsCbsDTO{" +
            "id=" + getId() +
            ", mainProductName='" + getMainProductName() + "'" +
            ", mainProductId='" + getMainProductId() + "'" +
            ", hlrProfilId='" + getHlrProfilId() + "'" +
            ", profileClass='" + getProfileClass() + "'" +
            "}";
    }
}
