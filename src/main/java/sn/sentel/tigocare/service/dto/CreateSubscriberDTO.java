package sn.sentel.tigocare.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import lombok.*;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.CreateSubscriber} entity.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateSubscriberDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String imsi;

    private ProcessingStatus processingStatus;

    private String statut;

    private String transactionId;

    private String backendResp;

    private String cos;

    private SIMCreationType creationType;

    private String createdBy;

    private Instant createdDate;

    private String name;

    private String firstName;

    private String dob;

    private String city;

    private String puk;

    private String tigoCashStatus;

    private String gender;

    private String altContactNum;

    private String district;

    private String documentReferenceType;

    private String proofNumber;

    private String createUser;

    private String createDate;

}
