package sn.sentel.tigocare.service.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.sentel.tigocare.domain.SimSwap} entity.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SimSwapDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String oldIMSI;

    @NotNull
    private String newIMSI;

    private ProcessingStatus processingStatus;

    private String transactionId;

    private String createdBy;

    private Instant createdDate;

    private String backendResp;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimSwapDTO simSwapDTO = (SimSwapDTO) o;
        if (simSwapDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simSwapDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimSwapDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", oldIMSI='" + getOldIMSI() + "'" +
            ", newIMSI='" + getNewIMSI() + "'" +
            "}";
    }
}
