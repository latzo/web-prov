package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DeleteSubscriber} and its DTO {@link DeleteSubscriberDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DeleteSubscriberMapper extends EntityMapper<DeleteSubscriberDTO, DeleteSubscriber> {



    default DeleteSubscriber fromId(Long id) {
        if (id == null) {
            return null;
        }
        DeleteSubscriber deleteSubscriber = new DeleteSubscriber();
        deleteSubscriber.setId(id);
        return deleteSubscriber;
    }
}
