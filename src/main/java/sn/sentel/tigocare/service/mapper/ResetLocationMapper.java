package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.ResetLocationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ResetLocation} and its DTO {@link ResetLocationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ResetLocationMapper extends EntityMapper<ResetLocationDTO, ResetLocation> {



    default ResetLocation fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResetLocation resetLocation = new ResetLocation();
        resetLocation.setId(id);
        return resetLocation;
    }
}
