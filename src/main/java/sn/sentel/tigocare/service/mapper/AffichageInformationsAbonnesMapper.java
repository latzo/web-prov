package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.AffichageInformationsAbonnesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AffichageInformationsAbonnes} and its DTO {@link AffichageInformationsAbonnesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AffichageInformationsAbonnesMapper extends EntityMapper<AffichageInformationsAbonnesDTO, AffichageInformationsAbonnes> {



    default AffichageInformationsAbonnes fromId(Long id) {
        if (id == null) {
            return null;
        }
        AffichageInformationsAbonnes affichageInformationsAbonnes = new AffichageInformationsAbonnes();
        affichageInformationsAbonnes.setId(id);
        return affichageInformationsAbonnes;
    }
}
