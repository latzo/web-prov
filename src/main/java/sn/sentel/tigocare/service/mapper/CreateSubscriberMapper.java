package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CreateSubscriber} and its DTO {@link CreateSubscriberDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CreateSubscriberMapper extends EntityMapper<CreateSubscriberDTO, CreateSubscriber> {



    default CreateSubscriber fromId(Long id) {
        if (id == null) {
            return null;
        }
        CreateSubscriber createSubscriber = new CreateSubscriber();
        createSubscriber.setId(id);
        return createSubscriber;
    }
}
