package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.APNChangeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link APNChange} and its DTO {@link APNChangeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface APNChangeMapper extends EntityMapper<APNChangeDTO, APNChange> {



    default APNChange fromId(Long id) {
        if (id == null) {
            return null;
        }
        APNChange aPNChange = new APNChange();
        aPNChange.setId(id);
        return aPNChange;
    }
}
