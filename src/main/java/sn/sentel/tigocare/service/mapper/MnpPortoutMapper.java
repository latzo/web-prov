package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.MnpPortoutDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MnpPortout} and its DTO {@link MnpPortoutDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MnpPortoutMapper extends EntityMapper<MnpPortoutDTO, MnpPortout> {



    default MnpPortout fromId(Long id) {
        if (id == null) {
            return null;
        }
        MnpPortout mnpPortout = new MnpPortout();
        mnpPortout.setId(id);
        return mnpPortout;
    }
}
