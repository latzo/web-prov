package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.RoamingServiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RoamingService} and its DTO {@link RoamingServiceDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoamingServiceMapper extends EntityMapper<RoamingServiceDTO, RoamingService> {



    default RoamingService fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoamingService roamingService = new RoamingService();
        roamingService.setId(id);
        return roamingService;
    }
}
