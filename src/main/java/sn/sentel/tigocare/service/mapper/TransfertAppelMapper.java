package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.TransfertAppelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TransfertAppel} and its DTO {@link TransfertAppelDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TransfertAppelMapper extends EntityMapper<TransfertAppelDTO, TransfertAppel> {



    default TransfertAppel fromId(Long id) {
        if (id == null) {
            return null;
        }
        TransfertAppel transfertAppel = new TransfertAppel();
        transfertAppel.setId(id);
        return transfertAppel;
    }
}
