package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.SimSwapDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SimSwap} and its DTO {@link SimSwapDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SimSwapMapper extends EntityMapper<SimSwapDTO, SimSwap> {



    default SimSwap fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimSwap simSwap = new SimSwap();
        simSwap.setId(id);
        return simSwap;
    }
}
