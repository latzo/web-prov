package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.ServiceChangeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ServiceChange} and its DTO {@link ServiceChangeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ServiceChangeMapper extends EntityMapper<ServiceChangeDTO, ServiceChange> {



    default ServiceChange fromId(Long id) {
        if (id == null) {
            return null;
        }
        ServiceChange serviceChange = new ServiceChange();
        serviceChange.setId(id);
        return serviceChange;
    }
}
