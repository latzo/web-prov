package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.MnpPortingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MnpPorting} and its DTO {@link MnpPortingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MnpPortingMapper extends EntityMapper<MnpPortingDTO, MnpPorting> {



    default MnpPorting fromId(Long id) {
        if (id == null) {
            return null;
        }
        MnpPorting mnpPorting = new MnpPorting();
        mnpPorting.setId(id);
        return mnpPorting;
    }
}
