package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.FullPortinDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FullPortin} and its DTO {@link FullPortinDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FullPortinMapper extends EntityMapper<FullPortinDTO, FullPortin> {



    default FullPortin fromId(Long id) {
        if (id == null) {
            return null;
        }
        FullPortin fullPortin = new FullPortin();
        fullPortin.setId(id);
        return fullPortin;
    }
}
