package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.MainProductsCbsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MainProductsCbs} and its DTO {@link MainProductsCbsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MainProductsCbsMapper extends EntityMapper<MainProductsCbsDTO, MainProductsCbs> {



    default MainProductsCbs fromId(Long id) {
        if (id == null) {
            return null;
        }
        MainProductsCbs mainProductsCbs = new MainProductsCbs();
        mainProductsCbs.setId(id);
        return mainProductsCbs;
    }
}
