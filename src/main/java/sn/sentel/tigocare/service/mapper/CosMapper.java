package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.CosDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cos} and its DTO {@link CosDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CosMapper extends EntityMapper<CosDTO, Cos> {



    default Cos fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cos cos = new Cos();
        cos.setId(id);
        return cos;
    }
}
