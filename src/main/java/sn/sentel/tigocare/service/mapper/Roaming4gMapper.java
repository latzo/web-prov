package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.Roaming4gDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Roaming4g} and its DTO {@link Roaming4gDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface Roaming4gMapper extends EntityMapper<Roaming4gDTO, Roaming4g> {



    default Roaming4g fromId(Long id) {
        if (id == null) {
            return null;
        }
        Roaming4g roaming4g = new Roaming4g();
        roaming4g.setId(id);
        return roaming4g;
    }
}
