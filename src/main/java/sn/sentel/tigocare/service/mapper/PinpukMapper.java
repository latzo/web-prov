package sn.sentel.tigocare.service.mapper;

import sn.sentel.tigocare.domain.*;
import sn.sentel.tigocare.service.dto.PinpukDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pinpuk} and its DTO {@link PinpukDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PinpukMapper extends EntityMapper<PinpukDTO, Pinpuk> {



    default Pinpuk fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pinpuk pinpuk = new Pinpuk();
        pinpuk.setId(id);
        return pinpuk;
    }
}
