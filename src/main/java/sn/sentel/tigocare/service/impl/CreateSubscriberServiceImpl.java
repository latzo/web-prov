package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
import sn.sentel.tigocare.engrafi.registration.client.EngrafiClient;
import sn.sentel.tigocare.hlr.services.auc.services.AUCService;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBS56BCClient;
import sn.sentel.tigocare.hlr.services.hss.services.HssService;
import sn.sentel.tigocare.hlr.services.imsichangeovercudb.services.ImsiChangeOverCudbService;
import sn.sentel.tigocare.hlr.services.mnp.services.MnpService;
import sn.sentel.tigocare.hlr.services.simchangeremoval.services.SimChangeRemovalService;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.jaxb2.auc.CreateResponse;
import sn.sentel.tigocare.jaxb2.cbs56.bc.CreateSubscriberResultMsg;
import sn.sentel.tigocare.jaxb2.cbs56.bc.SubDeactivationResultMsg;
import sn.sentel.tigocare.jaxb2.imsichangeovercudb.GetResponse;
import sn.sentel.tigocare.jaxb2.nms.DoNmsLinkResult;
import sn.sentel.tigocare.nms.client.NmsClient;
import sn.sentel.tigocare.repository.CreateSubscriberRepository;
import sn.sentel.tigocare.repository.MainProductsCbsRepository;
import sn.sentel.tigocare.service.CreateSubscriberService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.UtilsService;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
import sn.sentel.tigocare.service.mapper.CreateSubscriberMapper;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;


/**
 * Service Implementation for managing {@link CreateSubscriber}.
 */
@Service
@Transactional
public class CreateSubscriberServiceImpl implements CreateSubscriberService {

    private static final String LOG_HEADER = "[CREATE SUBSCRIBER SERVICE]";
    private final Logger log = LoggerFactory.getLogger(CreateSubscriberServiceImpl.class);

    private final CreateSubscriberRepository createSubscriberRepository;
    private final CreateSubscriberMapper createSubscriberMapper;
    private final SubscriberInfoService subscriberInfoService;
    private final AUCService aucService;
    private final CBS56BCClient cbs56BCClient;
    private final MainProductsCbsRepository cbsRepository;
    private final CSVParser csvParser;
    private final HssService hssService;
    private final ImsiChangeOverCudbService imsiChangeOverCudbService;
    private final SimChangeRemovalService simChangeRemovalService;
    private final UserService userService;
    private final MnpService mnpService;
    private final EngrafiClient engrafiClient;
    private final NmsClient nmsClient;

    @Autowired
    private final UtilsService utilsService;

    private Map<String, String> engrafiRespDesc = new HashMap<>();

    @Value("${app.ws.hss.epsProfileId}")
    private String epsProfileId;
    @Value("${app.ws.mnp.prefixTigo}")
    private String prefixTigo;


    public CreateSubscriberServiceImpl(UserService userService, CreateSubscriberRepository createSubscriberRepository, CreateSubscriberMapper createSubscriberMapper, SubscriberInfoService subscriberInfoService, AUCService aucService, CBS56BCClient cbs56BCClient, MainProductsCbsRepository cbsRepository, HssService hssService, ImsiChangeOverCudbService imsiChangeOverCudbService, SimChangeRemovalService simChangeRemovalService, MnpService mnpService, EngrafiClient engrafiClient, NmsClient nmsClient, UtilsService utilsService) {
        this.createSubscriberRepository = createSubscriberRepository;
        this.createSubscriberMapper = createSubscriberMapper;
        this.subscriberInfoService = subscriberInfoService;
        this.cbs56BCClient = cbs56BCClient;
        this.hssService = hssService;
        this.imsiChangeOverCudbService = imsiChangeOverCudbService;
        this.simChangeRemovalService = simChangeRemovalService;
        this.engrafiClient = engrafiClient;
        this.nmsClient = nmsClient;
        this.utilsService = utilsService;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
        this.aucService = aucService;
        this.cbsRepository = cbsRepository;
        this.userService = userService;
        this.mnpService = mnpService;
        engrafiRespDesc.put("SC0000", "SUCCESS");
        engrafiRespDesc.put("FL0000", "FAILURE - Unknown error");
        engrafiRespDesc.put("FL0001", "Database connection failed");
        engrafiRespDesc.put("FL0002", "No data found");
        engrafiRespDesc.put("FL0004", "Duplicate entry");
        engrafiRespDesc.put("FL0005", "Some required fields are missing");
        engrafiRespDesc.put("FL0006", "Remote server response failure");
        engrafiRespDesc.put("FL0007", "Incomplete parameter's in request");
        engrafiRespDesc.put("FL0003", "Database Exception");
        engrafiRespDesc.put("FL0008", "Internal error");
        engrafiRespDesc.put("FL0009", "Validation of parameters failed");
        engrafiRespDesc.put("FL0012", "Invalid Credentials.");
        engrafiRespDesc.put("FL0100", "Parsing the request failed.");
        engrafiRespDesc.put("FL1002", "Failed to Add New Registration");
        engrafiRespDesc.put("FL1004", "Validation failure");
        engrafiRespDesc.put("FL1006", "No such registration record exists");
        engrafiRespDesc.put("FL1009", "MSISDN already exist");

    }

    public static void sanitizeObject(CreateSubscriberDTO createSubscriberDTO) {
        createSubscriberDTO.setMsisdn(clearEntry(createSubscriberDTO.getMsisdn()));
        createSubscriberDTO.setImsi(clearEntry(createSubscriberDTO.getImsi()));
    }

    public static String clearEntry(String entry) {
        return entry.replaceAll("\\s", "");
    }

    @Override
    public Page<CreateSubscriberDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all createsubscribers by TransactionId");
        return this.createSubscriberRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.createSubscriberMapper::toDto);
    }

    /**
     * Save a createSubscriber.
     *
     * @param createSubscriberDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CreateSubscriberDTO save(CreateSubscriberDTO createSubscriberDTO) {
        sanitizeObject(createSubscriberDTO);
        this.log.debug("Request to save CreateSubscriber : {}", createSubscriberDTO);
        CreateSubscriber createSubscriber = this.createSubscriberMapper.toEntity(createSubscriberDTO);
        createSubscriber = this.createSubscriberRepository.save(createSubscriber);
        return this.createSubscriberMapper.toDto(createSubscriber);
    }

    @PostConstruct
    void setGlobalSecurityContext() {
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    /**
     * Get all the createSubscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CreateSubscriberDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all CreateSubscribers");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.createSubscriberRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.createSubscriberMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<CreateSubscriberDTO> findAllFullPortin(Pageable pageable) {
        this.log.debug("Request to get all CreateSubscribers");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.createSubscriberRepository.findAllByCreationTypeAndCreatedByIn(SIMCreationType.FULL_PORTIN, loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.createSubscriberMapper::toDto);
    }


    /**
     * Get one createSubscriber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CreateSubscriberDTO> findOne(Long id) {
        this.log.debug("Request to get CreateSubscriber : {}", id);
        return this.createSubscriberRepository.findById(id)
            .map(this.createSubscriberMapper::toDto);
    }

    /**
     * Delete the createSubscriber by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete CreateSubscriber : {}", id);
        this.createSubscriberRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, SIMCreationType creationType, String statut, String cos, String txId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(CreateSubscriberDTO.builder()
            .msisdn("221" + aLine[0])
            .imsi("60802" + aLine[1])
            .transactionId(txId)
            .cos(cos)
            .processingStatus(ProcessingStatus.SUBMITTED)
            .creationType(creationType)
            .build()));
    }

    @Scheduled(fixedRate = 10000, initialDelay = 10000)
    public void processPending() {
        this.createSubscriberRepository.findAllByProcessingStatus(ProcessingStatus.SUBMITTED)
            .parallelStream()
            .forEach(c -> {
                this.log.info("{} processing this creation {}", LOG_HEADER, c);
                try {
                    String eventualDifferentImsiOnHss = "";
                    String imsiImsiChangeOverData = "";
                    //release sim with which the number is being createc
                    CreateSubscriberResultMsg newSubscriber = null;
                    try {
                        imsiImsiChangeOverData = this.getImsiChangeOverData(c);
                    }
                    catch (SoapFaultClientException sfc){
                        this.log.warn("{} removal failed {}", LOG_HEADER, sfc.getFaultStringOrReason());
                    }
                    if(imsiImsiChangeOverData != null && !imsiImsiChangeOverData.isEmpty()){
                        try {
                            this.simChangeRemovalService.simRemovalImsi(imsiImsiChangeOverData);
                        } catch (SoapFaultClientException sfce) {
                            this.log.warn("{} removal failed {}", LOG_HEADER, sfce.getFaultStringOrReason());
                        }
                    }
                    //release the msisdn in case it is attached to another imsi on hss
                    try {
                        eventualDifferentImsiOnHss = this.getEventualDifferentImsiOnHssAssociatedWithMsisdnToCreate(c);
                    }
                    catch (SoapFaultClientException sfc){
                        this.log.warn("{} removal failed {}", LOG_HEADER, sfc.getFaultStringOrReason());
                    }
                    if(eventualDifferentImsiOnHss != null && !eventualDifferentImsiOnHss.isEmpty()){
                        try{
                            this.hssService.deleteSubscriber(eventualDifferentImsiOnHss);
                        }catch (Exception e){
                            log.warn("{} fault while deleting on HSS imsi = {}", LOG_HEADER, eventualDifferentImsiOnHss);
                        }
                    }
                    switch (c.getCreationType()) {
                        case IN:
                            //IN
                            //Debut modif
                            this.cbs56BCClient.deleteSubscriber(c.getMsisdn().substring(3));
                            //Fin modif
                            newSubscriber = this.cbs56BCClient.createNewSubscriber(c.getMsisdn().substring(3), c.getImsi(), c.getCos());
                            createSubsCBS(c, newSubscriber);
                            break;
                        case SWITCH:
                            //AUC
                            try {
                                this.aucService.deleteSIM(c.getImsi());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createAUC(c)) break;
                            //HLR
                            try {
                                this.subscriberInfoService.deleteSubscriber(c.getImsi(), c.getMsisdn());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            try {
                                this.subscriberInfoService.createSubscriber(c.getMsisdn(), c.getImsi(), getProfileId(c.getCos()));
                                c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                            } catch (Exception e) {
                                this.log.warn("{} this fault creating sub on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                                c.setProcessingStatus(ProcessingStatus.FAILED);
                                c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
                                break;
                            }
                            break;
                        case AUC:
                            try {
                                this.aucService.deleteSIM(c.getImsi());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            try{
                                CreateResponse res = this.aucService.createSIM(c.getImsi());
                                log.info("{} AUC CREATION RESPONSE {}", c.getImsi(), res);
                                c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                            } catch (Exception e) {
                                this.log.warn("{} this fault while creating imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                                c.setProcessingStatus(ProcessingStatus.FAILED);
                                c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
                            }
                            sn.sentel.tigocare.jaxb2.auc.GetResponse res = this.aucService.getImsi(c.getImsi());
                            log.info("{} AUC DISPLAY RESPONSE {}", c.getImsi(), res);
                            break;
                        case HSS:
                            if(this.utilsService.is4gNumber(c.getImsi())){
                                try {
                                    this.hssService.deleteSubscriber(c.getImsi());
                                } catch (Exception e) {
                                    this.log.warn("{} removal imsi on hss failed {}", LOG_HEADER, e.getMessage());
                                }
                                try{
                                    this.hssService.createSubscriber(c.getImsi(), c.getMsisdn(), this.epsProfileId);
                                    c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                                }catch(Exception e){
                                    this.log.warn("{} this fault while creating imsi on hss {} => {}", LOG_HEADER, c.getImsi(), e);
                                    c.setProcessingStatus(ProcessingStatus.FAILED);
                                    c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
                                }
                            }
                            else {
                                throw new IllegalArgumentException("HSS Creation not permitted");
                            }
                            break;
                        case IN_SWITCH:
                            //AUC
                            try {
                                this.aucService.deleteSIM(c.getImsi());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createAUC(c)) break;
                            //HLR
                            try {
                                this.subscriberInfoService.deleteSubscriber(c.getImsi(), c.getMsisdn());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createHLR(c)) break;
                            //IN
                            this.cbs56BCClient.deleteSubscriber(c.getMsisdn().substring(3));
                            newSubscriber = this.cbs56BCClient.createNewSubscriber(c.getMsisdn().substring(3), c.getImsi(), c.getCos());
                            createSubsCBS(c, newSubscriber);
                            break;
                        case IN_SWITCH_HSS:
                            //AUC
                            try {
                                this.aucService.deleteSIM(c.getImsi());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createAUC(c)) break;
                            //HLR
                            try {
                                this.subscriberInfoService.deleteSubscriber(c.getImsi(), c.getMsisdn());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createHLR(c)) break;
                            //HSS
                            try{
                                this.hssService.deleteSubscriber(c.getImsi());
                            }catch (Exception e){
                                log.warn("{} fault while deleting on HSS imsi = {}", LOG_HEADER, c.getImsi());
                            }
                            if (createHSS(c)) break;
                            //IN
                            this.cbs56BCClient.deleteSubscriber(c.getMsisdn().substring(3));
                            newSubscriber = this.cbs56BCClient.createNewSubscriber(c.getMsisdn().substring(3), c.getImsi(), c.getCos());
                            createSubsCBS(c, newSubscriber);
                            break;
                        case PORTIN:
                            //AUC
                            try {
                                this.aucService.deleteSIM(c.getImsi());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createAUC(c)) break;
                            //HLR
                            try {
                                this.subscriberInfoService.deleteSubscriber(c.getImsi(), c.getMsisdn());
                            } catch (Exception e) {
                                this.log.warn("{} this fault while deleting imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                            }
                            if (createHLR(c)) break;
                            //HSS
                            try{
                                this.hssService.deleteSubscriber(c.getImsi());
                            }catch (Exception e){
                                log.warn("{} fault while deleting on HSS imsi = {}", LOG_HEADER, c.getImsi());
                            }
                            if (createHSS(c)) break;
                            //IN
                            this.cbs56BCClient.deleteSubscriber(c.getMsisdn().substring(3));
                            newSubscriber = this.cbs56BCClient.createPortedSubscriber(c.getMsisdn().substring(3), c.getImsi(), c.getCos());
                            createSubsCBS(c, newSubscriber);
                            // MNP PORTIN
                            this.log.debug("Request to save MnpPorting of : {}", c.getMsisdn());
                            ProcessingStatus processingStatus;
                            try{
                                processingStatus = this.mnpService.createNumberPortability(c.getMsisdn(), this.prefixTigo).getMOId().getMsisdn().equals(c.getMsisdn()) ? ProcessingStatus.SUCCEEDED : ProcessingStatus.FAILED;
                                if(processingStatus.equals(ProcessingStatus.FAILED)){
                                    //failure MNP PORTIN
                                    c.setProcessingStatus(ProcessingStatus.FAILED);
                                    c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation OK - PORTIN failed " + c.getBackendResp()));
                                    c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                                    return;
                                }
                                else{
                                    c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                                }
                            }
                            catch (Exception e){
                                //failure MNP PORTIN
                                c.setProcessingStatus(ProcessingStatus.FAILED);
                                c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation OK - PORTIN failed " + c.getBackendResp()));
                                c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                                return;
                            }
                            break;
                        default:
                            throw new IllegalArgumentException("Creation type unknown");
                    }
                } catch (Exception e) {
                    this.log.warn("{} this fault while subscribing this msisdn {} => {}", LOG_HEADER, c.getMsisdn(), e);
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
                } finally {
                    this.createSubscriberRepository.save(c);
                }
            });
    }

    private String getImsiChangeOverData(CreateSubscriber c){
        GetResponse res = this.imsiChangeOverCudbService.getImsiChangeOverData(c.getImsi());
        if(res.getMOAttributes() == null){
            return "";
        }
        return res.getMOAttributes().getGetIMSIChangeover().getIMSIChangeoverData().getImsi();
    }

    private String getEventualDifferentImsiOnHssAssociatedWithMsisdnToCreate(CreateSubscriber c){
        try {
            sn.sentel.tigocare.jaxb2.hss.GetResponse res = this.hssService.getSubscriberInfoHssByMsisdn(c.getMsisdn());
            if(res.getMOAttributes().getGetResponseEPSMultiSC().getImsi() == null){
                return "";
            }
            return res.getMOAttributes().getGetResponseEPSMultiSC().getImsi();
        }
        catch (Exception e){
            return "";
        }
    }

    private boolean createHSS(CreateSubscriber c) {
        if(this.utilsService.is4gNumber(c.getImsi())){
            /*if (!c.getMsisdn().startsWith("221766752") &&
            if (!c.getMsisdn().startsWith("221766752") &&
                !(allowedUsers.contains(c.getCreatedBy())
                )
            ){
                throw new IllegalArgumentException("Not an employee number");
            }*/
            try {
                this.hssService.deleteSubscriber(c.getImsi());
            } catch (Exception e) {
                this.log.warn("{} removal imsi on hss failed {}", LOG_HEADER, e.getMessage());
            }
            try{
                this.hssService.createSubscriber(c.getImsi(), c.getMsisdn(), this.epsProfileId);
                c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
            }catch(Exception e){
                this.log.warn("{} this fault while creating imsi on hss {} => {}", LOG_HEADER, c.getImsi(), e);
                c.setProcessingStatus(ProcessingStatus.FAILED);
                c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
                return true;
            }
        }
        return false;
    }

    private boolean createHLR(CreateSubscriber c) {
        try{
            this.subscriberInfoService.createSubscriber(c.getMsisdn(), c.getImsi(), getProfileId(c.getCos()));
            c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
        }
        catch (Exception e){
            this.log.warn("{} this fault while creating imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
            c.setProcessingStatus(ProcessingStatus.FAILED);
            c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
            return true;
        }
        return false;
    }

    private boolean createAUC(CreateSubscriber c) {
        try{
            this.aucService.createSIM(c.getImsi());
            c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
        } catch (Exception e) {
            this.log.warn("{} this fault while creating imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
            c.setProcessingStatus(ProcessingStatus.FAILED);
            c.setBackendResp(c.getBackendResp() + "<br/>" + e.getMessage());
            return true;
        }
        return false;
    }

    private void createSubsCBS(CreateSubscriber c, CreateSubscriberResultMsg newSubscriber) {
        if (newSubscriber != null){
            c.setBackendResp(newSubscriber.getResultHeader().getResultDesc());
            if(c.getBackendResp().length()>=255){
                c.setBackendResp(c.getBackendResp().substring(0,150));
            }
            if(newSubscriber.getResultHeader().getResultCode().equals("0")) {
                c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
            }
            else {
                c.setProcessingStatus(ProcessingStatus.FAILED);
            }
        }
        else{
            c.setBackendResp("CBS not reachable!");
            c.setProcessingStatus(ProcessingStatus.FAILED);
        }
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush() {
//        this.createSubscriberRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }


    @Override
    public String createLTE(String msisdn, String imsi) {
        try {
            this.hssService.createSubscriber(imsi, msisdn, this.epsProfileId);
        } catch (Exception e) {
            log.error("Failed {}", e.getMessage());
            return e.getMessage();
        }
        return null;

    }
        private Integer getProfileId(String cos) {
        Integer profileId = Integer.valueOf(this.cbsRepository.findByMainProductId(cos).orElseThrow(IllegalArgumentException::new).getHlrProfilId());
        log.info("HLR profile id "+ profileId);
        return profileId;
    }

    public List<CreateSubscriberDTO> fullPortinListOfSubscribers(List<CreateSubscriberDTO> createSubscriberListDTO) {
        /*createSubscriberListDTO.stream();
            .map(createSubscriberDTO -> this.createSubscriberMapper.(createSubscriberDTO))*/
        List<CreateSubscriber> createSubscriberList = this.createSubscriberMapper.toEntity(createSubscriberListDTO);
        return this.createSubscriberMapper.toDto(dofullPortinListOfSubscribers(createSubscriberList));
    }

    public List<CreateSubscriber> dofullPortinListOfSubscribers (List<CreateSubscriber> createSubscriberList){
        //Map<String,CreateSubscriber> createSubscriberHashMap = new HashMap<String,CreateSubscriber>();
        List<CreateSubscriber> subscriberCreationResponseList = new ArrayList<>();
        //Parcourir liste subss
        //foreach IDENTIFY create HSS IN HLR AUC portin
        // if one fail put it on the hashmap
        createSubscriberList
            .parallelStream()
            .forEach(c -> {
                c.setCreationType(SIMCreationType.FULL_PORTIN);
                //Creation numero NMS
                DoNmsLinkResult nmsLinkResult = this.nmsClient.doNMSLink(c.getMsisdn().substring(3), c.getImsi());
                if(!nmsLinkResult.getStatus().equals("OK")){
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setBackendResp(nmsLinkResult.getDescription());
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                //Deletion HSS
                try{
                    this.hssService.deleteSubscriber(c.getImsi());
                }
                catch (Exception e){
                    this.log.warn("{} this fault while deleting imsi on hss {} => {}", LOG_HEADER, c.getImsi(), e);
                }
                //AUC
                try {
                    this.aucService.deleteSIM(c.getImsi());
                } catch (Exception e) {
                    this.log.warn("{} this fault while deleting imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                }
                try{
                    this.aucService.createSIM(c.getImsi());
                } catch (Exception e) {
                    //failure AUC
                    this.log.warn("{} this fault while creating imsi on auc {} => {}", LOG_HEADER, c.getImsi(), e);
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    this.log.info("IMSI AUC creation failed : " + e.getMessage());
                    c.setBackendResp(("AUC creation failed : " + e.getMessage()));
                    c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                    //createSubscriberHashMap.put(c.getMsisdn(), c);
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                //HLR
                try {
                    this.subscriberInfoService.deleteSubscriber(c.getImsi(), c.getMsisdn());
                } catch (Exception e) {
                    this.log.warn("{} this fault while deleting imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                }
                try{
                    this.subscriberInfoService.createSubscriber(c.getMsisdn(), c.getImsi(), getProfileId(c.getCos()));
                } catch (Exception e) {
                    //failure HLR
                    this.log.warn("{} this fault while creating imsi on hlr {} => {}", LOG_HEADER, c.getImsi(), e);
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setBackendResp(("AUC Creation OK - HLR creation failed : "  + e.getMessage()));
                    c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                    //createSubscriberHashMap.put(c.getMsisdn(), c);
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                //Creation HSS
                if(this.utilsService.is4gNumber(c.getImsi())){
                    try{
                        this.hssService.createSubscriber(c.getImsi(), c.getMsisdn(), this.epsProfileId);
                    }catch(Exception e){
                        //failure HSS
                        this.log.warn("{} this fault while creating imsi on hss {} => {}", LOG_HEADER, c.getImsi(), e);
                        c.setProcessingStatus(ProcessingStatus.FAILED);
                        c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation failed : " + e.getMessage()));
                        c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                        //createSubscriberHashMap.put(c.getMsisdn(), c);
                        subscriberCreationResponseList.add(c);
                        this.createSubscriberRepository.save(c);
                        return;
                    }
                }
                //IN
                SubDeactivationResultMsg deleteSubscriberResultMsg = this.cbs56BCClient.deleteSubscriber(c.getMsisdn().substring(3));
                CreateSubscriberResultMsg newSubscriber  = this.cbs56BCClient.createPortedSubscriber(c.getMsisdn().substring(3), c.getImsi(), c.getCos());
                if (newSubscriber != null){
                    if(!newSubscriber.getResultHeader().getResultCode().equals("0")) {
                        //failure IN
                        //get Backend Resp
                        c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation failed : " + newSubscriber.getResultHeader().getResultDesc()));
                        c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                        c.setProcessingStatus(ProcessingStatus.FAILED);
                        //createSubscriberHashMap.put(c.getMsisdn(), c);
                        subscriberCreationResponseList.add(c);
                        this.createSubscriberRepository.save(c);
                        return;
                    }
                }else{
                    //FAILURE IN
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setBackendResp("No response from CBS");
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                // MNP PORTIN
                this.log.debug("Request to save MnpPorting of : {}", c.getMsisdn());
                ProcessingStatus processingStatus;
                try{
                    processingStatus = this.mnpService.createNumberPortability(c.getMsisdn(), this.prefixTigo).getMOId().getMsisdn().equals(c.getMsisdn()) ? ProcessingStatus.SUCCEEDED : ProcessingStatus.FAILED;
                    if(processingStatus.equals(ProcessingStatus.FAILED)){
                        //failure MNP PORTIN
                        c.setProcessingStatus(ProcessingStatus.FAILED);
                        c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation OK - PORTIN failed " + c.getBackendResp()));
                        c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                        subscriberCreationResponseList.add(c);
                        this.createSubscriberRepository.save(c);
                        return;
                    }
                }
                catch (Exception e){
                    //failure MNP PORTIN
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    c.setBackendResp(("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation OK - PORTIN failed " + c.getBackendResp()));
                    c.setBackendResp(c.getBackendResp().substring(0, Math.min(250, c.getBackendResp().length())));
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                //IdentificationRequest ENGRAFI
                String engrafiResponseCode = this.engrafiClient.doIdentification(c.getMsisdn().substring(3), c.getName(), c.getFirstName(), c.getDob(), c.getCity(), c.getPuk(), c.getTigoCashStatus(),
                    c.getGender(), c.getAltContactNum(), c.getDistrict(), c.getDocumentReferenceType(), c.getProofNumber(),
                    c.getCreateUser(), c.getCreateDate() );
                if(!engrafiResponseCode.equals("SC0000")){
                    log.info("ENGRAFI RESPONSE CODE " + engrafiResponseCode);
                    c.setBackendResp("AUC Creation OK - HLR creation OK - HSS creation OK - IN creation OK - PORTIN OK - ENGRAFI IDENTIFICATION failed : ");
                    if(this.engrafiRespDesc.containsKey(engrafiResponseCode)){
                        log.info("Getting the respDEsc");
                        c.setBackendResp((c.getBackendResp() + this.engrafiRespDesc.get(engrafiResponseCode)));
                        c.getBackendResp().substring(0, Math.min(220, c.getBackendResp().length()));
                    }
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                    //TODO
                    //gerer rollback nms auc hlr
                    subscriberCreationResponseList.add(c);
                    this.createSubscriberRepository.save(c);
                    return;
                }
                //ALL OK
                c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                c.setBackendResp("Fullportin operation successfully!");
                //addind to response list
                subscriberCreationResponseList.add(c);
                this.createSubscriberRepository.save(c);
            });
        return subscriberCreationResponseList;
    }
}
