package sn.sentel.tigocare.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.ResetLocation;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.repository.ResetLocationRepository;
import sn.sentel.tigocare.service.ResetLocationService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.ResetLocationDTO;
import sn.sentel.tigocare.service.mapper.ResetLocationMapper;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ResetLocation}.
 */
@Service
@Transactional
public class ResetLocationServiceImpl implements ResetLocationService {

    private final Logger log = LoggerFactory.getLogger(ResetLocationServiceImpl.class);

    private final ResetLocationRepository resetLocationRepository;

    private final ResetLocationMapper resetLocationMapper;

    private final SubscriberInfoService hlr;
    private final UserService userService;

//    @Override
//    public Page<ResetLocationDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
//        return null;
//    }

    public ResetLocationServiceImpl(ResetLocationRepository resetLocationRepository, ResetLocationMapper resetLocationMapper, SubscriberInfoService hlr, UserService userService) {
        this.resetLocationRepository = resetLocationRepository;
        this.resetLocationMapper = resetLocationMapper;
        this.hlr = hlr;
        this.userService = userService;
    }

    public static void sanitizeObject(ResetLocationDTO resetLocationDTO){
        resetLocationDTO.setMsisdn(clearEntry(resetLocationDTO.getMsisdn()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a resetLocation.
     *
     * @param resetLocationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ResetLocationDTO save(ResetLocationDTO resetLocationDTO) {
        sanitizeObject(resetLocationDTO);
        this.log.debug("Request to save ResetLocation : {}", resetLocationDTO);
        ResetLocation resetLocation = this.resetLocationMapper.toEntity(resetLocationDTO);
        try{
            this.hlr.cancelVLR(resetLocation.getMsisdn());
            resetLocation.setProcessingStatus(ProcessingStatus.SUCCEEDED);
        }catch (Exception e){
            resetLocation.setProcessingStatus(ProcessingStatus.FAILED);
        }finally {
            resetLocation = this.resetLocationRepository.save(resetLocation);
        }
        return this.resetLocationMapper.toDto(resetLocation);
    }

    /**
     * Get all the resetLocations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ResetLocationDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all ResetLocations");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.resetLocationRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.resetLocationMapper::toDto);
    }


    /**
     * Get one resetLocation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ResetLocationDTO> findOne(Long id) {
        this.log.debug("Request to get ResetLocation : {}", id);
        return this.resetLocationRepository.findById(id)
            .map(this.resetLocationMapper::toDto);
    }

    /**
     * Delete the resetLocation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete ResetLocation : {}", id);
        this.resetLocationRepository.deleteById(id);
    }
}
