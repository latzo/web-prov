package sn.sentel.tigocare.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.MnpPortout;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.mnp.services.MnpService;
import sn.sentel.tigocare.repository.MnpPortoutRepository;
import sn.sentel.tigocare.service.MnpPortoutService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.MnpPortoutDTO;
import sn.sentel.tigocare.service.mapper.MnpPortoutMapper;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MnpPortout}.
 */
@Service
@Transactional
public class MnpPortoutServiceImpl implements MnpPortoutService {

    private final Logger log = LoggerFactory.getLogger(MnpPortoutServiceImpl.class);

    private final MnpPortoutRepository mnpPortoutRepository;

    private final MnpPortoutMapper mnpPortoutMapper;

    private final MnpService mnpService;

    @Value("${app.ws.mnp.prefixOrange}")
    private String prefixOrange;

    @Value("${app.ws.mnp.prefixExpresso}")
    private String prefixExpresso;
    private final UserService userService;

    public MnpPortoutServiceImpl(MnpPortoutRepository mnpPortoutRepository, MnpPortoutMapper mnpPortoutMapper, MnpService mnpService, UserService userService) {
        this.mnpPortoutRepository = mnpPortoutRepository;
        this.mnpPortoutMapper = mnpPortoutMapper;
        this.mnpService = mnpService;
        this.userService = userService;
    }

    public static void sanitizeObject(MnpPortoutDTO mnpPortoutDTO){
        mnpPortoutDTO.setMsisdn(clearEntry(mnpPortoutDTO.getMsisdn()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a mnpPortout.
     *
     * @param mnpPortoutDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MnpPortoutDTO save(MnpPortoutDTO mnpPortoutDTO) {
        sanitizeObject(mnpPortoutDTO);
        this.log.debug("Request to save MnpPortout : {}", mnpPortoutDTO);
        String prefix = "";
        String processingStatus;
        if(mnpPortoutDTO.getOperateur().equalsIgnoreCase("orange")) {
            prefix = this.prefixOrange;
        }
        if(mnpPortoutDTO.getOperateur().equalsIgnoreCase("expresso")) {
            prefix = this.prefixExpresso;
        }
        try{
            processingStatus = this.mnpService.createNumberPortability(mnpPortoutDTO.getMsisdn(), prefix).getMOId().getMsisdn().equals(mnpPortoutDTO.getMsisdn()) ? "SUCCEEDED" : "FAILED";
        }
        catch (Exception e){
            processingStatus = ProcessingStatus.FAILED.toString();
        }
        mnpPortoutDTO.setProcessingStatus(processingStatus);
        MnpPortout mnpPortout = this.mnpPortoutMapper.toEntity(mnpPortoutDTO);
        mnpPortout = this.mnpPortoutRepository.save(mnpPortout);
        return this.mnpPortoutMapper.toDto(mnpPortout);
    }

    /**
     * Get all the mnpPortouts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MnpPortoutDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all MnpPortouts");
//        return this.mnpPortoutRepository.findAll(pageable)
//            .map(this.mnpPortoutMapper::toDto);
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.mnpPortoutRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.mnpPortoutMapper::toDto);
    }


    /**
     * Get one mnpPortout by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MnpPortoutDTO> findOne(Long id) {
        this.log.debug("Request to get MnpPortout : {}", id);
        return this.mnpPortoutRepository.findById(id)
            .map(this.mnpPortoutMapper::toDto);
    }

    /**
     * Delete the mnpPortout by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete MnpPortout : {}", id);
        this.mnpPortoutRepository.deleteById(id);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.mnpPortoutRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
