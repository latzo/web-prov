package sn.sentel.tigocare.service.impl;

import sn.sentel.tigocare.service.MainProductsCbsService;
import sn.sentel.tigocare.domain.MainProductsCbs;
import sn.sentel.tigocare.repository.MainProductsCbsRepository;
import sn.sentel.tigocare.service.dto.MainProductsCbsDTO;
import sn.sentel.tigocare.service.mapper.MainProductsCbsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MainProductsCbs}.
 */
@Service
@Transactional
public class MainProductsCbsServiceImpl implements MainProductsCbsService {

    private final Logger log = LoggerFactory.getLogger(MainProductsCbsServiceImpl.class);

    private final MainProductsCbsRepository mainProductsCbsRepository;

    private final MainProductsCbsMapper mainProductsCbsMapper;

    public MainProductsCbsServiceImpl(MainProductsCbsRepository mainProductsCbsRepository, MainProductsCbsMapper mainProductsCbsMapper) {
        this.mainProductsCbsRepository = mainProductsCbsRepository;
        this.mainProductsCbsMapper = mainProductsCbsMapper;
    }

    /**
     * Save a mainProductsCbs.
     *
     * @param mainProductsCbsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MainProductsCbsDTO save(MainProductsCbsDTO mainProductsCbsDTO) {
        log.debug("Request to save MainProductsCbs : {}", mainProductsCbsDTO);
        MainProductsCbs mainProductsCbs = mainProductsCbsMapper.toEntity(mainProductsCbsDTO);
        mainProductsCbs = mainProductsCbsRepository.save(mainProductsCbs);
        return mainProductsCbsMapper.toDto(mainProductsCbs);
    }

    /**
     * Get all the mainProductsCbs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MainProductsCbsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MainProductsCbs");
        return mainProductsCbsRepository.findAll(pageable)
            .map(mainProductsCbsMapper::toDto);
    }


    /**
     * Get one mainProductsCbs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MainProductsCbsDTO> findOne(Long id) {
        log.debug("Request to get MainProductsCbs : {}", id);
        return mainProductsCbsRepository.findById(id)
            .map(mainProductsCbsMapper::toDto);
    }

    /**
     * Delete the mainProductsCbs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MainProductsCbs : {}", id);
        mainProductsCbsRepository.deleteById(id);
    }
}
