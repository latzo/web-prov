package sn.sentel.tigocare.service.impl;

import sn.sentel.tigocare.service.CosService;
import sn.sentel.tigocare.domain.Cos;
import sn.sentel.tigocare.repository.CosRepository;
import sn.sentel.tigocare.service.dto.CosDTO;
import sn.sentel.tigocare.service.mapper.CosMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Cos}.
 */
@Service
@Transactional
public class CosServiceImpl implements CosService {

    private final Logger log = LoggerFactory.getLogger(CosServiceImpl.class);

    private final CosRepository cosRepository;

    private final CosMapper cosMapper;

    public CosServiceImpl(CosRepository cosRepository, CosMapper cosMapper) {
        this.cosRepository = cosRepository;
        this.cosMapper = cosMapper;
    }

    /**
     * Save a cos.
     *
     * @param cosDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CosDTO save(CosDTO cosDTO) {
        log.debug("Request to save Cos : {}", cosDTO);
        Cos cos = cosMapper.toEntity(cosDTO);
        cos = cosRepository.save(cos);
        return cosMapper.toDto(cos);
    }

    /**
     * Get all the cos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CosDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cos");
        return cosRepository.findAll(pageable)
            .map(cosMapper::toDto);
    }


    /**
     * Get one cos by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CosDTO> findOne(Long id) {
        log.debug("Request to get Cos : {}", id);
        return cosRepository.findById(id)
            .map(cosMapper::toDto);
    }

    /**
     * Delete the cos by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Cos : {}", id);
        cosRepository.deleteById(id);
    }
}
