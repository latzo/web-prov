package sn.sentel.tigocare.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.MnpPorting;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.mnp.services.MnpService;
import sn.sentel.tigocare.repository.MnpPortingRepository;
import sn.sentel.tigocare.service.MnpPortingService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.MnpPortingDTO;
import sn.sentel.tigocare.service.mapper.MnpPortingMapper;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link MnpPorting}.
 */
@Service
@Transactional
public class MnpPortingServiceImpl implements MnpPortingService {

    private final Logger log = LoggerFactory.getLogger(MnpPortingServiceImpl.class);

    private final MnpPortingRepository mnpPortingRepository;

    private final MnpPortingMapper mnpPortingMapper;

    private final MnpService mnpService;

    @Value("${app.ws.mnp.prefixTigo}")
    private String prefixTigo;
    private final UserService userService;

    public MnpPortingServiceImpl(MnpPortingRepository mnpPortingRepository, MnpPortingMapper mnpPortingMapper, MnpService mnpService, UserService userService) {
        this.mnpPortingRepository = mnpPortingRepository;
        this.mnpPortingMapper = mnpPortingMapper;
        this.mnpService = mnpService;
        this.userService = userService;
    }

    public static void sanitizeObject(MnpPortingDTO mnpPortingDTO){
        mnpPortingDTO.setMsisdn(clearEntry(mnpPortingDTO.getMsisdn()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a mnpPorting.
     *
     * @param mnpPortingDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MnpPortingDTO save(MnpPortingDTO mnpPortingDTO) {
        sanitizeObject(mnpPortingDTO);
        this.log.debug("Request to save MnpPorting : {}", mnpPortingDTO);
        String processingStatus;
        try{
            processingStatus = this.mnpService.createNumberPortability(mnpPortingDTO.getMsisdn(), this.prefixTigo).getMOId().getMsisdn().equals(mnpPortingDTO.getMsisdn()) ? "SUCCEEDED" : "FAILED";
        }
        catch (Exception e){
            processingStatus = ProcessingStatus.FAILED.toString();
        }
        mnpPortingDTO.setProcessingStatus(processingStatus);
        MnpPorting mnpPorting = this.mnpPortingMapper.toEntity(mnpPortingDTO);
        mnpPorting = this.mnpPortingRepository.save(mnpPorting);
        return this.mnpPortingMapper.toDto(mnpPorting);
    }

    /**
     * Get all the mnpPortings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MnpPortingDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all MnpPortings");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.mnpPortingRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.mnpPortingMapper::toDto);
    }


//    @Override
//    public Page<MnpPortingDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
//        this.log.debug("Request to get all apnchangges by TransactionId");
//        return this.mnpPortingRepository.findAllByTransactionId(transactionId, pageable)
//            .map(this.mnp::toDto);
//    }

    /**
     * Get one mnpPorting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MnpPortingDTO> findOne(Long id) {
        this.log.debug("Request to get MnpPorting : {}", id);
        return this.mnpPortingRepository.findById(id)
            .map(this.mnpPortingMapper::toDto);
    }

    /**
     * Delete the mnpPorting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete MnpPorting : {}", id);
        this.mnpPortingRepository.deleteById(id);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.mnpPortingRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
