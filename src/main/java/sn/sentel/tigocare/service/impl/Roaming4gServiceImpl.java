package sn.sentel.tigocare.service.impl;

import com.opencsv.*;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.Roaming4gEnum;
import sn.sentel.tigocare.domain.enumeration.RoamingServiceEnum;
import sn.sentel.tigocare.hlr.services.hss.clients.HssClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.service.Roaming4gService;
import sn.sentel.tigocare.domain.Roaming4g;
import sn.sentel.tigocare.repository.Roaming4gRepository;
import sn.sentel.tigocare.service.dto.Roaming4gDTO;
import sn.sentel.tigocare.service.dto.RoamingServiceDTO;
import sn.sentel.tigocare.service.mapper.Roaming4gMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Roaming4g}.
 */
@Service
@Transactional
public class Roaming4gServiceImpl implements Roaming4gService {

    private final Logger log = LoggerFactory.getLogger(Roaming4gServiceImpl.class);

    private final Roaming4gRepository roaming4gRepository;
    private final Roaming4gMapper roaming4gMapper;
    private final HssClient hssClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;
    @Value("${app.ws.password}")
    private String pwd;

    private final ICSVParser csvParser;

    public Roaming4gServiceImpl(Roaming4gRepository roaming4gRepository, Roaming4gMapper roaming4gMapper, HssClient hssClient, SessionControl sessionControl) {
        this.roaming4gRepository = roaming4gRepository;
        this.roaming4gMapper = roaming4gMapper;
        this.hssClient = hssClient;
        this.sessionControl = sessionControl;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
    }

    /**
     * Save a roaming4g.
     *
     * @param roaming4gDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Roaming4gDTO save(Roaming4gDTO roaming4gDTO) {
        log.debug("Request to save Roaming4g : {}", roaming4gDTO);
        Roaming4g roaming4g = roaming4gMapper.toEntity(roaming4gDTO);
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        this.doRoaming4GChangePostpaid(roaming4g.getImsi(), roaming4g.getServices(), sessionId);
        roaming4g = roaming4gRepository.save(roaming4g);
        return roaming4gMapper.toDto(roaming4g);
    }

    private void doRoaming4GChangePostpaid(String imsi, Roaming4gEnum services, String sessionId) {
        switch (services) {
            case F4G_ROAMING:
                this.hssClient.changeServiceActivateRoaming4g(imsi, sessionId);
                break;
            case NO_4G_ROAMING:
                this.hssClient.changeServiceBarreRoaming4g(imsi, sessionId);
                break;
        }

    }


    /**
     * Get all the roaming4gs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Roaming4gDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Roaming4gs");
        return roaming4gRepository.findAll(pageable)
            .map(roaming4gMapper::toDto);
    }


    /**
     * Get one roaming4g by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Roaming4gDTO> findOne(Long id) {
        log.debug("Request to get Roaming4g : {}", id);
        return roaming4gRepository.findById(id)
            .map(roaming4gMapper::toDto);
    }

    /**
     * Delete the roaming4g by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Roaming4g : {}", id);
        roaming4gRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, String serviceBulk, String txId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(Roaming4gDTO.builder()
            .imsi(aLine[0])
            .processingStatus(ProcessingStatus.SUBMITTED)
            .services(Roaming4gEnum.valueOf(serviceBulk))
            .build()));
    }
}
