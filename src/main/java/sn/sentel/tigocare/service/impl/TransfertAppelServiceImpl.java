package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.jaxb2.subscriberinfo.SetResponse;
import sn.sentel.tigocare.service.TransfertAppelService;
import sn.sentel.tigocare.domain.TransfertAppel;
import sn.sentel.tigocare.repository.TransfertAppelRepository;
import sn.sentel.tigocare.service.dto.TransfertAppelDTO;
import sn.sentel.tigocare.service.mapper.TransfertAppelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TransfertAppel}.
 */
@Service
@Transactional
public class TransfertAppelServiceImpl implements TransfertAppelService {

    private final Logger log = LoggerFactory.getLogger(TransfertAppelServiceImpl.class);

    private static final String LOG_HEADER = "[TRANSFERT APPEL SERVICE]";

    private final TransfertAppelRepository transfertAppelRepository;

    private final TransfertAppelMapper transfertAppelMapper;

    private final SubscriberInfoService subscriberInfoService;

    private final CSVParser csvParser;

    public TransfertAppelServiceImpl(TransfertAppelRepository transfertAppelRepository, TransfertAppelMapper transfertAppelMapper, SubscriberInfoService subscriberInfoService) {
        this.transfertAppelRepository = transfertAppelRepository;
        this.transfertAppelMapper = transfertAppelMapper;
        this.subscriberInfoService = subscriberInfoService;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
    }

    /**
     * Save a transfertAppel.
     *
     * @param transfertAppelDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TransfertAppelDTO save(TransfertAppelDTO transfertAppelDTO) {
        log.debug("Request to save TransfertAppel : {}", transfertAppelDTO);
        TransfertAppel transfertAppel = transfertAppelMapper.toEntity(transfertAppelDTO);
        transfertAppel.setProcessingStatus(ProcessingStatus.SUBMITTED);
        transfertAppel = transfertAppelRepository.save(transfertAppel);
        return transfertAppelMapper.toDto(transfertAppel);
    }

    /**
     * Get all the transfertAppels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TransfertAppelDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TransfertAppels");
        return transfertAppelRepository.findAll(pageable)
            .map(transfertAppelMapper::toDto);
    }

    @Override
    public Page<TransfertAppelDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all createsubscribers by TransactionId");
        return this.transfertAppelRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.transfertAppelMapper::toDto);
    }

    /**
     * Get one transfertAppel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TransfertAppelDTO> findOne(Long id) {
        log.debug("Request to get TransfertAppel : {}", id);
        return transfertAppelRepository.findById(id)
            .map(transfertAppelMapper::toDto);
    }

    /**
     * Delete the transfertAppel by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TransfertAppel : {}", id);
        transfertAppelRepository.deleteById(id);
    }

    @Scheduled(fixedRate = 10000, initialDelay = 10000)
    public void processPending() {
        this.transfertAppelRepository.findAllByProcessingStatus(ProcessingStatus.SUBMITTED)
            .parallelStream()
            .forEach(c -> {
                SetResponse response = null;
                this.log.info("{} processing this TRANSFERT APPEL {}", LOG_HEADER, c);
                try {
                    response = this.subscriberInfoService.doTransfertAppelVersAutreNum(c.getMsisdnSource(), c.getMsisdnDest(), c.getOperation());
                    c.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                    if(response == null){
                        c.setProcessingStatus(ProcessingStatus.FAILED);
                        c.setBackendResp("HLR connection failed");
                        this.transfertAppelRepository.save(c);
                    }
                }catch (Exception e){
                    c.setBackendResp(e.getMessage());
                    c.setBackendResp(c.getBackendResp().substring(0, Math.min(c.getBackendResp().length(), 200)));
                    c.setProcessingStatus(ProcessingStatus.FAILED);
                }
                finally {
                    this.transfertAppelRepository.save(c);
                }
            });
    }

    @Override
    public void bulk(InputStream inputStream, OperationEnum operationEnum, String txId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(TransfertAppelDTO.builder()
            .msisdnSource("221" + aLine[0])
            .msisdnDest("221" + aLine[1])
            .operation(operationEnum)
            .transactionId(txId)
            .processingStatus(ProcessingStatus.SUBMITTED.toString())
            .build()));
    }

}
