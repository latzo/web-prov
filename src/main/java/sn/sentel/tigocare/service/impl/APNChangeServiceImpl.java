package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.ICSVParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.APNChange;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;
import sn.sentel.tigocare.repository.APNChangeRepository;
import sn.sentel.tigocare.service.APNChangeService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.APNChangeDtoVTalend;
import sn.sentel.tigocare.service.mapper.APNChangeMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Service Implementation for managing {@link APNChange}.
 */
@Service
@Transactional
public class APNChangeServiceImpl implements APNChangeService {

    private final Logger log = LoggerFactory.getLogger(APNChangeServiceImpl.class);
    private final APNChangeRepository aPNChangeRepository;
    private final APNChangeMapper aPNChangeMapper;
    private final SubscriberInfoClient hlr;
    private final SessionControl sessionControl;
    private final UserService userService;
    @Value("${app.ws.login}")
    private String userId;
    @Value("${app.ws.password}")
    private String pwd;

    @Value("${app.talend.host}")
    private String talendServiceUrl;
    private final ICSVParser csvParser;

    public APNChangeServiceImpl(APNChangeRepository aPNChangeRepository, APNChangeMapper aPNChangeMapper, SubscriberInfoClient hlr, SessionControl sessionControl, UserService userService) {
        this.aPNChangeRepository = aPNChangeRepository;
        this.aPNChangeMapper = aPNChangeMapper;
        this.hlr = hlr;
        this.sessionControl = sessionControl;
        this.userService = userService;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
    }

    @Override
    public Page<APNChangeDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all apnchangges by TransactionId");
        return this.aPNChangeRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.aPNChangeMapper::toDto);
    }

    public static void sanitizeObject(APNChangeDTO apnChangeDTO ){
        apnChangeDTO.setMsisdn(clearEntry(apnChangeDTO.getMsisdn()));
        apnChangeDTO.setImsi(clearEntry(apnChangeDTO.getImsi()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a aPNChange.
     *
     * @param aPNChangeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public APNChangeDTO save(APNChangeDTO aPNChangeDTO) {
        sanitizeObject(aPNChangeDTO);
        this.log.debug("Request to save APNChange : {}", aPNChangeDTO);
        APNChange aPNChange = this.aPNChangeMapper.toEntity(aPNChangeDTO);
        aPNChange = this.aPNChangeRepository.save(aPNChange);
        return this.aPNChangeMapper.toDto(aPNChange);
    }

    /**
     * Get all the aPNChanges.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<APNChangeDTO> findAll(Pageable pageable) {

        /*return this.aPNChangeRepository.findAll().stream()
            .map(this.aPNChangeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));*/

        this.log.debug("Request to get all APNChanges");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.aPNChangeRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.aPNChangeMapper::toDto);

    }


    /**
     * Get one aPNChange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<APNChangeDTO> findOne(Long id) {
        this.log.debug("Request to get APNChange : {}", id);
        return this.aPNChangeRepository.findById(id)
            .map(this.aPNChangeMapper::toDto);
    }

    /**
     * Delete the aPNChange by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete APNChange : {}", id);
        this.aPNChangeRepository.deleteById(id);
    }

    @Scheduled(fixedRate = 10000)
    public void process(){
        this.aPNChangeRepository.findAllByProcessingStatus(ProcessingStatus.SUBMITTED)
            .parallelStream()
            .forEach(apnChange -> {
                String sessionId = this.sessionControl.login(this.userId, this.pwd);
                if (sessionId != null) {
                    try {
                        this.hlr.changeAPN(apnChange.getMsisdn(), apnChange.getOperation(), apnChange.getApn(), apnChange.getProfil(), sessionId);
                        apnChange.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                    }catch (Exception e){
                        apnChange.setProcessingStatus(ProcessingStatus.FAILED);
                    }finally {
                        this.aPNChangeRepository.save(apnChange);
                    }
                }
                this.sessionControl.logout(sessionId);
            });
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.aPNChangeRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }

    @Override
    public void bulk(InputStream inputStream, String apn, String profil, OperationEnum operation, String txId) throws IOException, RuntimeException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(APNChangeDTO.builder()
            .msisdn("221"+aLine[0])
            .apn(apn)
            .profil(profil)
            .imsi("")
            .operation(operation)
            .transactionId(txId)
            .processingStatus(ProcessingStatus.SUBMITTED)
            .build()));
    }

    private APNChangeDTO activateOrDeactivateApn(APNChangeDtoVTalend aPNChangeDTO){
        sanitizeObject(aPNChangeDTO);
        String url = talendServiceUrl;

        if (aPNChangeDTO.getOperation().equals(OperationEnum.ACTIVATE)){
            // url += "/osb/services/lifecycle/core?serviceHlr=ACTIVATE-APN&msisdn=" + aPNChangeDTO.getMsisdn() + "&apn_name=" + aPNChangeDTO.getApn();
            url += "/services/osb/services/lifecycle/core?serviceHlr=ACTIVATE-APN&msisdn=" + aPNChangeDTO.getMsisdn() + "&apn_name=" + aPNChangeDTO.getApn();

            if(aPNChangeDTO.getIpAddr() != null && !aPNChangeDTO.getIpAddr().trim().equals("")){
                url+= "&ip_addr=" + aPNChangeDTO.getIpAddr();
            }
        }
        else {
            // url += "/osb/services/lifecycle/core?serviceHlr=DEACTIVATE-APN&msisdn=" + aPNChangeDTO.getMsisdn() + "&apn_name=" + aPNChangeDTO.getApn();
            url += "/services/osb/services/lifecycle/core?serviceHlr=DEACTIVATE-APN&msisdn=" + aPNChangeDTO.getMsisdn() + "&apn_name=" + aPNChangeDTO.getApn();
        }

        if(aPNChangeDTO.getTechnology() != null && !aPNChangeDTO.getTechnology().trim().equals("")){
            url+= "&network=" + aPNChangeDTO.getTechnology();
        }

        System.out.println("========================================="+ url +"==========================================");

        OkHttpClient httpClient = new OkHttpClient.Builder().readTimeout(10, TimeUnit.SECONDS).build();
        Request request = new Request.Builder().url(url).build();

        int indexOfResultMessage;
        String resultMessage = null;

        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            String responseString = Objects.requireNonNull(response.body()).string().replaceAll("\n|\r", "").trim();
            int indexOfResultCode = ordinalIndexOf(responseString, "value", 0) + 7;
            String resultCode = responseString.substring(indexOfResultCode, indexOfResultCode + 1);

            indexOfResultMessage = ordinalIndexOf(responseString, "value", 1) + 7;
            resultMessage = responseString.substring(indexOfResultMessage, responseString.length() - 9);

            if ("0".equals(resultCode)) {
                aPNChangeDTO.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                aPNChangeDTO.setErrorMessage(resultMessage);
            }
            else {
                aPNChangeDTO.setProcessingStatus(ProcessingStatus.FAILED);
                aPNChangeDTO.setErrorMessage(resultMessage);
            }
        }
        catch (IOException | RuntimeException e) {
            aPNChangeDTO.setProcessingStatus(ProcessingStatus.FAILED);
            aPNChangeDTO.setErrorMessage(e.getMessage());
        }

        return aPNChangeDTO;
    }

    // SINGLE SAVE: ACTIVATE OR DEACTIVATE
    @Override
    public APNChangeDTO saveApnThroughTalend(APNChangeDtoVTalend aPNChangeDTO) {
        APNChangeDTO apnChangeDto = activateOrDeactivateApn(aPNChangeDTO);
        APNChange apnChange = this.aPNChangeRepository.save(this.aPNChangeMapper.toEntity(apnChangeDto));
        return this.aPNChangeMapper.toDto(apnChange);
    }

    // BULK SAVE
    @Override
    public void saveApnThroughTalend(InputStream inputStream, String apn, String technology, String operation) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> {
            if(!aLine[0].trim().isEmpty()){
                APNChangeDtoVTalend aPNChangeDTO = new APNChangeDtoVTalend();
                aPNChangeDTO.setMsisdn(aLine[0]);
                aPNChangeDTO.setImsi(aLine[1]);
                aPNChangeDTO.setApn(apn);
                aPNChangeDTO.setIpAddr(aLine.length <= 2 ? "" : aLine[2]);
                aPNChangeDTO.setTechnology(technology);
                aPNChangeDTO.setOperation(OperationEnum.valueOf(operation));
                aPNChangeDTO.setProfil("profile");
                aPNChangeDTO.setTransactionId(UUID.randomUUID().toString());

                APNChangeDTO apnChangeDto = activateOrDeactivateApn(aPNChangeDTO);
                this.aPNChangeRepository.save(this.aPNChangeMapper.toEntity(apnChangeDto));
            }
        });
    }

    // RETURN INDEX OF N°th SUBSTRING CONTAINING IN A GIVEN STRING
    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = -1;
        do {
            pos = str.indexOf(substr, pos + 1);
        } while (n-- > 0 && pos != -1);
        return pos;
    }

}
