package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.SimSwap;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.auc.services.AUCService;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBS56BCClient;
import sn.sentel.tigocare.hlr.services.hss.services.HssService;
import sn.sentel.tigocare.hlr.services.simchange.services.SimChangeService;
import sn.sentel.tigocare.hlr.services.simchangeremoval.services.SimChangeRemovalService;
import sn.sentel.tigocare.jaxb2.hss.CreateResponse;
import sn.sentel.tigocare.jaxb2.cbs56.bc.ChangeSubIdentityResultMsg;
import sn.sentel.tigocare.repository.SimSwapRepository;
import sn.sentel.tigocare.service.SimSwapService;
import sn.sentel.tigocare.service.UtilsService;
import sn.sentel.tigocare.service.dto.SimSwapDTO;
import sn.sentel.tigocare.service.mapper.SimSwapMapper;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link SimSwap}.
 */
@Service
@Transactional
public class SimSwapServiceImpl implements SimSwapService {

    @Value("${app.ws.hss.epsProfileId}")
    private String epsProfileId;

    private static final String LOG_HEADER = "[SIM CHANGE OVER SERVICE]";
    private final Logger log = LoggerFactory.getLogger(SimSwapServiceImpl.class);

    private final SimSwapRepository simSwapRepository;
    private final SimSwapMapper simSwapMapper;
    private final SimChangeService simChangeService;
    private final SimChangeRemovalService simChangeRemovalService;
    private final AUCService aucService;
    private final HssService hssService;
    private final CSVParser csvParser;
    private final CBS56BCClient cbs56BCClient;
    @Autowired
    private final UtilsService utilsService;

    public SimSwapServiceImpl(SimSwapRepository simSwapRepository, SimSwapMapper simSwapMapper, SimChangeService simChangeService, AUCService aucService, SimChangeRemovalService simChangeRemovalService, HssService hssService, CBS56BCClient cbs56BCClient, UtilsService utilsService) {
        this.simSwapRepository = simSwapRepository;
        this.simSwapMapper = simSwapMapper;
        this.simChangeService = simChangeService;
        this.hssService = hssService;
        this.cbs56BCClient = cbs56BCClient;
        this.utilsService = utilsService;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
        this.aucService = aucService;
        this.simChangeRemovalService = simChangeRemovalService;
    }

    @PostConstruct
    void setGlobalSecurityContext() {
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    public static void sanitizeObject(SimSwapDTO simSwapDTO) {
        simSwapDTO.setMsisdn(clearEntry(simSwapDTO.getMsisdn()));
        simSwapDTO.setNewIMSI(clearEntry(simSwapDTO.getNewIMSI()));
        simSwapDTO.setOldIMSI(clearEntry(simSwapDTO.getOldIMSI()));
    }

    public static String clearEntry(String entry) {
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a simSwap.
     *
     * @param simSwapDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SimSwapDTO save(SimSwapDTO simSwapDTO) {
        sanitizeObject(simSwapDTO);
        this.log.debug("Request to save SimSwap : {}", simSwapDTO);
        SimSwap simSwap = this.simSwapMapper.toEntity(simSwapDTO);
        simSwap = this.simSwapRepository.save(simSwap);
        return this.simSwapMapper.toDto(simSwap);
    }

    /**
     * Get all the simSwaps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SimSwapDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all SimSwaps");
        return this.simSwapRepository.findAll(pageable)
            .map(this.simSwapMapper::toDto);
    }


    @Override
    public Page<SimSwapDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all SimSwaps by TransactionId");
        return this.simSwapRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.simSwapMapper::toDto);
    }

    /**
     * Get one simSwap by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SimSwapDTO> findOne(Long id) {
        this.log.debug("Request to get SimSwap : {}", id);
        return this.simSwapRepository.findById(id)
            .map(this.simSwapMapper::toDto);
    }

    /**
     * Delete the simSwap by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete SimSwap : {}", id);
        this.simSwapRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, String txId) throws IOException {
        SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(SimSwapDTO.builder()
            .msisdn("221" + aLine[0])
            .oldIMSI("60802" + aLine[1])
            .newIMSI("60802" + aLine[2])
            .transactionId(txId)
            .processingStatus(ProcessingStatus.SUBMITTED)
            .build()));
    }

    @Override
    public SimSwapDTO doSimswap(SimSwapDTO simSwapDTO) {

        SimSwap simSwap = this.simSwapMapper.toEntity(simSwapDTO);
        return doSimSwap2(simSwap);
    }

    private SimSwapDTO doSimSwap2(SimSwap simSwap) {
        this.log.info("{} processing this sim swap {}", LOG_HEADER, simSwap);
        try {
            //delete new imsi auc
            try {
                this.aucService.deleteSIM(simSwap.getNewIMSI());
            } catch (Exception e) {
                this.log.warn("{} removal failed {}", LOG_HEADER, e.getMessage());
            }

            //create new imsi auc
            //throws exception when KI NOT FOUND not catched but it s blocking so normal to stop the process with exception and failure
            this.aucService.createSIM(simSwap.getNewIMSI());

            //imsi change over removal with msisdn
            try {
                this.simChangeRemovalService.simRemoval(simSwap.getMsisdn());
            } catch (SoapFaultClientException sfce) {
                this.log.warn("{} removal failed {}", LOG_HEADER, sfce.getFaultStringOrReason());
            }

            //sim change on hlr
            //throws exception when KI NOT FOUND not catched but it s blocking so normal to stop the process with exception and failure
            this.simChangeService.changeSIM(simSwap.getMsisdn(), simSwap.getNewIMSI(), simSwap.getOldIMSI());

            //perform imsi change over removal of old imsi
            try {
                this.simChangeRemovalService.simRemovalImsi(simSwap.getOldIMSI());
            } catch (SoapFaultClientException sfce) {
                this.log.warn("{} removal failed {}", LOG_HEADER, sfce.getFaultStringOrReason());
            }

            //delete old imsi auc
            //throws exception when error
            this.aucService.deleteSIM(simSwap.getOldIMSI());

            //if 4g number
            if(this.utilsService.is4gNumber(simSwap.getNewIMSI())){
                //delete old imsi hss
                try {
                    this.hssService.deleteSubscriber(simSwap.getOldIMSI());
                } catch (Exception e) {
                    this.log.warn("{} removal old imsi on hss failed {}", LOG_HEADER, e.getMessage());
                }
                //delete new imsi hss
                try {
                    this.hssService.deleteSubscriber(simSwap.getNewIMSI());
                } catch (Exception e) {
                    this.log.warn("{} removal new imsi on hss failed so it can be created {}", LOG_HEADER, e.getMessage());
                }
                //create new imsi hss
                CreateResponse res = this.hssService.createSubscriber(simSwap.getNewIMSI(), simSwap.getMsisdn(), this.epsProfileId);
                if(!res.getMOId().getImsi().equals(simSwap.getNewIMSI())){
                    throw new IllegalArgumentException("HSS creation failed");
                }
            }
            //change sim iN CBS
            ChangeSubIdentityResultMsg changeSIMResultMsg = this.cbs56BCClient.changeSIM(simSwap.getMsisdn().substring(3), simSwap.getOldIMSI(), simSwap.getNewIMSI());
            if (!changeSIMResultMsg.getResultHeader().getResultCode().equals("0")) {
                simSwap.setBackendResp(changeSIMResultMsg.getResultHeader().getResultCode() + " - " + changeSIMResultMsg.getResultHeader().getResultDesc());
                simSwap.setProcessingStatus(ProcessingStatus.FAILED);
            }
            else{
                simSwap.setProcessingStatus(ProcessingStatus.SUCCEEDED);
            }
            //fin modif
        } catch (Exception sfce) {
            this.log.warn("{} this fault while changing sim for {} => {}", LOG_HEADER, simSwap.getMsisdn(), sfce);
            simSwap.setProcessingStatus(ProcessingStatus.FAILED);
            simSwap.setBackendResp(sfce.getMessage());

        } finally {
            this.log.info("{} saving simswap {}", LOG_HEADER, simSwap.toString());
            this.simSwapRepository.save(simSwap);
        }
        return this.simSwapMapper.toDto(simSwap);
    }

    @Scheduled(fixedRate = 10000)
    public void processPending() {
        this.simSwapRepository.findAllByProcessingStatus(ProcessingStatus.SUBMITTED)
            .parallelStream()
            .forEach(this::doSimSwap2);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush() {
//        this.simSwapRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
