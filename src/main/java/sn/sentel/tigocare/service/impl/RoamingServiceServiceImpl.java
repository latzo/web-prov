package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.ICSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.RoamingService;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.domain.enumeration.RoamingServiceEnum;
import sn.sentel.tigocare.hlr.services.hss.clients.HssClient;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;
import sn.sentel.tigocare.jaxb2.subscriberinfo.GetResponse;
import sn.sentel.tigocare.repository.RoamingServiceRepository;
import sn.sentel.tigocare.service.RoamingServiceService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.RoamingServiceDTO;
import sn.sentel.tigocare.service.mapper.RoamingServiceMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link RoamingService}.
 */
@Service
@Transactional
public class RoamingServiceServiceImpl implements RoamingServiceService {

    private final Logger log = LoggerFactory.getLogger(RoamingServiceServiceImpl.class);

    private final RoamingServiceRepository roamingServiceRepository;
    private final RoamingServiceMapper roamingServiceMapper;
    private final SubscriberInfoClient hlrClient;
    private final HssClient hssClient;
    private final SessionControl sessionControl;

    @Value("${app.ws.login}")
    private String userId;
    @Value("${app.ws.password}")
    private String pwd;
    private final UserService userService;
    private final ICSVParser csvParser;

    public RoamingServiceServiceImpl(RoamingServiceRepository roamingServiceRepository, RoamingServiceMapper roamingServiceMapper, SubscriberInfoClient hlrClient, HssClient hssClient, SessionControl sessionControl, UserService userService) {
        this.roamingServiceRepository = roamingServiceRepository;
        this.roamingServiceMapper = roamingServiceMapper;
        this.hlrClient = hlrClient;
        this.hssClient = hssClient;
        this.sessionControl = sessionControl;
        this.userService = userService;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
    }

    public static void sanitizeObject(RoamingServiceDTO roamingServiceDTO){
        roamingServiceDTO.setMsisdn(clearEntry(roamingServiceDTO.getMsisdn()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a roamingService.
     *
     * @param roamingServiceDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RoamingServiceDTO save(RoamingServiceDTO roamingServiceDTO) {
        sanitizeObject(roamingServiceDTO);
        this.log.debug("Request to save RoamingServiceEnum : {}", roamingServiceDTO);
        RoamingService roamingService = this.roamingServiceMapper.toEntity(roamingServiceDTO);
        String sessionId = this.sessionControl.login(this.userId, this.pwd);
        GetResponse subscriberInfo;

        Integer csp;
        try{
            subscriberInfo = this.hlrClient.getSubscriberInfo(roamingService.getMsisdn(), sessionId);
            csp = subscriberInfo.getMOAttributes().getGetResponseSubscription().getCsp();
        }catch (Exception e){
            roamingService.setProcessingStatus(ProcessingStatus.FAILED);
            this.roamingServiceRepository.save(roamingService);
            throw new IllegalArgumentException("User CSP not found");
        }
        try {
            if (csp == 1) {
                doRoamingServiceChangePrepaid(roamingService.getMsisdn(), roamingService.getServices(), sessionId);

            }
            if (csp == 3) {
                doRoamingServiceChangePostpaid(roamingService.getMsisdn(), roamingService.getServices(), sessionId);
            }
            roamingService.setProcessingStatus(ProcessingStatus.SUCCEEDED);
        } catch (Exception e) {
            roamingService.setProcessingStatus(ProcessingStatus.FAILED);
        } finally {
            roamingService = this.roamingServiceRepository.save(roamingService);
        }
        return this.roamingServiceMapper.toDto(roamingService);
    }

    private void doRoamingServiceChangePostpaid(String msisdn, RoamingServiceEnum services, String sessionId) {
        switch (services) {
            case VOICE_DATA:
                this.hlrClient.roamingPostVoiceData(msisdn, sessionId);
                break;
            case VOICE_NODATA:
                this.hlrClient.roamingPostVoiceNoData(msisdn, sessionId);
                break;
            case NOVOICE_DATA:
                this.hlrClient.roamingPostNoVoiceData(msisdn, sessionId);
                break;
            case NOVOICE_NODATA:
                this.hlrClient.roamingPostNoVoiceNoData(msisdn, sessionId);
                break;
        }

    }

    private void doRoamingServiceChangePrepaid(String msisdn, RoamingServiceEnum services, String sessionId) {
        switch (services) {
            case VOICE_DATA:
                this.hlrClient.roamingPreVoiceData(msisdn, sessionId);
                break;
            case VOICE_NODATA:
                this.hlrClient.roamingPreVoiceNoData(msisdn, sessionId);
                break;
            case NOVOICE_DATA:
                this.hlrClient.roamingPreNoVoiceData(msisdn, sessionId);
                break;
            case NOVOICE_NODATA:
                this.hlrClient.roamingPreNoVoiceNoData(msisdn, sessionId);
                break;
        }
    }

    /**
     * Get all the roamingServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RoamingServiceDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all RoamingServices");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.roamingServiceRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.roamingServiceMapper::toDto);
    }


    /**
     * Get one roamingService by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RoamingServiceDTO> findOne(Long id) {
        this.log.debug("Request to get RoamingServiceEnum : {}", id);
        return this.roamingServiceRepository.findById(id)
            .map(this.roamingServiceMapper::toDto);
    }

    /**
     * Delete the roamingService by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete RoamingServiceEnum : {}", id);
        this.roamingServiceRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, String serviceBulk, String txId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(RoamingServiceDTO.builder()
            .msisdn(aLine[0])
            .processingStatus(ProcessingStatus.SUBMITTED)
            .services(RoamingServiceEnum.valueOf(serviceBulk))
            .build()));
    }
}
