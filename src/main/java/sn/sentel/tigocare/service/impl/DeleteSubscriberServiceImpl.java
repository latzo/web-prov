package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.ICSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.DeleteSubscriber;
import sn.sentel.tigocare.hlr.services.auc.services.AUCService;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBS56BCClient;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBSClient;
import sn.sentel.tigocare.hlr.services.hss.services.HssService;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.jaxb2.cbs56.bc.SubDeactivationResultMsg;
import sn.sentel.tigocare.repository.DeleteSubscriberRepository;
import sn.sentel.tigocare.service.DeleteSubscriberService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;
import sn.sentel.tigocare.service.mapper.DeleteSubscriberMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import static sn.sentel.tigocare.domain.enumeration.ProcessingStatus.*;

/**
 * Service Implementation for managing {@link DeleteSubscriber}.
 */
@Service
@Transactional
public class DeleteSubscriberServiceImpl implements DeleteSubscriberService {

    private final Logger log = LoggerFactory.getLogger(DeleteSubscriberServiceImpl.class);

    private final DeleteSubscriberRepository deleteSubscriberRepository;

    private final DeleteSubscriberMapper deleteSubscriberMapper;
    private final AUCService aucService;
    private final SubscriberInfoService hlrService;
    private final HssService hssService;
    private final CBS56BCClient cbs56BCClient;
    private ICSVParser csvParser =  new CSVParserBuilder().withSeparator(';').build();
    private final UserService userService;

    public DeleteSubscriberServiceImpl(DeleteSubscriberRepository deleteSubscriberRepository, DeleteSubscriberMapper deleteSubscriberMapper, AUCService aucService, SubscriberInfoService hlrService, HssService hssService, CBSClient cbsClient, CBS56BCClient cbs56BCClient, UserService userService) {
        this.deleteSubscriberRepository = deleteSubscriberRepository;
        this.deleteSubscriberMapper = deleteSubscriberMapper;
        this.aucService = aucService;
        this.hlrService = hlrService;
        this.hssService = hssService;
        this.cbs56BCClient = cbs56BCClient;
        this.userService = userService;
    }

    public static void sanitizeObject(DeleteSubscriberDTO deleteSubscriberDTO){
        deleteSubscriberDTO.setMsisdn(clearEntry(deleteSubscriberDTO.getMsisdn()));
        deleteSubscriberDTO.setImsi(clearEntry(deleteSubscriberDTO.getImsi()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a deleteSubscriber.
     *
     * @param deleteSubscriberDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DeleteSubscriberDTO save(DeleteSubscriberDTO deleteSubscriberDTO) {
        sanitizeObject(deleteSubscriberDTO);
        this.log.debug("Request to save DeleteSubscriber : {}", deleteSubscriberDTO);
        DeleteSubscriber deleteSubscriber = this.deleteSubscriberMapper.toEntity(deleteSubscriberDTO);
        deleteSubscriber = this.deleteSubscriberRepository.save(deleteSubscriber);
        return this.deleteSubscriberMapper.toDto(deleteSubscriber);
    }

    /**
     * Get all the deleteSubscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DeleteSubscriberDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all DeleteSubscribers");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.deleteSubscriberRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.deleteSubscriberMapper::toDto);
//
//        return this.deleteSubscriberRepository.findAll(pageable)
//            .map(this.deleteSubscriberMapper::toDto);
    }

    @Override
    public Page<DeleteSubscriberDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all deletesubscribers by TransactionId");
        return this.deleteSubscriberRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.deleteSubscriberMapper::toDto);
    }


    /**
     * Get one deleteSubscriber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DeleteSubscriberDTO> findOne(Long id) {
        this.log.debug("Request to get DeleteSubscriber : {}", id);
        return this.deleteSubscriberRepository.findById(id)
            .map(this.deleteSubscriberMapper::toDto);
    }

    /**
     * Delete the deleteSubscriber by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete DeleteSubscriber : {}", id);
        this.deleteSubscriberRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, boolean deleteOnAuc, boolean deleteOnHlr, boolean deleteOnHss, boolean deleteOnCbs, String txId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(DeleteSubscriberDTO.builder()
            .msisdn("221"+aLine[0])
            .imsi("60802"+aLine[1])
            .transanctionId(txId)
            .deleteOnAuc(deleteOnAuc)
            .deleteOnCbs(deleteOnCbs)
            .deleteOnHlr(deleteOnHlr)
            .deleteOnHss(deleteOnHss)
            .processingStatus(SUBMITTED)
            .build()));
    }

    @Scheduled(fixedDelay = 10000)
    public void processPending() {
        this.deleteSubscriberRepository.findAllByProcessingStatus(SUBMITTED)
            .parallelStream()
            .forEach(ds -> {
                try {
                    if (ds.getDeleteOnAuc()) {
                        try {
                            this.aucService.deleteSIM(ds.getImsi());
                            ds.setAucDeletedStatus(SUCCEEDED);
                        } catch (Exception e) {
                            ds.setAucDeletedStatus(FAILED);
                            ds.setErrorMessage(e.getMessage());
                            throw new IllegalStateException();
                        }
                    }
                    if (ds.getDeleteOnHlr()) {
                        try {
                            this.hlrService.deleteSubscriber(ds.getImsi(), ds.getMsisdn());
                            ds.setHlrDeletedStatus(SUCCEEDED);
                        } catch (Exception e) {
                            ds.setHlrDeletedStatus(FAILED);
                            ds.setErrorMessage(e.getMessage());
                            throw new IllegalStateException();
                        }
                    }
                    if (ds.getDeleteOnHss()) {
                        try {
                            this.hssService.deleteSubscriber(ds.getImsi());
                            ds.setHssDeletedStatus(SUCCEEDED);
                        } catch (Exception e) {
                            ds.setHssDeletedStatus(FAILED);
                            ds.setErrorMessage(e.getMessage());
                            throw new IllegalStateException();
                        }
                    }
                    if (ds.getDeleteOnCbs()) {
                        try {
                            //Debut modif
                            SubDeactivationResultMsg resultMsg = this.cbs56BCClient.deleteSubscriber(ds.getMsisdn().substring(3));
                            if(resultMsg.getResultHeader().getResultCode().equals("0")){
                                ds.setCbsDeletedStatus(SUCCEEDED);
                            }
                            else {
                                ds.setCbsDeletedStatus(FAILED);
                            }
                            //Fin modif
                        } catch (Exception e) {
                            ds.setCbsDeletedStatus(FAILED);
                            ds.setErrorMessage(e.getMessage());
                            throw new IllegalStateException();
                        }
                    }
                    ds.setProcessingStatus(SUCCEEDED);
                    if((ds.getDeleteOnAuc() && ds.getAucDeletedStatus().equals(FAILED))||
                        (ds.getDeleteOnCbs() && ds.getCbsDeletedStatus().equals(FAILED))||
                        (ds.getDeleteOnHlr() && ds.getHlrDeletedStatus().equals(FAILED))||
                        (ds.getDeleteOnHss() && ds.getHssDeletedStatus().equals(FAILED))
                    ){
                        ds.setProcessingStatus(FAILED);
                    }
                    else{
                        ds.setProcessingStatus(SUCCEEDED);
                    }
                }catch (IllegalStateException ise){
                    ds.setProcessingStatus(FAILED);
                }finally {
                    this.deleteSubscriberRepository.save(ds);
                }

            });
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.deleteSubscriberRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
