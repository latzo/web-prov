package sn.sentel.tigocare.service.impl;

import sn.sentel.tigocare.service.SubscriberStatusService;
import sn.sentel.tigocare.domain.SubscriberStatus;
import sn.sentel.tigocare.repository.SubscriberStatusRepository;
import sn.sentel.tigocare.service.dto.SubscriberStatusDTO;
import sn.sentel.tigocare.service.mapper.SubscriberStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SubscriberStatus}.
 */
@Service
@Transactional
public class SubscriberStatusServiceImpl implements SubscriberStatusService {

    private final Logger log = LoggerFactory.getLogger(SubscriberStatusServiceImpl.class);

    private final SubscriberStatusRepository subscriberStatusRepository;

    private final SubscriberStatusMapper subscriberStatusMapper;

    public SubscriberStatusServiceImpl(SubscriberStatusRepository subscriberStatusRepository, SubscriberStatusMapper subscriberStatusMapper) {
        this.subscriberStatusRepository = subscriberStatusRepository;
        this.subscriberStatusMapper = subscriberStatusMapper;
    }

    /**
     * Save a subscriberStatus.
     *
     * @param subscriberStatusDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SubscriberStatusDTO save(SubscriberStatusDTO subscriberStatusDTO) {
        log.debug("Request to save SubscriberStatus : {}", subscriberStatusDTO);
        SubscriberStatus subscriberStatus = subscriberStatusMapper.toEntity(subscriberStatusDTO);
        subscriberStatus = subscriberStatusRepository.save(subscriberStatus);
        return subscriberStatusMapper.toDto(subscriberStatus);
    }

    /**
     * Get all the subscriberStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubscriberStatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubscriberStatuses");
        return subscriberStatusRepository.findAll(pageable)
            .map(subscriberStatusMapper::toDto);
    }


    /**
     * Get one subscriberStatus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubscriberStatusDTO> findOne(Long id) {
        log.debug("Request to get SubscriberStatus : {}", id);
        return subscriberStatusRepository.findById(id)
            .map(subscriberStatusMapper::toDto);
    }

    /**
     * Delete the subscriberStatus by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubscriberStatus : {}", id);
        subscriberStatusRepository.deleteById(id);
    }
}
