package sn.sentel.tigocare.service.impl;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.soap.client.SoapFaultClientException;
import sn.sentel.tigocare.domain.ServiceChange;
import sn.sentel.tigocare.domain.Services;
import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
import sn.sentel.tigocare.hlr.services.sessioncontrol.services.SessionControl;
import sn.sentel.tigocare.hlr.services.subscriberinfo.clients.SubscriberInfoClient;
import sn.sentel.tigocare.repository.ServiceChangeRepository;
import sn.sentel.tigocare.repository.ServicesRepository;
import sn.sentel.tigocare.service.ServiceChangeService;
import sn.sentel.tigocare.service.UserService;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.ServiceChangeDTO;
import sn.sentel.tigocare.service.mapper.ServiceChangeMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ServiceChange}.
 */
@Service
@Transactional
public class ServiceChangeServiceImpl implements ServiceChangeService {

    private static final String LOG_HEADER = "[SERVICE CHANGE SERVICE]";
    private final Logger log = LoggerFactory.getLogger(ServiceChangeServiceImpl.class);
    private final ServiceChangeRepository serviceChangeRepository;
    private final ServiceChangeMapper serviceChangeMapper;
    private final SubscriberInfoClient subscriberInfoClient;
    private final ServicesRepository servicesRepository;
    private final SessionControl sessionControl;
    private final CSVParser csvParser;
    @Value("${app.ws.login}")
    private String userId;
    @Value("${app.ws.password}")
    private String pwd;
    private final UserService userService;

    public ServiceChangeServiceImpl(ServiceChangeRepository serviceChangeRepository, ServiceChangeMapper serviceChangeMapper, SubscriberInfoClient subscriberInfoClient, ServicesRepository servicesRepository, SessionControl sessionControl, UserService userService) {
        this.serviceChangeRepository = serviceChangeRepository;
        this.serviceChangeMapper = serviceChangeMapper;
        this.subscriberInfoClient = subscriberInfoClient;
        this.servicesRepository = servicesRepository;
        this.sessionControl = sessionControl;
        this.csvParser = new CSVParserBuilder().withSeparator(';').build();
        this.userService = userService;
    }

    public static void sanitizeObject(ServiceChangeDTO serviceChangeDTO){
        serviceChangeDTO.setMsisdn(clearEntry(serviceChangeDTO.getMsisdn()));
        serviceChangeDTO.setImsi(clearEntry(serviceChangeDTO.getImsi()));
    }

    public static String clearEntry(String entry){
        return entry.replaceAll("\\s", "");
    }

    /**
     * Save a serviceChange.
     *
     * @param serviceChangeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ServiceChangeDTO save(ServiceChangeDTO serviceChangeDTO) {
        sanitizeObject(serviceChangeDTO);
        this.log.debug("Request to save ServiceChange : {}", serviceChangeDTO);
        ServiceChange serviceChange = this.serviceChangeMapper.toEntity(serviceChangeDTO);
        serviceChange = this.serviceChangeRepository.save(serviceChange);
        return this.serviceChangeMapper.toDto(serviceChange);
    }

    /**
     * Get all the serviceChanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ServiceChangeDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all ServiceChanges");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.serviceChangeRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.serviceChangeMapper::toDto);
    }

    @Override
    public Page<ServiceChangeDTO> findAllByTransactionId(String transactionId, Pageable pageable) {
        this.log.debug("Request to get all servicechanges by TransactionId");
        return this.serviceChangeRepository.findAllByTransactionId(transactionId, pageable)
            .map(this.serviceChangeMapper::toDto);
    }


    /**
     * Get one serviceChange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ServiceChangeDTO> findOne(Long id) {
        this.log.debug("Request to get ServiceChange : {}", id);
        return this.serviceChangeRepository.findById(id)
            .map(this.serviceChangeMapper::toDto);
    }

    /**
     * Delete the serviceChange by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        this.log.debug("Request to delete ServiceChange : {}", id);
        this.serviceChangeRepository.deleteById(id);
    }

    @Override
    public void bulk(InputStream inputStream, String txId, OperationEnum operation, String serviceId) throws IOException {
        CSVReader csvReader = new CSVReaderBuilder(new InputStreamReader(inputStream)).withCSVParser(this.csvParser).build();
        List<String[]> allLine = csvReader.readAll();
        allLine.parallelStream().forEach(aLine -> this.save(ServiceChangeDTO.builder()
            .msisdn("221"+aLine[0])
            .imsi("60802"+aLine[1])
            .serviceId(serviceId)
            .operation(operation)
            .transactionId(txId)
            .processingStatus(ProcessingStatus.SUBMITTED)
            .build()));
    }

    @Scheduled(fixedRate = 10000)
    public void processSubmitted(){
        this.serviceChangeRepository.findAllByProcessingStatus(ProcessingStatus.SUBMITTED)
            .parallelStream()
            .forEach(serviceChange -> {
                this.log.info("{} processing this service change {}", LOG_HEADER, serviceChange);
                try{
                    doServiceChange(serviceChange.getMsisdn(), serviceChange.getServiceId(), serviceChange.getOperation());
                    serviceChange.setProcessingStatus(ProcessingStatus.SUCCEEDED);
                }  catch (SoapFaultClientException | InvocationTargetException | IllegalAccessException sfce) {
                    this.log.warn("{} this fault while changing service for {} => {}", LOG_HEADER, serviceChange.getMsisdn(), sfce);
                    serviceChange.setProcessingStatus(ProcessingStatus.FAILED);
                    serviceChange.setBackendResp(sfce.getMessage());
                } finally {
                    this.serviceChangeRepository.save(serviceChange);
                }
            });
    }

    private void doServiceChange(String msisdn, String serviceId, OperationEnum operation) throws InvocationTargetException, IllegalAccessException {



        Optional<Services> serviceOpt = this.servicesRepository.findOneByCode(serviceId);
        if(serviceOpt.isPresent()) {
            Class<? extends SubscriberInfoClient> subscriberInfoClientClass = this.subscriberInfoClient.getClass();
            Method toCall;
            try {
                toCall = subscriberInfoClientClass.getMethod(serviceOpt.get().getMethodToInvoke(), String.class, OperationEnum.class, String.class);
            } catch (NoSuchMethodException e) {
                this.log.error("{} error while invoking service change method {}", LOG_HEADER, e.getMessage());
                e.printStackTrace();
                return;
            }
            String sessionId = this.sessionControl.login(this.userId, this.pwd);
            if (sessionId == null) {
                return;
            }
            try{
                toCall.invoke(this.subscriberInfoClient, msisdn, operation, sessionId);
            } finally {
                this.sessionControl.logout(sessionId);
            }
        }
        this.log.error("{} service doesn't exit", LOG_HEADER);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.serviceChangeRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
