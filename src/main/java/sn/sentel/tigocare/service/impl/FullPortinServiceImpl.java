package sn.sentel.tigocare.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import sn.sentel.tigocare.domain.CreateSubscriber;
import sn.sentel.tigocare.service.CreateSubscriberService;
import sn.sentel.tigocare.service.FullPortinService;
import sn.sentel.tigocare.domain.FullPortin;
import sn.sentel.tigocare.repository.FullPortinRepository;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
import sn.sentel.tigocare.service.dto.FullPortinDTO;
import sn.sentel.tigocare.service.mapper.CreateSubscriberMapper;
import sn.sentel.tigocare.service.mapper.FullPortinMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link FullPortin}.
 */
@Service
@Transactional
public class FullPortinServiceImpl implements FullPortinService {

    private final Logger log = LoggerFactory.getLogger(FullPortinServiceImpl.class);

    private final FullPortinRepository fullPortinRepository;

    private final FullPortinMapper fullPortinMapper;

    @Autowired
    private CreateSubscriberService createSubscriberService;

    @Autowired
    private CreateSubscriberMapper createSubscriberMapper;

    public FullPortinServiceImpl(FullPortinRepository fullPortinRepository, FullPortinMapper fullPortinMapper) {
        this.fullPortinRepository = fullPortinRepository;
        this.fullPortinMapper = fullPortinMapper;
    }

    /**
     * Save a fullPortin.
     *
     * @param fullPortinDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FullPortinDTO save(FullPortinDTO fullPortinDTO) {
        log.debug("Request to save FullPortin : {}", fullPortinDTO);
        FullPortin fullPortin = fullPortinMapper.toEntity(fullPortinDTO);
        CreateSubscriber createSubscriber = (CreateSubscriber) fullPortin;
        List<CreateSubscriber> createSubscriberList = new ArrayList<>();
        createSubscriberList.add(createSubscriber);
        List<CreateSubscriberDTO> createSubscriberDTOList  = this.createSubscriberMapper.toDto(createSubscriberList);
        List<CreateSubscriberDTO> result = this.createSubscriberService.fullPortinListOfSubscribers(createSubscriberDTOList);
        CreateSubscriber createSubscriberres = this.createSubscriberMapper.toEntity(result.get(0));
        FullPortin fullPortinres = new FullPortin();
        BeanUtils.copyProperties(createSubscriberres, fullPortinres);
        fullPortin = fullPortinRepository.save(fullPortinres);
        return fullPortinMapper.toDto(fullPortinres);
    }

    /**
     * Get all the fullPortins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FullPortinDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FullPortins");
        return fullPortinRepository.findAll(pageable)
            .map(fullPortinMapper::toDto);
    }


    /**
     * Get one fullPortin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FullPortinDTO> findOne(Long id) {
        log.debug("Request to get FullPortin : {}", id);
        return fullPortinRepository.findById(id)
            .map(fullPortinMapper::toDto);
    }

    /**
     * Delete the fullPortin by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FullPortin : {}", id);
        fullPortinRepository.deleteById(id);
    }
}
