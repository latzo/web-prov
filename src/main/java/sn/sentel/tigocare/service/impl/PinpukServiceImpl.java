package sn.sentel.tigocare.service.impl;

import sn.sentel.tigocare.service.PinpukService;
import sn.sentel.tigocare.domain.Pinpuk;
import sn.sentel.tigocare.repository.PinpukRepository;
import sn.sentel.tigocare.service.dto.PinpukDTO;
import sn.sentel.tigocare.service.mapper.PinpukMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Pinpuk}.
 */
@Service
@Transactional
public class PinpukServiceImpl implements PinpukService {

    private final Logger log = LoggerFactory.getLogger(PinpukServiceImpl.class);

    private final PinpukRepository pinpukRepository;

    private final PinpukMapper pinpukMapper;

    public PinpukServiceImpl(PinpukRepository pinpukRepository, PinpukMapper pinpukMapper) {
        this.pinpukRepository = pinpukRepository;
        this.pinpukMapper = pinpukMapper;
    }

    /**
     * Save a pinpuk.
     *
     * @param pinpukDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PinpukDTO save(PinpukDTO pinpukDTO) {
        log.debug("Request to save Pinpuk : {}", pinpukDTO);
        Pinpuk pinpuk = pinpukMapper.toEntity(pinpukDTO);
        pinpuk = pinpukRepository.save(pinpuk);
        return pinpukMapper.toDto(pinpuk);
    }

    /**
     * Get all the pinpuks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PinpukDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pinpuks");
        return pinpukRepository.findAll(pageable)
            .map(pinpukMapper::toDto);
    }


    /**
     * Get one pinpuk by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PinpukDTO> findOne(Long id) {
        log.debug("Request to get Pinpuk : {}", id);
        return pinpukRepository.findById(id)
            .map(pinpukMapper::toDto);
    }

    /**
     * Delete the pinpuk by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pinpuk : {}", id);
        pinpukRepository.deleteById(id);
    }

    @Override
    public Optional<Pinpuk> findOneByImsi(String imsi) {
        log.debug("Trying to find ki from imsi {}", imsi);
        return pinpukRepository.findOneByImsi(imsi);
    }

    @Override
    public Optional<Pinpuk> findFirstByImsi(String imsi) {
        log.debug("Trying to find ki from imsi top {}", imsi);
        return pinpukRepository.findFirstByImsi(imsi);
    }
}
