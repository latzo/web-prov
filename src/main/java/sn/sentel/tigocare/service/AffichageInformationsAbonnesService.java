package sn.sentel.tigocare.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.sentel.tigocare.domain.AffichageInformationsAbonnes;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBS56BCClient;
import sn.sentel.tigocare.hlr.services.cbs.clients.CBSClient;
import sn.sentel.tigocare.hlr.services.subscriberinfo.services.SubscriberInfoService;
import sn.sentel.tigocare.repository.AffichageInformationsAbonnesRepository;
import sn.sentel.tigocare.repository.AuthorityRepository;
import sn.sentel.tigocare.repository.UserRepository;
import sn.sentel.tigocare.service.dto.AffichageInformationsAbonnesDTO;
import sn.sentel.tigocare.service.mapper.AffichageInformationsAbonnesMapper;
import sn.sentel.tigocare.jaxb2.cbs56.bc.QueryCustomerInfoResultMsg;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link AffichageInformationsAbonnes}.
 */
@Service
@Transactional
public class AffichageInformationsAbonnesService {

    private final Logger log = LoggerFactory.getLogger(AffichageInformationsAbonnesService.class);

    private final AffichageInformationsAbonnesRepository affichageInformationsAbonnesRepository;

    private final UserRepository userRepository;

    private final UserService userService;

    private final AuthorityRepository authorityRepository;

    private final AffichageInformationsAbonnesMapper affichageInformationsAbonnesMapper;

    private final SubscriberInfoService subscriberInfoService;

    private final CBSClient cbsClient;

    private final CBS56BCClient cbs56BCClient;

    public AffichageInformationsAbonnesService(AffichageInformationsAbonnesRepository affichageInformationsAbonnesRepository, UserRepository userRepository, UserService userService, AuthorityRepository authorityRepository, AffichageInformationsAbonnesMapper affichageInformationsAbonnesMapper,
                                               SubscriberInfoService subscriberInfoService, CBSClient cbsClient, CBS56BCClient cbs56BCClient) {
        this.affichageInformationsAbonnesRepository = affichageInformationsAbonnesRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.authorityRepository = authorityRepository;
        this.affichageInformationsAbonnesMapper = affichageInformationsAbonnesMapper;
        this.subscriberInfoService = subscriberInfoService;
        this.cbsClient = cbsClient;
        this.cbs56BCClient = cbs56BCClient;
    }

    public static void sanitizeObject(AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO){
        affichageInformationsAbonnesDTO.setMsisdn(clearEntry(affichageInformationsAbonnesDTO.getMsisdn()));
        affichageInformationsAbonnesDTO.setImsi(clearEntry(affichageInformationsAbonnesDTO.getImsi()));
        affichageInformationsAbonnesDTO.setImsiHlr(affichageInformationsAbonnesDTO.getImsiHlr());
        affichageInformationsAbonnesDTO.setImsiCbs(affichageInformationsAbonnesDTO.getImsiCbs());
    }

    public static String clearEntry(String entry){
        if(!StringUtils.isBlank(entry)){
            return entry.replaceAll("\\s", "");
        }
        return "";
    }

    /**
     * Save a affichageInformationsAbonnes.
     *
     * @param affichageInformationsAbonnesDTO the entity to save.
     * @return the persisted entity.
     */
    public AffichageInformationsAbonnesDTO save(AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO) {
        sanitizeObject(affichageInformationsAbonnesDTO);
        affichageInformationsAbonnesDTO.setImsiCbs("Gotta get it from CBS");
        if (affichageInformationsAbonnesDTO.getMsisdn() != null && !affichageInformationsAbonnesDTO.getMsisdn().isEmpty()) {
            String imsiCbs;
            /* Debut modif */
            try {
                QueryCustomerInfoResultMsg queryCustomerInfoResultMsg = this.cbs56BCClient.queryBasicInfo(affichageInformationsAbonnesDTO.getMsisdn().trim());
                imsiCbs = getImsiFromCbs(queryCustomerInfoResultMsg);
            } catch (Exception e) {
                imsiCbs = e.getMessage();
            }
            /* Fin modiff */
            affichageInformationsAbonnesDTO.setImsiCbs(imsiCbs);
            affichageInformationsAbonnesDTO.setMsisdn("221" + affichageInformationsAbonnesDTO.getMsisdn().trim());
            String imsiHlr;
            try {
                imsiHlr = this.subscriberInfoService.getSubscriberInfo(affichageInformationsAbonnesDTO.getMsisdn()).getMOAttributes().getGetResponseSubscription().getImsi();
            } catch (Exception e) {
                imsiHlr = e.getMessage();
            }
            affichageInformationsAbonnesDTO.setImsiHlr(imsiHlr);
            affichageInformationsAbonnesDTO.setImsi(imsiHlr);
            this.log.debug("Request to save AffichageInformationsAbonnes : {}", affichageInformationsAbonnesDTO);
            AffichageInformationsAbonnes affichageInformationsAbonnes = this.affichageInformationsAbonnesMapper.toEntity(affichageInformationsAbonnesDTO);
            affichageInformationsAbonnes = this.affichageInformationsAbonnesRepository.save(affichageInformationsAbonnes);
            return this.affichageInformationsAbonnesMapper.toDto(affichageInformationsAbonnes);
        }
        if (affichageInformationsAbonnesDTO.getImsi() != null && !affichageInformationsAbonnesDTO.getImsi().isEmpty() && (affichageInformationsAbonnesDTO.getMsisdn() == null || affichageInformationsAbonnesDTO.getMsisdn().isEmpty())) {
            try {
                affichageInformationsAbonnesDTO.setImsi("60802" + affichageInformationsAbonnesDTO.getImsi().trim());
                String imsi;
                try {
                    imsi = this.subscriberInfoService.getSubscriberInfoImsi(affichageInformationsAbonnesDTO.getImsi()).getMOAttributes().getGetResponseSubscription().getImsi();
                } catch (Exception e) {
                    imsi = e.getMessage();
                }
                affichageInformationsAbonnesDTO.setImsiHlr(imsi);
                String msisdn;
                try {
                    msisdn = this.subscriberInfoService.getSubscriberInfoImsi(affichageInformationsAbonnesDTO.getImsi()).getMOAttributes().getGetResponseSubscription().getMsisdn();
                } catch (Exception e) {
                    msisdn = e.getMessage();
                }
                affichageInformationsAbonnesDTO.setMsisdn(msisdn);
                String imsiCbs;
                /* Debut modif */
                try {
                    QueryCustomerInfoResultMsg queryCustomerInfoResultMsg = this.cbs56BCClient.queryBasicInfo(affichageInformationsAbonnesDTO.getMsisdn().trim().substring(3));
                    imsiCbs = getImsiFromCbs(queryCustomerInfoResultMsg);
                } catch (Exception e) {
                    imsiCbs = e.getMessage();
                }
                /* Fin modif */
                affichageInformationsAbonnesDTO.setImsiCbs(imsiCbs);
                this.log.debug("Request to save AffichageInformationsAbonnes : {}", affichageInformationsAbonnesDTO);
                AffichageInformationsAbonnes affichageInformationsAbonnes = this.affichageInformationsAbonnesMapper.toEntity(affichageInformationsAbonnesDTO);
                affichageInformationsAbonnes = this.affichageInformationsAbonnesRepository.save(affichageInformationsAbonnes);
                return this.affichageInformationsAbonnesMapper.toDto(affichageInformationsAbonnes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return affichageInformationsAbonnesDTO;
    }

    private String getImsiFromCbs(QueryCustomerInfoResultMsg queryCustomerInfoResultMsg) {
        String imsiCbs = "";
        if (queryCustomerInfoResultMsg.getResultHeader().getResultCode().equals("0")) {
            List<String> collect = queryCustomerInfoResultMsg.getQueryCustomerInfoResult().getSubscriber().get(0).getSubscriberInfo().getSubIdentity().stream()
                .filter(subIdentity -> subIdentity.getPrimaryFlag().equals("2"))
                .map(subIdentity -> subIdentity.getSubIdentity())
                .collect(Collectors.toList());
            imsiCbs = collect.size() == 1 ? collect.get(0).toString() : "This msisdn has no imsi on CBS";
            log.info("IMSI GOT FROM CBS 5.6 " + imsiCbs);
        } else {
            imsiCbs = queryCustomerInfoResultMsg.getResultHeader().getResultDesc();
            if(imsiCbs.length()>255){
                imsiCbs = imsiCbs.substring(0,250) + "...";
                imsiCbs = "CBS Backend unavailable";
            }
        }
        return imsiCbs;
    }

    /**
     * Get all the affichageInformationsAbonnes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AffichageInformationsAbonnesDTO> findAll(Pageable pageable) {
        this.log.debug("Request to get all AffichageInformationsAbonnes");
        List<String> loginOfUsersOfTheSameGroupThanCurrentUser = this.userService.getLoginOfUsersOfTheSameGroupThanCurrentUser();
        return this.affichageInformationsAbonnesRepository.findByCreatedByIn(loginOfUsersOfTheSameGroupThanCurrentUser, pageable)
            .map(this.affichageInformationsAbonnesMapper::toDto);
    }




    /**
     * Get one affichageInformationsAbonnes by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AffichageInformationsAbonnesDTO> findOne(Long id) {
        this.log.debug("Request to get AffichageInformationsAbonnes : {}", id);
        return this.affichageInformationsAbonnesRepository.findById(id)
            .map(this.affichageInformationsAbonnesMapper::toDto);
    }

    /**
     * Delete the affichageInformationsAbonnes by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        this.log.debug("Request to delete AffichageInformationsAbonnes : {}", id);
        this.affichageInformationsAbonnesRepository.deleteById(id);
    }

//    @Scheduled(cron = "0 1 1 * * ?")
//    public void flush(){
//        this.affichageInformationsAbonnesRepository.deleteByCreatedDateBefore(LocalDateTime.now().minusDays(7L).toInstant(ZoneOffset.UTC));
//    }
}
