package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.Roaming4gDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.Roaming4g}.
 */
public interface Roaming4gService {

    /**
     * Save a roaming4g.
     *
     * @param roaming4gDTO the entity to save.
     * @return the persisted entity.
     */
    Roaming4gDTO save(Roaming4gDTO roaming4gDTO);

    /**
     * Get all the roaming4gs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Roaming4gDTO> findAll(Pageable pageable);


    /**
     * Get the "id" roaming4g.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Roaming4gDTO> findOne(Long id);

    /**
     * Delete the "id" roaming4g.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, String serviceBulk, String txId) throws IOException;
}
