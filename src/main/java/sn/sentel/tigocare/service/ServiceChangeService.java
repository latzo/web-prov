package sn.sentel.tigocare.service;

import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.ServiceChangeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.ServiceChange}.
 */
public interface ServiceChangeService {

    /**
     * Save a serviceChange.
     *
     * @param serviceChangeDTO the entity to save.
     * @return the persisted entity.
     */
    ServiceChangeDTO save(ServiceChangeDTO serviceChangeDTO);

    /**
     * Get all the serviceChanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServiceChangeDTO> findAll(Pageable pageable);

    Page<ServiceChangeDTO> findAllByTransactionId(String transactionId, Pageable pageable);

    /**
     * Get the "id" serviceChange.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ServiceChangeDTO> findOne(Long id);

    /**
     * Delete the "id" serviceChange.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, String txId, OperationEnum operation, String serviceId) throws IOException;
}
