package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.RoamingServiceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.RoamingService}.
 */
public interface RoamingServiceService {

    /**
     * Save a roamingService.
     *
     * @param roamingServiceDTO the entity to save.
     * @return the persisted entity.
     */
    RoamingServiceDTO save(RoamingServiceDTO roamingServiceDTO);

    /**
     * Get all the roamingServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RoamingServiceDTO> findAll(Pageable pageable);

    //Page<RoamingServiceDTO> findAllByTransactionId(String transactionId, Pageable pageable);


    /**
     * Get the "id" roamingService.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RoamingServiceDTO> findOne(Long id);

    /**
     * Delete the "id" roamingService.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, String serviceBulk, String txId) throws IOException;
}
