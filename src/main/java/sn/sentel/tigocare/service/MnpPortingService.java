package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
import sn.sentel.tigocare.service.dto.MnpPortingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.MnpPorting}.
 */
public interface MnpPortingService {

    /**
     * Save a mnpPorting.
     *
     * @param mnpPortingDTO the entity to save.
     * @return the persisted entity.
     */
    MnpPortingDTO save(MnpPortingDTO mnpPortingDTO);

    /**
     * Get all the mnpPortings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MnpPortingDTO> findAll(Pageable pageable);

    //Page<MnpPortingDTO> findAllByTransactionId(String transactionId, Pageable pageable);

    //Page<MnpPortingDTO> findAllByTransactionId(String transactionId, Pageable pageable);


    /**
     * Get the "id" mnpPorting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MnpPortingDTO> findOne(Long id);

    /**
     * Delete the "id" mnpPorting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
