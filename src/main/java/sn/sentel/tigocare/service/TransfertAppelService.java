package sn.sentel.tigocare.service;

import sn.sentel.tigocare.domain.enumeration.OperationEnum;
import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
import sn.sentel.tigocare.service.dto.TransfertAppelDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.TransfertAppel}.
 */
public interface TransfertAppelService {

    /**
     * Save a transfertAppel.
     *
     * @param transfertAppelDTO the entity to save.
     * @return the persisted entity.
     */
    TransfertAppelDTO save(TransfertAppelDTO transfertAppelDTO);

    /**
     * Get all the transfertAppels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransfertAppelDTO> findAll(Pageable pageable);


    /**
     * Get the "id" transfertAppel.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransfertAppelDTO> findOne(Long id);

    /**
     * Delete the "id" transfertAppel.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, OperationEnum operationEnum, String txId) throws IOException;

    Page<TransfertAppelDTO> findAllByTransactionId(String transactionId, Pageable pageable);
}
