package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.MnpPortoutDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.MnpPortout}.
 */
public interface MnpPortoutService {

    /**
     * Save a mnpPortout.
     *
     * @param mnpPortoutDTO the entity to save.
     * @return the persisted entity.
     */
    MnpPortoutDTO save(MnpPortoutDTO mnpPortoutDTO);

    /**
     * Get all the mnpPortouts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MnpPortoutDTO> findAll(Pageable pageable);

    //Page<MnpPortoutDTO> findAllByTransactionId(String transactionId, Pageable pageable);


    /**
     * Get the "id" mnpPortout.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MnpPortoutDTO> findOne(Long id);

    /**
     * Delete the "id" mnpPortout.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
