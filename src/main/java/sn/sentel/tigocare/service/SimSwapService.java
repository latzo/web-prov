package sn.sentel.tigocare.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.sentel.tigocare.domain.SimSwap;
import sn.sentel.tigocare.service.dto.SimSwapDTO;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.SimSwap}.
 */
public interface SimSwapService {

    /**
     * Save a simSwap.
     *
     * @param simSwapDTO the entity to save.
     * @return the persisted entity.
     */
    SimSwapDTO save(SimSwapDTO simSwapDTO);

    /**
     * Get all the simSwaps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SimSwapDTO> findAll(Pageable pageable);


    Page<SimSwapDTO> findAllByTransactionId(String transactionId, Pageable pageable);

    /**
     * Get the "id" simSwap.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SimSwapDTO> findOne(Long id);

    /**
     * Delete the "id" simSwap.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Create sim swap using csv file
     * @param inputStream csv files
     * @param txId transaction id
     */

    void bulk(InputStream inputStream, String txId) throws IOException;

    SimSwapDTO doSimswap(SimSwapDTO simSwapDTO);
}
