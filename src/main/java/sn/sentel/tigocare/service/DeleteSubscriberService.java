package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.DeleteSubscriber}.
 */
public interface DeleteSubscriberService {

    /**
     * Save a deleteSubscriber.
     *
     * @param deleteSubscriberDTO the entity to save.
     * @return the persisted entity.
     */
    DeleteSubscriberDTO save(DeleteSubscriberDTO deleteSubscriberDTO);

    /**
     * Get all the deleteSubscribers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DeleteSubscriberDTO> findAll(Pageable pageable);

    Page<DeleteSubscriberDTO> findAllByTransactionId(String transactionId, Pageable pageable);

    /**
     * Get the "id" deleteSubscriber.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DeleteSubscriberDTO> findOne(Long id);

    /**
     * Delete the "id" deleteSubscriber.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void bulk(InputStream inputStream, boolean deleteOnAuc, boolean deleteOnHlr, boolean deleteOnHss, boolean deleteOnCbs, String txId) throws IOException;
}
