package sn.sentel.tigocare.service;

import sn.sentel.tigocare.service.dto.APNChangeDTO;
import sn.sentel.tigocare.service.dto.ResetLocationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.sentel.tigocare.domain.ResetLocation}.
 */
public interface ResetLocationService {

    /**
     * Save a resetLocation.
     *
     * @param resetLocationDTO the entity to save.
     * @return the persisted entity.
     */
    ResetLocationDTO save(ResetLocationDTO resetLocationDTO);

    /**
     * Get all the resetLocations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ResetLocationDTO> findAll(Pageable pageable);

    //Page<ResetLocationDTO> findAllByTransactionId(String transactionId, Pageable pageable);

    /**
     * Get the "id" resetLocation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ResetLocationDTO> findOne(Long id);

    /**
     * Delete the "id" resetLocation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
