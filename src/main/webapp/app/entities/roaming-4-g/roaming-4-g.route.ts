import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Roaming4g } from 'app/shared/model/roaming-4-g.model';
import { Roaming4gService } from './roaming-4-g.service';
import { Roaming4gComponent } from './roaming-4-g.component';
import { Roaming4gDetailComponent } from './roaming-4-g-detail.component';
import { Roaming4gUpdateComponent } from './roaming-4-g-update.component';
import { Roaming4gDeletePopupComponent } from './roaming-4-g-delete-dialog.component';
import { IRoaming4g } from 'app/shared/model/roaming-4-g.model';

@Injectable({ providedIn: 'root' })
export class Roaming4gResolve implements Resolve<IRoaming4g> {
  constructor(private service: Roaming4gService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRoaming4g> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Roaming4g>) => response.ok),
        map((roaming4g: HttpResponse<Roaming4g>) => roaming4g.body)
      );
    }
    return of(new Roaming4g());
  }
}

export const roaming4gRoute: Routes = [
  {
    path: '',
    component: Roaming4gComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Roaming4gs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: Roaming4gDetailComponent,
    resolve: {
      roaming4g: Roaming4gResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Roaming4gs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: Roaming4gUpdateComponent,
    resolve: {
      roaming4g: Roaming4gResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Roaming4gs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: Roaming4gUpdateComponent,
    resolve: {
      roaming4g: Roaming4gResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Roaming4gs'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const roaming4gPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: Roaming4gDeletePopupComponent,
    resolve: {
      roaming4g: Roaming4gResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Roaming4gs'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
