import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IRoaming4g, Roaming4g } from 'app/shared/model/roaming-4-g.model';
import { Roaming4gService } from './roaming-4-g.service';

@Component({
  selector: 'jhi-roaming-4-g-update',
  templateUrl: './roaming-4-g-update.component.html'
})
export class Roaming4gUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    imsi: [null, [Validators.required]],
    services: [null, [Validators.required]]
  });

  editFormBulk = this.fb.group({
    imsi_bulk: [null, [Validators.required]],
    services: [null, [Validators.required]]
  });
  private imsi_bulk: File;
  service_bulk: string;
  constructor(
    protected roaming4gService: Roaming4gService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ roaming4g }) => {
      this.updateForm(roaming4g);
    });
  }

  updateForm(roaming4g: IRoaming4g) {
    this.editForm.patchValue({
      imsi: roaming4g.imsi,
      services: roaming4g.services
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const roaming4g = this.createFromForm();
    if (roaming4g.id !== undefined) {
      this.subscribeToSaveResponse(this.roaming4gService.update(roaming4g));
    } else {
      this.subscribeToSaveResponse(this.roaming4gService.create(roaming4g));
    }
  }

  private createFromForm(): IRoaming4g {
    return {
      ...new Roaming4g(),
      imsi: this.editForm.get(['imsi']).value,
      services: this.editForm.get(['services']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRoaming4g>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('imsi_bulk', this.imsi_bulk);
    formData.append('service_bulk', this.editFormBulk.get('services').value);
    this.subscribeToSaveResponse(this.roaming4gService.upload(formData));
  }

  onFileChange(event) {
    this.imsi_bulk = event.target.files.length > 0 ? event.target.files[0] : null;
  }
}
