import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRoaming4g } from 'app/shared/model/roaming-4-g.model';

type EntityResponseType = HttpResponse<IRoaming4g>;
type EntityArrayResponseType = HttpResponse<IRoaming4g[]>;

@Injectable({ providedIn: 'root' })
export class Roaming4gService {
  public resourceUrl = SERVER_API_URL + 'api/roaming-4-gs';

  constructor(protected http: HttpClient) {}

  create(roaming4g: IRoaming4g): Observable<EntityResponseType> {
    return this.http.post<IRoaming4g>(this.resourceUrl, roaming4g, { observe: 'response' });
  }

  update(roaming4g: IRoaming4g): Observable<EntityResponseType> {
    return this.http.put<IRoaming4g>(this.resourceUrl, roaming4g, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRoaming4g>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRoaming4g[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
