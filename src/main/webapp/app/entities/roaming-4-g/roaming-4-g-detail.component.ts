import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRoaming4g } from 'app/shared/model/roaming-4-g.model';

@Component({
  selector: 'jhi-roaming-4-g-detail',
  templateUrl: './roaming-4-g-detail.component.html'
})
export class Roaming4gDetailComponent implements OnInit {
  roaming4g: IRoaming4g;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ roaming4g }) => {
      this.roaming4g = roaming4g;
    });
  }

  previousState() {
    window.history.back();
  }
}
