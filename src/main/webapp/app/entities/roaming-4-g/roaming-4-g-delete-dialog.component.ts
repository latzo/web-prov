import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRoaming4g } from 'app/shared/model/roaming-4-g.model';
import { Roaming4gService } from './roaming-4-g.service';

@Component({
  selector: 'jhi-roaming-4-g-delete-dialog',
  templateUrl: './roaming-4-g-delete-dialog.component.html'
})
export class Roaming4gDeleteDialogComponent {
  roaming4g: IRoaming4g;

  constructor(protected roaming4gService: Roaming4gService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.roaming4gService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'roaming4gListModification',
        content: 'Deleted an roaming4g'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-roaming-4-g-delete-popup',
  template: ''
})
export class Roaming4gDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ roaming4g }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(Roaming4gDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.roaming4g = roaming4g;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/roaming-4-g', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/roaming-4-g', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
