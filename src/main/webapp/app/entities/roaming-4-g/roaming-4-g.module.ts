import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared/shared.module';
import { Roaming4gComponent } from './roaming-4-g.component';
import { Roaming4gDetailComponent } from './roaming-4-g-detail.component';
import { Roaming4gUpdateComponent } from './roaming-4-g-update.component';
import { Roaming4gDeletePopupComponent, Roaming4gDeleteDialogComponent } from './roaming-4-g-delete-dialog.component';
import { roaming4gRoute, roaming4gPopupRoute } from './roaming-4-g.route';

const ENTITY_STATES = [...roaming4gRoute, ...roaming4gPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    Roaming4gComponent,
    Roaming4gDetailComponent,
    Roaming4gUpdateComponent,
    Roaming4gDeleteDialogComponent,
    Roaming4gDeletePopupComponent
  ],
  entryComponents: [Roaming4gDeleteDialogComponent]
})
export class TigocareRoaming4gModule {}
