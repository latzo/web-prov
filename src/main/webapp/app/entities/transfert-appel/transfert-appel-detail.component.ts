import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITransfertAppel } from 'app/shared/model/transfert-appel.model';

@Component({
  selector: 'jhi-transfert-appel-detail',
  templateUrl: './transfert-appel-detail.component.html'
})
export class TransfertAppelDetailComponent implements OnInit {
  transfertAppel: ITransfertAppel;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transfertAppel }) => {
      this.transfertAppel = transfertAppel;
    });
  }

  previousState() {
    window.history.back();
  }
}
