import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITransfertAppel } from 'app/shared/model/transfert-appel.model';
import { TransfertAppelService } from './transfert-appel.service';

@Component({
  selector: 'jhi-transfert-appel-delete-dialog',
  templateUrl: './transfert-appel-delete-dialog.component.html'
})
export class TransfertAppelDeleteDialogComponent {
  transfertAppel: ITransfertAppel;

  constructor(
    protected transfertAppelService: TransfertAppelService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.transfertAppelService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'transfertAppelListModification',
        content: 'Deleted an transfertAppel'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-transfert-appel-delete-popup',
  template: ''
})
export class TransfertAppelDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transfertAppel }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TransfertAppelDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.transfertAppel = transfertAppel;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/transfert-appel', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/transfert-appel', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
