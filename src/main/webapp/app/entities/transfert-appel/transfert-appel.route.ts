import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TransfertAppel } from 'app/shared/model/transfert-appel.model';
import { TransfertAppelService } from './transfert-appel.service';
import { TransfertAppelComponent } from './transfert-appel.component';
import { TransfertAppelDetailComponent } from './transfert-appel-detail.component';
import { TransfertAppelUpdateComponent } from './transfert-appel-update.component';
import { TransfertAppelDeletePopupComponent } from './transfert-appel-delete-dialog.component';
import { ITransfertAppel } from 'app/shared/model/transfert-appel.model';

@Injectable({ providedIn: 'root' })
export class TransfertAppelResolve implements Resolve<ITransfertAppel> {
  constructor(private service: TransfertAppelService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITransfertAppel> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TransfertAppel>) => response.ok),
        map((transfertAppel: HttpResponse<TransfertAppel>) => transfertAppel.body)
      );
    }
    return of(new TransfertAppel());
  }
}

export const transfertAppelRoute: Routes = [
  {
    path: '',
    component: TransfertAppelComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'TransfertAppels'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TransfertAppelDetailComponent,
    resolve: {
      transfertAppel: TransfertAppelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransfertAppels'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TransfertAppelUpdateComponent,
    resolve: {
      transfertAppel: TransfertAppelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransfertAppels'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TransfertAppelUpdateComponent,
    resolve: {
      transfertAppel: TransfertAppelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransfertAppels'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const transfertAppelPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TransfertAppelDeletePopupComponent,
    resolve: {
      transfertAppel: TransfertAppelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TransfertAppels'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
