import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITransfertAppel } from 'app/shared/model/transfert-appel.model';

type EntityResponseType = HttpResponse<ITransfertAppel>;
type EntityArrayResponseType = HttpResponse<ITransfertAppel[]>;

@Injectable({ providedIn: 'root' })
export class TransfertAppelService {
  public resourceUrl = SERVER_API_URL + 'api/transfert-appels';

  constructor(protected http: HttpClient) {}

  create(transfertAppel: ITransfertAppel): Observable<EntityResponseType> {
    return this.http.post<ITransfertAppel>(this.resourceUrl, transfertAppel, { observe: 'response' });
  }

  update(transfertAppel: ITransfertAppel): Observable<EntityResponseType> {
    return this.http.put<ITransfertAppel>(this.resourceUrl, transfertAppel, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITransfertAppel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITransfertAppel[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
