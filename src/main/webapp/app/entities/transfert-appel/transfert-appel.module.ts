import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  TransfertAppelComponent,
  TransfertAppelDetailComponent,
  TransfertAppelUpdateComponent,
  TransfertAppelDeletePopupComponent,
  TransfertAppelDeleteDialogComponent,
  transfertAppelRoute,
  transfertAppelPopupRoute
} from './';

const ENTITY_STATES = [...transfertAppelRoute, ...transfertAppelPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TransfertAppelComponent,
    TransfertAppelDetailComponent,
    TransfertAppelUpdateComponent,
    TransfertAppelDeleteDialogComponent,
    TransfertAppelDeletePopupComponent
  ],
  entryComponents: [
    TransfertAppelComponent,
    TransfertAppelUpdateComponent,
    TransfertAppelDeleteDialogComponent,
    TransfertAppelDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareTransfertAppelModule {}
