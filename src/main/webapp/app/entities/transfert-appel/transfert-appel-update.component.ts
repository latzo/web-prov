import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITransfertAppel, TransfertAppel } from 'app/shared/model/transfert-appel.model';
import { TransfertAppelService } from './transfert-appel.service';

@Component({
  selector: 'jhi-transfert-appel-update',
  templateUrl: './transfert-appel-update.component.html'
})
export class TransfertAppelUpdateComponent implements OnInit {
  isSaving: boolean;
  private file: File;
  public operationType: string;

  editForm = this.fb.group({
    id: [],
    msisdnSource: [null, [Validators.required]],
    msisdnDest: [],
    operation: [null, [Validators.required]],
    processingStatus: [],
    transactionId: [],
    backendResp: []
  });

  constructor(protected transfertAppelService: TransfertAppelService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ transfertAppel }) => {
      this.updateForm(transfertAppel);
    });
  }

  updateForm(transfertAppel: ITransfertAppel) {
    this.editForm.patchValue({
      id: transfertAppel.id,
      msisdnSource: transfertAppel.msisdnSource,
      msisdnDest: transfertAppel.msisdnDest,
      operation: transfertAppel.operation,
      processingStatus: transfertAppel.processingStatus,
      transactionId: transfertAppel.transactionId,
      backendResp: transfertAppel.backendResp
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const transfertAppel = this.createFromForm();
    if (transfertAppel.id !== undefined) {
      this.subscribeToSaveResponse(this.transfertAppelService.update(transfertAppel));
    } else {
      this.subscribeToSaveResponse(this.transfertAppelService.create(transfertAppel));
    }
  }

  private createFromForm(): ITransfertAppel {
    return {
      ...new TransfertAppel(),
      id: this.editForm.get(['id']).value,
      msisdnSource: this.editForm.get(['msisdnSource']).value,
      msisdnDest: this.editForm.get(['msisdnDest']).value || '',
      operation: this.editForm.get(['operation']).value,
      processingStatus: this.editForm.get(['processingStatus']).value,
      transactionId: this.editForm.get(['transactionId']).value,
      backendResp: this.editForm.get(['backendResp']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransfertAppel>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  onFileChange(event) {
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('operation', this.operationType);
    this.subscribeToSaveResponse(this.transfertAppelService.upload(formData));
  }
}
