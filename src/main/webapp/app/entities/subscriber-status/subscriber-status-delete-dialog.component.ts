import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubscriberStatus } from 'app/shared/model/subscriber-status.model';
import { SubscriberStatusService } from './subscriber-status.service';

@Component({
  selector: 'jhi-subscriber-status-delete-dialog',
  templateUrl: './subscriber-status-delete-dialog.component.html'
})
export class SubscriberStatusDeleteDialogComponent {
  subscriberStatus: ISubscriberStatus;

  constructor(
    protected subscriberStatusService: SubscriberStatusService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.subscriberStatusService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'subscriberStatusListModification',
        content: 'Deleted an subscriberStatus'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-subscriber-status-delete-popup',
  template: ''
})
export class SubscriberStatusDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ subscriberStatus }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SubscriberStatusDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.subscriberStatus = subscriberStatus;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/subscriber-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/subscriber-status', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
