import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISubscriberStatus } from 'app/shared/model/subscriber-status.model';

type EntityResponseType = HttpResponse<ISubscriberStatus>;
type EntityArrayResponseType = HttpResponse<ISubscriberStatus[]>;

@Injectable({ providedIn: 'root' })
export class SubscriberStatusService {
  public resourceUrl = SERVER_API_URL + 'api/subscriber-statuses';

  constructor(protected http: HttpClient) {}

  create(subscriberStatus: ISubscriberStatus): Observable<EntityResponseType> {
    return this.http.post<ISubscriberStatus>(this.resourceUrl, subscriberStatus, { observe: 'response' });
  }

  update(subscriberStatus: ISubscriberStatus): Observable<EntityResponseType> {
    return this.http.put<ISubscriberStatus>(this.resourceUrl, subscriberStatus, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISubscriberStatus>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISubscriberStatus[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
