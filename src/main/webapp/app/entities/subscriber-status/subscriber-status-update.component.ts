import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ISubscriberStatus, SubscriberStatus } from 'app/shared/model/subscriber-status.model';
import { SubscriberStatusService } from './subscriber-status.service';

@Component({
  selector: 'jhi-subscriber-status-update',
  templateUrl: './subscriber-status-update.component.html'
})
export class SubscriberStatusUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    statusId: [],
    label: []
  });

  constructor(
    protected subscriberStatusService: SubscriberStatusService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ subscriberStatus }) => {
      this.updateForm(subscriberStatus);
    });
  }

  updateForm(subscriberStatus: ISubscriberStatus) {
    this.editForm.patchValue({
      id: subscriberStatus.id,
      statusId: subscriberStatus.statusId,
      label: subscriberStatus.label
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const subscriberStatus = this.createFromForm();
    if (subscriberStatus.id !== undefined) {
      this.subscribeToSaveResponse(this.subscriberStatusService.update(subscriberStatus));
    } else {
      this.subscribeToSaveResponse(this.subscriberStatusService.create(subscriberStatus));
    }
  }

  private createFromForm(): ISubscriberStatus {
    return {
      ...new SubscriberStatus(),
      id: this.editForm.get(['id']).value,
      statusId: this.editForm.get(['statusId']).value,
      label: this.editForm.get(['label']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubscriberStatus>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
