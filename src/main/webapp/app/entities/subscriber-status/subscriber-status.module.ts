import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  SubscriberStatusComponent,
  SubscriberStatusDetailComponent,
  SubscriberStatusUpdateComponent,
  SubscriberStatusDeletePopupComponent,
  SubscriberStatusDeleteDialogComponent,
  subscriberStatusRoute,
  subscriberStatusPopupRoute
} from './';

const ENTITY_STATES = [...subscriberStatusRoute, ...subscriberStatusPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SubscriberStatusComponent,
    SubscriberStatusDetailComponent,
    SubscriberStatusUpdateComponent,
    SubscriberStatusDeleteDialogComponent,
    SubscriberStatusDeletePopupComponent
  ],
  entryComponents: [
    SubscriberStatusComponent,
    SubscriberStatusUpdateComponent,
    SubscriberStatusDeleteDialogComponent,
    SubscriberStatusDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareSubscriberStatusModule {}
