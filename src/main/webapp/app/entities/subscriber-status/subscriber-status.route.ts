import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SubscriberStatus } from 'app/shared/model/subscriber-status.model';
import { SubscriberStatusService } from './subscriber-status.service';
import { SubscriberStatusComponent } from './subscriber-status.component';
import { SubscriberStatusDetailComponent } from './subscriber-status-detail.component';
import { SubscriberStatusUpdateComponent } from './subscriber-status-update.component';
import { SubscriberStatusDeletePopupComponent } from './subscriber-status-delete-dialog.component';
import { ISubscriberStatus } from 'app/shared/model/subscriber-status.model';

@Injectable({ providedIn: 'root' })
export class SubscriberStatusResolve implements Resolve<ISubscriberStatus> {
  constructor(private service: SubscriberStatusService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISubscriberStatus> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SubscriberStatus>) => response.ok),
        map((subscriberStatus: HttpResponse<SubscriberStatus>) => subscriberStatus.body)
      );
    }
    return of(new SubscriberStatus());
  }
}

export const subscriberStatusRoute: Routes = [
  {
    path: '',
    component: SubscriberStatusComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'SubscriberStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SubscriberStatusDetailComponent,
    resolve: {
      subscriberStatus: SubscriberStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscriberStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SubscriberStatusUpdateComponent,
    resolve: {
      subscriberStatus: SubscriberStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscriberStatuses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SubscriberStatusUpdateComponent,
    resolve: {
      subscriberStatus: SubscriberStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscriberStatuses'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const subscriberStatusPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SubscriberStatusDeletePopupComponent,
    resolve: {
      subscriberStatus: SubscriberStatusResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SubscriberStatuses'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
