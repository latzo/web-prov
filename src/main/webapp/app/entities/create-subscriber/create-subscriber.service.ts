import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICreateSubscriber, IMainProducts } from 'app/shared/model/create-subscriber.model';

type EntityResponseType = HttpResponse<ICreateSubscriber>;
type EntityArrayResponseType = HttpResponse<ICreateSubscriber[]>;

@Injectable({ providedIn: 'root' })
export class CreateSubscriberService {
  public resourceUrl = SERVER_API_URL + 'api/create-subscribers';
  public mainProductsResourceUrl = SERVER_API_URL + 'api/main-products-cbs';

  constructor(protected http: HttpClient) {}

  getAllElementsOfABulk(transanctionId: String, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICreateSubscriber[]>(`${this.resourceUrl}/bulks/${transanctionId}`, { params: options, observe: 'response' });
  }

  create(createSubscriber: ICreateSubscriber): Observable<EntityResponseType> {
    return this.http.post<ICreateSubscriber>(this.resourceUrl, createSubscriber, { observe: 'response' });
  }

  update(createSubscriber: ICreateSubscriber): Observable<EntityResponseType> {
    return this.http.put<ICreateSubscriber>(this.resourceUrl, createSubscriber, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICreateSubscriber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICreateSubscriber[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  queryAllCos(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMainProducts[]>(this.mainProductsResourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
