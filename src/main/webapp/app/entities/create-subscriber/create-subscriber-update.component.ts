import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CreateSubscriber, ICreateSubscriber, IMainProducts } from 'app/shared/model/create-subscriber.model';
import { CreateSubscriberService } from './create-subscriber.service';
import { JhiAlertService } from 'ng-jhipster';
import { filter, map } from 'rxjs/operators';
import { AccountService } from 'app/core';

@Component({
  selector: 'jhi-create-subscriber-update',
  templateUrl: './create-subscriber-update.component.html'
})
export class CreateSubscriberUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    processingStatus: [],
    statut: [],
    transactionId: [],
    backendResp: [],
    cos: [[], [Validators.required]],
    creationType: ['IN_SWITCH', [Validators.required]]
  });
  creationType: string;
  statut: string;
  cos: string;
  mainProducts: IMainProducts[];
  private file: File;
  private authorities: string[];

  constructor(
    protected createSubscriberService: CreateSubscriberService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.accountService.fetch().subscribe(
      a => {
        this.authorities = a.body.authorities;
      },
      err => console.error('Cant get authorities' + err)
    );
    this.createSubscriberService
      .queryAllCos()
      .pipe(
        filter((res: HttpResponse<IMainProducts[]>) => res.ok),
        map((res: HttpResponse<IMainProducts[]>) => res.body)
      )
      .subscribe(
        (res: IMainProducts[]) => {
          this.mainProducts = res;
          this.mainProducts = this.mainProducts.filter(mainProduct => mainProduct.profileClass.toUpperCase() === 'PREPAID');
          if (!this.authorities.includes('ROLE_ADMIN')) {
            this.mainProducts = this.mainProducts.filter(mainProduct => mainProduct.mainProductName !== 'EPIN');
          }
          this.cos = this.mainProducts[0].mainProductId;
          this.creationType = 'IN_SWITCH';
          this.activatedRoute.data.subscribe(({ createSubscriber }) => {
            this.updateForm(createSubscriber);
          });
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(createSubscriber: ICreateSubscriber) {
    this.editForm.patchValue({
      id: createSubscriber.id,
      msisdn: createSubscriber.msisdn,
      imsi: createSubscriber.imsi,
      processingStatus: createSubscriber.processingStatus,
      statut: createSubscriber.statut,
      transactionId: createSubscriber.transactionId,
      backendResp: createSubscriber.backendResp,
      cos: createSubscriber.cos || this.mainProducts[0].mainProductId,
      creationType: createSubscriber.creationType || 'IN_SWITCH'
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const createSubscriber = this.createFromForm();
    if (!createSubscriber.msisdn.startsWith('221') && createSubscriber.msisdn.length === 9) {
      createSubscriber.msisdn = '221' + createSubscriber.msisdn;
    }
    if (createSubscriber.id !== undefined) {
      this.subscribeToSaveResponse(this.createSubscriberService.update(createSubscriber));
    } else {
      this.subscribeToSaveResponse(this.createSubscriberService.create(createSubscriber));
    }
  }

  onFileChange(event) {
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('creationType', this.creationType);
    formData.append('statut', this.statut);
    formData.append('cos', this.cos);
    this.subscribeToSaveResponse(this.createSubscriberService.upload(formData));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICreateSubscriber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['create-subscriber']);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): ICreateSubscriber {
    return {
      ...new CreateSubscriber(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      processingStatus: this.editForm.get(['processingStatus']).value,
      statut: this.editForm.get(['statut']).value,
      transactionId: this.editForm.get(['transactionId']).value,
      backendResp: this.editForm.get(['backendResp']).value,
      cos: this.editForm.get(['cos']).value,
      creationType: this.editForm.get(['creationType']).value
    };
  }
}
