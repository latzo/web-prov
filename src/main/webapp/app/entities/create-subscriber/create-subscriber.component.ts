import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ICreateSubscriber } from 'app/shared/model/create-subscriber.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { CreateSubscriberService } from './create-subscriber.service';
import { ExcelService } from 'app/shared/excel/excel.service';

@Component({
  selector: 'jhi-create-subscriber',
  templateUrl: './create-subscriber.component.html'
})
export class CreateSubscriberComponent implements OnInit, OnDestroy {
  currentAccount: any;
  createSubscribers: ICreateSubscriber[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  onSiblings = false;
  currentTransanctionId: string;

  constructor(
    protected createSubscriberService: CreateSubscriberService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private excelService: ExcelService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  exportAsXLSX(): void {
    this.createSubscriberService
      .query({
        page: 0,
        size: 1048575,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ICreateSubscriber[]>) => this.excelService.exportAsExcelFile(res.body, 'StatutCreationAbonnes_'),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadAll() {
    this.onSiblings = false;
    this.createSubscriberService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ICreateSubscriber[]>) => this.paginateCreateSubscribers(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  getAllSiblings(transanctionId: string) {
    if (transanctionId !== undefined && transanctionId != null) {
      this.createSubscriberService
        .getAllElementsOfABulk(transanctionId, {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<ICreateSubscriber[]>) => this.paginateCreateSubscribers(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      this.onSiblings = true;
      this.currentTransanctionId = transanctionId;
    }
  }

  loadPage(page: number) {
    console.log('Current page', page);
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/create-subscriber'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    if (this.onSiblings) {
      this.getAllSiblings(this.currentTransanctionId);
    } else {
      this.loadAll();
    }
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/create-subscriber',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCreateSubscribers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICreateSubscriber) {
    return item.id;
  }

  registerChangeInCreateSubscribers() {
    this.eventSubscriber = this.eventManager.subscribe('createSubscriberListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCreateSubscribers(data: ICreateSubscriber[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.createSubscribers = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
