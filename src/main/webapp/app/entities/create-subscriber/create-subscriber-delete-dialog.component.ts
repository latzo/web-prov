import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICreateSubscriber } from 'app/shared/model/create-subscriber.model';
import { CreateSubscriberService } from './create-subscriber.service';

@Component({
  selector: 'jhi-create-subscriber-delete-dialog',
  templateUrl: './create-subscriber-delete-dialog.component.html'
})
export class CreateSubscriberDeleteDialogComponent {
  createSubscriber: ICreateSubscriber;

  constructor(
    protected createSubscriberService: CreateSubscriberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.createSubscriberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'createSubscriberListModification',
        content: 'Deleted an createSubscriber'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-create-subscriber-delete-popup',
  template: ''
})
export class CreateSubscriberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ createSubscriber }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CreateSubscriberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.createSubscriber = createSubscriber;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/create-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/create-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
