import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CreateSubscriber, ICreateSubscriber } from 'app/shared/model/create-subscriber.model';
import { CreateSubscriberService } from './create-subscriber.service';
import { CreateSubscriberComponent } from './create-subscriber.component';
import { CreateSubscriberDetailComponent } from './create-subscriber-detail.component';
import { CreateSubscriberUpdateComponent } from './create-subscriber-update.component';
import { CreateSubscriberDeletePopupComponent } from './create-subscriber-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class CreateSubscriberResolve implements Resolve<ICreateSubscriber> {
  constructor(private service: CreateSubscriberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICreateSubscriber> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CreateSubscriber>) => response.ok),
        map((createSubscriber: HttpResponse<CreateSubscriber>) => createSubscriber.body)
      );
    }
    return of(new CreateSubscriber());
  }
}

export const createSubscriberRoute: Routes = [
  {
    path: '',
    component: CreateSubscriberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'CreateSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CreateSubscriberDetailComponent,
    resolve: {
      createSubscriber: CreateSubscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'CreateSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CreateSubscriberUpdateComponent,
    resolve: {
      createSubscriber: CreateSubscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'CreateSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CreateSubscriberUpdateComponent,
    resolve: {
      createSubscriber: CreateSubscriberResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'CreateSubscribers'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const createSubscriberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CreateSubscriberDeletePopupComponent,
    resolve: {
      createSubscriber: CreateSubscriberResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'CreateSubscribers'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
