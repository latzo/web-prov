import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  CreateSubscriberComponent,
  CreateSubscriberDetailComponent,
  CreateSubscriberUpdateComponent,
  CreateSubscriberDeletePopupComponent,
  CreateSubscriberDeleteDialogComponent,
  createSubscriberRoute,
  createSubscriberPopupRoute
} from './';

const ENTITY_STATES = [...createSubscriberRoute, ...createSubscriberPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CreateSubscriberComponent,
    CreateSubscriberDetailComponent,
    CreateSubscriberUpdateComponent,
    CreateSubscriberDeleteDialogComponent,
    CreateSubscriberDeletePopupComponent
  ],
  entryComponents: [
    CreateSubscriberComponent,
    CreateSubscriberUpdateComponent,
    CreateSubscriberDeleteDialogComponent,
    CreateSubscriberDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareCreateSubscriberModule {}
