import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICreateSubscriber } from 'app/shared/model/create-subscriber.model';

@Component({
  selector: 'jhi-create-subscriber-detail',
  templateUrl: './create-subscriber-detail.component.html'
})
export class CreateSubscriberDetailComponent implements OnInit {
  createSubscriber: ICreateSubscriber;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ createSubscriber }) => {
      this.createSubscriber = createSubscriber;
    });
  }

  previousState() {
    window.history.back();
  }
}
