import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IMnpPorting, MnpPorting } from 'app/shared/model/mnp-porting.model';
import { MnpPortingService } from './mnp-porting.service';

@Component({
  selector: 'jhi-mnp-porting-update',
  templateUrl: './mnp-porting-update.component.html'
})
export class MnpPortingUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    processingStatus: []
  });

  constructor(
    protected mnpPortingService: MnpPortingService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ mnpPorting }) => {
      this.updateForm(mnpPorting);
    });
  }

  updateForm(mnpPorting: IMnpPorting) {
    this.editForm.patchValue({
      id: mnpPorting.id,
      msisdn: mnpPorting.msisdn,
      processingStatus: mnpPorting.processingStatus
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const mnpPorting = this.createFromForm();
    if (!mnpPorting.msisdn.startsWith('221') && mnpPorting.msisdn.length === 9) {
      mnpPorting.msisdn = '221' + mnpPorting.msisdn;
    }
    if (mnpPorting.id !== undefined) {
      this.subscribeToSaveResponse(this.mnpPortingService.update(mnpPorting));
    } else {
      this.subscribeToSaveResponse(this.mnpPortingService.create(mnpPorting));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMnpPorting>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['mnp-porting']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IMnpPorting {
    return {
      ...new MnpPorting(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      processingStatus: this.editForm.get(['processingStatus']).value
    };
  }
}
