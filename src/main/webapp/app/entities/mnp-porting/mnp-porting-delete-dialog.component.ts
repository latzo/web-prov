import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMnpPorting } from 'app/shared/model/mnp-porting.model';
import { MnpPortingService } from './mnp-porting.service';

@Component({
  selector: 'jhi-mnp-porting-delete-dialog',
  templateUrl: './mnp-porting-delete-dialog.component.html'
})
export class MnpPortingDeleteDialogComponent {
  mnpPorting: IMnpPorting;

  constructor(
    protected mnpPortingService: MnpPortingService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.mnpPortingService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'mnpPortingListModification',
        content: 'Deleted an mnpPorting'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-mnp-porting-delete-popup',
  template: ''
})
export class MnpPortingDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mnpPorting }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MnpPortingDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.mnpPorting = mnpPorting;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/mnp-porting', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/mnp-porting', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
