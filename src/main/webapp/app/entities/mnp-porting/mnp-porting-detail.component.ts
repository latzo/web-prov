import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMnpPorting } from 'app/shared/model/mnp-porting.model';

@Component({
  selector: 'jhi-mnp-porting-detail',
  templateUrl: './mnp-porting-detail.component.html'
})
export class MnpPortingDetailComponent implements OnInit {
  mnpPorting: IMnpPorting;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mnpPorting }) => {
      this.mnpPorting = mnpPorting;
    });
  }

  previousState() {
    window.history.back();
  }
}
