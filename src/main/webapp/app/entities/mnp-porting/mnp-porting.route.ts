import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IMnpPorting, MnpPorting } from 'app/shared/model/mnp-porting.model';
import { MnpPortingService } from './mnp-porting.service';
import { MnpPortingComponent } from './mnp-porting.component';
import { MnpPortingDetailComponent } from './mnp-porting-detail.component';
import { MnpPortingUpdateComponent } from './mnp-porting-update.component';
import { MnpPortingDeletePopupComponent } from './mnp-porting-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class MnpPortingResolve implements Resolve<IMnpPorting> {
  constructor(private service: MnpPortingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMnpPorting> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MnpPorting>) => response.ok),
        map((mnpPorting: HttpResponse<MnpPorting>) => mnpPorting.body)
      );
    }
    return of(new MnpPorting());
  }
}

export const mnpPortingRoute: Routes = [
  {
    path: '',
    component: MnpPortingComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'MnpPortings'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MnpPortingDetailComponent,
    resolve: {
      mnpPorting: MnpPortingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MnpPortings'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MnpPortingUpdateComponent,
    resolve: {
      mnpPorting: MnpPortingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MnpPortings'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MnpPortingUpdateComponent,
    resolve: {
      mnpPorting: MnpPortingResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'MnpPortings'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const mnpPortingPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MnpPortingDeletePopupComponent,
    resolve: {
      mnpPorting: MnpPortingResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'MnpPortings'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
