import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  MnpPortingComponent,
  MnpPortingDetailComponent,
  MnpPortingUpdateComponent,
  MnpPortingDeletePopupComponent,
  MnpPortingDeleteDialogComponent,
  mnpPortingRoute,
  mnpPortingPopupRoute
} from './';

const ENTITY_STATES = [...mnpPortingRoute, ...mnpPortingPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MnpPortingComponent,
    MnpPortingDetailComponent,
    MnpPortingUpdateComponent,
    MnpPortingDeleteDialogComponent,
    MnpPortingDeletePopupComponent
  ],
  entryComponents: [MnpPortingComponent, MnpPortingUpdateComponent, MnpPortingDeleteDialogComponent, MnpPortingDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareMnpPortingModule {}
