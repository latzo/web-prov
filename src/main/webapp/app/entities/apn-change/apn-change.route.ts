import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { APNChange } from 'app/shared/model/apn-change.model';
import { APNChangeService } from './apn-change.service';
import { APNChangeComponent } from './apn-change.component';
import { APNChangeDetailComponent } from './apn-change-detail.component';
import { APNChangeUpdateComponent } from './apn-change-update.component';
import { APNChangeDeletePopupComponent } from './apn-change-delete-dialog.component';
import { IAPNChange } from 'app/shared/model/apn-change.model';

@Injectable({ providedIn: 'root' })
export class APNChangeResolve implements Resolve<IAPNChange> {
  constructor(private service: APNChangeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAPNChange> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<APNChange>) => response.ok),
        map((aPNChange: HttpResponse<APNChange>) => aPNChange.body)
      );
    }
    return of(new APNChange());
  }
}

export const aPNChangeRoute: Routes = [
  {
    path: '',
    component: APNChangeComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'APNChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: APNChangeDetailComponent,
    resolve: {
      aPNChange: APNChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'APNChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: APNChangeUpdateComponent,
    resolve: {
      aPNChange: APNChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'APNChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: APNChangeUpdateComponent,
    resolve: {
      aPNChange: APNChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'APNChanges'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const aPNChangePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: APNChangeDeletePopupComponent,
    resolve: {
      aPNChange: APNChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'APNChanges'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
