import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAPNChange, IAPNChangeVTalend } from 'app/shared/model/apn-change.model';

type EntityResponseType = HttpResponse<IAPNChange>;
type EntityArrayResponseType = HttpResponse<IAPNChange[]>;

@Injectable({ providedIn: 'root' })
export class APNChangeService {
  public resourceUrl = SERVER_API_URL + 'api/apn-changes';

  constructor(protected http: HttpClient) {}

  getAllElementsOfABulk(transanctionId: String, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAPNChange[]>(`${this.resourceUrl}/bulks/${transanctionId}`, { params: options, observe: 'response' });
  }

  create(aPNChange: IAPNChange): Observable<EntityResponseType> {
    return this.http.post<IAPNChange>(this.resourceUrl, aPNChange, { observe: 'response' });
  }

  update(aPNChange: IAPNChange): Observable<EntityResponseType> {
    return this.http.put<IAPNChange>(this.resourceUrl, aPNChange, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAPNChange>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAPNChange[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }

  createOrUpdate(aPNChange: IAPNChangeVTalend): Observable<EntityResponseType> {
    const url = this.resourceUrl + '/vTalend?';
    return this.http.post<IAPNChangeVTalend>(url, aPNChange, { observe: 'response' });
  }

  upload1(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/bulk/vTalend`, formData, { observe: 'response' });
  }
}
