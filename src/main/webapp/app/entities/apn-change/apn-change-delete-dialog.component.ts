import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAPNChange } from 'app/shared/model/apn-change.model';
import { APNChangeService } from './apn-change.service';

@Component({
  selector: 'jhi-apn-change-delete-dialog',
  templateUrl: './apn-change-delete-dialog.component.html'
})
export class APNChangeDeleteDialogComponent {
  aPNChange: IAPNChange;

  constructor(protected aPNChangeService: APNChangeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.aPNChangeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'aPNChangeListModification',
        content: 'Deleted an aPNChange'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-apn-change-delete-popup',
  template: ''
})
export class APNChangeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ aPNChange }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(APNChangeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.aPNChange = aPNChange;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/apn-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/apn-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
