import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';

import { IAPNChange } from 'app/shared/model/apn-change.model';
import { AccountService } from 'app/core';
import { APNChangeService } from './apn-change.service';
import { ITEMS_PER_PAGE } from 'app/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcelService } from 'app/shared/excel/excel.service';

@Component({
  selector: 'jhi-apn-change',
  templateUrl: './apn-change.component.html'
})
export class APNChangeComponent implements OnInit, OnDestroy {
  aPNChanges: IAPNChange[];
  currentAccount: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  onSiblings = false;
  currentTransanctionId: string;

  constructor(
    protected aPNChangeService: APNChangeService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private excelService: ExcelService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.aPNChanges, 'StatutAPNChange_');
  }

  loadAll() {
    this.onSiblings = false;
    this.aPNChangeService
      .query()
      .pipe(
        filter((res: HttpResponse<IAPNChange[]>) => res.ok),
        map((res: HttpResponse<IAPNChange[]>) => res.body)
      )
      .subscribe(
        (res: IAPNChange[]) => {
          this.aPNChanges = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/apn-change'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    if (this.onSiblings) {
      this.getAllSiblings(this.currentTransanctionId);
    } else {
      this.loadAll();
    }
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  getAllSiblings(transanctionId: string) {
    if (transanctionId !== undefined && transanctionId != null) {
      this.aPNChangeService
        .getAllElementsOfABulk(transanctionId, {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IAPNChange[]>) => this.paginateApnChanges(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      this.onSiblings = true;
      this.currentTransanctionId = transanctionId;
    }
  }

  protected paginateApnChanges(data: IAPNChange[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.aPNChanges = data;
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInAPNChanges();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IAPNChange) {
    return item.id;
  }

  registerChangeInAPNChanges() {
    this.eventSubscriber = this.eventManager.subscribe('aPNChangeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
