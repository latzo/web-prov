import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { APNChange, IAPNChange } from 'app/shared/model/apn-change.model';
import { APNChangeService } from './apn-change.service';

@Component({
  selector: 'jhi-apn-change-update',
  templateUrl: './apn-change-update.component.html'
})
export class APNChangeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    operation: [null, [Validators.required]],
    apn: [null, [Validators.required]],
    profil: [null, [Validators.required]],
    processingStatus: [],
    transactionId: [],
    createdBy: []
  });

  editForm1 = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    apn: [null, [Validators.required]],
    ipAddr: [null],
    technology: [null],
    operation: [null, [Validators.required]],
    processingStatus: [],
    profil: ['profil'],
    transactionId: [],
    createdBy: []
  });

  editFormBulk = this.fb.group({
    msisdn_bulk: [null, [Validators.required]],
    operation_bulk: [null, [Validators.required]],
    apn_bulk: [null, [Validators.required]],
    profil_bulk: [null, [Validators.required]],
    processingStatus: [],
    transactionId: [],
    createdBy: []
  });

  editFormBulk1 = this.fb.group({
    msisdn_bulk: [null, [Validators.required]],
    apn_bulk: [null, [Validators.required]],
    technology_bulk: [null, [Validators.required]],
    operation: [null, [Validators.required]],
    processingStatus: [],
    transactionId: [],
    createdBy: []
  });

  public msisdn_bulk: File;
  public operation_bulk: string;
  public apn_bulk: string;
  public profil_bulk: string;
  public technology_bulk: string;
  public operation: string;

  constructor(
    protected aPNChangeService: APNChangeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ aPNChange }) => {
      this.updateForm(aPNChange);
    });
  }

  onFileChange(event) {
    this.msisdn_bulk = event.target.files.length > 0 ? event.target.files[0] : null;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('msisdn_bulk', this.msisdn_bulk);
    formData.append('operation_bulk', this.operation_bulk);
    formData.append('apn_bulk', this.apn_bulk);
    formData.append('profil_bulk', this.profil_bulk);
    this.subscribeToSaveResponse(this.aPNChangeService.upload(formData));
  }

  uploadFile1() {
    const formData = new FormData();
    formData.append('msisdn_bulk', this.msisdn_bulk);
    formData.append('apn_bulk', this.apn_bulk);
    formData.append('technology_bulk', this.technology_bulk);
    formData.append('operation', this.operation);
    this.subscribeToSaveResponse(this.aPNChangeService.upload1(formData));
  }

  updateForm(aPNChange: IAPNChange) {
    this.editForm.patchValue({
      id: aPNChange.id,
      msisdn: aPNChange.msisdn,
      imsi: aPNChange.imsi,
      operation: aPNChange.operation,
      apn: aPNChange.apn,
      profil: aPNChange.profil,
      processingStatus: aPNChange.processingStatus,
      transactionId: aPNChange.transactionId,
      createdBy: aPNChange.createdBy
    });

    this.editForm1.patchValue({
      id: aPNChange.id,
      msisdn: aPNChange.msisdn,
      imsi: aPNChange.imsi,
      operation: aPNChange.operation,
      apn: aPNChange.apn,
      processingStatus: aPNChange.processingStatus,
      transactionId: aPNChange.transactionId,
      createdBy: aPNChange.createdBy
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const aPNChange = this.createFromForm();
    if (aPNChange.id !== undefined) {
      this.subscribeToSaveResponse(this.aPNChangeService.update(aPNChange));
    } else {
      this.subscribeToSaveResponse(this.aPNChangeService.create(aPNChange));
    }
  }

  save1() {
    this.isSaving = true;
    const apnChange = this.createFromForm1();
    if (apnChange.id !== undefined) {
      this.subscribeToSaveResponse(this.aPNChangeService.createOrUpdate(apnChange));
    } else {
      this.subscribeToSaveResponse(this.aPNChangeService.createOrUpdate(apnChange));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAPNChange>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['apn-change']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IAPNChange {
    return {
      ...new APNChange(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      operation: this.editForm.get(['operation']).value,
      apn: this.editForm.get(['apn']).value,
      profil: this.editForm.get(['profil']).value,
      processingStatus: this.editForm.get(['processingStatus']).value,
      transactionId: this.editForm.get(['transactionId']).value,
      createdBy: this.editForm.get(['createdBy']).value
    };
  }

  private createFromForm1() {
    return {
      ...new APNChange(),
      id: this.editForm1.get(['id']).value,
      msisdn: this.editForm1.get(['msisdn']).value,
      imsi: this.editForm1.get(['imsi']).value,
      apn: this.editForm1.get(['apn']).value,
      ipAddr: this.editForm1.get(['ipAddr']).value,
      technology: this.editForm1.get(['technology']).value,
      operation: this.editForm1.get(['operation']).value,
      processingStatus: this.editForm1.get(['processingStatus']).value,
      profil: 'profile',
      transactionId: this.editForm1.get(['transactionId']).value,
      createdBy: this.editForm1.get(['createdBy']).value
    };
  }
}
