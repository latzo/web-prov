import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAPNChange } from 'app/shared/model/apn-change.model';

@Component({
  selector: 'jhi-apn-change-detail',
  templateUrl: './apn-change-detail.component.html'
})
export class APNChangeDetailComponent implements OnInit {
  aPNChange: IAPNChange;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ aPNChange }) => {
      this.aPNChange = aPNChange;
    });
  }

  previousState() {
    window.history.back();
  }
}
