import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  APNChangeComponent,
  APNChangeDetailComponent,
  APNChangeUpdateComponent,
  APNChangeDeletePopupComponent,
  APNChangeDeleteDialogComponent,
  aPNChangeRoute,
  aPNChangePopupRoute
} from './';

const ENTITY_STATES = [...aPNChangeRoute, ...aPNChangePopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    APNChangeComponent,
    APNChangeDetailComponent,
    APNChangeUpdateComponent,
    APNChangeDeleteDialogComponent,
    APNChangeDeletePopupComponent
  ],
  entryComponents: [APNChangeComponent, APNChangeUpdateComponent, APNChangeDeleteDialogComponent, APNChangeDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareAPNChangeModule {}
