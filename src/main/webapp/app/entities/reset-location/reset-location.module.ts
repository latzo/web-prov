import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  ResetLocationComponent,
  ResetLocationDetailComponent,
  ResetLocationUpdateComponent,
  ResetLocationDeletePopupComponent,
  ResetLocationDeleteDialogComponent,
  resetLocationRoute,
  resetLocationPopupRoute
} from './';

const ENTITY_STATES = [...resetLocationRoute, ...resetLocationPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ResetLocationComponent,
    ResetLocationDetailComponent,
    ResetLocationUpdateComponent,
    ResetLocationDeleteDialogComponent,
    ResetLocationDeletePopupComponent
  ],
  entryComponents: [
    ResetLocationComponent,
    ResetLocationUpdateComponent,
    ResetLocationDeleteDialogComponent,
    ResetLocationDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareResetLocationModule {}
