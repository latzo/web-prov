import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IResetLocation, ResetLocation } from 'app/shared/model/reset-location.model';
import { ResetLocationService } from './reset-location.service';

@Component({
  selector: 'jhi-reset-location-update',
  templateUrl: './reset-location-update.component.html'
})
export class ResetLocationUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]]
  });

  constructor(
    protected resetLocationService: ResetLocationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ resetLocation }) => {
      this.updateForm(resetLocation);
    });
  }

  updateForm(resetLocation: IResetLocation) {
    this.editForm.patchValue({
      id: resetLocation.id,
      msisdn: resetLocation.msisdn
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const resetLocation = this.createFromForm();
    if (resetLocation.id !== undefined) {
      this.subscribeToSaveResponse(this.resetLocationService.update(resetLocation));
    } else {
      this.subscribeToSaveResponse(this.resetLocationService.create(resetLocation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IResetLocation>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['reset-location']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IResetLocation {
    return {
      ...new ResetLocation(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value
    };
  }
}
