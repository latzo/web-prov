import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ResetLocation } from 'app/shared/model/reset-location.model';
import { ResetLocationService } from './reset-location.service';
import { ResetLocationComponent } from './reset-location.component';
import { ResetLocationDetailComponent } from './reset-location-detail.component';
import { ResetLocationUpdateComponent } from './reset-location-update.component';
import { ResetLocationDeletePopupComponent } from './reset-location-delete-dialog.component';
import { IResetLocation } from 'app/shared/model/reset-location.model';

@Injectable({ providedIn: 'root' })
export class ResetLocationResolve implements Resolve<IResetLocation> {
  constructor(private service: ResetLocationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IResetLocation> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ResetLocation>) => response.ok),
        map((resetLocation: HttpResponse<ResetLocation>) => resetLocation.body)
      );
    }
    return of(new ResetLocation());
  }
}

export const resetLocationRoute: Routes = [
  {
    path: '',
    component: ResetLocationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'ResetLocations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ResetLocationDetailComponent,
    resolve: {
      resetLocation: ResetLocationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ResetLocations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ResetLocationUpdateComponent,
    resolve: {
      resetLocation: ResetLocationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ResetLocations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ResetLocationUpdateComponent,
    resolve: {
      resetLocation: ResetLocationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ResetLocations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const resetLocationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ResetLocationDeletePopupComponent,
    resolve: {
      resetLocation: ResetLocationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ResetLocations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
