import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResetLocation } from 'app/shared/model/reset-location.model';

@Component({
  selector: 'jhi-reset-location-detail',
  templateUrl: './reset-location-detail.component.html'
})
export class ResetLocationDetailComponent implements OnInit {
  resetLocation: IResetLocation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ resetLocation }) => {
      this.resetLocation = resetLocation;
    });
  }

  previousState() {
    window.history.back();
  }
}
