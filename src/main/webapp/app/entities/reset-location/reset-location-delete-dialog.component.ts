import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IResetLocation } from 'app/shared/model/reset-location.model';
import { ResetLocationService } from './reset-location.service';

@Component({
  selector: 'jhi-reset-location-delete-dialog',
  templateUrl: './reset-location-delete-dialog.component.html'
})
export class ResetLocationDeleteDialogComponent {
  resetLocation: IResetLocation;

  constructor(
    protected resetLocationService: ResetLocationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.resetLocationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'resetLocationListModification',
        content: 'Deleted an resetLocation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-reset-location-delete-popup',
  template: ''
})
export class ResetLocationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ resetLocation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ResetLocationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.resetLocation = resetLocation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/reset-location', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/reset-location', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
