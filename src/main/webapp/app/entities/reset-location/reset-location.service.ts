import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IResetLocation } from 'app/shared/model/reset-location.model';

type EntityResponseType = HttpResponse<IResetLocation>;
type EntityArrayResponseType = HttpResponse<IResetLocation[]>;

@Injectable({ providedIn: 'root' })
export class ResetLocationService {
  public resourceUrl = SERVER_API_URL + 'api/reset-locations';

  constructor(protected http: HttpClient) {}

  create(resetLocation: IResetLocation): Observable<EntityResponseType> {
    return this.http.post<IResetLocation>(this.resourceUrl, resetLocation, { observe: 'response' });
  }

  update(resetLocation: IResetLocation): Observable<EntityResponseType> {
    return this.http.put<IResetLocation>(this.resourceUrl, resetLocation, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IResetLocation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IResetLocation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
