import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IArchivage } from 'app/shared/model/archivage.model';
import { ArchivageService } from './archivage.service';

@Component({
  selector: 'jhi-archivage-delete-dialog',
  templateUrl: './archivage-delete-dialog.component.html'
})
export class ArchivageDeleteDialogComponent {
  archivage: IArchivage;

  constructor(protected archivageService: ArchivageService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.archivageService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'archivageListModification',
        content: 'Deleted an archivage'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-archivage-delete-popup',
  template: ''
})
export class ArchivageDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ archivage }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ArchivageDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.archivage = archivage;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/archivage', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/archivage', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
