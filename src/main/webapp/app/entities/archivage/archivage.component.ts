import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IArchivage } from 'app/shared/model/archivage.model';
import { AccountService } from 'app/core/auth/account.service';
import { ArchivageService } from './archivage.service';

@Component({
  selector: 'jhi-archivage',
  templateUrl: './archivage.component.html'
})
export class ArchivageComponent implements OnInit, OnDestroy {
  archivages: IArchivage[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected archivageService: ArchivageService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.archivageService
      .query()
      .pipe(
        filter((res: HttpResponse<IArchivage[]>) => res.ok),
        map((res: HttpResponse<IArchivage[]>) => res.body)
      )
      .subscribe(
        (res: IArchivage[]) => {
          this.archivages = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInArchivages();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IArchivage) {
    return item.id;
  }

  registerChangeInArchivages() {
    this.eventSubscriber = this.eventManager.subscribe('archivageListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
