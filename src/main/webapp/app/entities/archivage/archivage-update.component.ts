import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IArchivage, Archivage } from 'app/shared/model/archivage.model';
import { ArchivageService } from './archivage.service';
import { JhiEventManager, JhiAlert, JhiAlertService } from 'ng-jhipster';

@Component({
  selector: 'jhi-archivage-update',
  templateUrl: './archivage-update.component.html'
})
export class ArchivageUpdateComponent implements OnInit {
  isSaving: boolean;
  err = true;
  echec: number;

  editForm = this.fb.group({
    id: [],
    msisdn: []
  });

  constructor(
    protected archivageService: ArchivageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private alertService: JhiAlertService
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ archivage }) => {
      this.updateForm(archivage);
    });
  }

  updateForm(archivage: IArchivage) {
    this.editForm.patchValue({
      id: archivage.id,
      msisdn: archivage.msisdn
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const archivage = this.createFromForm();
    /*  if (archivage.id !== undefined) {
      this.subscribeToSaveResponse(this.archivageService.update(archivage));
    } else {
      this.subscribeToSaveResponse(this.archivageService.create(archivage));
    }*/

    this.archivageService.find(archivage.msisdn);
  }
  archive(msisdn): void {
    console.log(msisdn);

    this.archivageService.find(msisdn).subscribe(data => {
      console.log(data);
      if (!(<any>data).includes('<code>SC0000</code>')) {
        console.log('dedans');
        document.getElementById('arch-alert').style.display = 'block';
        this.err = false;
        this.echec = 1;
      } else {
        document.getElementById('arch-alert').style.display = 'block';
        this.err = false;
        this.echec = 2;
      }
    });
  }
  closeWindow() {
    document.getElementById('arch-alert').style.display = 'none';
    this.err = true;
  }
  private createFromForm(): IArchivage {
    return {
      ...new Archivage(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IArchivage>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
