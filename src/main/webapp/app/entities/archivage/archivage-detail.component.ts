import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IArchivage } from 'app/shared/model/archivage.model';

@Component({
  selector: 'jhi-archivage-detail',
  templateUrl: './archivage-detail.component.html'
})
export class ArchivageDetailComponent implements OnInit {
  archivage: IArchivage;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ archivage }) => {
      this.archivage = archivage;
    });
  }

  previousState() {
    window.history.back();
  }
}
