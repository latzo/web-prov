import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Archivage } from 'app/shared/model/archivage.model';
import { ArchivageService } from './archivage.service';
import { ArchivageComponent } from './archivage.component';
import { ArchivageDetailComponent } from './archivage-detail.component';
import { ArchivageUpdateComponent } from './archivage-update.component';
import { ArchivageDeletePopupComponent } from './archivage-delete-dialog.component';
import { IArchivage } from 'app/shared/model/archivage.model';

@Injectable({ providedIn: 'root' })
export class ArchivageResolve implements Resolve<IArchivage> {
  constructor(private service: ArchivageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IArchivage> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Archivage>) => response.ok),
        map((archivage: HttpResponse<Archivage>) => archivage.body)
      );
    }
    return of(new Archivage());
  }
}

export const archivageRoute: Routes = [
  {
    path: '',
    component: ArchivageComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Archivages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ArchivageDetailComponent,
    resolve: {
      archivage: ArchivageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Archivages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ArchivageUpdateComponent,
    resolve: {
      archivage: ArchivageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Archivages'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ArchivageUpdateComponent,
    resolve: {
      archivage: ArchivageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Archivages'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const archivagePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ArchivageDeletePopupComponent,
    resolve: {
      archivage: ArchivageResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Archivages'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
