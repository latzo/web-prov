import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IArchivage } from 'app/shared/model/archivage.model';

type EntityResponseType = HttpResponse<IArchivage>;
type EntityArrayResponseType = HttpResponse<IArchivage[]>;

@Injectable({ providedIn: 'root' })
export class ArchivageService {
  public resourceUrl = SERVER_API_URL + 'api/archivages';

  constructor(protected http: HttpClient) {}

  create(archivage: IArchivage): Observable<EntityResponseType> {
    return this.http.post<IArchivage>(this.resourceUrl, archivage, { observe: 'response' });
  }

  update(archivage: IArchivage): Observable<EntityResponseType> {
    return this.http.put<IArchivage>(this.resourceUrl, archivage, { observe: 'response' });
  }

  find(id: String): Observable<EntityResponseType> {
    return this.http.get<any>(`${this.resourceUrl}/${id}`, { responseType: 'text' as 'json' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IArchivage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
