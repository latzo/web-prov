import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared/shared.module';
import { ArchivageComponent } from './archivage.component';
import { ArchivageDetailComponent } from './archivage-detail.component';
import { ArchivageUpdateComponent } from './archivage-update.component';
import { ArchivageDeletePopupComponent, ArchivageDeleteDialogComponent } from './archivage-delete-dialog.component';
import { archivageRoute, archivagePopupRoute } from './archivage.route';

const ENTITY_STATES = [...archivageRoute, ...archivagePopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ArchivageComponent,
    ArchivageDetailComponent,
    ArchivageUpdateComponent,
    ArchivageDeleteDialogComponent,
    ArchivageDeletePopupComponent
  ],
  entryComponents: [ArchivageDeleteDialogComponent]
})
export class TigocareArchivageModule {}
