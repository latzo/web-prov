import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRoamingService } from 'app/shared/model/roaming-service.model';
import { RoamingServiceService } from './roaming-service.service';

@Component({
  selector: 'jhi-roaming-service-delete-dialog',
  templateUrl: './roaming-service-delete-dialog.component.html'
})
export class RoamingServiceDeleteDialogComponent {
  roamingService: IRoamingService;

  constructor(
    protected roamingServiceService: RoamingServiceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.roamingServiceService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'roamingServiceListModification',
        content: 'Deleted an roamingService'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-roaming-service-delete-popup',
  template: ''
})
export class RoamingServiceDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ roamingService }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RoamingServiceDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.roamingService = roamingService;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/roaming-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/roaming-service', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
