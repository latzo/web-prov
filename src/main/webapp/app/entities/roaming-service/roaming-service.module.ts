import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  RoamingServiceComponent,
  RoamingServiceDetailComponent,
  RoamingServiceUpdateComponent,
  RoamingServiceDeletePopupComponent,
  RoamingServiceDeleteDialogComponent,
  roamingServiceRoute,
  roamingServicePopupRoute
} from './';

const ENTITY_STATES = [...roamingServiceRoute, ...roamingServicePopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RoamingServiceComponent,
    RoamingServiceDetailComponent,
    RoamingServiceUpdateComponent,
    RoamingServiceDeleteDialogComponent,
    RoamingServiceDeletePopupComponent
  ],
  entryComponents: [
    RoamingServiceComponent,
    RoamingServiceUpdateComponent,
    RoamingServiceDeleteDialogComponent,
    RoamingServiceDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareRoamingServiceModule {}
