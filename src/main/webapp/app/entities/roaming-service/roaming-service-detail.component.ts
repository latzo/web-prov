import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRoamingService } from 'app/shared/model/roaming-service.model';

@Component({
  selector: 'jhi-roaming-service-detail',
  templateUrl: './roaming-service-detail.component.html'
})
export class RoamingServiceDetailComponent implements OnInit {
  roamingService: IRoamingService;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ roamingService }) => {
      this.roamingService = roamingService;
    });
  }

  previousState() {
    window.history.back();
  }
}
