import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRoamingService } from 'app/shared/model/roaming-service.model';

type EntityResponseType = HttpResponse<IRoamingService>;
type EntityArrayResponseType = HttpResponse<IRoamingService[]>;

@Injectable({ providedIn: 'root' })
export class RoamingServiceService {
  public resourceUrl = SERVER_API_URL + 'api/roaming-services';

  constructor(protected http: HttpClient) {}

  create(roamingService: IRoamingService): Observable<EntityResponseType> {
    return this.http.post<IRoamingService>(this.resourceUrl, roamingService, { observe: 'response' });
  }

  update(roamingService: IRoamingService): Observable<EntityResponseType> {
    return this.http.put<IRoamingService>(this.resourceUrl, roamingService, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRoamingService>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRoamingService[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
