import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IRoamingService, RoamingService } from 'app/shared/model/roaming-service.model';
import { RoamingServiceService } from './roaming-service.service';

@Component({
  selector: 'jhi-roaming-service-update',
  templateUrl: './roaming-service-update.component.html'
})
export class RoamingServiceUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    services: [null, [Validators.required]]
  });

  editFormBulk = this.fb.group({
    msisdn_bulk: [null, [Validators.required]],
    service: [null, [Validators.required]]
  });

  private msisdn_bulk: File;
  service_bulk: string;

  constructor(
    protected roamingServiceService: RoamingServiceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ roamingService }) => {
      this.updateForm(roamingService);
    });
  }

  updateForm(roamingService: IRoamingService) {
    this.editForm.patchValue({
      id: roamingService.id,
      msisdn: roamingService.msisdn,
      services: roamingService.services
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const roamingService = this.createFromForm();
    if (roamingService.id !== undefined) {
      this.subscribeToSaveResponse(this.roamingServiceService.update(roamingService));
    } else {
      this.subscribeToSaveResponse(this.roamingServiceService.create(roamingService));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRoamingService>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['roaming-service']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IRoamingService {
    return {
      ...new RoamingService(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      services: this.editForm.get(['services']).value
    };
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('msisdn_bulk', this.msisdn_bulk);
    formData.append('service_bulk', this.editFormBulk.get('service').value);
    this.subscribeToSaveResponse(this.roamingServiceService.upload(formData));
  }

  onFileChange(event) {
    this.msisdn_bulk = event.target.files.length > 0 ? event.target.files[0] : null;
  }
}
