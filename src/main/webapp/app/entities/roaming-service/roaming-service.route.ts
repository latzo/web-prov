import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { RoamingService } from 'app/shared/model/roaming-service.model';
import { RoamingServiceService } from './roaming-service.service';
import { RoamingServiceComponent } from './roaming-service.component';
import { RoamingServiceDetailComponent } from './roaming-service-detail.component';
import { RoamingServiceUpdateComponent } from './roaming-service-update.component';
import { RoamingServiceDeletePopupComponent } from './roaming-service-delete-dialog.component';
import { IRoamingService } from 'app/shared/model/roaming-service.model';

@Injectable({ providedIn: 'root' })
export class RoamingServiceResolve implements Resolve<IRoamingService> {
  constructor(private service: RoamingServiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRoamingService> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<RoamingService>) => response.ok),
        map((roamingService: HttpResponse<RoamingService>) => roamingService.body)
      );
    }
    return of(new RoamingService());
  }
}

export const roamingServiceRoute: Routes = [
  {
    path: '',
    component: RoamingServiceComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'RoamingServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RoamingServiceDetailComponent,
    resolve: {
      roamingService: RoamingServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RoamingServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RoamingServiceUpdateComponent,
    resolve: {
      roamingService: RoamingServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RoamingServices'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RoamingServiceUpdateComponent,
    resolve: {
      roamingService: RoamingServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RoamingServices'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const roamingServicePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RoamingServiceDeletePopupComponent,
    resolve: {
      roamingService: RoamingServiceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'RoamingServices'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
