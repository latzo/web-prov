import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISimSwap } from 'app/shared/model/sim-swap.model';
import { SimSwapService } from './sim-swap.service';

@Component({
  selector: 'jhi-sim-swap-delete-dialog',
  templateUrl: './sim-swap-delete-dialog.component.html'
})
export class SimSwapDeleteDialogComponent {
  simSwap: ISimSwap;

  constructor(protected simSwapService: SimSwapService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.simSwapService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'simSwapListModification',
        content: 'Deleted an simSwap'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-sim-swap-delete-popup',
  template: ''
})
export class SimSwapDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ simSwap }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SimSwapDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.simSwap = simSwap;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/sim-swap', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/sim-swap', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
