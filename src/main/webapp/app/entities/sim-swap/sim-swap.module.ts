import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FileSelectDirective } from 'ng2-file-upload';

import { TigocareSharedModule } from 'app/shared';
import {
  SimSwapComponent,
  SimSwapDetailComponent,
  SimSwapUpdateComponent,
  SimSwapDeletePopupComponent,
  SimSwapDeleteDialogComponent,
  simSwapRoute,
  simSwapPopupRoute
} from './';

const ENTITY_STATES = [...simSwapRoute, ...simSwapPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SimSwapComponent,
    SimSwapDetailComponent,
    SimSwapUpdateComponent,
    SimSwapDeleteDialogComponent,
    SimSwapDeletePopupComponent
  ],
  entryComponents: [SimSwapComponent, SimSwapUpdateComponent, SimSwapDeleteDialogComponent, SimSwapDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareSimSwapModule {}
