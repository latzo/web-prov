import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ISimSwap, SimSwap } from 'app/shared/model/sim-swap.model';
import { SimSwapService } from './sim-swap.service';
import { SimSwapComponent } from './sim-swap.component';
import { SimSwapDetailComponent } from './sim-swap-detail.component';
import { SimSwapUpdateComponent } from './sim-swap-update.component';
import { SimSwapDeletePopupComponent } from './sim-swap-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class SimSwapResolve implements Resolve<ISimSwap> {
  constructor(private service: SimSwapService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISimSwap> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SimSwap>) => response.ok),
        map((simSwap: HttpResponse<SimSwap>) => simSwap.body)
      );
    }
    return of(new SimSwap());
  }
}

export const simSwapRoute: Routes = [
  {
    path: '',
    component: SimSwapComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'SimSwaps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SimSwapDetailComponent,
    resolve: {
      simSwap: SimSwapResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SimSwaps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SimSwapUpdateComponent,
    resolve: {
      simSwap: SimSwapResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'SimSwaps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SimSwapUpdateComponent,
    resolve: {
      simSwap: SimSwapResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SimSwaps'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const simSwapPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SimSwapDeletePopupComponent,
    resolve: {
      simSwap: SimSwapResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SimSwaps'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
