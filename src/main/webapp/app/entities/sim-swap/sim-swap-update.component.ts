import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ISimSwap, SimSwap } from 'app/shared/model/sim-swap.model';
import { SimSwapService } from './sim-swap.service';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'jhi-sim-swap-update',
  templateUrl: './sim-swap-update.component.html'
})
export class SimSwapUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: ['76', [Validators.required]],
    oldIMSI: [null, [Validators.required]],
    newIMSI: [null, [Validators.required]]
  });

  public uploader: FileUploader = new FileUploader({ url: '', itemAlias: 'csvFile' });
  private file: File;

  constructor(
    protected simSwapService: SimSwapService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ simSwap }) => {
      this.updateForm(simSwap);
    });
  }

  updateForm(simSwap: ISimSwap) {
    this.editForm.patchValue({
      id: simSwap.id,
      msisdn: simSwap.msisdn,
      oldIMSI: simSwap.oldIMSI,
      newIMSI: simSwap.newIMSI
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const simSwap = this.createFromForm();
    if (!simSwap.msisdn.startsWith('221') && simSwap.msisdn.length === 9) {
      simSwap.msisdn = '221' + simSwap.msisdn;
    }
    if (simSwap.id !== undefined) {
      this.subscribeToSaveResponse(this.simSwapService.update(simSwap));
    } else {
      this.subscribeToSaveResponse(this.simSwapService.create(simSwap));
    }
  }

  private createFromForm(): ISimSwap {
    return {
      ...new SimSwap(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      oldIMSI: this.editForm.get(['oldIMSI']).value,
      newIMSI: this.editForm.get(['newIMSI']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISimSwap>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['sim-swap']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  onFileChange(event) {
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('file', this.file);
    this.subscribeToSaveResponse(this.simSwapService.upload(formData));
  }
}
