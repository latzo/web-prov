import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISimSwap } from 'app/shared/model/sim-swap.model';

type EntityResponseType = HttpResponse<ISimSwap>;
type EntityArrayResponseType = HttpResponse<ISimSwap[]>;

@Injectable({ providedIn: 'root' })
export class SimSwapService {
  public resourceUrl = SERVER_API_URL + 'api/sim-swaps';

  constructor(protected http: HttpClient) {}

  create(simSwap: ISimSwap): Observable<EntityResponseType> {
    return this.http.post<ISimSwap>(this.resourceUrl, simSwap, { observe: 'response' });
  }

  update(simSwap: ISimSwap): Observable<EntityResponseType> {
    return this.http.put<ISimSwap>(this.resourceUrl, simSwap, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISimSwap>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAllElementsOfABulk(transanctionId: String, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISimSwap[]>(`${this.resourceUrl}/bulks/${transanctionId}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISimSwap[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData): Observable<EntityResponseType> {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
