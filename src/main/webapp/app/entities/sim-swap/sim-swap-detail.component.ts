import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISimSwap } from 'app/shared/model/sim-swap.model';

@Component({
  selector: 'jhi-sim-swap-detail',
  templateUrl: './sim-swap-detail.component.html'
})
export class SimSwapDetailComponent implements OnInit {
  simSwap: ISimSwap;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ simSwap }) => {
      this.simSwap = simSwap;
    });
  }

  previousState() {
    window.history.back();
  }
}
