import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMnpPortout } from 'app/shared/model/mnp-portout.model';
import { MnpPortoutService } from './mnp-portout.service';

@Component({
  selector: 'jhi-mnp-portout-delete-dialog',
  templateUrl: './mnp-portout-delete-dialog.component.html'
})
export class MnpPortoutDeleteDialogComponent {
  mnpPortout: IMnpPortout;

  constructor(
    protected mnpPortoutService: MnpPortoutService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.mnpPortoutService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'mnpPortoutListModification',
        content: 'Deleted an mnpPortout'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-mnp-portout-delete-popup',
  template: ''
})
export class MnpPortoutDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mnpPortout }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MnpPortoutDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.mnpPortout = mnpPortout;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/mnp-portout', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/mnp-portout', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
