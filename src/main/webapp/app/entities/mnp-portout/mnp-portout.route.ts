import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IMnpPortout, MnpPortout } from 'app/shared/model/mnp-portout.model';
import { MnpPortoutService } from './mnp-portout.service';
import { MnpPortoutComponent } from './mnp-portout.component';
import { MnpPortoutDetailComponent } from './mnp-portout-detail.component';
import { MnpPortoutUpdateComponent } from './mnp-portout-update.component';
import { MnpPortoutDeletePopupComponent } from './mnp-portout-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class MnpPortoutResolve implements Resolve<IMnpPortout> {
  constructor(private service: MnpPortoutService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMnpPortout> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MnpPortout>) => response.ok),
        map((mnpPortout: HttpResponse<MnpPortout>) => mnpPortout.body)
      );
    }
    return of(new MnpPortout());
  }
}

export const mnpPortoutRoute: Routes = [
  {
    path: '',
    component: MnpPortoutComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'MnpPortouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MnpPortoutDetailComponent,
    resolve: {
      mnpPortout: MnpPortoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MnpPortouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MnpPortoutUpdateComponent,
    resolve: {
      mnpPortout: MnpPortoutResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'MnpPortouts'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MnpPortoutUpdateComponent,
    resolve: {
      mnpPortout: MnpPortoutResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'MnpPortouts'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const mnpPortoutPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MnpPortoutDeletePopupComponent,
    resolve: {
      mnpPortout: MnpPortoutResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'MnpPortouts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
