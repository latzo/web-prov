import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IMnpPortout, MnpPortout } from 'app/shared/model/mnp-portout.model';
import { MnpPortoutService } from './mnp-portout.service';

@Component({
  selector: 'jhi-mnp-portout-update',
  templateUrl: './mnp-portout-update.component.html'
})
export class MnpPortoutUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    operateur: [null, [Validators.required]],
    processingStatus: []
  });

  constructor(
    protected mnpPortoutService: MnpPortoutService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ mnpPortout }) => {
      this.updateForm(mnpPortout);
    });
  }

  updateForm(mnpPortout: IMnpPortout) {
    this.editForm.patchValue({
      id: mnpPortout.id,
      msisdn: mnpPortout.msisdn,
      operateur: mnpPortout.operateur,
      processingStatus: mnpPortout.processingStatus
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const mnpPortout = this.createFromForm();
    if (!mnpPortout.msisdn.startsWith('221') && mnpPortout.msisdn.length === 9) {
      mnpPortout.msisdn = '221' + mnpPortout.msisdn;
    }
    if (mnpPortout.id !== undefined) {
      this.subscribeToSaveResponse(this.mnpPortoutService.update(mnpPortout));
    } else {
      this.subscribeToSaveResponse(this.mnpPortoutService.create(mnpPortout));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMnpPortout>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['mnp-portout']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  private createFromForm(): IMnpPortout {
    return {
      ...new MnpPortout(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      operateur: this.editForm.get(['operateur']).value,
      processingStatus: this.editForm.get(['processingStatus']).value
    };
  }
}
