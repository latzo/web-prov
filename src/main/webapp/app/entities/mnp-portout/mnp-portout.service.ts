import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMnpPortout } from 'app/shared/model/mnp-portout.model';

type EntityResponseType = HttpResponse<IMnpPortout>;
type EntityArrayResponseType = HttpResponse<IMnpPortout[]>;

@Injectable({ providedIn: 'root' })
export class MnpPortoutService {
  public resourceUrl = SERVER_API_URL + 'api/mnp-portouts';

  constructor(protected http: HttpClient) {}

  create(mnpPortout: IMnpPortout): Observable<EntityResponseType> {
    return this.http.post<IMnpPortout>(this.resourceUrl, mnpPortout, { observe: 'response' });
  }

  update(mnpPortout: IMnpPortout): Observable<EntityResponseType> {
    return this.http.put<IMnpPortout>(this.resourceUrl, mnpPortout, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMnpPortout>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMnpPortout[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
