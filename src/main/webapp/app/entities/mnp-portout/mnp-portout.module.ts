import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  MnpPortoutComponent,
  MnpPortoutDetailComponent,
  MnpPortoutUpdateComponent,
  MnpPortoutDeletePopupComponent,
  MnpPortoutDeleteDialogComponent,
  mnpPortoutRoute,
  mnpPortoutPopupRoute
} from './';

const ENTITY_STATES = [...mnpPortoutRoute, ...mnpPortoutPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MnpPortoutComponent,
    MnpPortoutDetailComponent,
    MnpPortoutUpdateComponent,
    MnpPortoutDeleteDialogComponent,
    MnpPortoutDeletePopupComponent
  ],
  entryComponents: [MnpPortoutComponent, MnpPortoutUpdateComponent, MnpPortoutDeleteDialogComponent, MnpPortoutDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareMnpPortoutModule {}
