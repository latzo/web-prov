import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMnpPortout } from 'app/shared/model/mnp-portout.model';

@Component({
  selector: 'jhi-mnp-portout-detail',
  templateUrl: './mnp-portout-detail.component.html'
})
export class MnpPortoutDetailComponent implements OnInit {
  mnpPortout: IMnpPortout;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ mnpPortout }) => {
      this.mnpPortout = mnpPortout;
    });
  }

  previousState() {
    window.history.back();
  }
}
