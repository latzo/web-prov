import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { TigocareSharedModule } from 'app/shared';
import { ExcelService } from './../../shared/excel/excel.service';

import {
  ServiceChangeComponent,
  ServiceChangeDetailComponent,
  ServiceChangeUpdateComponent,
  ServiceChangeDeletePopupComponent,
  ServiceChangeDeleteDialogComponent,
  serviceChangeRoute,
  serviceChangePopupRoute
} from './';

const ENTITY_STATES = [...serviceChangeRoute, ...serviceChangePopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES), AgGridModule.withComponents([])],
  declarations: [
    ServiceChangeComponent,
    ServiceChangeDetailComponent,
    ServiceChangeUpdateComponent,
    ServiceChangeDeleteDialogComponent,
    ServiceChangeDeletePopupComponent
  ],
  entryComponents: [
    ServiceChangeComponent,
    ServiceChangeUpdateComponent,
    ServiceChangeDeleteDialogComponent,
    ServiceChangeDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareServiceChangeModule {}
