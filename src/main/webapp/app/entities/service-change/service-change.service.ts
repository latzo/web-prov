import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IService, IServiceChange } from 'app/shared/model/service-change.model';

type EntityResponseType = HttpResponse<IServiceChange>;
type EntityArrayResponseType = HttpResponse<IServiceChange[]>;

@Injectable({ providedIn: 'root' })
export class ServiceChangeService {
  public resourceUrl = SERVER_API_URL + 'api/service-changes';

  constructor(protected http: HttpClient) {}

  create(serviceChange: IServiceChange): Observable<EntityResponseType> {
    return this.http.post<IServiceChange>(this.resourceUrl, serviceChange, { observe: 'response' });
  }

  getAllElementsOfABulk(transanctionId: String, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IServiceChange[]>(`${this.resourceUrl}/bulks/${transanctionId}`, { params: options, observe: 'response' });
  }

  update(serviceChange: IServiceChange): Observable<EntityResponseType> {
    return this.http.put<IServiceChange>(this.resourceUrl, serviceChange, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IServiceChange>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IServiceChange[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  upload(formData: FormData): Observable<HttpResponse<IServiceChange>> {
    return this.http.post<any>(`${this.resourceUrl}/upload`, formData, { observe: 'response' });
  }
}
