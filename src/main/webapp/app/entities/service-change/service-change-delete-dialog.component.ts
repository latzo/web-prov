import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IServiceChange } from 'app/shared/model/service-change.model';
import { ServiceChangeService } from './service-change.service';

@Component({
  selector: 'jhi-service-change-delete-dialog',
  templateUrl: './service-change-delete-dialog.component.html'
})
export class ServiceChangeDeleteDialogComponent {
  serviceChange: IServiceChange;

  constructor(
    protected serviceChangeService: ServiceChangeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.serviceChangeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'serviceChangeListModification',
        content: 'Deleted an serviceChange'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-service-change-delete-popup',
  template: ''
})
export class ServiceChangeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ serviceChange }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ServiceChangeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.serviceChange = serviceChange;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/service-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/service-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
