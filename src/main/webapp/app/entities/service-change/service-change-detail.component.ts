import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IServiceChange } from 'app/shared/model/service-change.model';

@Component({
  selector: 'jhi-service-change-detail',
  templateUrl: './service-change-detail.component.html'
})
export class ServiceChangeDetailComponent implements OnInit {
  serviceChange: IServiceChange;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ serviceChange }) => {
      this.serviceChange = serviceChange;
    });
  }

  previousState() {
    window.history.back();
  }
}
