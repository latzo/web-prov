import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IServiceChange, ServiceChange } from 'app/shared/model/service-change.model';
import { ServiceChangeService } from './service-change.service';
import { ServiceChangeComponent } from './service-change.component';
import { ServiceChangeDetailComponent } from './service-change-detail.component';
import { ServiceChangeUpdateComponent } from './service-change-update.component';
import { ServiceChangeDeletePopupComponent } from './service-change-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ServiceChangeResolve implements Resolve<IServiceChange> {
  constructor(private service: ServiceChangeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IServiceChange> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ServiceChange>) => response.ok),
        map((serviceChange: HttpResponse<ServiceChange>) => serviceChange.body)
      );
    }
    return of(new ServiceChange());
  }
}

export const serviceChangeRoute: Routes = [
  {
    path: '',
    component: ServiceChangeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'ServiceChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ServiceChangeDetailComponent,
    resolve: {
      serviceChange: ServiceChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ServiceChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ServiceChangeUpdateComponent,
    resolve: {
      serviceChange: ServiceChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ServiceChanges'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ServiceChangeUpdateComponent,
    resolve: {
      serviceChange: ServiceChangeResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ServiceChanges'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const serviceChangePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ServiceChangeDeletePopupComponent,
    resolve: {
      serviceChange: ServiceChangeResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ServiceChanges'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
