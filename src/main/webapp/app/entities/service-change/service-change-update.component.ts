import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IService, IServiceChange, OperationEnum, ServiceChange } from 'app/shared/model/service-change.model';
import { ServiceChangeService } from './service-change.service';
import { IServices } from 'app/shared/model/services.model';
import { ServicesService } from 'app/entities/services';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { AccountService } from 'app/core';

@Component({
  selector: 'jhi-service-change-update',
  templateUrl: './service-change-update.component.html'
})
export class ServiceChangeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    operation: [null, [Validators.required]],
    serviceId: [null, [Validators.required]]
  });
  services: IService[];

  operation2: OperationEnum;
  serviceId2: string;
  file: any;
  private authorities: string[];
  private sensibleServices: string[] = [
    'CALL_BARRING',
    'SUSPINCOMING',
    'SUSPOUTGOING',
    'PARTIAL_BLOCK',
    'FULL_BLOCK',
    'SIMBOX',
    'TOPOSTPAID',
    'TOPREPAID',
    'SUSPEND',
    'EPIN1',
    'FULL_SUSPENSION',
    'GPRS',
    'CLIP',
    'CALL_FORWARD'
  ];

  constructor(
    protected accountService: AccountService,
    protected serviceChangeService: ServiceChangeService,
    protected servicesService: ServicesService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected jhiAlertService: JhiAlertService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.accountService.fetch().subscribe(
      a => {
        this.authorities = a.body.authorities;
        this.loadServices();
      },
      err => this.loadServices()
    );
    this.activatedRoute.data.subscribe(({ serviceChange }) => {
      this.updateForm(serviceChange);
    });
  }

  updateForm(serviceChange: IServiceChange) {
    this.editForm.patchValue({
      id: serviceChange.id,
      msisdn: serviceChange.msisdn,
      imsi: serviceChange.imsi,
      operation: serviceChange.operation,
      serviceId: serviceChange.serviceId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const serviceChange = this.createFromForm();
    if (!serviceChange.msisdn.startsWith('221') && serviceChange.msisdn.length === 9) {
      serviceChange.msisdn = '221' + serviceChange.msisdn;
    }
    if (serviceChange.id !== undefined) {
      this.subscribeToSaveResponse(this.serviceChangeService.update(serviceChange));
    } else {
      this.subscribeToSaveResponse(this.serviceChangeService.create(serviceChange));
    }
  }

  onFileChange(event) {
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;
  }

  uploadFile() {
    console.log(this.file, this.serviceId2, this.operation2);
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('serviceId', this.serviceId2);
    formData.append('operation', this.operation2);
    this.subscribeToSaveResponse(this.serviceChangeService.upload(formData));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IServiceChange>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['service-change']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private loadServices() {
    this.servicesService
      .query()
      .pipe(
        filter((res: HttpResponse<IServices[]>) => res.ok),
        map((res: HttpResponse<IServices[]>) => res.body)
      )
      .subscribe(
        (res: IServices[]) => {
          this.services = res;
          this.services = this.services.filter(service => service.methodToInvoke !== '');
          if (this.authorities.includes('ROLE_CALL_CENTER') || this.authorities.includes('ROLE_AGENT')) {
            const toFilter = this.sensibleServices;
            this.services = this.services.filter(service => !toFilter.includes(service.code));
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  private createFromForm(): IServiceChange {
    return {
      ...new ServiceChange(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      operation: this.editForm.get(['operation']).value,
      serviceId: this.editForm.get(['serviceId']).value
    };
  }
}
