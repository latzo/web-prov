import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ExcelService } from './../../shared/excel/excel.service';
import { IServiceChange } from 'app/shared/model/service-change.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ServiceChangeService } from './service-change.service';

@Component({
  selector: 'jhi-service-change',
  templateUrl: './service-change.component.html'
})
export class ServiceChangeComponent implements OnInit, OnDestroy {
  currentAccount: any;
  serviceChanges: IServiceChange[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  onSiblings = false;
  currentTransanctionId: string;

  constructor(
    protected serviceChangeService: ServiceChangeService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private excelService: ExcelService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.onSiblings = false;
    this.serviceChangeService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IServiceChange[]>) => this.paginateServiceChanges(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  getAllSiblings(transanctionId: string) {
    if (transanctionId !== undefined && transanctionId != null) {
      this.serviceChangeService
        .getAllElementsOfABulk(transanctionId, {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IServiceChange[]>) => this.paginateServiceChanges(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      this.onSiblings = true;
      this.currentTransanctionId = transanctionId;
    }
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/service-change'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    if (this.onSiblings) {
      this.getAllSiblings(this.currentTransanctionId);
    } else {
      this.loadAll();
    }
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/service-change',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInServiceChanges();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IServiceChange) {
    return item.id;
  }

  registerChangeInServiceChanges() {
    this.eventSubscriber = this.eventManager.subscribe('serviceChangeListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateServiceChanges(data: IServiceChange[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.serviceChanges = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.serviceChanges, 'StatutService_');
  }
}
