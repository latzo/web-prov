import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'sim-swap',
        loadChildren: './sim-swap/sim-swap.module#TigocareSimSwapModule'
      },
      {
        path: 'service-change',
        loadChildren: './service-change/service-change.module#TigocareServiceChangeModule'
      },
      {
        path: 'services',
        loadChildren: './services/services.module#TigocareServicesModule'
      },
      {
        path: 'affichage-informations-abonnes',
        loadChildren: './affichage-informations-abonnes/affichage-informations-abonnes.module#TigocareAffichageInformationsAbonnesModule'
      },
      {
        path: 'create-subscriber',
        loadChildren: './create-subscriber/create-subscriber.module#TigocareCreateSubscriberModule'
      },
      {
        path: 'subscriber-status',
        loadChildren: './subscriber-status/subscriber-status.module#TigocareSubscriberStatusModule'
      },
      {
        path: 'delete-subscriber',
        loadChildren: './delete-subscriber/delete-subscriber.module#TigocareDeleteSubscriberModule'
      },
      {
        path: 'mnp-porting',
        loadChildren: './mnp-porting/mnp-porting.module#TigocareMnpPortingModule'
      },
      {
        path: 'mnp-portout',
        loadChildren: './mnp-portout/mnp-portout.module#TigocareMnpPortoutModule'
      },
      {
        path: 'apn-change',
        loadChildren: './apn-change/apn-change.module#TigocareAPNChangeModule'
      },
      {
        path: 'roaming-service',
        loadChildren: './roaming-service/roaming-service.module#TigocareRoamingServiceModule'
      },
      {
        path: 'reset-location',
        loadChildren: './reset-location/reset-location.module#TigocareResetLocationModule'
      },
      {
        path: 'transfert-appel',
        loadChildren: './transfert-appel/transfert-appel.module#TigocareTransfertAppelModule'
      },
      {
        path: 'roaming-4-g',
        loadChildren: './roaming-4-g/roaming-4-g.module#TigocareRoaming4gModule'
      },
      {
        path: 'archivage',
        loadChildren: './archivage/archivage.module#TigocareArchivageModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareEntityModule {}
