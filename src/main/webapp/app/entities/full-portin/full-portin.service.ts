import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICreateSubscriber } from 'app/shared/model/create-subscriber.model';

type EntityResponseType = HttpResponse<ICreateSubscriber>;
type EntityArrayResponseType = HttpResponse<ICreateSubscriber[]>;

@Injectable({ providedIn: 'root' })
export class FullPortinService {
  public resourceUrlSync = SERVER_API_URL + 'api/create-subscribers/sync';
  public resourceUrl = SERVER_API_URL + 'api/create-subscribers';

  constructor(protected http: HttpClient) {}

  create(fullPortin: ICreateSubscriber): Observable<EntityResponseType> {
    const liste: Array<ICreateSubscriber> = new Array<ICreateSubscriber>();
    liste.push(fullPortin);
    return this.http.post<ICreateSubscriber>(this.resourceUrlSync, liste, { observe: 'response' });
  }

  update(fullPortin: ICreateSubscriber): Observable<EntityResponseType> {
    return this.http.put<ICreateSubscriber>(this.resourceUrl, fullPortin, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICreateSubscriber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICreateSubscriber[]>(this.resourceUrl + '/full-portin', { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
