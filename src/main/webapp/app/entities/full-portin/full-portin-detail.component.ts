import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFullPortin } from 'app/shared/model/full-portin.model';

@Component({
  selector: 'jhi-full-portin-detail',
  templateUrl: './full-portin-detail.component.html'
})
export class FullPortinDetailComponent implements OnInit {
  fullPortin: IFullPortin;
  resp: string[] = [];

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ fullPortin }) => {
      this.fullPortin = fullPortin;
      this.resp = this.fullPortin.backendResp.split('-');
    });
  }

  previousState() {
    window.history.back();
  }
}
