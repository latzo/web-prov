import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FullPortinService } from './full-portin.service';
import { FullPortinComponent } from './full-portin.component';
import { FullPortinDetailComponent } from './full-portin-detail.component';
import { FullPortinUpdateComponent } from './full-portin-update.component';
import { FullPortinDeletePopupComponent } from './full-portin-delete-dialog.component';
import { ICreateSubscriber, CreateSubscriber } from 'app/shared/model/create-subscriber.model';

@Injectable({ providedIn: 'root' })
export class FullPortinResolve implements Resolve<ICreateSubscriber> {
  constructor(private service: FullPortinService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICreateSubscriber> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CreateSubscriber>) => response.ok),
        map((fullPortin: HttpResponse<CreateSubscriber>) => fullPortin.body)
      );
    }
    return of(new CreateSubscriber());
  }
}

export const fullPortinRoute: Routes = [
  {
    path: '',
    component: FullPortinComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'FullPortins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FullPortinDetailComponent,
    resolve: {
      fullPortin: FullPortinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FullPortins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FullPortinUpdateComponent,
    resolve: {
      fullPortin: FullPortinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FullPortins'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FullPortinUpdateComponent,
    resolve: {
      fullPortin: FullPortinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FullPortins'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const fullPortinPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FullPortinDeletePopupComponent,
    resolve: {
      fullPortin: FullPortinResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'FullPortins'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
