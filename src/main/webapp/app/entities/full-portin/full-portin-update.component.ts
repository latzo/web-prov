import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ICreateSubscriber, CreateSubscriber, IMainProducts } from 'app/shared/model/create-subscriber.model';
import { FullPortinService } from './full-portin.service';
import { CreateSubscriberService } from 'app/entities/create-subscriber';
import { JhiAlertService } from 'ng-jhipster';
import { filter, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { AccountService } from 'app/core';

@Component({
  selector: 'jhi-full-portin-update',
  templateUrl: './full-portin-update.component.html'
})
export class FullPortinUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    processingStatus: [],
    statut: [],
    transactionId: [],
    backendResp: [],
    cos: [null, [Validators.required]],
    creationType: [],
    name: [null, [Validators.required]],
    firstName: [null, [Validators.required]],
    dob: [null, [Validators.required]],
    city: [],
    puk: [null, [Validators.required]],
    tigoCashStatus: [null, [Validators.required]],
    gender: [],
    altContactNum: [],
    district: [],
    documentReferenceType: [null, [Validators.required]],
    proofNumber: [null, [Validators.required]],
    createUser: [],
    createDate: []
  });

  mainProducts: IMainProducts[];
  cos: String;
  private authorities: string[];

  constructor(
    protected fullPortinService: FullPortinService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected createSubscriberService: CreateSubscriberService,
    protected jhiAlertService: JhiAlertService,
    protected router: Router,
    protected accountService: AccountService,
    public datepipe: DatePipe
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.accountService.fetch().subscribe(
      a => {
        this.authorities = a.body.authorities;
      },
      err => console.error('Cant get authorities' + err)
    );
    this.activatedRoute.data.subscribe(({ fullPortin }) => {
      this.updateForm(fullPortin);
    });
    this.createSubscriberService
      .queryAllCos()
      .pipe(
        filter((res: HttpResponse<IMainProducts[]>) => res.ok),
        map((res: HttpResponse<IMainProducts[]>) => res.body)
      )
      .subscribe(
        (res: IMainProducts[]) => {
          this.mainProducts = res;
          this.mainProducts = this.mainProducts.filter(mainProduct => mainProduct.profileClass.toUpperCase() === 'PREPAID');
          if (!this.authorities.includes('ROLE_ADMIN')) {
            this.mainProducts = this.mainProducts.filter(mainProduct => mainProduct.mainProductName !== 'EPIN');
          }
          this.cos = this.mainProducts[0].mainProductId;
          this.activatedRoute.data.subscribe(({ createSubscriber }) => {
            this.updateForm(createSubscriber);
          });
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(fullPortin: ICreateSubscriber) {
    this.editForm.patchValue({
      id: fullPortin.id,
      msisdn: fullPortin.msisdn,
      imsi: fullPortin.imsi,
      processingStatus: fullPortin.processingStatus,
      statut: fullPortin.statut,
      transactionId: fullPortin.transactionId,
      backendResp: fullPortin.backendResp,
      cos: fullPortin.cos,
      creationType: fullPortin.creationType,
      name: fullPortin.name,
      firstName: fullPortin.firstName,
      dob: this.datepipe.transform(fullPortin.dob, 'ddMMyyyy'),
      city: fullPortin.city,
      puk: fullPortin.puk,
      tigoCashStatus: fullPortin.tigoCashStatus,
      gender: fullPortin.gender,
      altContactNum: fullPortin.altContactNum,
      district: fullPortin.district,
      documentReferenceType: fullPortin.documentReferenceType,
      proofNumber: fullPortin.proofNumber,
      createUser: fullPortin.createUser,
      createDate: fullPortin.createDate
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const fullPortin = this.createFromForm();
    if (!fullPortin.msisdn.startsWith('221') && fullPortin.msisdn.length === 9) {
      fullPortin.msisdn = '221' + fullPortin.msisdn;
    }
    if (fullPortin.id !== undefined) {
      this.subscribeToSaveResponse(this.fullPortinService.update(fullPortin));
    } else {
      this.subscribeToSaveResponse(this.fullPortinService.create(fullPortin));
    }
  }

  private createFromForm(): ICreateSubscriber {
    return {
      ...new CreateSubscriber(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      processingStatus: this.editForm.get(['processingStatus']).value,
      statut: this.editForm.get(['statut']).value,
      transactionId: this.editForm.get(['transactionId']).value,
      backendResp: this.editForm.get(['backendResp']).value,
      cos: this.editForm.get(['cos']).value || this.mainProducts[0].mainProductId,
      creationType: this.editForm.get(['creationType']).value,
      name: this.editForm.get(['name']).value,
      firstName: this.editForm.get(['firstName']).value,
      dob: this.editForm.get(['dob']).value,
      city: this.editForm.get(['city']).value,
      puk: this.editForm.get(['puk']).value,
      tigoCashStatus: this.editForm.get(['tigoCashStatus']).value,
      gender: this.editForm.get(['gender']).value,
      altContactNum: this.editForm.get(['altContactNum']).value,
      district: this.editForm.get(['district']).value,
      documentReferenceType: this.editForm.get(['documentReferenceType']).value,
      proofNumber: this.editForm.get(['proofNumber']).value,
      createUser: this.editForm.get(['createUser']).value,
      createDate: this.editForm.get(['createDate']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICreateSubscriber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['full-portin']);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  protected onSaveError() {
    this.isSaving = false;
    this.router.navigate(['full-portin']);
  }
}
