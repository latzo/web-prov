import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICreateSubscriber } from 'app/shared/model/create-subscriber.model';
import { FullPortinService } from './full-portin.service';

@Component({
  selector: 'jhi-full-portin-delete-dialog',
  templateUrl: './full-portin-delete-dialog.component.html'
})
export class FullPortinDeleteDialogComponent {
  fullPortin: ICreateSubscriber;

  constructor(
    protected fullPortinService: FullPortinService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.fullPortinService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'fullPortinListModification',
        content: 'Deleted an fullPortin'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-full-portin-delete-popup',
  template: ''
})
export class FullPortinDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ fullPortin }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FullPortinDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.fullPortin = fullPortin;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/full-portin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/full-portin', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
