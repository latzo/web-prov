import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  FullPortinComponent,
  FullPortinDetailComponent,
  FullPortinUpdateComponent,
  FullPortinDeletePopupComponent,
  FullPortinDeleteDialogComponent,
  fullPortinRoute,
  fullPortinPopupRoute
} from './';

const ENTITY_STATES = [...fullPortinRoute, ...fullPortinPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    FullPortinComponent,
    FullPortinDetailComponent,
    FullPortinUpdateComponent,
    FullPortinDeleteDialogComponent,
    FullPortinDeletePopupComponent
  ],
  entryComponents: [FullPortinComponent, FullPortinUpdateComponent, FullPortinDeleteDialogComponent, FullPortinDeletePopupComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareFullPortinModule {}
