import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IServices } from 'app/shared/model/services.model';
import { AccountService } from 'app/core';
import { ServicesService } from './services.service';
import { ExcelService } from 'app/shared/excel/excel.service';

@Component({
  selector: 'jhi-services',
  templateUrl: './services.component.html'
})
export class ServicesComponent implements OnInit, OnDestroy {
  services: IServices[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected servicesService: ServicesService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService,
    private excelService: ExcelService
  ) {}

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.services, 'StatutMNMPortout_');
  }

  loadAll() {
    this.servicesService
      .query()
      .pipe(
        filter((res: HttpResponse<IServices[]>) => res.ok),
        map((res: HttpResponse<IServices[]>) => res.body)
      )
      .subscribe(
        (res: IServices[]) => {
          this.services = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInServices();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IServices) {
    return item.id;
  }

  registerChangeInServices() {
    this.eventSubscriber = this.eventManager.subscribe('servicesListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
