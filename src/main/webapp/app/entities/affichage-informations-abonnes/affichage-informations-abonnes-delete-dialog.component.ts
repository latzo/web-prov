import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';
import { AffichageInformationsAbonnesService } from './affichage-informations-abonnes.service';

@Component({
  selector: 'jhi-affichage-informations-abonnes-delete-dialog',
  templateUrl: './affichage-informations-abonnes-delete-dialog.component.html'
})
export class AffichageInformationsAbonnesDeleteDialogComponent {
  affichageInformationsAbonnes: IAffichageInformationsAbonnes;

  constructor(
    protected affichageInformationsAbonnesService: AffichageInformationsAbonnesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.affichageInformationsAbonnesService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'affichageInformationsAbonnesListModification',
        content: 'Deleted an affichageInformationsAbonnes'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-affichage-informations-abonnes-delete-popup',
  template: ''
})
export class AffichageInformationsAbonnesDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ affichageInformationsAbonnes }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AffichageInformationsAbonnesDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.affichageInformationsAbonnes = affichageInformationsAbonnes;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/affichage-informations-abonnes', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/affichage-informations-abonnes', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
