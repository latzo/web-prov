import { Component, OnInit, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { IAffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AffichageInformationsAbonnesService } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes.service';
import { DecimalPipe } from '@angular/common';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { IPinpuk } from 'app/shared/model/pinpuk.model';
import { getSubscriberInfoHlrResponse } from 'app/shared/model/getSubscriberInfoHlrResponse.model';
import { serviceState } from 'app/shared/model/service-state.model';
import * as xml2js from 'xml2js';
import { Builder } from 'xml2js';

@Component({
  selector: 'jhi-affichage-informations-abonnes-detail',
  templateUrl: './affichage-informations-abonnes-detail.component.html',
  providers: [DecimalPipe]
})
export class AffichageInformationsAbonnesDetailComponent implements OnInit {
  affichageInformationsAbonnes: IAffichageInformationsAbonnes;
  pinpuk: IPinpuk;
  responseGetInfoHlr: getSubscriberInfoHlrResponse;
  responseGetInfoHss: any;
  Xmlcontent: string;
  XmlcontentHss: string;
  services: serviceState[] = [];
  public isCollapsed = false;
  public try = 0;
  services$: Observable<Object[]>;
  filter = new FormControl('');
  epsProfileID = '';
  hssStatus = 'DEACTIVATED';
  epsLocationState = '';
  epsLastUpdateLocationDate = '';

  constructor(
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    protected affichageInformationsAbonnesService: AffichageInformationsAbonnesService,
    pipe: DecimalPipe
  ) {
    // this.services$ = this.filter.valueChanges.pipe(
    //   startWith(''),
    //   map(text => this.search(text, pipe))
    // );
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ affichageInformationsAbonnes }) => {
      this.affichageInformationsAbonnes = affichageInformationsAbonnes;
    });
    this.getPgHlr();
    this.getPgHss();
  }

  // search(text: string, pipe: PipeTransform): Object[] {
  //   return this.services.filter(service => {
  //     const term = text.toLowerCase();
  //     return service.name.toLowerCase().includes(term)
  //       || pipe.transform(service.state).includes(term);
  //   });
  // }

  clickQueryNetwork() {
    this.isCollapsed = !this.isCollapsed;
    this.queryNetwork();
  }

  getPinPukInfo() {
    // Pinpuk
    this.affichageInformationsAbonnesService.getPinpuk(this.affichageInformationsAbonnes.imsi).subscribe(
      result => {
        this.pinpuk = result.body;
        console.log(this.pinpuk);
      },
      error => console.log(error)
    );
  }

  getServicesInfo() {
    this.try++;
    // Services
    this.affichageInformationsAbonnesService.getInfosAbonneHlr(this.affichageInformationsAbonnes.imsi).subscribe(
      response => {
        const builder = new Builder();
        this.Xmlcontent = builder.buildObject(response.body.moattributes).trim();
        this.responseGetInfoHlr = response.body;
        this.services.push({
          name: 'CALL INCOMING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.baic.ts10.activationState === 1 ? 'INACTIVE' : 'ACTIVE'
        });
        this.services.push({
          name: 'CALL OUTGOING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.baoc.ts10.activationState === 1 ? 'INACTIVE' : 'ACTIVE'
        });
        this.services.push({
          name: 'SMS INCOMING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.ts21 === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'SMS OUTGOING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.ts22 === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'GPRS',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.nam.prov === 1 ? 'INACTIVE' : 'ACTIVE'
        });
        this.services.push({
          name: 'CLIP',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.clip === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'USSD CUSTOMER',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.oick !== 15 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'USSD EPIN',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.oick === 15 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'LISTE ROUGE TEMPORAIRE NON RESTREINTE',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.soclir === 2 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'LISTE ROUGE PERMANENTE',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.soclir === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'RBT',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.prbt === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'CALL WAITING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.caw.activationState === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'CALL FORWARDING UNCONDITIONAL',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.cfu.activationState === 1 ? 'ACTIVE' : 'INACTIVE'
        });
        this.services.push({
          name: 'ROAMING',
          state: this.responseGetInfoHlr.moattributes.getResponseSubscription.obr !== 0 ? 'ACTIVE' : 'INACTIVE'
        });
      },
      error => console.log(error)
    );
  }

  queryNetwork() {
    this.try++;
    if (this.try === 1) {
      this.getServicesInfo();
    }
    if (this.pinpuk === undefined) {
      this.getPinPukInfo();
    }
  }

  getPgHlr() {
    if (this.affichageInformationsAbonnes.msisdn !== undefined) {
      // this.subscribeToGetPgHlr(this.affichageInformationsAbonnesService.get(this.affichageInformationsAbonnes.msisdn));
      this.getServicesInfo();
    }
  }

  getPgHss() {
    if (this.affichageInformationsAbonnes.msisdn !== undefined) {
      // this.subscribeToGetPgHlr(this.affichageInformationsAbonnesService.get(this.affichageInformationsAbonnes.msisdn));
      this.try++;
      // Services
      this.affichageInformationsAbonnesService.getInfosAbonneHss(this.affichageInformationsAbonnes.imsi).subscribe(
        response => {
          const builder = new Builder();
          this.XmlcontentHss = builder.buildObject(response.body.moattributes).trim();
          this.responseGetInfoHss = response.body;
          if (this.responseGetInfoHss.moattributes) {
            this.hssStatus = this.responseGetInfoHss.moattributes.getResponseEPSMultiSC.epsProfileId !== '' ? 'ACTIVATED' : 'DEACTIVATED';
            this.epsProfileID = this.responseGetInfoHss.moattributes.getResponseEPSMultiSC.epsProfileId;
            this.epsLocationState = this.responseGetInfoHss.moattributes.getResponseEPSMultiSC.epsLocationState;
            this.epsLastUpdateLocationDate = this.responseGetInfoHss.moattributes.getResponseEPSMultiSC.epsLastUpdateLocationDate;
          }
        },
        error => console.log(error)
      );
    }
  }

  // protected subscribeToGetPgHlr(result: Observable<HttpResponse<string>>) {
  //   result.subscribe(
  //     res => {
  //       this.Xmlcontent = res.body.trim();
  //     },
  //     err => console.log(err)
  //   );
  // }

  previousState() {
    window.history.back();
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
