import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IAffichageInformationsAbonnes, AffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';
import { AffichageInformationsAbonnesService } from './affichage-informations-abonnes.service';
import { affichageInformationsAbonnesPopupRoute } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes.route';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-affichage-informations-abonnes-update',
  templateUrl: './affichage-informations-abonnes-update.component.html'
})
export class AffichageInformationsAbonnesUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [],
    imsi: [],
    imsiHlr: [],
    imsiCbs: []
  });
  private newlocation: string;

  constructor(
    protected affichageInformationsAbonnesService: AffichageInformationsAbonnesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ affichageInformationsAbonnes }) => {
      this.updateForm(affichageInformationsAbonnes);
    });
  }

  updateForm(affichageInformationsAbonnes: IAffichageInformationsAbonnes) {
    this.editForm.patchValue({
      id: affichageInformationsAbonnes.id,
      msisdn: affichageInformationsAbonnes.msisdn,
      imsi: affichageInformationsAbonnes.imsi,
      imsiHlr: affichageInformationsAbonnes.imsiHlr,
      imsiCbs: affichageInformationsAbonnes.imsiCbs
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const affichageInformationsAbonnes = this.createFromForm();
    if (affichageInformationsAbonnes.id !== undefined) {
      this.subscribeToSaveResponse(this.affichageInformationsAbonnesService.update(affichageInformationsAbonnes));
    } else {
      this.subscribeToSaveResponse(this.affichageInformationsAbonnesService.create(affichageInformationsAbonnes));
    }
  }

  private createFromForm(): IAffichageInformationsAbonnes {
    return {
      ...new AffichageInformationsAbonnes(),
      id: this.editForm.get(['id']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      imsi: this.editForm.get(['imsi']).value,
      imsiHlr: this.editForm.get(['imsiHlr']).value,
      imsiCbs: this.editForm.get(['imsiCbs']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffichageInformationsAbonnes>>) {
    result.subscribe(
      res => {
        this.onSaveSuccess(res.headers.get('location'));
      },
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(newlocation: string) {
    newlocation = newlocation.substr(4) + '/view';
    console.log('Gotta navigate to ', newlocation);
    this.isSaving = false;
    this.router.navigateByUrl(newlocation);
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
