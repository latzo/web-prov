import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  AffichageInformationsAbonnesComponent,
  AffichageInformationsAbonnesDetailComponent,
  AffichageInformationsAbonnesUpdateComponent,
  AffichageInformationsAbonnesDeletePopupComponent,
  AffichageInformationsAbonnesDeleteDialogComponent,
  affichageInformationsAbonnesRoute,
  affichageInformationsAbonnesPopupRoute
} from './';

const ENTITY_STATES = [...affichageInformationsAbonnesRoute, ...affichageInformationsAbonnesPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AffichageInformationsAbonnesComponent,
    AffichageInformationsAbonnesDetailComponent,
    AffichageInformationsAbonnesUpdateComponent,
    AffichageInformationsAbonnesDeleteDialogComponent,
    AffichageInformationsAbonnesDeletePopupComponent
  ],
  entryComponents: [
    AffichageInformationsAbonnesComponent,
    AffichageInformationsAbonnesUpdateComponent,
    AffichageInformationsAbonnesDeleteDialogComponent,
    AffichageInformationsAbonnesDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareAffichageInformationsAbonnesModule {}
