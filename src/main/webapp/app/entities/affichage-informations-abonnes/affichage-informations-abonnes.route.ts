import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AffichageInformationsAbonnes, IAffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';
import { AffichageInformationsAbonnesService } from './affichage-informations-abonnes.service';
import { AffichageInformationsAbonnesComponent } from './affichage-informations-abonnes.component';
import { AffichageInformationsAbonnesDetailComponent } from './affichage-informations-abonnes-detail.component';
import { AffichageInformationsAbonnesUpdateComponent } from './affichage-informations-abonnes-update.component';
import { AffichageInformationsAbonnesDeletePopupComponent } from './affichage-informations-abonnes-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class AffichageInformationsAbonnesResolve implements Resolve<IAffichageInformationsAbonnes> {
  constructor(private service: AffichageInformationsAbonnesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAffichageInformationsAbonnes> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AffichageInformationsAbonnes>) => response.ok),
        map((affichageInformationsAbonnes: HttpResponse<AffichageInformationsAbonnes>) => affichageInformationsAbonnes.body)
      );
    }
    return of(new AffichageInformationsAbonnes());
  }
}

export const affichageInformationsAbonnesRoute: Routes = [
  {
    path: '',
    component: AffichageInformationsAbonnesComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'AffichageInformationsAbonnes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AffichageInformationsAbonnesDetailComponent,
    resolve: {
      affichageInformationsAbonnes: AffichageInformationsAbonnesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AffichageInformationsAbonnes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AffichageInformationsAbonnesUpdateComponent,
    resolve: {
      affichageInformationsAbonnes: AffichageInformationsAbonnesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'AffichageInformationsAbonnes'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AffichageInformationsAbonnesUpdateComponent,
    resolve: {
      affichageInformationsAbonnes: AffichageInformationsAbonnesResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'AffichageInformationsAbonnes'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const affichageInformationsAbonnesPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AffichageInformationsAbonnesDeletePopupComponent,
    resolve: {
      affichageInformationsAbonnes: AffichageInformationsAbonnesResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'AffichageInformationsAbonnes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
