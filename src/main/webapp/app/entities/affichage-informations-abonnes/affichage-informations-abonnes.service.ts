import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HttpHeaders } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';
import { IPinpuk } from 'app/shared/model/pinpuk.model';
import { IGetSubscriberInfoHlrResponse } from 'app/shared/model/getSubscriberInfoHlrResponse.model';

type EntityResponseType = HttpResponse<IAffichageInformationsAbonnes>;
type PinpukEntityResponseType = HttpResponse<IPinpuk>;
type GetInfoHlrEntityResponseType = HttpResponse<IGetSubscriberInfoHlrResponse>;
type EntityArrayResponseType = HttpResponse<IAffichageInformationsAbonnes[]>;

@Injectable({ providedIn: 'root' })
export class AffichageInformationsAbonnesService {
  public resourceUrl = SERVER_API_URL + 'api/affichage-informations-abonnes';
  public resourceXmlUrl = SERVER_API_URL + 'api/affichage-informations-abonnes/xml';
  public pinpukResourceUrl = SERVER_API_URL + 'api/pinpuks/imsi';

  constructor(protected http: HttpClient) {}

  get(msisdn: string): Observable<HttpResponse<string>> {
    return this.http.get<string>(`${this.resourceXmlUrl}/${msisdn}`, {
      observe: 'response',
      headers: new HttpHeaders({ 'Content-Type': 'text/xml' }).set('Accept', 'text/xml'),
      responseType: 'text' as 'json'
    });
  }

  getInfosAbonneHlr(imsi: string): Observable<GetInfoHlrEntityResponseType> {
    return this.http.get<IGetSubscriberInfoHlrResponse>(`${this.resourceUrl}/subscriberinfo/${imsi}`, { observe: 'response' });
  }

  getInfosAbonneHss(imsi: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/subscriberinfohss/${imsi}`, { observe: 'response' });
  }

  getPinpuk(imsi: string): Observable<PinpukEntityResponseType> {
    return this.http.get<IPinpuk>(`${this.pinpukResourceUrl}/${imsi}`, { observe: 'response' });
  }

  create(affichageInformationsAbonnes: IAffichageInformationsAbonnes): Observable<EntityResponseType> {
    return this.http.post<IAffichageInformationsAbonnes>(this.resourceUrl, affichageInformationsAbonnes, { observe: 'response' });
  }

  update(affichageInformationsAbonnes: IAffichageInformationsAbonnes): Observable<EntityResponseType> {
    return this.http.put<IAffichageInformationsAbonnes>(this.resourceUrl, affichageInformationsAbonnes, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAffichageInformationsAbonnes>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAffichageInformationsAbonnes[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
