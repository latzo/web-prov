import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDeleteSubscriber } from 'app/shared/model/delete-subscriber.model';
import { DeleteSubscriberService } from './delete-subscriber.service';

@Component({
  selector: 'jhi-delete-subscriber-delete-dialog',
  templateUrl: './delete-subscriber-delete-dialog.component.html'
})
export class DeleteSubscriberDeleteDialogComponent {
  deleteSubscriber: IDeleteSubscriber;

  constructor(
    protected deleteSubscriberService: DeleteSubscriberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.deleteSubscriberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'deleteSubscriberListModification',
        content: 'Deleted an deleteSubscriber'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-delete-subscriber-delete-popup',
  template: ''
})
export class DeleteSubscriberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ deleteSubscriber }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DeleteSubscriberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.deleteSubscriber = deleteSubscriber;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/delete-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/delete-subscriber', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
