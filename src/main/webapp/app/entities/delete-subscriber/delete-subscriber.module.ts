import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TigocareSharedModule } from 'app/shared';
import {
  DeleteSubscriberComponent,
  DeleteSubscriberDetailComponent,
  DeleteSubscriberUpdateComponent,
  DeleteSubscriberDeletePopupComponent,
  DeleteSubscriberDeleteDialogComponent,
  deleteSubscriberRoute,
  deleteSubscriberPopupRoute
} from './';

const ENTITY_STATES = [...deleteSubscriberRoute, ...deleteSubscriberPopupRoute];

@NgModule({
  imports: [TigocareSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DeleteSubscriberComponent,
    DeleteSubscriberDetailComponent,
    DeleteSubscriberUpdateComponent,
    DeleteSubscriberDeleteDialogComponent,
    DeleteSubscriberDeletePopupComponent
  ],
  entryComponents: [
    DeleteSubscriberComponent,
    DeleteSubscriberUpdateComponent,
    DeleteSubscriberDeleteDialogComponent,
    DeleteSubscriberDeletePopupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareDeleteSubscriberModule {}
