import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeleteSubscriber } from 'app/shared/model/delete-subscriber.model';

@Component({
  selector: 'jhi-delete-subscriber-detail',
  templateUrl: './delete-subscriber-detail.component.html'
})
export class DeleteSubscriberDetailComponent implements OnInit {
  deleteSubscriber: IDeleteSubscriber;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ deleteSubscriber }) => {
      this.deleteSubscriber = deleteSubscriber;
    });
  }

  previousState() {
    window.history.back();
  }
}
