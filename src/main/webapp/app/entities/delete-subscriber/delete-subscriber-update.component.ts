import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IDeleteSubscriber, DeleteSubscriber } from 'app/shared/model/delete-subscriber.model';
import { DeleteSubscriberService } from './delete-subscriber.service';

@Component({
  selector: 'jhi-delete-subscriber-update',
  templateUrl: './delete-subscriber-update.component.html'
})
export class DeleteSubscriberUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    imsi: [null, [Validators.required]],
    deleteOnAuc: [],
    deleteOnHlr: [],
    deleteOnHss: [],
    deleteOnCbs: [],
    aucDeletedStatus: [],
    hlrDeletedStatus: [],
    hssDeletedStatus: [],
    cbsDeletedStatus: [],
    errorMessage: []
  });
  deleteOnAuc: string;
  deleteOnHlr: string;
  deleteOnHss: string;
  deleteOnCbs: string;
  private file: File;

  constructor(
    protected deleteSubscriberService: DeleteSubscriberService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ deleteSubscriber }) => {
      this.updateForm(deleteSubscriber);
    });
  }

  updateForm(deleteSubscriber: IDeleteSubscriber) {
    this.editForm.patchValue({
      id: deleteSubscriber.id,
      imsi: deleteSubscriber.imsi,
      msisdn: deleteSubscriber.msisdn,
      deleteOnAuc: deleteSubscriber.deleteOnAuc,
      deleteOnHlr: deleteSubscriber.deleteOnHlr,
      deleteOnHss: deleteSubscriber.deleteOnHss,
      deleteOnCbs: deleteSubscriber.deleteOnCbs,
      aucDeletedStatus: deleteSubscriber.aucDeletedStatus,
      hlrDeletedStatus: deleteSubscriber.hlrDeletedStatus,
      hssDeletedStatus: deleteSubscriber.hssDeletedStatus,
      cbsDeletedStatus: deleteSubscriber.cbsDeletedStatus,
      errorMessage: deleteSubscriber.errorMessage
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const deleteSubscriber = this.createFromForm();
    if (!deleteSubscriber.msisdn.startsWith('221') && deleteSubscriber.msisdn.length === 9) {
      deleteSubscriber.msisdn = '221' + deleteSubscriber.msisdn;
    }
    if (deleteSubscriber.id !== undefined) {
      this.subscribeToSaveResponse(this.deleteSubscriberService.update(deleteSubscriber));
    } else {
      this.subscribeToSaveResponse(this.deleteSubscriberService.create(deleteSubscriber));
    }
  }

  private createFromForm(): IDeleteSubscriber {
    return {
      ...new DeleteSubscriber(),
      id: this.editForm.get(['id']).value,
      imsi: this.editForm.get(['imsi']).value,
      msisdn: this.editForm.get(['msisdn']).value,
      deleteOnAuc: this.editForm.get(['deleteOnAuc']).value,
      deleteOnHlr: this.editForm.get(['deleteOnHlr']).value,
      deleteOnHss: this.editForm.get(['deleteOnHss']).value,
      deleteOnCbs: this.editForm.get(['deleteOnCbs']).value,
      aucDeletedStatus: this.editForm.get(['aucDeletedStatus']).value,
      hlrDeletedStatus: this.editForm.get(['hlrDeletedStatus']).value,
      hssDeletedStatus: this.editForm.get(['hssDeletedStatus']).value,
      cbsDeletedStatus: this.editForm.get(['cbsDeletedStatus']).value,
      errorMessage: this.editForm.get(['errorMessage']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeleteSubscriber>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.router.navigate(['delete-subscriber']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  uploadFile() {
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('deleteOnAuc', this.deleteOnAuc);
    formData.append('deleteOnHlr', this.deleteOnHlr);
    formData.append('deleteOnHss', this.deleteOnHss);
    formData.append('deleteOnCbs', this.deleteOnCbs);
    this.subscribeToSaveResponse(this.deleteSubscriberService.bulk(formData));
  }

  onFileChange(event) {
    this.file = event.target.files.length > 0 ? event.target.files[0] : null;
  }
}
