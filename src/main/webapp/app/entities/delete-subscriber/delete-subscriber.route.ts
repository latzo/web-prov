import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DeleteSubscriber, IDeleteSubscriber } from 'app/shared/model/delete-subscriber.model';
import { DeleteSubscriberService } from './delete-subscriber.service';
import { DeleteSubscriberComponent } from './delete-subscriber.component';
import { DeleteSubscriberDetailComponent } from './delete-subscriber-detail.component';
import { DeleteSubscriberUpdateComponent } from './delete-subscriber-update.component';
import { DeleteSubscriberDeletePopupComponent } from './delete-subscriber-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class DeleteSubscriberResolve implements Resolve<IDeleteSubscriber> {
  constructor(private service: DeleteSubscriberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDeleteSubscriber> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DeleteSubscriber>) => response.ok),
        map((deleteSubscriber: HttpResponse<DeleteSubscriber>) => deleteSubscriber.body)
      );
    }
    return of(new DeleteSubscriber());
  }
}

export const deleteSubscriberRoute: Routes = [
  {
    path: '',
    component: DeleteSubscriberComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,desc',
      pageTitle: 'DeleteSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DeleteSubscriberDetailComponent,
    resolve: {
      deleteSubscriber: DeleteSubscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DeleteSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DeleteSubscriberUpdateComponent,
    resolve: {
      deleteSubscriber: DeleteSubscriberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'DeleteSubscribers'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DeleteSubscriberUpdateComponent,
    resolve: {
      deleteSubscriber: DeleteSubscriberResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DeleteSubscribers'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const deleteSubscriberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DeleteSubscriberDeletePopupComponent,
    resolve: {
      deleteSubscriber: DeleteSubscriberResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DeleteSubscribers'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
