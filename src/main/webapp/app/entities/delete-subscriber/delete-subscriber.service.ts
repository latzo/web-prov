import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDeleteSubscriber } from 'app/shared/model/delete-subscriber.model';

type EntityResponseType = HttpResponse<IDeleteSubscriber>;
type EntityArrayResponseType = HttpResponse<IDeleteSubscriber[]>;

@Injectable({ providedIn: 'root' })
export class DeleteSubscriberService {
  public resourceUrl = SERVER_API_URL + 'api/delete-subscribers';

  constructor(protected http: HttpClient) {}

  getAllElementsOfABulk(transanctionId: String, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeleteSubscriber[]>(`${this.resourceUrl}/bulks/${transanctionId}`, { params: options, observe: 'response' });
  }

  create(deleteSubscriber: IDeleteSubscriber): Observable<EntityResponseType> {
    return this.http.post<IDeleteSubscriber>(this.resourceUrl, deleteSubscriber, { observe: 'response' });
  }

  update(deleteSubscriber: IDeleteSubscriber): Observable<EntityResponseType> {
    return this.http.put<IDeleteSubscriber>(this.resourceUrl, deleteSubscriber, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDeleteSubscriber>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeleteSubscriber[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  bulk(formData: FormData) {
    return this.http.post<any>(`${this.resourceUrl}/bulk`, formData, { observe: 'response' });
  }
}
