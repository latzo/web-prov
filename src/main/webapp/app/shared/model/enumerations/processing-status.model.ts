export const enum ProcessingStatus {
  SUBMITTED = 'SUBMITTED',
  SUCCEEDED = 'SUCCEEDED',
  FAILED = 'FAILED'
}
