export interface IAffichageInformationsAbonnes {
  id?: number;
  msisdn?: string;
  imsi?: string;
  imsiHlr?: string;
  imsiCbs?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class AffichageInformationsAbonnes implements IAffichageInformationsAbonnes {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public imsiHlr?: string,
    public imsiCbs?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
