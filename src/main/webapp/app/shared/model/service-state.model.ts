export interface IServiceState {
  name?: string;
  state?: string;
}

export class serviceState implements IServiceState {
  constructor(public name?: string, public state?: string) {}
}
