export interface IGetSubscriberInfoHlrResponse {
  moattributes?: Moattributes;
}

export interface Moattributes {
  getResponseSubscription?: getResponseSubscription;
}

export interface getResponseSubscription {
  msisdn?: string;
  imsi?: string;
  msisdnstate?: string;
  rid?: any;
  zoneid?: any;
  authd?: string;
  profileId?: any;
  pdpcp?: number;
  csp?: number;
  gsap?: any;
  amsisdn?: any[];
  camel?: Camel;
  mmintMo?: any[];
  closedUserGroup?: any[];
  cugBsgOption?: any;
  gprs?: any[];
  nam?: Nam;
  baic?: Baic;
  baoc?: Baic;
  boic?: Baic;
  bicro?: Baic;
  boiexh?: Baic;
  cfu?: Cfb;
  cfb?: Cfb;
  cfnrc?: Cfb;
  cfnry?: Cfnry;
  dcf?: any;
  spn?: any;
  caw?: Baic;
  acc?: any;
  aoc?: any;
  bs21?: any;
  bs22?: any;
  bs23?: any;
  bs24?: any;
  bs25?: any;
  bs26?: number;
  bs2F?: any;
  bs2G?: any;
  bs31?: any;
  bs32?: any;
  bs33?: any;
  bs34?: any;
  bs3F?: any;
  bs3G?: number;
  capl?: any;
  cat?: string;
  clip?: number;
  clir?: any;
  colp?: any;
  colr?: any;
  dbsg?: number;
  hold?: number;
  hpn?: any;
  ici?: any;
  mpty?: number;
  oba?: any;
  obi?: any;
  obo?: any;
  obopre?: any;
  obopri?: any;
  obrf?: any;
  obssm?: any;
  obr?: any;
  obzi?: any;
  obzo?: any;
  oin?: any;
  oick?: any;
  osb1?: any;
  osb2?: any;
  osb3?: any;
  osb4?: any;
  ofa?: number;
  pai?: any;
  regser?: any;
  pici?: any;
  pici2?: any;
  pici3?: any;
  pwd?: string;
  socb?: number;
  socfb?: number;
  socfrc?: number;
  socfry?: number;
  socfu?: number;
  soclip?: number;
  soclir?: any;
  socolp?: any;
  sodcf?: any;
  soplcs?: any;
  sosdcf?: any;
  stype?: any;
  tick?: any;
  tin?: any;
  ts11?: number;
  ts21?: number;
  ts22?: number;
  ts61?: any;
  ts62?: any;
  locationData?: LocationData;
  vlrData?: string;
  emlpp?: any;
  demlpp?: any;
  memlpp?: any;
  tsmo?: number;
  rsa?: any;
  obp?: any;
  ect?: any;
  obct?: any;
  obdct?: any;
  obmct?: any;
  schar?: string;
  ocsi?: any;
  tcsi?: any;
  gprcsi?: any;
  osmcsi?: any;
  mcsi?: any;
  dcsi?: any;
  tsmcsi?: any;
  vtcsi?: any;
  mmint?: any;
  cug?: any;
  tsd1?: any;
  red?: any;
  asl?: any;
  bsl?: any;
  crel?: any;
  cunrl?: any;
  plmno?: any;
  ttp?: any;
  univ?: any;
  servtl?: any;
  shplmn?: any;
  smshr1?: any;
  smshr2?: any;
  mpid?: any;
  ste?: any;
  teardown?: any;
  gmlca?: any[];
  gmlca12?: any;
  lcsd?: any;
  steMo?: any;
  prbt?: any;
  acr?: any;
  rtca?: any;
  redmch?: any;
  mca?: any;
  smspam?: any;
  ist?: any;
  istcso?: any;
  istgso?: any;
  istvso?: any;
  msim?: any;
  multiSim?: any;
  smsSpam?: SMSSpam;
  ard?: any;
  imeisv?: any;
  rdp?: any;
  grdp?: any;
  obcc?: any;
  mrdpid?: any;
  mrdmch?: any;
  mderbt?: any;
  ora?: any;
  cbnf?: any;
  cfnf?: any;
  chnf?: any;
  clipnf?: any;
  clirnf?: any;
  cwnf?: any;
  dcsinf?: any;
  ectnf?: any;
  gprscsinf?: any;
  mcsinf?: any;
  ocsinf?: any;
  odbnf?: any;
  osmcsinf?: any;
  tcsinf?: any;
  tifcsinf?: any;
  tsmcsinf?: any;
  vtcsinf?: any;
  dcsist?: any;
  gprscsist?: any;
  mcsist?: any;
  ocsist?: number;
  osmcsist?: any;
  tcsist?: number;
  tsmcsist?: any;
  vtcsist?: any;
  ics?: any;
  msisdnAttr?: string;
  imsiAttr?: string;
}

export interface Baic {
  provisionState?: number;
  activationState?: any;
  ts10?: Ts20Class;
  ts20?: Ts20Class;
  ts60?: any;
  tsd0?: any;
  bs20?: Ts20Class;
  bs30?: Ts20Class;
}

export interface Ts20Class {
  activationState?: number;
}

export interface Camel {
  eoinci?: number;
  eoick?: number;
  etinci?: number;
  etick?: number;
  gcso?: number;
  sslo?: number;
  mcso?: number;
  gc2So?: number;
  mc2So?: number;
  tif?: number;
  gc3So?: number;
  mc3So?: number;
  gprsso?: number;
  osmsso?: number;
  tsmsso?: number;
  mmso?: number;
  gc4So?: number;
  mc4So?: number;
  ctdp?: any[];
  ccamel?: any;
}

export interface Cfb {
  provisionState?: number;
  activationState?: any;
  fnum?: any;
  subAddress?: any;
  ofa?: any;
  keep?: any;
  ts10?: PurpleBs20;
  ts60?: any;
  tsd0?: any;
  bs20?: PurpleBs20;
  bs30?: PurpleBs20;
}

export interface PurpleBs20 {
  activationState?: number;
  fnum?: any;
  subAddress?: any;
  ofa?: any;
  keep?: any;
  noReplyTime?: any;
}

export interface Cfnry {
  provisionState?: number;
  activationState?: any;
  fnum?: any;
  noReplyTime?: any;
  subAddress?: any;
  ofa?: any;
  keep?: any;
  ts10?: PurpleBs20;
  ts60?: any;
  tsd0?: any;
  bs20?: PurpleBs20;
  bs30?: PurpleBs20;
}

export interface LocationData {
  vlrAddress?: string;
  msrn?: any;
  mscNumber?: string;
  lmsid?: any;
  sgsnNumber?: string;
  locState?: any;
  cgisailai?: any;
  loctype?: any;
  ageloc?: any;
  substate?: any;
  lastcsupd?: any;
  lastpsupd?: any;
  cstimest1?: any;
  cstimest2?: any;
  cstimest3?: any;
  cstimest4?: any;
  cstimest5?: any;
  pstimest1?: any;
  pstimest2?: any;
  pstimest3?: any;
  pstimest4?: any;
  pstimest5?: any;
}

export interface Nam {
  prov?: number;
  keep?: any;
}

export interface SMSSpam {
  active?: string;
  scAdds?: any[];
}

export class getSubscriberInfoHlrResponse implements IGetSubscriberInfoHlrResponse {
  constructor(public moattributes?: Moattributes) {}
}
