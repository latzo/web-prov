export interface ISimSwap {
  id?: number;
  msisdn?: string;
  oldIMSI?: string;
  newIMSI?: string;
  backendResp?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class SimSwap implements ISimSwap {
  constructor(
    public id?: number,
    public msisdn?: string,
    public oldIMSI?: string,
    public newIMSI?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
