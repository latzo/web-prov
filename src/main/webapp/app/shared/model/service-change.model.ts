export const enum OperationEnum {
  ACTIVATE = 'ACTIVATE',
  DEACTIVATE = 'DEACTIVATE'
}

export interface IServiceChange {
  id?: number;
  msisdn?: string;
  imsi?: string;
  operation?: OperationEnum;
  serviceId?: string;
  createdBy?: string;
  createdDate?: Date;
}

export interface IService {
  id?: number;
  code?: string;
  label?: string;
  methodToInvoke?: string;
}

export class ServiceChange implements IServiceChange {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public operation?: OperationEnum,
    public serviceId?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
