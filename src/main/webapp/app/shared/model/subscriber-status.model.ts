export interface ISubscriberStatus {
  id?: number;
  statusId?: string;
  label?: string;
}

export class SubscriberStatus implements ISubscriberStatus {
  constructor(public id?: number, public statusId?: string, public label?: string) {}
}
