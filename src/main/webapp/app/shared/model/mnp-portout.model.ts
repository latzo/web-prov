export interface IMnpPortout {
  id?: number;
  msisdn?: string;
  operateur?: string;
  processingStatus?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class MnpPortout implements IMnpPortout {
  constructor(
    public id?: number,
    public msisdn?: string,
    public operateur?: string,
    public processingStatus?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
