export interface IPinpuk {
  id?: number;
  iccid?: number;
  msisdn?: string;
  imsi?: string;
  pin1?: number;
  pin2?: number;
  puk1?: number;
  puk2?: number;
  ki?: string;
  adm1?: string;
  nomfichier?: string;
  supplier?: string;
  loaddate?: Date;
}

export class Pinpuk implements IPinpuk {
  constructor(
    public id?: number,
    public iccid?: number,
    public msisdn?: string,
    public imsi?: string,
    public pin1?: number,
    public pin2?: number,
    public puk1?: number,
    public puk2?: number,
    public ki?: string,
    public adm1?: string,
    public nomfichier?: string,
    public supplier?: string,
    public loaddate?: Date
  ) {}
}
