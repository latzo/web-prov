export interface IArchivage {
  id?: number;
  msisdn?: string;
}

export class Archivage implements IArchivage {
  constructor(public id?: number, public msisdn?: string) {}
}
