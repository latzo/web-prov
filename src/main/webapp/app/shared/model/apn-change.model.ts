export const enum OperationEnum {
  ACTIVATE = 'ACTIVATE',
  DEACTIVATE = 'DEACTIVATE'
}

export interface IAPNChange {
  id?: number;
  msisdn?: string;
  imsi?: string;
  operation?: OperationEnum;
  apn?: string;
  profil?: string;
  processingStatus?: string;
  errorMessage?: string;
  transactionId?: string;
  createdBy?: string;
}

export interface IAPNChangeVTalend extends IAPNChange {
  ipAddr?: string;
  technology?: string;
}

export class APNChange implements IAPNChange {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public operation?: OperationEnum,
    public apn?: string,
    public profil?: string,
    public processingStatus?: string,
    public errorMessage?: string,
    public transactionId?: string,
    public createdBy?: string
  ) {}
}
