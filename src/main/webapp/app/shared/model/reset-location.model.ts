export interface IResetLocation {
  id?: number;
  msisdn?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class ResetLocation implements IResetLocation {
  constructor(public id?: number, public msisdn?: string, public createdBy?: string, public createdDate?: Date) {}
}
