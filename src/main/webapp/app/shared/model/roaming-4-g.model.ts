import { Roaming4gEnum } from 'app/shared/model/enumerations/roaming-4-g-enum.model';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

export interface IRoaming4g {
  id?: number;
  imsi?: string;
  services?: Roaming4gEnum;
  processingStatus?: ProcessingStatus;
}

export class Roaming4g implements IRoaming4g {
  constructor(public id?: number, public imsi?: string, public services?: Roaming4gEnum, public processingStatus?: ProcessingStatus) {}
}
