export interface IDeleteSubscriber {
  id?: number;
  imsi?: string;
  msisdn?: string;
  deleteOnAuc?: boolean;
  deleteOnHlr?: boolean;
  deleteOnHss?: boolean;
  deleteOnCbs?: boolean;
  aucDeletedStatus?: string;
  hlrDeletedStatus?: string;
  hssDeletedStatus?: string;
  cbsDeletedStatus?: string;
  errorMessage?: string;
  transactionId?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class DeleteSubscriber implements IDeleteSubscriber {
  constructor(
    public id?: number,
    public imsi?: string,
    public msisdn?: string,
    public deleteOnAuc?: boolean,
    public deleteOnHlr?: boolean,
    public deleteOnHss?: boolean,
    public deleteOnCbs?: boolean,
    public aucDeletedStatus?: string,
    public hlrDeletedStatus?: string,
    public hssDeletedStatus?: string,
    public cbsDeletedStatus?: string,
    public errorMessage?: string,
    public transactionId?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {
    this.deleteOnAuc = this.deleteOnAuc || false;
    this.deleteOnHlr = this.deleteOnHlr || false;
    this.deleteOnHss = this.deleteOnHss || false;
    this.deleteOnCbs = this.deleteOnCbs || false;
  }
}
