export const enum SIMCreationType {
  IN = 'IN',
  SWITCH = 'SWITCH',
  IN_SWITCH = 'IN_SWITCH',
  HSS = 'HSS',
  IN_SWITCH_HSS = 'IN_SWITCH_HSS',
  AUC = 'AUC',
  FULL_PORTIN = 'FULL_PORTIN'
}

export interface ICreateSubscriber {
  id?: number;
  msisdn?: string;
  imsi?: string;
  processingStatus?: string;
  statut?: string;
  transactionId?: string;
  backendResp?: string;
  cos?: string;
  creationType?: SIMCreationType;
  name?: string;
  firstName?: string;
  dob?: string;
  city?: string;
  puk?: string;
  tigoCashStatus?: string;
  gender?: string;
  altContactNum?: string;
  district?: string;
  documentReferenceType?: string;
  proofNumber?: string;
  createUser?: string;
  createDate?: string;
  createdBy?: string;
  createdDate?: Date;
}

export interface IMainProducts {
  id?: number;
  mainProductName?: string;
  mainProductId?: string;
  hlrProfilId?: string;
  profileClass?: string;
}

export class CreateSubscriber implements ICreateSubscriber {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public processingStatus?: string,
    public statut?: string,
    public transactionId?: string,
    public backendResp?: string,
    public cos?: string,
    public creationType?: SIMCreationType,
    public createdBy?: string,
    public createdDate?: Date,
    public name?: string,
    public firstName?: string,
    public dob?: string,
    public city?: string,
    public puk?: string,
    public tigoCashStatus?: string,
    public gender?: string,
    public altContactNum?: string,
    public district?: string,
    public documentReferenceType?: string,
    public proofNumber?: string,
    public createUser?: string,
    public createDate?: string
  ) {}
}
