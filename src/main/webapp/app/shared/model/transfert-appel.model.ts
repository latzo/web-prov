export const enum OperationEnum {
  ACTIVATE = 'ACTIVATE',
  DEACTIVATE = 'DEACTIVATE'
}

export interface ITransfertAppel {
  id?: number;
  msisdnSource?: string;
  msisdnDest?: string;
  operation?: OperationEnum;
  processingStatus?: string;
  transactionId?: string;
  backendResp?: string;
}

export class TransfertAppel implements ITransfertAppel {
  constructor(
    public id?: number,
    public msisdnSource?: string,
    public msisdnDest?: string,
    public operation?: OperationEnum,
    public processingStatus?: string,
    public transactionId?: string,
    public backendResp?: string
  ) {}
}
