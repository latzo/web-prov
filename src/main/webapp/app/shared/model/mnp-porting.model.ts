export interface IMnpPorting {
  id?: number;
  msisdn?: string;
  processingStatus?: string;
  createdBy?: string;
  createdDate?: Date;
}

export class MnpPorting implements IMnpPorting {
  constructor(
    public id?: number,
    public msisdn?: string,
    public processingStatus?: string,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
