export const enum SIMCreationType {
  IN = 'IN',
  SWITCH = 'SWITCH',
  IN_SWITCH = 'IN_SWITCH',
  AUC = 'AUC'
}

export interface IFullPortin {
  id?: number;
  msisdn?: string;
  imsi?: string;
  processingStatus?: string;
  statut?: string;
  transactionId?: string;
  backendResp?: string;
  cos?: string;
  creationType?: SIMCreationType;
  name?: string;
  firstName?: string;
  dob?: string;
  city?: string;
  puk?: string;
  tigoCashStatus?: string;
  gender?: string;
  altContactNum?: string;
  district?: string;
  documentReferenceType?: string;
  proofNumber?: string;
  createUser?: string;
  createDate?: string;
}

export class FullPortin implements IFullPortin {
  constructor(
    public id?: number,
    public msisdn?: string,
    public imsi?: string,
    public processingStatus?: string,
    public statut?: string,
    public transactionId?: string,
    public backendResp?: string,
    public cos?: string,
    public creationType?: SIMCreationType,
    public name?: string,
    public firstName?: string,
    public dob?: string,
    public city?: string,
    public puk?: string,
    public tigoCashStatus?: string,
    public gender?: string,
    public altContactNum?: string,
    public district?: string,
    public documentReferenceType?: string,
    public proofNumber?: string,
    public createUser?: string,
    public createDate?: string
  ) {}
}
