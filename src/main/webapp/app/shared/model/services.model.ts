export interface IServices {
  id?: number;
  code?: string;
  label?: string;
  methodToInvoke?: string;
}

export class Services implements IServices {
  constructor(public id?: number, public code?: string, public label?: string, public methodToInvoke?: string) {}
}
