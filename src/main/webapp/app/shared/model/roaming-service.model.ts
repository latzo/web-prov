export const enum RoamingServiceEnum {
  VOICE_DATA = 'VOICE_DATA',
  VOICE_NODATA = 'VOICE_NODATA',
  NOVOICE_DATA = 'NOVOICE_DATA',
  NOVOICE_NODATA = 'NOVOICE_NODATA'
}

export interface IRoamingService {
  id?: number;
  msisdn?: string;
  services?: RoamingServiceEnum;
  createdBy?: string;
  createdDate?: Date;
}

export class RoamingService implements IRoamingService {
  constructor(
    public id?: number,
    public msisdn?: string,
    public services?: RoamingServiceEnum,
    public createdBy?: string,
    public createdDate?: Date
  ) {}
}
