import { NgModule } from '@angular/core';

import { TigocareSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [TigocareSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [TigocareSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TigocareSharedCommonModule {}
