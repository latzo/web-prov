import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TigocareSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [TigocareSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [TigocareSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TigocareSharedModule {
  static forRoot() {
    return {
      ngModule: TigocareSharedModule
    };
  }
}
