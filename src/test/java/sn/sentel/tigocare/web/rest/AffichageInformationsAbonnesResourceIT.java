/*
package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.AffichageInformationsAbonnes;
import sn.sentel.tigocare.repository.AffichageInformationsAbonnesRepository;
import sn.sentel.tigocare.service.AffichageInformationsAbonnesService;
import sn.sentel.tigocare.service.dto.AffichageInformationsAbonnesDTO;
import sn.sentel.tigocare.service.mapper.AffichageInformationsAbonnesMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

*/
/**
 * Integration tests for the {@Link AffichageInformationsAbonnesResource} REST controller.
 *//*

@SpringBootTest(classes = TigocareApp.class)
public class AffichageInformationsAbonnesResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
    private static final String UPDATED_IMSI = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI_HLR = "AAAAAAAAAA";
    private static final String UPDATED_IMSI_HLR = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI_CBS = "AAAAAAAAAA";
    private static final String UPDATED_IMSI_CBS = "BBBBBBBBBB";

    @Autowired
    private AffichageInformationsAbonnesRepository affichageInformationsAbonnesRepository;

    @Autowired
    private AffichageInformationsAbonnesMapper affichageInformationsAbonnesMapper;

    @Autowired
    private AffichageInformationsAbonnesService affichageInformationsAbonnesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAffichageInformationsAbonnesMockMvc;

    private AffichageInformationsAbonnes affichageInformationsAbonnes;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AffichageInformationsAbonnesResource affichageInformationsAbonnesResource = new AffichageInformationsAbonnesResource(affichageInformationsAbonnesService, subscriberInfoService);
        this.restAffichageInformationsAbonnesMockMvc = MockMvcBuilders.standaloneSetup(affichageInformationsAbonnesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    */
/**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*

    public static AffichageInformationsAbonnes createEntity(EntityManager em) {
        AffichageInformationsAbonnes affichageInformationsAbonnes = new AffichageInformationsAbonnes()
            .msisdn(DEFAULT_MSISDN)
            .imsi(DEFAULT_IMSI)
            .imsiHlr(DEFAULT_IMSI_HLR)
            .imsiCbs(DEFAULT_IMSI_CBS);
        return affichageInformationsAbonnes;
    }
    */
/**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*

    public static AffichageInformationsAbonnes createUpdatedEntity(EntityManager em) {
        AffichageInformationsAbonnes affichageInformationsAbonnes = new AffichageInformationsAbonnes()
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .imsiHlr(UPDATED_IMSI_HLR)
            .imsiCbs(UPDATED_IMSI_CBS);
        return affichageInformationsAbonnes;
    }

    @BeforeEach
    public void initTest() {
        affichageInformationsAbonnes = createEntity(em);
    }

    @Test
    @Transactional
    public void createAffichageInformationsAbonnes() throws Exception {
        int databaseSizeBeforeCreate = affichageInformationsAbonnesRepository.findAll().size();

        // Create the AffichageInformationsAbonnes
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO = affichageInformationsAbonnesMapper.toDto(affichageInformationsAbonnes);
        restAffichageInformationsAbonnesMockMvc.perform(post("/api/affichage-informations-abonnes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(affichageInformationsAbonnesDTO)))
            .andExpect(status().isCreated());

        // Validate the AffichageInformationsAbonnes in the database
        List<AffichageInformationsAbonnes> affichageInformationsAbonnesList = affichageInformationsAbonnesRepository.findAll();
        assertThat(affichageInformationsAbonnesList).hasSize(databaseSizeBeforeCreate + 1);
        AffichageInformationsAbonnes testAffichageInformationsAbonnes = affichageInformationsAbonnesList.get(affichageInformationsAbonnesList.size() - 1);
        assertThat(testAffichageInformationsAbonnes.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testAffichageInformationsAbonnes.getImsi()).isEqualTo(DEFAULT_IMSI);
        assertThat(testAffichageInformationsAbonnes.getImsiHlr()).isEqualTo(DEFAULT_IMSI_HLR);
        assertThat(testAffichageInformationsAbonnes.getImsiCbs()).isEqualTo(DEFAULT_IMSI_CBS);
    }

    @Test
    @Transactional
    public void createAffichageInformationsAbonnesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = affichageInformationsAbonnesRepository.findAll().size();

        // Create the AffichageInformationsAbonnes with an existing ID
        affichageInformationsAbonnes.setId(1L);
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO = affichageInformationsAbonnesMapper.toDto(affichageInformationsAbonnes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAffichageInformationsAbonnesMockMvc.perform(post("/api/affichage-informations-abonnes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(affichageInformationsAbonnesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AffichageInformationsAbonnes in the database
        List<AffichageInformationsAbonnes> affichageInformationsAbonnesList = affichageInformationsAbonnesRepository.findAll();
        assertThat(affichageInformationsAbonnesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAffichageInformationsAbonnes() throws Exception {
        // Initialize the database
        affichageInformationsAbonnesRepository.saveAndFlush(affichageInformationsAbonnes);

        // Get all the affichageInformationsAbonnesList
        restAffichageInformationsAbonnesMockMvc.perform(get("/api/affichage-informations-abonnes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(affichageInformationsAbonnes.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
            .andExpect(jsonPath("$.[*].imsiHlr").value(hasItem(DEFAULT_IMSI_HLR.toString())))
            .andExpect(jsonPath("$.[*].imsiCbs").value(hasItem(DEFAULT_IMSI_CBS.toString())));
    }
    
    @Test
    @Transactional
    public void getAffichageInformationsAbonnes() throws Exception {
        // Initialize the database
        affichageInformationsAbonnesRepository.saveAndFlush(affichageInformationsAbonnes);

        // Get the affichageInformationsAbonnes
        restAffichageInformationsAbonnesMockMvc.perform(get("/api/affichage-informations-abonnes/{id}", affichageInformationsAbonnes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(affichageInformationsAbonnes.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
            .andExpect(jsonPath("$.imsiHlr").value(DEFAULT_IMSI_HLR.toString()))
            .andExpect(jsonPath("$.imsiCbs").value(DEFAULT_IMSI_CBS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAffichageInformationsAbonnes() throws Exception {
        // Get the affichageInformationsAbonnes
        restAffichageInformationsAbonnesMockMvc.perform(get("/api/affichage-informations-abonnes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAffichageInformationsAbonnes() throws Exception {
        // Initialize the database
        affichageInformationsAbonnesRepository.saveAndFlush(affichageInformationsAbonnes);

        int databaseSizeBeforeUpdate = affichageInformationsAbonnesRepository.findAll().size();

        // Update the affichageInformationsAbonnes
        AffichageInformationsAbonnes updatedAffichageInformationsAbonnes = affichageInformationsAbonnesRepository.findById(affichageInformationsAbonnes.getId()).get();
        // Disconnect from session so that the updates on updatedAffichageInformationsAbonnes are not directly saved in db
        em.detach(updatedAffichageInformationsAbonnes);
        updatedAffichageInformationsAbonnes
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .imsiHlr(UPDATED_IMSI_HLR)
            .imsiCbs(UPDATED_IMSI_CBS);
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO = affichageInformationsAbonnesMapper.toDto(updatedAffichageInformationsAbonnes);

        restAffichageInformationsAbonnesMockMvc.perform(put("/api/affichage-informations-abonnes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(affichageInformationsAbonnesDTO)))
            .andExpect(status().isOk());

        // Validate the AffichageInformationsAbonnes in the database
        List<AffichageInformationsAbonnes> affichageInformationsAbonnesList = affichageInformationsAbonnesRepository.findAll();
        assertThat(affichageInformationsAbonnesList).hasSize(databaseSizeBeforeUpdate);
        AffichageInformationsAbonnes testAffichageInformationsAbonnes = affichageInformationsAbonnesList.get(affichageInformationsAbonnesList.size() - 1);
        assertThat(testAffichageInformationsAbonnes.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testAffichageInformationsAbonnes.getImsi()).isEqualTo(UPDATED_IMSI);
        assertThat(testAffichageInformationsAbonnes.getImsiHlr()).isEqualTo(UPDATED_IMSI_HLR);
        assertThat(testAffichageInformationsAbonnes.getImsiCbs()).isEqualTo(UPDATED_IMSI_CBS);
    }

    @Test
    @Transactional
    public void updateNonExistingAffichageInformationsAbonnes() throws Exception {
        int databaseSizeBeforeUpdate = affichageInformationsAbonnesRepository.findAll().size();

        // Create the AffichageInformationsAbonnes
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO = affichageInformationsAbonnesMapper.toDto(affichageInformationsAbonnes);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAffichageInformationsAbonnesMockMvc.perform(put("/api/affichage-informations-abonnes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(affichageInformationsAbonnesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AffichageInformationsAbonnes in the database
        List<AffichageInformationsAbonnes> affichageInformationsAbonnesList = affichageInformationsAbonnesRepository.findAll();
        assertThat(affichageInformationsAbonnesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAffichageInformationsAbonnes() throws Exception {
        // Initialize the database
        affichageInformationsAbonnesRepository.saveAndFlush(affichageInformationsAbonnes);

        int databaseSizeBeforeDelete = affichageInformationsAbonnesRepository.findAll().size();

        // Delete the affichageInformationsAbonnes
        restAffichageInformationsAbonnesMockMvc.perform(delete("/api/affichage-informations-abonnes/{id}", affichageInformationsAbonnes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AffichageInformationsAbonnes> affichageInformationsAbonnesList = affichageInformationsAbonnesRepository.findAll();
        assertThat(affichageInformationsAbonnesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AffichageInformationsAbonnes.class);
        AffichageInformationsAbonnes affichageInformationsAbonnes1 = new AffichageInformationsAbonnes();
        affichageInformationsAbonnes1.setId(1L);
        AffichageInformationsAbonnes affichageInformationsAbonnes2 = new AffichageInformationsAbonnes();
        affichageInformationsAbonnes2.setId(affichageInformationsAbonnes1.getId());
        assertThat(affichageInformationsAbonnes1).isEqualTo(affichageInformationsAbonnes2);
        affichageInformationsAbonnes2.setId(2L);
        assertThat(affichageInformationsAbonnes1).isNotEqualTo(affichageInformationsAbonnes2);
        affichageInformationsAbonnes1.setId(null);
        assertThat(affichageInformationsAbonnes1).isNotEqualTo(affichageInformationsAbonnes2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AffichageInformationsAbonnesDTO.class);
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO1 = new AffichageInformationsAbonnesDTO();
        affichageInformationsAbonnesDTO1.setId(1L);
        AffichageInformationsAbonnesDTO affichageInformationsAbonnesDTO2 = new AffichageInformationsAbonnesDTO();
        assertThat(affichageInformationsAbonnesDTO1).isNotEqualTo(affichageInformationsAbonnesDTO2);
        affichageInformationsAbonnesDTO2.setId(affichageInformationsAbonnesDTO1.getId());
        assertThat(affichageInformationsAbonnesDTO1).isEqualTo(affichageInformationsAbonnesDTO2);
        affichageInformationsAbonnesDTO2.setId(2L);
        assertThat(affichageInformationsAbonnesDTO1).isNotEqualTo(affichageInformationsAbonnesDTO2);
        affichageInformationsAbonnesDTO1.setId(null);
        assertThat(affichageInformationsAbonnesDTO1).isNotEqualTo(affichageInformationsAbonnesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(affichageInformationsAbonnesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(affichageInformationsAbonnesMapper.fromId(null)).isNull();
    }
}
*/
