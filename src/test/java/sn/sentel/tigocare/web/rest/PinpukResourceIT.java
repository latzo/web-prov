//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.Pinpuk;
//import sn.sentel.tigocare.repository.PinpukRepository;
//import sn.sentel.tigocare.service.PinpukService;
//import sn.sentel.tigocare.service.dto.PinpukDTO;
//import sn.sentel.tigocare.service.mapper.PinpukMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.time.LocalDate;
//import java.time.ZoneId;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@Link PinpukResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class PinpukResourceIT {
//
//    private static final Long DEFAULT_ICCID = 1L;
//    private static final Long UPDATED_ICCID = 2L;
//
//    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_IMSI = "BBBBBBBBBB";
//
//    private static final Long DEFAULT_PIN_1 = 1L;
//    private static final Long UPDATED_PIN_1 = 2L;
//
//    private static final Long DEFAULT_PUK_1 = 1L;
//    private static final Long UPDATED_PUK_1 = 2L;
//
//    private static final Long DEFAULT_PIN_2 = 1L;
//    private static final Long UPDATED_PIN_2 = 2L;
//
//    private static final Long DEFAULT_PUK_2 = 1L;
//    private static final Long UPDATED_PUK_2 = 2L;
//
//    private static final String DEFAULT_KI = "AAAAAAAAAA";
//    private static final String UPDATED_KI = "BBBBBBBBBB";
//
//    private static final String DEFAULT_ADM_1 = "AAAAAAAAAA";
//    private static final String UPDATED_ADM_1 = "BBBBBBBBBB";
//
//    private static final String DEFAULT_NOMFICHIER = "AAAAAAAAAA";
//    private static final String UPDATED_NOMFICHIER = "BBBBBBBBBB";
//
//    private static final String DEFAULT_SUPPLIER = "AAAAAAAAAA";
//    private static final String UPDATED_SUPPLIER = "BBBBBBBBBB";
//
//    private static final LocalDate DEFAULT_LOADDATE = LocalDate.ofEpochDay(0L);
//    private static final LocalDate UPDATED_LOADDATE = LocalDate.now(ZoneId.systemDefault());
//
//    @Autowired
//    private PinpukRepository pinpukRepository;
//
//    @Autowired
//    private PinpukMapper pinpukMapper;
//
//    @Autowired
//    private PinpukService pinpukService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restPinpukMockMvc;
//
//    private Pinpuk pinpuk;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final PinpukResource pinpukResource = new PinpukResource(pinpukService);
//        this.restPinpukMockMvc = MockMvcBuilders.standaloneSetup(pinpukResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Pinpuk createEntity(EntityManager em) {
//        Pinpuk pinpuk = new Pinpuk()
//            .iccid(DEFAULT_ICCID)
//            .imsi(DEFAULT_IMSI)
//            .pin1(DEFAULT_PIN_1)
//            .puk1(DEFAULT_PUK_1)
//            .pin2(DEFAULT_PIN_2)
//            .puk2(DEFAULT_PUK_2)
//            .ki(DEFAULT_KI)
//            .adm1(DEFAULT_ADM_1)
//            .nomfichier(DEFAULT_NOMFICHIER)
//            .supplier(DEFAULT_SUPPLIER)
//            .loaddate(DEFAULT_LOADDATE);
//        return pinpuk;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Pinpuk createUpdatedEntity(EntityManager em) {
//        Pinpuk pinpuk = new Pinpuk()
//            .iccid(UPDATED_ICCID)
//            .imsi(UPDATED_IMSI)
//            .pin1(UPDATED_PIN_1)
//            .puk1(UPDATED_PUK_1)
//            .pin2(UPDATED_PIN_2)
//            .puk2(UPDATED_PUK_2)
//            .ki(UPDATED_KI)
//            .adm1(UPDATED_ADM_1)
//            .nomfichier(UPDATED_NOMFICHIER)
//            .supplier(UPDATED_SUPPLIER)
//            .loaddate(UPDATED_LOADDATE);
//        return pinpuk;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        pinpuk = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createPinpuk() throws Exception {
//        int databaseSizeBeforeCreate = pinpukRepository.findAll().size();
//
//        // Create the Pinpuk
//        PinpukDTO pinpukDTO = pinpukMapper.toDto(pinpuk);
//        restPinpukMockMvc.perform(post("/api/pinpuks")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(pinpukDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the Pinpuk in the database
//        List<Pinpuk> pinpukList = pinpukRepository.findAll();
//        assertThat(pinpukList).hasSize(databaseSizeBeforeCreate + 1);
//        Pinpuk testPinpuk = pinpukList.get(pinpukList.size() - 1);
//        assertThat(testPinpuk.getIccid()).isEqualTo(DEFAULT_ICCID);
//        assertThat(testPinpuk.getImsi()).isEqualTo(DEFAULT_IMSI);
//        assertThat(testPinpuk.getPin1()).isEqualTo(DEFAULT_PIN_1);
//        assertThat(testPinpuk.getPuk1()).isEqualTo(DEFAULT_PUK_1);
//        assertThat(testPinpuk.getPin2()).isEqualTo(DEFAULT_PIN_2);
//        assertThat(testPinpuk.getPuk2()).isEqualTo(DEFAULT_PUK_2);
//        assertThat(testPinpuk.getKi()).isEqualTo(DEFAULT_KI);
//        assertThat(testPinpuk.getAdm1()).isEqualTo(DEFAULT_ADM_1);
//        assertThat(testPinpuk.getNomfichier()).isEqualTo(DEFAULT_NOMFICHIER);
//        assertThat(testPinpuk.getSupplier()).isEqualTo(DEFAULT_SUPPLIER);
//        assertThat(testPinpuk.getLoaddate()).isEqualTo(DEFAULT_LOADDATE);
//    }
//
//    @Test
//    @Transactional
//    public void createPinpukWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = pinpukRepository.findAll().size();
//
//        // Create the Pinpuk with an existing ID
//        pinpuk.setId(1L);
//        PinpukDTO pinpukDTO = pinpukMapper.toDto(pinpuk);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restPinpukMockMvc.perform(post("/api/pinpuks")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(pinpukDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Pinpuk in the database
//        List<Pinpuk> pinpukList = pinpukRepository.findAll();
//        assertThat(pinpukList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllPinpuks() throws Exception {
//        // Initialize the database
//        pinpukRepository.saveAndFlush(pinpuk);
//
//        // Get all the pinpukList
//        restPinpukMockMvc.perform(get("/api/pinpuks?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(pinpuk.getId().intValue())))
//            .andExpect(jsonPath("$.[*].iccid").value(hasItem(DEFAULT_ICCID.intValue())))
//            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].pin1").value(hasItem(DEFAULT_PIN_1.intValue())))
//            .andExpect(jsonPath("$.[*].puk1").value(hasItem(DEFAULT_PUK_1.intValue())))
//            .andExpect(jsonPath("$.[*].pin2").value(hasItem(DEFAULT_PIN_2.intValue())))
//            .andExpect(jsonPath("$.[*].puk2").value(hasItem(DEFAULT_PUK_2.intValue())))
//            .andExpect(jsonPath("$.[*].ki").value(hasItem(DEFAULT_KI.toString())))
//            .andExpect(jsonPath("$.[*].adm1").value(hasItem(DEFAULT_ADM_1.toString())))
//            .andExpect(jsonPath("$.[*].nomfichier").value(hasItem(DEFAULT_NOMFICHIER.toString())))
//            .andExpect(jsonPath("$.[*].supplier").value(hasItem(DEFAULT_SUPPLIER.toString())))
//            .andExpect(jsonPath("$.[*].loaddate").value(hasItem(DEFAULT_LOADDATE.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getPinpuk() throws Exception {
//        // Initialize the database
//        pinpukRepository.saveAndFlush(pinpuk);
//
//        // Get the pinpuk
//        restPinpukMockMvc.perform(get("/api/pinpuks/{id}", pinpuk.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(pinpuk.getId().intValue()))
//            .andExpect(jsonPath("$.iccid").value(DEFAULT_ICCID.intValue()))
//            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
//            .andExpect(jsonPath("$.pin1").value(DEFAULT_PIN_1.intValue()))
//            .andExpect(jsonPath("$.puk1").value(DEFAULT_PUK_1.intValue()))
//            .andExpect(jsonPath("$.pin2").value(DEFAULT_PIN_2.intValue()))
//            .andExpect(jsonPath("$.puk2").value(DEFAULT_PUK_2.intValue()))
//            .andExpect(jsonPath("$.ki").value(DEFAULT_KI.toString()))
//            .andExpect(jsonPath("$.adm1").value(DEFAULT_ADM_1.toString()))
//            .andExpect(jsonPath("$.nomfichier").value(DEFAULT_NOMFICHIER.toString()))
//            .andExpect(jsonPath("$.supplier").value(DEFAULT_SUPPLIER.toString()))
//            .andExpect(jsonPath("$.loaddate").value(DEFAULT_LOADDATE.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingPinpuk() throws Exception {
//        // Get the pinpuk
//        restPinpukMockMvc.perform(get("/api/pinpuks/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updatePinpuk() throws Exception {
//        // Initialize the database
//        pinpukRepository.saveAndFlush(pinpuk);
//
//        int databaseSizeBeforeUpdate = pinpukRepository.findAll().size();
//
//        // Update the pinpuk
//        Pinpuk updatedPinpuk = pinpukRepository.findById(pinpuk.getId()).get();
//        // Disconnect from session so that the updates on updatedPinpuk are not directly saved in db
//        em.detach(updatedPinpuk);
//        updatedPinpuk
//            .iccid(UPDATED_ICCID)
//            .imsi(UPDATED_IMSI)
//            .pin1(UPDATED_PIN_1)
//            .puk1(UPDATED_PUK_1)
//            .pin2(UPDATED_PIN_2)
//            .puk2(UPDATED_PUK_2)
//            .ki(UPDATED_KI)
//            .adm1(UPDATED_ADM_1)
//            .nomfichier(UPDATED_NOMFICHIER)
//            .supplier(UPDATED_SUPPLIER)
//            .loaddate(UPDATED_LOADDATE);
//        PinpukDTO pinpukDTO = pinpukMapper.toDto(updatedPinpuk);
//
//        restPinpukMockMvc.perform(put("/api/pinpuks")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(pinpukDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the Pinpuk in the database
//        List<Pinpuk> pinpukList = pinpukRepository.findAll();
//        assertThat(pinpukList).hasSize(databaseSizeBeforeUpdate);
//        Pinpuk testPinpuk = pinpukList.get(pinpukList.size() - 1);
//        assertThat(testPinpuk.getIccid()).isEqualTo(UPDATED_ICCID);
//        assertThat(testPinpuk.getImsi()).isEqualTo(UPDATED_IMSI);
//        assertThat(testPinpuk.getPin1()).isEqualTo(UPDATED_PIN_1);
//        assertThat(testPinpuk.getPuk1()).isEqualTo(UPDATED_PUK_1);
//        assertThat(testPinpuk.getPin2()).isEqualTo(UPDATED_PIN_2);
//        assertThat(testPinpuk.getPuk2()).isEqualTo(UPDATED_PUK_2);
//        assertThat(testPinpuk.getKi()).isEqualTo(UPDATED_KI);
//        assertThat(testPinpuk.getAdm1()).isEqualTo(UPDATED_ADM_1);
//        assertThat(testPinpuk.getNomfichier()).isEqualTo(UPDATED_NOMFICHIER);
//        assertThat(testPinpuk.getSupplier()).isEqualTo(UPDATED_SUPPLIER);
//        assertThat(testPinpuk.getLoaddate()).isEqualTo(UPDATED_LOADDATE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingPinpuk() throws Exception {
//        int databaseSizeBeforeUpdate = pinpukRepository.findAll().size();
//
//        // Create the Pinpuk
//        PinpukDTO pinpukDTO = pinpukMapper.toDto(pinpuk);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restPinpukMockMvc.perform(put("/api/pinpuks")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(pinpukDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Pinpuk in the database
//        List<Pinpuk> pinpukList = pinpukRepository.findAll();
//        assertThat(pinpukList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deletePinpuk() throws Exception {
//        // Initialize the database
//        pinpukRepository.saveAndFlush(pinpuk);
//
//        int databaseSizeBeforeDelete = pinpukRepository.findAll().size();
//
//        // Delete the pinpuk
//        restPinpukMockMvc.perform(delete("/api/pinpuks/{id}", pinpuk.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<Pinpuk> pinpukList = pinpukRepository.findAll();
//        assertThat(pinpukList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(Pinpuk.class);
//        Pinpuk pinpuk1 = new Pinpuk();
//        pinpuk1.setId(1L);
//        Pinpuk pinpuk2 = new Pinpuk();
//        pinpuk2.setId(pinpuk1.getId());
//        assertThat(pinpuk1).isEqualTo(pinpuk2);
//        pinpuk2.setId(2L);
//        assertThat(pinpuk1).isNotEqualTo(pinpuk2);
//        pinpuk1.setId(null);
//        assertThat(pinpuk1).isNotEqualTo(pinpuk2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(PinpukDTO.class);
//        PinpukDTO pinpukDTO1 = new PinpukDTO();
//        pinpukDTO1.setId(1L);
//        PinpukDTO pinpukDTO2 = new PinpukDTO();
//        assertThat(pinpukDTO1).isNotEqualTo(pinpukDTO2);
//        pinpukDTO2.setId(pinpukDTO1.getId());
//        assertThat(pinpukDTO1).isEqualTo(pinpukDTO2);
//        pinpukDTO2.setId(2L);
//        assertThat(pinpukDTO1).isNotEqualTo(pinpukDTO2);
//        pinpukDTO1.setId(null);
//        assertThat(pinpukDTO1).isNotEqualTo(pinpukDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(pinpukMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(pinpukMapper.fromId(null)).isNull();
//    }
//}
