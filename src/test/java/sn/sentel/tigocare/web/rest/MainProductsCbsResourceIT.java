package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.MainProductsCbs;
import sn.sentel.tigocare.repository.MainProductsCbsRepository;
import sn.sentel.tigocare.service.MainProductsCbsService;
import sn.sentel.tigocare.service.dto.MainProductsCbsDTO;
import sn.sentel.tigocare.service.mapper.MainProductsCbsMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MainProductsCbsResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class MainProductsCbsResourceIT {

    private static final String DEFAULT_MAIN_PRODUCT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_PRODUCT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MAIN_PRODUCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_PRODUCT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_HLR_PROFIL_ID = "AAAAAAAAAA";
    private static final String UPDATED_HLR_PROFIL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PROFILE_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_PROFILE_CLASS = "BBBBBBBBBB";

    @Autowired
    private MainProductsCbsRepository mainProductsCbsRepository;

    @Autowired
    private MainProductsCbsMapper mainProductsCbsMapper;

    @Autowired
    private MainProductsCbsService mainProductsCbsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMainProductsCbsMockMvc;

    private MainProductsCbs mainProductsCbs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MainProductsCbsResource mainProductsCbsResource = new MainProductsCbsResource(mainProductsCbsService);
        this.restMainProductsCbsMockMvc = MockMvcBuilders.standaloneSetup(mainProductsCbsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MainProductsCbs createEntity(EntityManager em) {
        MainProductsCbs mainProductsCbs = new MainProductsCbs()
            .mainProductName(DEFAULT_MAIN_PRODUCT_NAME)
            .mainProductId(DEFAULT_MAIN_PRODUCT_ID)
            .hlrProfilId(DEFAULT_HLR_PROFIL_ID)
            .profileClass(DEFAULT_PROFILE_CLASS);
        return mainProductsCbs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MainProductsCbs createUpdatedEntity(EntityManager em) {
        MainProductsCbs mainProductsCbs = new MainProductsCbs()
            .mainProductName(UPDATED_MAIN_PRODUCT_NAME)
            .mainProductId(UPDATED_MAIN_PRODUCT_ID)
            .hlrProfilId(UPDATED_HLR_PROFIL_ID)
            .profileClass(UPDATED_PROFILE_CLASS);
        return mainProductsCbs;
    }

    @BeforeEach
    public void initTest() {
        mainProductsCbs = createEntity(em);
    }

    @Test
    @Transactional
    public void createMainProductsCbs() throws Exception {
        int databaseSizeBeforeCreate = mainProductsCbsRepository.findAll().size();

        // Create the MainProductsCbs
        MainProductsCbsDTO mainProductsCbsDTO = mainProductsCbsMapper.toDto(mainProductsCbs);
        restMainProductsCbsMockMvc.perform(post("/api/main-products-cbs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainProductsCbsDTO)))
            .andExpect(status().isCreated());

        // Validate the MainProductsCbs in the database
        List<MainProductsCbs> mainProductsCbsList = mainProductsCbsRepository.findAll();
        assertThat(mainProductsCbsList).hasSize(databaseSizeBeforeCreate + 1);
        MainProductsCbs testMainProductsCbs = mainProductsCbsList.get(mainProductsCbsList.size() - 1);
        assertThat(testMainProductsCbs.getMainProductName()).isEqualTo(DEFAULT_MAIN_PRODUCT_NAME);
        assertThat(testMainProductsCbs.getMainProductId()).isEqualTo(DEFAULT_MAIN_PRODUCT_ID);
        assertThat(testMainProductsCbs.getHlrProfilId()).isEqualTo(DEFAULT_HLR_PROFIL_ID);
        assertThat(testMainProductsCbs.getProfileClass()).isEqualTo(DEFAULT_PROFILE_CLASS);
    }

    @Test
    @Transactional
    public void createMainProductsCbsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mainProductsCbsRepository.findAll().size();

        // Create the MainProductsCbs with an existing ID
        mainProductsCbs.setId(1L);
        MainProductsCbsDTO mainProductsCbsDTO = mainProductsCbsMapper.toDto(mainProductsCbs);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMainProductsCbsMockMvc.perform(post("/api/main-products-cbs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainProductsCbsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MainProductsCbs in the database
        List<MainProductsCbs> mainProductsCbsList = mainProductsCbsRepository.findAll();
        assertThat(mainProductsCbsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMainProductsCbs() throws Exception {
        // Initialize the database
        mainProductsCbsRepository.saveAndFlush(mainProductsCbs);

        // Get all the mainProductsCbsList
        restMainProductsCbsMockMvc.perform(get("/api/main-products-cbs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mainProductsCbs.getId().intValue())))
            .andExpect(jsonPath("$.[*].mainProductName").value(hasItem(DEFAULT_MAIN_PRODUCT_NAME.toString())))
            .andExpect(jsonPath("$.[*].mainProductId").value(hasItem(DEFAULT_MAIN_PRODUCT_ID.toString())))
            .andExpect(jsonPath("$.[*].hlrProfilId").value(hasItem(DEFAULT_HLR_PROFIL_ID.toString())))
            .andExpect(jsonPath("$.[*].profileClass").value(hasItem(DEFAULT_PROFILE_CLASS.toString())));
    }
    
    @Test
    @Transactional
    public void getMainProductsCbs() throws Exception {
        // Initialize the database
        mainProductsCbsRepository.saveAndFlush(mainProductsCbs);

        // Get the mainProductsCbs
        restMainProductsCbsMockMvc.perform(get("/api/main-products-cbs/{id}", mainProductsCbs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mainProductsCbs.getId().intValue()))
            .andExpect(jsonPath("$.mainProductName").value(DEFAULT_MAIN_PRODUCT_NAME.toString()))
            .andExpect(jsonPath("$.mainProductId").value(DEFAULT_MAIN_PRODUCT_ID.toString()))
            .andExpect(jsonPath("$.hlrProfilId").value(DEFAULT_HLR_PROFIL_ID.toString()))
            .andExpect(jsonPath("$.profileClass").value(DEFAULT_PROFILE_CLASS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMainProductsCbs() throws Exception {
        // Get the mainProductsCbs
        restMainProductsCbsMockMvc.perform(get("/api/main-products-cbs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMainProductsCbs() throws Exception {
        // Initialize the database
        mainProductsCbsRepository.saveAndFlush(mainProductsCbs);

        int databaseSizeBeforeUpdate = mainProductsCbsRepository.findAll().size();

        // Update the mainProductsCbs
        MainProductsCbs updatedMainProductsCbs = mainProductsCbsRepository.findById(mainProductsCbs.getId()).get();
        // Disconnect from session so that the updates on updatedMainProductsCbs are not directly saved in db
        em.detach(updatedMainProductsCbs);
        updatedMainProductsCbs
            .mainProductName(UPDATED_MAIN_PRODUCT_NAME)
            .mainProductId(UPDATED_MAIN_PRODUCT_ID)
            .hlrProfilId(UPDATED_HLR_PROFIL_ID)
            .profileClass(UPDATED_PROFILE_CLASS);
        MainProductsCbsDTO mainProductsCbsDTO = mainProductsCbsMapper.toDto(updatedMainProductsCbs);

        restMainProductsCbsMockMvc.perform(put("/api/main-products-cbs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainProductsCbsDTO)))
            .andExpect(status().isOk());

        // Validate the MainProductsCbs in the database
        List<MainProductsCbs> mainProductsCbsList = mainProductsCbsRepository.findAll();
        assertThat(mainProductsCbsList).hasSize(databaseSizeBeforeUpdate);
        MainProductsCbs testMainProductsCbs = mainProductsCbsList.get(mainProductsCbsList.size() - 1);
        assertThat(testMainProductsCbs.getMainProductName()).isEqualTo(UPDATED_MAIN_PRODUCT_NAME);
        assertThat(testMainProductsCbs.getMainProductId()).isEqualTo(UPDATED_MAIN_PRODUCT_ID);
        assertThat(testMainProductsCbs.getHlrProfilId()).isEqualTo(UPDATED_HLR_PROFIL_ID);
        assertThat(testMainProductsCbs.getProfileClass()).isEqualTo(UPDATED_PROFILE_CLASS);
    }

    @Test
    @Transactional
    public void updateNonExistingMainProductsCbs() throws Exception {
        int databaseSizeBeforeUpdate = mainProductsCbsRepository.findAll().size();

        // Create the MainProductsCbs
        MainProductsCbsDTO mainProductsCbsDTO = mainProductsCbsMapper.toDto(mainProductsCbs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMainProductsCbsMockMvc.perform(put("/api/main-products-cbs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mainProductsCbsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MainProductsCbs in the database
        List<MainProductsCbs> mainProductsCbsList = mainProductsCbsRepository.findAll();
        assertThat(mainProductsCbsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMainProductsCbs() throws Exception {
        // Initialize the database
        mainProductsCbsRepository.saveAndFlush(mainProductsCbs);

        int databaseSizeBeforeDelete = mainProductsCbsRepository.findAll().size();

        // Delete the mainProductsCbs
        restMainProductsCbsMockMvc.perform(delete("/api/main-products-cbs/{id}", mainProductsCbs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MainProductsCbs> mainProductsCbsList = mainProductsCbsRepository.findAll();
        assertThat(mainProductsCbsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MainProductsCbs.class);
        MainProductsCbs mainProductsCbs1 = new MainProductsCbs();
        mainProductsCbs1.setId(1L);
        MainProductsCbs mainProductsCbs2 = new MainProductsCbs();
        mainProductsCbs2.setId(mainProductsCbs1.getId());
        assertThat(mainProductsCbs1).isEqualTo(mainProductsCbs2);
        mainProductsCbs2.setId(2L);
        assertThat(mainProductsCbs1).isNotEqualTo(mainProductsCbs2);
        mainProductsCbs1.setId(null);
        assertThat(mainProductsCbs1).isNotEqualTo(mainProductsCbs2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MainProductsCbsDTO.class);
        MainProductsCbsDTO mainProductsCbsDTO1 = new MainProductsCbsDTO();
        mainProductsCbsDTO1.setId(1L);
        MainProductsCbsDTO mainProductsCbsDTO2 = new MainProductsCbsDTO();
        assertThat(mainProductsCbsDTO1).isNotEqualTo(mainProductsCbsDTO2);
        mainProductsCbsDTO2.setId(mainProductsCbsDTO1.getId());
        assertThat(mainProductsCbsDTO1).isEqualTo(mainProductsCbsDTO2);
        mainProductsCbsDTO2.setId(2L);
        assertThat(mainProductsCbsDTO1).isNotEqualTo(mainProductsCbsDTO2);
        mainProductsCbsDTO1.setId(null);
        assertThat(mainProductsCbsDTO1).isNotEqualTo(mainProductsCbsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mainProductsCbsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mainProductsCbsMapper.fromId(null)).isNull();
    }
}
