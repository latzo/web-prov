package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.ServiceChange;
import sn.sentel.tigocare.repository.ServiceChangeRepository;
import sn.sentel.tigocare.service.ServiceChangeService;
import sn.sentel.tigocare.service.dto.ServiceChangeDTO;
import sn.sentel.tigocare.service.mapper.ServiceChangeMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.sentel.tigocare.domain.enumeration.OperationEnum;
/**
 * Integration tests for the {@Link ServiceChangeResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class ServiceChangeResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
    private static final String UPDATED_IMSI = "BBBBBBBBBB";

    private static final OperationEnum DEFAULT_OPERATION = OperationEnum.ACTIVATE;
    private static final OperationEnum UPDATED_OPERATION = OperationEnum.DEACTIVATE;

    private static final String DEFAULT_SERVICE_ID = "AAAAAAAAAA";
    private static final String UPDATED_SERVICE_ID = "BBBBBBBBBB";

    @Autowired
    private ServiceChangeRepository serviceChangeRepository;

    @Autowired
    private ServiceChangeMapper serviceChangeMapper;

    @Autowired
    private ServiceChangeService serviceChangeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restServiceChangeMockMvc;

    private ServiceChange serviceChange;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ServiceChangeResource serviceChangeResource = new ServiceChangeResource(serviceChangeService);
        this.restServiceChangeMockMvc = MockMvcBuilders.standaloneSetup(serviceChangeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceChange createEntity(EntityManager em) {
        ServiceChange serviceChange = new ServiceChange()
            .msisdn(DEFAULT_MSISDN)
            .imsi(DEFAULT_IMSI)
            .operation(DEFAULT_OPERATION)
            .serviceId(DEFAULT_SERVICE_ID);
        return serviceChange;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceChange createUpdatedEntity(EntityManager em) {
        ServiceChange serviceChange = new ServiceChange()
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .operation(UPDATED_OPERATION)
            .serviceId(UPDATED_SERVICE_ID);
        return serviceChange;
    }

    @BeforeEach
    public void initTest() {
        serviceChange = createEntity(em);
    }

    @Test
    @Transactional
    public void createServiceChange() throws Exception {
        int databaseSizeBeforeCreate = serviceChangeRepository.findAll().size();

        // Create the ServiceChange
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);
        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isCreated());

        // Validate the ServiceChange in the database
        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceChange testServiceChange = serviceChangeList.get(serviceChangeList.size() - 1);
        assertThat(testServiceChange.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testServiceChange.getImsi()).isEqualTo(DEFAULT_IMSI);
        assertThat(testServiceChange.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testServiceChange.getServiceId()).isEqualTo(DEFAULT_SERVICE_ID);
    }

    @Test
    @Transactional
    public void createServiceChangeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = serviceChangeRepository.findAll().size();

        // Create the ServiceChange with an existing ID
        serviceChange.setId(1L);
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ServiceChange in the database
        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMsisdnIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceChangeRepository.findAll().size();
        // set the field null
        serviceChange.setMsisdn(null);

        // Create the ServiceChange, which fails.
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkImsiIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceChangeRepository.findAll().size();
        // set the field null
        serviceChange.setImsi(null);

        // Create the ServiceChange, which fails.
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOperationIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceChangeRepository.findAll().size();
        // set the field null
        serviceChange.setOperation(null);

        // Create the ServiceChange, which fails.
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkServiceIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceChangeRepository.findAll().size();
        // set the field null
        serviceChange.setServiceId(null);

        // Create the ServiceChange, which fails.
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        restServiceChangeMockMvc.perform(post("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllServiceChanges() throws Exception {
        // Initialize the database
        serviceChangeRepository.saveAndFlush(serviceChange);

        // Get all the serviceChangeList
        restServiceChangeMockMvc.perform(get("/api/service-changes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION.toString())))
            .andExpect(jsonPath("$.[*].serviceId").value(hasItem(DEFAULT_SERVICE_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getServiceChange() throws Exception {
        // Initialize the database
        serviceChangeRepository.saveAndFlush(serviceChange);

        // Get the serviceChange
        restServiceChangeMockMvc.perform(get("/api/service-changes/{id}", serviceChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(serviceChange.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION.toString()))
            .andExpect(jsonPath("$.serviceId").value(DEFAULT_SERVICE_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingServiceChange() throws Exception {
        // Get the serviceChange
        restServiceChangeMockMvc.perform(get("/api/service-changes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateServiceChange() throws Exception {
        // Initialize the database
        serviceChangeRepository.saveAndFlush(serviceChange);

        int databaseSizeBeforeUpdate = serviceChangeRepository.findAll().size();

        // Update the serviceChange
        ServiceChange updatedServiceChange = serviceChangeRepository.findById(serviceChange.getId()).get();
        // Disconnect from session so that the updates on updatedServiceChange are not directly saved in db
        em.detach(updatedServiceChange);
        updatedServiceChange
            .msisdn(UPDATED_MSISDN)
            .imsi(UPDATED_IMSI)
            .operation(UPDATED_OPERATION)
            .serviceId(UPDATED_SERVICE_ID);
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(updatedServiceChange);

        restServiceChangeMockMvc.perform(put("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isOk());

        // Validate the ServiceChange in the database
        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeUpdate);
        ServiceChange testServiceChange = serviceChangeList.get(serviceChangeList.size() - 1);
        assertThat(testServiceChange.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testServiceChange.getImsi()).isEqualTo(UPDATED_IMSI);
        assertThat(testServiceChange.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testServiceChange.getServiceId()).isEqualTo(UPDATED_SERVICE_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingServiceChange() throws Exception {
        int databaseSizeBeforeUpdate = serviceChangeRepository.findAll().size();

        // Create the ServiceChange
        ServiceChangeDTO serviceChangeDTO = serviceChangeMapper.toDto(serviceChange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServiceChangeMockMvc.perform(put("/api/service-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceChangeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ServiceChange in the database
        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteServiceChange() throws Exception {
        // Initialize the database
        serviceChangeRepository.saveAndFlush(serviceChange);

        int databaseSizeBeforeDelete = serviceChangeRepository.findAll().size();

        // Delete the serviceChange
        restServiceChangeMockMvc.perform(delete("/api/service-changes/{id}", serviceChange.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ServiceChange> serviceChangeList = serviceChangeRepository.findAll();
        assertThat(serviceChangeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceChange.class);
        ServiceChange serviceChange1 = new ServiceChange();
        serviceChange1.setId(1L);
        ServiceChange serviceChange2 = new ServiceChange();
        serviceChange2.setId(serviceChange1.getId());
        assertThat(serviceChange1).isEqualTo(serviceChange2);
        serviceChange2.setId(2L);
        assertThat(serviceChange1).isNotEqualTo(serviceChange2);
        serviceChange1.setId(null);
        assertThat(serviceChange1).isNotEqualTo(serviceChange2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceChangeDTO.class);
        ServiceChangeDTO serviceChangeDTO1 = new ServiceChangeDTO();
        serviceChangeDTO1.setId(1L);
        ServiceChangeDTO serviceChangeDTO2 = new ServiceChangeDTO();
        assertThat(serviceChangeDTO1).isNotEqualTo(serviceChangeDTO2);
        serviceChangeDTO2.setId(serviceChangeDTO1.getId());
        assertThat(serviceChangeDTO1).isEqualTo(serviceChangeDTO2);
        serviceChangeDTO2.setId(2L);
        assertThat(serviceChangeDTO1).isNotEqualTo(serviceChangeDTO2);
        serviceChangeDTO1.setId(null);
        assertThat(serviceChangeDTO1).isNotEqualTo(serviceChangeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(serviceChangeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(serviceChangeMapper.fromId(null)).isNull();
    }
}
