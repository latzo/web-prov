package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.Cos;
import sn.sentel.tigocare.repository.CosRepository;
import sn.sentel.tigocare.service.CosService;
import sn.sentel.tigocare.service.dto.CosDTO;
import sn.sentel.tigocare.service.mapper.CosMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CosResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class CosResourceIT {

    private static final String DEFAULT_CLASS_ID_CBS = "AAAAAAAAAA";
    private static final String UPDATED_CLASS_ID_CBS = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    @Autowired
    private CosRepository cosRepository;

    @Autowired
    private CosMapper cosMapper;

    @Autowired
    private CosService cosService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCosMockMvc;

    private Cos cos;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CosResource cosResource = new CosResource(cosService);
        this.restCosMockMvc = MockMvcBuilders.standaloneSetup(cosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cos createEntity(EntityManager em) {
        Cos cos = new Cos()
            .classIdCbs(DEFAULT_CLASS_ID_CBS)
            .label(DEFAULT_LABEL);
        return cos;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cos createUpdatedEntity(EntityManager em) {
        Cos cos = new Cos()
            .classIdCbs(UPDATED_CLASS_ID_CBS)
            .label(UPDATED_LABEL);
        return cos;
    }

    @BeforeEach
    public void initTest() {
        cos = createEntity(em);
    }

    @Test
    @Transactional
    public void createCos() throws Exception {
        int databaseSizeBeforeCreate = cosRepository.findAll().size();

        // Create the Cos
        CosDTO cosDTO = cosMapper.toDto(cos);
        restCosMockMvc.perform(post("/api/cos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cosDTO)))
            .andExpect(status().isCreated());

        // Validate the Cos in the database
        List<Cos> cosList = cosRepository.findAll();
        assertThat(cosList).hasSize(databaseSizeBeforeCreate + 1);
        Cos testCos = cosList.get(cosList.size() - 1);
        assertThat(testCos.getClassIdCbs()).isEqualTo(DEFAULT_CLASS_ID_CBS);
        assertThat(testCos.getLabel()).isEqualTo(DEFAULT_LABEL);
    }

    @Test
    @Transactional
    public void createCosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cosRepository.findAll().size();

        // Create the Cos with an existing ID
        cos.setId(1L);
        CosDTO cosDTO = cosMapper.toDto(cos);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCosMockMvc.perform(post("/api/cos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cos in the database
        List<Cos> cosList = cosRepository.findAll();
        assertThat(cosList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCos() throws Exception {
        // Initialize the database
        cosRepository.saveAndFlush(cos);

        // Get all the cosList
        restCosMockMvc.perform(get("/api/cos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cos.getId().intValue())))
            .andExpect(jsonPath("$.[*].classIdCbs").value(hasItem(DEFAULT_CLASS_ID_CBS.toString())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())));
    }
    
    @Test
    @Transactional
    public void getCos() throws Exception {
        // Initialize the database
        cosRepository.saveAndFlush(cos);

        // Get the cos
        restCosMockMvc.perform(get("/api/cos/{id}", cos.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cos.getId().intValue()))
            .andExpect(jsonPath("$.classIdCbs").value(DEFAULT_CLASS_ID_CBS.toString()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCos() throws Exception {
        // Get the cos
        restCosMockMvc.perform(get("/api/cos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCos() throws Exception {
        // Initialize the database
        cosRepository.saveAndFlush(cos);

        int databaseSizeBeforeUpdate = cosRepository.findAll().size();

        // Update the cos
        Cos updatedCos = cosRepository.findById(cos.getId()).get();
        // Disconnect from session so that the updates on updatedCos are not directly saved in db
        em.detach(updatedCos);
        updatedCos
            .classIdCbs(UPDATED_CLASS_ID_CBS)
            .label(UPDATED_LABEL);
        CosDTO cosDTO = cosMapper.toDto(updatedCos);

        restCosMockMvc.perform(put("/api/cos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cosDTO)))
            .andExpect(status().isOk());

        // Validate the Cos in the database
        List<Cos> cosList = cosRepository.findAll();
        assertThat(cosList).hasSize(databaseSizeBeforeUpdate);
        Cos testCos = cosList.get(cosList.size() - 1);
        assertThat(testCos.getClassIdCbs()).isEqualTo(UPDATED_CLASS_ID_CBS);
        assertThat(testCos.getLabel()).isEqualTo(UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingCos() throws Exception {
        int databaseSizeBeforeUpdate = cosRepository.findAll().size();

        // Create the Cos
        CosDTO cosDTO = cosMapper.toDto(cos);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCosMockMvc.perform(put("/api/cos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cosDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cos in the database
        List<Cos> cosList = cosRepository.findAll();
        assertThat(cosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCos() throws Exception {
        // Initialize the database
        cosRepository.saveAndFlush(cos);

        int databaseSizeBeforeDelete = cosRepository.findAll().size();

        // Delete the cos
        restCosMockMvc.perform(delete("/api/cos/{id}", cos.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cos> cosList = cosRepository.findAll();
        assertThat(cosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cos.class);
        Cos cos1 = new Cos();
        cos1.setId(1L);
        Cos cos2 = new Cos();
        cos2.setId(cos1.getId());
        assertThat(cos1).isEqualTo(cos2);
        cos2.setId(2L);
        assertThat(cos1).isNotEqualTo(cos2);
        cos1.setId(null);
        assertThat(cos1).isNotEqualTo(cos2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CosDTO.class);
        CosDTO cosDTO1 = new CosDTO();
        cosDTO1.setId(1L);
        CosDTO cosDTO2 = new CosDTO();
        assertThat(cosDTO1).isNotEqualTo(cosDTO2);
        cosDTO2.setId(cosDTO1.getId());
        assertThat(cosDTO1).isEqualTo(cosDTO2);
        cosDTO2.setId(2L);
        assertThat(cosDTO1).isNotEqualTo(cosDTO2);
        cosDTO1.setId(null);
        assertThat(cosDTO1).isNotEqualTo(cosDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cosMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cosMapper.fromId(null)).isNull();
    }
}
