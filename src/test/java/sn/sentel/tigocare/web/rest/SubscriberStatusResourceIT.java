package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.SubscriberStatus;
import sn.sentel.tigocare.repository.SubscriberStatusRepository;
import sn.sentel.tigocare.service.SubscriberStatusService;
import sn.sentel.tigocare.service.dto.SubscriberStatusDTO;
import sn.sentel.tigocare.service.mapper.SubscriberStatusMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SubscriberStatusResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class SubscriberStatusResourceIT {

    private static final String DEFAULT_STATUS_ID = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    @Autowired
    private SubscriberStatusRepository subscriberStatusRepository;

    @Autowired
    private SubscriberStatusMapper subscriberStatusMapper;

    @Autowired
    private SubscriberStatusService subscriberStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSubscriberStatusMockMvc;

    private SubscriberStatus subscriberStatus;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubscriberStatusResource subscriberStatusResource = new SubscriberStatusResource(subscriberStatusService);
        this.restSubscriberStatusMockMvc = MockMvcBuilders.standaloneSetup(subscriberStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubscriberStatus createEntity(EntityManager em) {
        SubscriberStatus subscriberStatus = new SubscriberStatus()
            .statusId(DEFAULT_STATUS_ID)
            .label(DEFAULT_LABEL);
        return subscriberStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubscriberStatus createUpdatedEntity(EntityManager em) {
        SubscriberStatus subscriberStatus = new SubscriberStatus()
            .statusId(UPDATED_STATUS_ID)
            .label(UPDATED_LABEL);
        return subscriberStatus;
    }

    @BeforeEach
    public void initTest() {
        subscriberStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubscriberStatus() throws Exception {
        int databaseSizeBeforeCreate = subscriberStatusRepository.findAll().size();

        // Create the SubscriberStatus
        SubscriberStatusDTO subscriberStatusDTO = subscriberStatusMapper.toDto(subscriberStatus);
        restSubscriberStatusMockMvc.perform(post("/api/subscriber-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subscriberStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the SubscriberStatus in the database
        List<SubscriberStatus> subscriberStatusList = subscriberStatusRepository.findAll();
        assertThat(subscriberStatusList).hasSize(databaseSizeBeforeCreate + 1);
        SubscriberStatus testSubscriberStatus = subscriberStatusList.get(subscriberStatusList.size() - 1);
        assertThat(testSubscriberStatus.getStatusId()).isEqualTo(DEFAULT_STATUS_ID);
        assertThat(testSubscriberStatus.getLabel()).isEqualTo(DEFAULT_LABEL);
    }

    @Test
    @Transactional
    public void createSubscriberStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subscriberStatusRepository.findAll().size();

        // Create the SubscriberStatus with an existing ID
        subscriberStatus.setId(1L);
        SubscriberStatusDTO subscriberStatusDTO = subscriberStatusMapper.toDto(subscriberStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubscriberStatusMockMvc.perform(post("/api/subscriber-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subscriberStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubscriberStatus in the database
        List<SubscriberStatus> subscriberStatusList = subscriberStatusRepository.findAll();
        assertThat(subscriberStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSubscriberStatuses() throws Exception {
        // Initialize the database
        subscriberStatusRepository.saveAndFlush(subscriberStatus);

        // Get all the subscriberStatusList
        restSubscriberStatusMockMvc.perform(get("/api/subscriber-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subscriberStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].statusId").value(hasItem(DEFAULT_STATUS_ID.toString())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())));
    }
    
    @Test
    @Transactional
    public void getSubscriberStatus() throws Exception {
        // Initialize the database
        subscriberStatusRepository.saveAndFlush(subscriberStatus);

        // Get the subscriberStatus
        restSubscriberStatusMockMvc.perform(get("/api/subscriber-statuses/{id}", subscriberStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subscriberStatus.getId().intValue()))
            .andExpect(jsonPath("$.statusId").value(DEFAULT_STATUS_ID.toString()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubscriberStatus() throws Exception {
        // Get the subscriberStatus
        restSubscriberStatusMockMvc.perform(get("/api/subscriber-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubscriberStatus() throws Exception {
        // Initialize the database
        subscriberStatusRepository.saveAndFlush(subscriberStatus);

        int databaseSizeBeforeUpdate = subscriberStatusRepository.findAll().size();

        // Update the subscriberStatus
        SubscriberStatus updatedSubscriberStatus = subscriberStatusRepository.findById(subscriberStatus.getId()).get();
        // Disconnect from session so that the updates on updatedSubscriberStatus are not directly saved in db
        em.detach(updatedSubscriberStatus);
        updatedSubscriberStatus
            .statusId(UPDATED_STATUS_ID)
            .label(UPDATED_LABEL);
        SubscriberStatusDTO subscriberStatusDTO = subscriberStatusMapper.toDto(updatedSubscriberStatus);

        restSubscriberStatusMockMvc.perform(put("/api/subscriber-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subscriberStatusDTO)))
            .andExpect(status().isOk());

        // Validate the SubscriberStatus in the database
        List<SubscriberStatus> subscriberStatusList = subscriberStatusRepository.findAll();
        assertThat(subscriberStatusList).hasSize(databaseSizeBeforeUpdate);
        SubscriberStatus testSubscriberStatus = subscriberStatusList.get(subscriberStatusList.size() - 1);
        assertThat(testSubscriberStatus.getStatusId()).isEqualTo(UPDATED_STATUS_ID);
        assertThat(testSubscriberStatus.getLabel()).isEqualTo(UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingSubscriberStatus() throws Exception {
        int databaseSizeBeforeUpdate = subscriberStatusRepository.findAll().size();

        // Create the SubscriberStatus
        SubscriberStatusDTO subscriberStatusDTO = subscriberStatusMapper.toDto(subscriberStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubscriberStatusMockMvc.perform(put("/api/subscriber-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subscriberStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubscriberStatus in the database
        List<SubscriberStatus> subscriberStatusList = subscriberStatusRepository.findAll();
        assertThat(subscriberStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSubscriberStatus() throws Exception {
        // Initialize the database
        subscriberStatusRepository.saveAndFlush(subscriberStatus);

        int databaseSizeBeforeDelete = subscriberStatusRepository.findAll().size();

        // Delete the subscriberStatus
        restSubscriberStatusMockMvc.perform(delete("/api/subscriber-statuses/{id}", subscriberStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubscriberStatus> subscriberStatusList = subscriberStatusRepository.findAll();
        assertThat(subscriberStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubscriberStatus.class);
        SubscriberStatus subscriberStatus1 = new SubscriberStatus();
        subscriberStatus1.setId(1L);
        SubscriberStatus subscriberStatus2 = new SubscriberStatus();
        subscriberStatus2.setId(subscriberStatus1.getId());
        assertThat(subscriberStatus1).isEqualTo(subscriberStatus2);
        subscriberStatus2.setId(2L);
        assertThat(subscriberStatus1).isNotEqualTo(subscriberStatus2);
        subscriberStatus1.setId(null);
        assertThat(subscriberStatus1).isNotEqualTo(subscriberStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubscriberStatusDTO.class);
        SubscriberStatusDTO subscriberStatusDTO1 = new SubscriberStatusDTO();
        subscriberStatusDTO1.setId(1L);
        SubscriberStatusDTO subscriberStatusDTO2 = new SubscriberStatusDTO();
        assertThat(subscriberStatusDTO1).isNotEqualTo(subscriberStatusDTO2);
        subscriberStatusDTO2.setId(subscriberStatusDTO1.getId());
        assertThat(subscriberStatusDTO1).isEqualTo(subscriberStatusDTO2);
        subscriberStatusDTO2.setId(2L);
        assertThat(subscriberStatusDTO1).isNotEqualTo(subscriberStatusDTO2);
        subscriberStatusDTO1.setId(null);
        assertThat(subscriberStatusDTO1).isNotEqualTo(subscriberStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subscriberStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subscriberStatusMapper.fromId(null)).isNull();
    }
}
