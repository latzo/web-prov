//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.MnpPortout;
//import sn.sentel.tigocare.repository.MnpPortoutRepository;
//import sn.sentel.tigocare.service.MnpPortoutService;
//import sn.sentel.tigocare.service.dto.MnpPortoutDTO;
//import sn.sentel.tigocare.service.mapper.MnpPortoutMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@Link MnpPortoutResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class MnpPortoutResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_OPERATEUR = "AAAAAAAAAA";
//    private static final String UPDATED_OPERATEUR = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    @Autowired
//    private MnpPortoutRepository mnpPortoutRepository;
//
//    @Autowired
//    private MnpPortoutMapper mnpPortoutMapper;
//
//    @Autowired
//    private MnpPortoutService mnpPortoutService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restMnpPortoutMockMvc;
//
//    private MnpPortout mnpPortout;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final MnpPortoutResource mnpPortoutResource = new MnpPortoutResource(mnpPortoutService);
//        this.restMnpPortoutMockMvc = MockMvcBuilders.standaloneSetup(mnpPortoutResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static MnpPortout createEntity(EntityManager em) {
//        MnpPortout mnpPortout = new MnpPortout()
//            .msisdn(DEFAULT_MSISDN)
//            .operateur(DEFAULT_OPERATEUR)
//            .processingStatus(DEFAULT_PROCESSING_STATUS);
//        return mnpPortout;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static MnpPortout createUpdatedEntity(EntityManager em) {
//        MnpPortout mnpPortout = new MnpPortout()
//            .msisdn(UPDATED_MSISDN)
//            .operateur(UPDATED_OPERATEUR)
//            .processingStatus(UPDATED_PROCESSING_STATUS);
//        return mnpPortout;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        mnpPortout = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createMnpPortout() throws Exception {
//        int databaseSizeBeforeCreate = mnpPortoutRepository.findAll().size();
//
//        // Create the MnpPortout
//        MnpPortoutDTO mnpPortoutDTO = mnpPortoutMapper.toDto(mnpPortout);
//        restMnpPortoutMockMvc.perform(post("/api/mnp-portouts")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortoutDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the MnpPortout in the database
//        List<MnpPortout> mnpPortoutList = mnpPortoutRepository.findAll();
//        assertThat(mnpPortoutList).hasSize(databaseSizeBeforeCreate + 1);
//        MnpPortout testMnpPortout = mnpPortoutList.get(mnpPortoutList.size() - 1);
//        assertThat(testMnpPortout.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testMnpPortout.getOperateur()).isEqualTo(DEFAULT_OPERATEUR);
//        assertThat(testMnpPortout.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//    }
//
//    @Test
//    @Transactional
//    public void createMnpPortoutWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = mnpPortoutRepository.findAll().size();
//
//        // Create the MnpPortout with an existing ID
//        mnpPortout.setId(1L);
//        MnpPortoutDTO mnpPortoutDTO = mnpPortoutMapper.toDto(mnpPortout);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restMnpPortoutMockMvc.perform(post("/api/mnp-portouts")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortoutDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the MnpPortout in the database
//        List<MnpPortout> mnpPortoutList = mnpPortoutRepository.findAll();
//        assertThat(mnpPortoutList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllMnpPortouts() throws Exception {
//        // Initialize the database
//        mnpPortoutRepository.saveAndFlush(mnpPortout);
//
//        // Get all the mnpPortoutList
//        restMnpPortoutMockMvc.perform(get("/api/mnp-portouts?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(mnpPortout.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].operateur").value(hasItem(DEFAULT_OPERATEUR.toString())))
//            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getMnpPortout() throws Exception {
//        // Initialize the database
//        mnpPortoutRepository.saveAndFlush(mnpPortout);
//
//        // Get the mnpPortout
//        restMnpPortoutMockMvc.perform(get("/api/mnp-portouts/{id}", mnpPortout.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(mnpPortout.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.operateur").value(DEFAULT_OPERATEUR.toString()))
//            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingMnpPortout() throws Exception {
//        // Get the mnpPortout
//        restMnpPortoutMockMvc.perform(get("/api/mnp-portouts/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateMnpPortout() throws Exception {
//        // Initialize the database
//        mnpPortoutRepository.saveAndFlush(mnpPortout);
//
//        int databaseSizeBeforeUpdate = mnpPortoutRepository.findAll().size();
//
//        // Update the mnpPortout
//        MnpPortout updatedMnpPortout = mnpPortoutRepository.findById(mnpPortout.getId()).get();
//        // Disconnect from session so that the updates on updatedMnpPortout are not directly saved in db
//        em.detach(updatedMnpPortout);
//        updatedMnpPortout
//            .msisdn(UPDATED_MSISDN)
//            .operateur(UPDATED_OPERATEUR)
//            .processingStatus(UPDATED_PROCESSING_STATUS);
//        MnpPortoutDTO mnpPortoutDTO = mnpPortoutMapper.toDto(updatedMnpPortout);
//
//        restMnpPortoutMockMvc.perform(put("/api/mnp-portouts")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortoutDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the MnpPortout in the database
//        List<MnpPortout> mnpPortoutList = mnpPortoutRepository.findAll();
//        assertThat(mnpPortoutList).hasSize(databaseSizeBeforeUpdate);
//        MnpPortout testMnpPortout = mnpPortoutList.get(mnpPortoutList.size() - 1);
//        assertThat(testMnpPortout.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testMnpPortout.getOperateur()).isEqualTo(UPDATED_OPERATEUR);
//        assertThat(testMnpPortout.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingMnpPortout() throws Exception {
//        int databaseSizeBeforeUpdate = mnpPortoutRepository.findAll().size();
//
//        // Create the MnpPortout
//        MnpPortoutDTO mnpPortoutDTO = mnpPortoutMapper.toDto(mnpPortout);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restMnpPortoutMockMvc.perform(put("/api/mnp-portouts")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortoutDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the MnpPortout in the database
//        List<MnpPortout> mnpPortoutList = mnpPortoutRepository.findAll();
//        assertThat(mnpPortoutList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteMnpPortout() throws Exception {
//        // Initialize the database
//        mnpPortoutRepository.saveAndFlush(mnpPortout);
//
//        int databaseSizeBeforeDelete = mnpPortoutRepository.findAll().size();
//
//        // Delete the mnpPortout
//        restMnpPortoutMockMvc.perform(delete("/api/mnp-portouts/{id}", mnpPortout.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<MnpPortout> mnpPortoutList = mnpPortoutRepository.findAll();
//        assertThat(mnpPortoutList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(MnpPortout.class);
//        MnpPortout mnpPortout1 = new MnpPortout();
//        mnpPortout1.setId(1L);
//        MnpPortout mnpPortout2 = new MnpPortout();
//        mnpPortout2.setId(mnpPortout1.getId());
//        assertThat(mnpPortout1).isEqualTo(mnpPortout2);
//        mnpPortout2.setId(2L);
//        assertThat(mnpPortout1).isNotEqualTo(mnpPortout2);
//        mnpPortout1.setId(null);
//        assertThat(mnpPortout1).isNotEqualTo(mnpPortout2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(MnpPortoutDTO.class);
//        MnpPortoutDTO mnpPortoutDTO1 = new MnpPortoutDTO();
//        mnpPortoutDTO1.setId(1L);
//        MnpPortoutDTO mnpPortoutDTO2 = new MnpPortoutDTO();
//        assertThat(mnpPortoutDTO1).isNotEqualTo(mnpPortoutDTO2);
//        mnpPortoutDTO2.setId(mnpPortoutDTO1.getId());
//        assertThat(mnpPortoutDTO1).isEqualTo(mnpPortoutDTO2);
//        mnpPortoutDTO2.setId(2L);
//        assertThat(mnpPortoutDTO1).isNotEqualTo(mnpPortoutDTO2);
//        mnpPortoutDTO1.setId(null);
//        assertThat(mnpPortoutDTO1).isNotEqualTo(mnpPortoutDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(mnpPortoutMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(mnpPortoutMapper.fromId(null)).isNull();
//    }
//}
