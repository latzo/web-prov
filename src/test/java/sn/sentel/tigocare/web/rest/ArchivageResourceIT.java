package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.Archivage;
import sn.sentel.tigocare.hlr.services.msa.ArchivageService;
import sn.sentel.tigocare.repository.ArchivageRepository;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ArchivageResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class ArchivageResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    @Autowired
    private ArchivageRepository archivageRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restArchivageMockMvc;

    private Archivage archivage;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ArchivageResource archivageResource = new ArchivageResource((ArchivageService) archivageRepository);
        this.restArchivageMockMvc = MockMvcBuilders.standaloneSetup(archivageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Archivage createEntity(EntityManager em) {
        Archivage archivage = new Archivage()
            .msisdn(DEFAULT_MSISDN);
        return archivage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Archivage createUpdatedEntity(EntityManager em) {
        Archivage archivage = new Archivage()
            .msisdn(UPDATED_MSISDN);
        return archivage;
    }

    @BeforeEach
    public void initTest() {
        archivage = createEntity(em);
    }

    @Test
    @Transactional
    public void createArchivage() throws Exception {
        int databaseSizeBeforeCreate = archivageRepository.findAll().size();

        // Create the Archivage
        restArchivageMockMvc.perform(post("/api/archivages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(archivage)))
            .andExpect(status().isCreated());

        // Validate the Archivage in the database
        List<Archivage> archivageList = archivageRepository.findAll();
        assertThat(archivageList).hasSize(databaseSizeBeforeCreate + 1);
        Archivage testArchivage = archivageList.get(archivageList.size() - 1);
        assertThat(testArchivage.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
    }

    @Test
    @Transactional
    public void createArchivageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = archivageRepository.findAll().size();

        // Create the Archivage with an existing ID
        archivage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArchivageMockMvc.perform(post("/api/archivages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(archivage)))
            .andExpect(status().isBadRequest());

        // Validate the Archivage in the database
        List<Archivage> archivageList = archivageRepository.findAll();
        assertThat(archivageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllArchivages() throws Exception {
        // Initialize the database
        archivageRepository.saveAndFlush(archivage);

        // Get all the archivageList
        restArchivageMockMvc.perform(get("/api/archivages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(archivage.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())));
    }
    
    @Test
    @Transactional
    public void getArchivage() throws Exception {
        // Initialize the database
        archivageRepository.saveAndFlush(archivage);

        // Get the archivage
        restArchivageMockMvc.perform(get("/api/archivages/{id}", archivage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(archivage.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingArchivage() throws Exception {
        // Get the archivage
        restArchivageMockMvc.perform(get("/api/archivages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArchivage() throws Exception {
        // Initialize the database
        archivageRepository.saveAndFlush(archivage);

        int databaseSizeBeforeUpdate = archivageRepository.findAll().size();

        // Update the archivage
        Archivage updatedArchivage = archivageRepository.findById(archivage.getId()).get();
        // Disconnect from session so that the updates on updatedArchivage are not directly saved in db
        em.detach(updatedArchivage);
        updatedArchivage
            .msisdn(UPDATED_MSISDN);

        restArchivageMockMvc.perform(put("/api/archivages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedArchivage)))
            .andExpect(status().isOk());

        // Validate the Archivage in the database
        List<Archivage> archivageList = archivageRepository.findAll();
        assertThat(archivageList).hasSize(databaseSizeBeforeUpdate);
        Archivage testArchivage = archivageList.get(archivageList.size() - 1);
        assertThat(testArchivage.getMsisdn()).isEqualTo(UPDATED_MSISDN);
    }

    @Test
    @Transactional
    public void updateNonExistingArchivage() throws Exception {
        int databaseSizeBeforeUpdate = archivageRepository.findAll().size();

        // Create the Archivage

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArchivageMockMvc.perform(put("/api/archivages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(archivage)))
            .andExpect(status().isBadRequest());

        // Validate the Archivage in the database
        List<Archivage> archivageList = archivageRepository.findAll();
        assertThat(archivageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteArchivage() throws Exception {
        // Initialize the database
        archivageRepository.saveAndFlush(archivage);

        int databaseSizeBeforeDelete = archivageRepository.findAll().size();

        // Delete the archivage
        restArchivageMockMvc.perform(delete("/api/archivages/{id}", archivage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Archivage> archivageList = archivageRepository.findAll();
        assertThat(archivageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Archivage.class);
        Archivage archivage1 = new Archivage();
        archivage1.setId(1L);
        Archivage archivage2 = new Archivage();
        archivage2.setId(archivage1.getId());
        assertThat(archivage1).isEqualTo(archivage2);
        archivage2.setId(2L);
        assertThat(archivage1).isNotEqualTo(archivage2);
        archivage1.setId(null);
        assertThat(archivage1).isNotEqualTo(archivage2);
    }
}
