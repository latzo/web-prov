//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.SimSwap;
//import sn.sentel.tigocare.repository.SimSwapRepository;
//import sn.sentel.tigocare.service.SimSwapService;
//import sn.sentel.tigocare.service.dto.SimSwapDTO;
//import sn.sentel.tigocare.service.mapper.SimSwapMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@Link SimSwapResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class SimSwapResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_OLD_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_OLD_IMSI = "BBBBBBBBBB";
//
//    private static final String DEFAULT_NEW_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_NEW_IMSI = "BBBBBBBBBB";
//
//    @Autowired
//    private SimSwapRepository simSwapRepository;
//
//    @Autowired
//    private SimSwapMapper simSwapMapper;
//
//    @Autowired
//    private SimSwapService simSwapService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restSimSwapMockMvc;
//
//    private SimSwap simSwap;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final SimSwapResource simSwapResource = new SimSwapResource(simSwapService);
//        this.restSimSwapMockMvc = MockMvcBuilders.standaloneSetup(simSwapResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static SimSwap createEntity(EntityManager em) {
//        SimSwap simSwap = new SimSwap()
//            .msisdn(DEFAULT_MSISDN)
//            .oldIMSI(DEFAULT_OLD_IMSI)
//            .newIMSI(DEFAULT_NEW_IMSI);
//        return simSwap;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static SimSwap createUpdatedEntity(EntityManager em) {
//        SimSwap simSwap = new SimSwap()
//            .msisdn(UPDATED_MSISDN)
//            .oldIMSI(UPDATED_OLD_IMSI)
//            .newIMSI(UPDATED_NEW_IMSI);
//        return simSwap;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        simSwap = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createSimSwap() throws Exception {
//        int databaseSizeBeforeCreate = simSwapRepository.findAll().size();
//
//        // Create the SimSwap
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(simSwap);
//        restSimSwapMockMvc.perform(post("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the SimSwap in the database
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeCreate + 1);
//        SimSwap testSimSwap = simSwapList.get(simSwapList.size() - 1);
//        assertThat(testSimSwap.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testSimSwap.getOldIMSI()).isEqualTo(DEFAULT_OLD_IMSI);
//        assertThat(testSimSwap.getNewIMSI()).isEqualTo(DEFAULT_NEW_IMSI);
//    }
//
//    @Test
//    @Transactional
//    public void createSimSwapWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = simSwapRepository.findAll().size();
//
//        // Create the SimSwap with an existing ID
//        simSwap.setId(1L);
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(simSwap);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restSimSwapMockMvc.perform(post("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the SimSwap in the database
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkOldIMSIIsRequired() throws Exception {
//        int databaseSizeBeforeTest = simSwapRepository.findAll().size();
//        // set the field null
//        simSwap.setOldIMSI(null);
//
//        // Create the SimSwap, which fails.
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(simSwap);
//
//        restSimSwapMockMvc.perform(post("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkNewIMSIIsRequired() throws Exception {
//        int databaseSizeBeforeTest = simSwapRepository.findAll().size();
//        // set the field null
//        simSwap.setNewIMSI(null);
//
//        // Create the SimSwap, which fails.
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(simSwap);
//
//        restSimSwapMockMvc.perform(post("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllSimSwaps() throws Exception {
//        // Initialize the database
//        simSwapRepository.saveAndFlush(simSwap);
//
//        // Get all the simSwapList
//        restSimSwapMockMvc.perform(get("/api/sim-swaps?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(simSwap.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].oldIMSI").value(hasItem(DEFAULT_OLD_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].newIMSI").value(hasItem(DEFAULT_NEW_IMSI.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getSimSwap() throws Exception {
//        // Initialize the database
//        simSwapRepository.saveAndFlush(simSwap);
//
//        // Get the simSwap
//        restSimSwapMockMvc.perform(get("/api/sim-swaps/{id}", simSwap.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(simSwap.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.oldIMSI").value(DEFAULT_OLD_IMSI.toString()))
//            .andExpect(jsonPath("$.newIMSI").value(DEFAULT_NEW_IMSI.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingSimSwap() throws Exception {
//        // Get the simSwap
//        restSimSwapMockMvc.perform(get("/api/sim-swaps/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateSimSwap() throws Exception {
//        // Initialize the database
//        simSwapRepository.saveAndFlush(simSwap);
//
//        int databaseSizeBeforeUpdate = simSwapRepository.findAll().size();
//
//        // Update the simSwap
//        SimSwap updatedSimSwap = simSwapRepository.findById(simSwap.getId()).get();
//        // Disconnect from session so that the updates on updatedSimSwap are not directly saved in db
//        em.detach(updatedSimSwap);
//        updatedSimSwap
//            .msisdn(UPDATED_MSISDN)
//            .oldIMSI(UPDATED_OLD_IMSI)
//            .newIMSI(UPDATED_NEW_IMSI);
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(updatedSimSwap);
//
//        restSimSwapMockMvc.perform(put("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the SimSwap in the database
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeUpdate);
//        SimSwap testSimSwap = simSwapList.get(simSwapList.size() - 1);
//        assertThat(testSimSwap.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testSimSwap.getOldIMSI()).isEqualTo(UPDATED_OLD_IMSI);
//        assertThat(testSimSwap.getNewIMSI()).isEqualTo(UPDATED_NEW_IMSI);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingSimSwap() throws Exception {
//        int databaseSizeBeforeUpdate = simSwapRepository.findAll().size();
//
//        // Create the SimSwap
//        SimSwapDTO simSwapDTO = simSwapMapper.toDto(simSwap);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restSimSwapMockMvc.perform(put("/api/sim-swaps")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(simSwapDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the SimSwap in the database
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteSimSwap() throws Exception {
//        // Initialize the database
//        simSwapRepository.saveAndFlush(simSwap);
//
//        int databaseSizeBeforeDelete = simSwapRepository.findAll().size();
//
//        // Delete the simSwap
//        restSimSwapMockMvc.perform(delete("/api/sim-swaps/{id}", simSwap.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<SimSwap> simSwapList = simSwapRepository.findAll();
//        assertThat(simSwapList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(SimSwap.class);
//        SimSwap simSwap1 = new SimSwap();
//        simSwap1.setId(1L);
//        SimSwap simSwap2 = new SimSwap();
//        simSwap2.setId(simSwap1.getId());
//        assertThat(simSwap1).isEqualTo(simSwap2);
//        simSwap2.setId(2L);
//        assertThat(simSwap1).isNotEqualTo(simSwap2);
//        simSwap1.setId(null);
//        assertThat(simSwap1).isNotEqualTo(simSwap2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(SimSwapDTO.class);
//        SimSwapDTO simSwapDTO1 = new SimSwapDTO();
//        simSwapDTO1.setId(1L);
//        SimSwapDTO simSwapDTO2 = new SimSwapDTO();
//        assertThat(simSwapDTO1).isNotEqualTo(simSwapDTO2);
//        simSwapDTO2.setId(simSwapDTO1.getId());
//        assertThat(simSwapDTO1).isEqualTo(simSwapDTO2);
//        simSwapDTO2.setId(2L);
//        assertThat(simSwapDTO1).isNotEqualTo(simSwapDTO2);
//        simSwapDTO1.setId(null);
//        assertThat(simSwapDTO1).isNotEqualTo(simSwapDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(simSwapMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(simSwapMapper.fromId(null)).isNull();
//    }
//}
