//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.MnpPorting;
//import sn.sentel.tigocare.repository.MnpPortingRepository;
//import sn.sentel.tigocare.service.MnpPortingService;
//import sn.sentel.tigocare.service.dto.MnpPortingDTO;
//import sn.sentel.tigocare.service.mapper.MnpPortingMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@Link MnpPortingResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class MnpPortingResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    @Autowired
//    private MnpPortingRepository mnpPortingRepository;
//
//    @Autowired
//    private MnpPortingMapper mnpPortingMapper;
//
//    @Autowired
//    private MnpPortingService mnpPortingService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restMnpPortingMockMvc;
//
//    private MnpPorting mnpPorting;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final MnpPortingResource mnpPortingResource = new MnpPortingResource(mnpPortingService);
//        this.restMnpPortingMockMvc = MockMvcBuilders.standaloneSetup(mnpPortingResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static MnpPorting createEntity(EntityManager em) {
//        MnpPorting mnpPorting = new MnpPorting()
//            .msisdn(DEFAULT_MSISDN)
//            .processingStatus(DEFAULT_PROCESSING_STATUS);
//        return mnpPorting;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static MnpPorting createUpdatedEntity(EntityManager em) {
//        MnpPorting mnpPorting = new MnpPorting()
//            .msisdn(UPDATED_MSISDN)
//            .processingStatus(UPDATED_PROCESSING_STATUS);
//        return mnpPorting;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        mnpPorting = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createMnpPorting() throws Exception {
//        int databaseSizeBeforeCreate = mnpPortingRepository.findAll().size();
//
//        // Create the MnpPorting
//        MnpPortingDTO mnpPortingDTO = mnpPortingMapper.toDto(mnpPorting);
//        restMnpPortingMockMvc.perform(post("/api/mnp-portings")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortingDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the MnpPorting in the database
//        List<MnpPorting> mnpPortingList = mnpPortingRepository.findAll();
//        assertThat(mnpPortingList).hasSize(databaseSizeBeforeCreate + 1);
//        MnpPorting testMnpPorting = mnpPortingList.get(mnpPortingList.size() - 1);
//        assertThat(testMnpPorting.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testMnpPorting.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//    }
//
//    @Test
//    @Transactional
//    public void createMnpPortingWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = mnpPortingRepository.findAll().size();
//
//        // Create the MnpPorting with an existing ID
//        mnpPorting.setId(1L);
//        MnpPortingDTO mnpPortingDTO = mnpPortingMapper.toDto(mnpPorting);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restMnpPortingMockMvc.perform(post("/api/mnp-portings")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortingDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the MnpPorting in the database
//        List<MnpPorting> mnpPortingList = mnpPortingRepository.findAll();
//        assertThat(mnpPortingList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllMnpPortings() throws Exception {
//        // Initialize the database
//        mnpPortingRepository.saveAndFlush(mnpPorting);
//
//        // Get all the mnpPortingList
//        restMnpPortingMockMvc.perform(get("/api/mnp-portings?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(mnpPorting.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getMnpPorting() throws Exception {
//        // Initialize the database
//        mnpPortingRepository.saveAndFlush(mnpPorting);
//
//        // Get the mnpPorting
//        restMnpPortingMockMvc.perform(get("/api/mnp-portings/{id}", mnpPorting.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(mnpPorting.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingMnpPorting() throws Exception {
//        // Get the mnpPorting
//        restMnpPortingMockMvc.perform(get("/api/mnp-portings/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateMnpPorting() throws Exception {
//        // Initialize the database
//        mnpPortingRepository.saveAndFlush(mnpPorting);
//
//        int databaseSizeBeforeUpdate = mnpPortingRepository.findAll().size();
//
//        // Update the mnpPorting
//        MnpPorting updatedMnpPorting = mnpPortingRepository.findById(mnpPorting.getId()).get();
//        // Disconnect from session so that the updates on updatedMnpPorting are not directly saved in db
//        em.detach(updatedMnpPorting);
//        updatedMnpPorting
//            .msisdn(UPDATED_MSISDN)
//            .processingStatus(UPDATED_PROCESSING_STATUS);
//        MnpPortingDTO mnpPortingDTO = mnpPortingMapper.toDto(updatedMnpPorting);
//
//        restMnpPortingMockMvc.perform(put("/api/mnp-portings")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortingDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the MnpPorting in the database
//        List<MnpPorting> mnpPortingList = mnpPortingRepository.findAll();
//        assertThat(mnpPortingList).hasSize(databaseSizeBeforeUpdate);
//        MnpPorting testMnpPorting = mnpPortingList.get(mnpPortingList.size() - 1);
//        assertThat(testMnpPorting.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testMnpPorting.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingMnpPorting() throws Exception {
//        int databaseSizeBeforeUpdate = mnpPortingRepository.findAll().size();
//
//        // Create the MnpPorting
//        MnpPortingDTO mnpPortingDTO = mnpPortingMapper.toDto(mnpPorting);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restMnpPortingMockMvc.perform(put("/api/mnp-portings")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(mnpPortingDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the MnpPorting in the database
//        List<MnpPorting> mnpPortingList = mnpPortingRepository.findAll();
//        assertThat(mnpPortingList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteMnpPorting() throws Exception {
//        // Initialize the database
//        mnpPortingRepository.saveAndFlush(mnpPorting);
//
//        int databaseSizeBeforeDelete = mnpPortingRepository.findAll().size();
//
//        // Delete the mnpPorting
//        restMnpPortingMockMvc.perform(delete("/api/mnp-portings/{id}", mnpPorting.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<MnpPorting> mnpPortingList = mnpPortingRepository.findAll();
//        assertThat(mnpPortingList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(MnpPorting.class);
//        MnpPorting mnpPorting1 = new MnpPorting();
//        mnpPorting1.setId(1L);
//        MnpPorting mnpPorting2 = new MnpPorting();
//        mnpPorting2.setId(mnpPorting1.getId());
//        assertThat(mnpPorting1).isEqualTo(mnpPorting2);
//        mnpPorting2.setId(2L);
//        assertThat(mnpPorting1).isNotEqualTo(mnpPorting2);
//        mnpPorting1.setId(null);
//        assertThat(mnpPorting1).isNotEqualTo(mnpPorting2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(MnpPortingDTO.class);
//        MnpPortingDTO mnpPortingDTO1 = new MnpPortingDTO();
//        mnpPortingDTO1.setId(1L);
//        MnpPortingDTO mnpPortingDTO2 = new MnpPortingDTO();
//        assertThat(mnpPortingDTO1).isNotEqualTo(mnpPortingDTO2);
//        mnpPortingDTO2.setId(mnpPortingDTO1.getId());
//        assertThat(mnpPortingDTO1).isEqualTo(mnpPortingDTO2);
//        mnpPortingDTO2.setId(2L);
//        assertThat(mnpPortingDTO1).isNotEqualTo(mnpPortingDTO2);
//        mnpPortingDTO1.setId(null);
//        assertThat(mnpPortingDTO1).isNotEqualTo(mnpPortingDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(mnpPortingMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(mnpPortingMapper.fromId(null)).isNull();
//    }
//}
