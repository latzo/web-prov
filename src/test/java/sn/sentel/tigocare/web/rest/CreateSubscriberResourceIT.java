//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.CreateSubscriber;
//import sn.sentel.tigocare.repository.CreateSubscriberRepository;
//import sn.sentel.tigocare.service.CreateSubscriberService;
//import sn.sentel.tigocare.service.dto.CreateSubscriberDTO;
//import sn.sentel.tigocare.service.mapper.CreateSubscriberMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.sentel.tigocare.domain.enumeration.SIMCreationType;
///**
// * Integration tests for the {@Link CreateSubscriberResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class CreateSubscriberResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_IMSI = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_STATUT = "AAAAAAAAAA";
//    private static final String UPDATED_STATUT = "BBBBBBBBBB";
//
//    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
//    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_BACKEND_RESP = "AAAAAAAAAA";
//    private static final String UPDATED_BACKEND_RESP = "BBBBBBBBBB";
//
//    private static final String DEFAULT_COS = "AAAAAAAAAA";
//    private static final String UPDATED_COS = "BBBBBBBBBB";
//
//    private static final SIMCreationType DEFAULT_CREATION_TYPE = SIMCreationType.IN;
//    private static final SIMCreationType UPDATED_CREATION_TYPE = SIMCreationType.SWITCH;
//
//    @Autowired
//    private CreateSubscriberRepository createSubscriberRepository;
//
//    @Autowired
//    private CreateSubscriberMapper createSubscriberMapper;
//
//    @Autowired
//    private CreateSubscriberService createSubscriberService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restCreateSubscriberMockMvc;
//
//    private CreateSubscriber createSubscriber;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final CreateSubscriberResource createSubscriberResource = new CreateSubscriberResource(createSubscriberService);
//        this.restCreateSubscriberMockMvc = MockMvcBuilders.standaloneSetup(createSubscriberResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static CreateSubscriber createEntity(EntityManager em) {
//        CreateSubscriber createSubscriber = new CreateSubscriber()
//            .msisdn(DEFAULT_MSISDN)
//            .imsi(DEFAULT_IMSI)
//            .processingStatus(DEFAULT_PROCESSING_STATUS)
//            .statut(DEFAULT_STATUT)
//            .transactionId(DEFAULT_TRANSACTION_ID)
//            .backendResp(DEFAULT_BACKEND_RESP)
//            .cos(DEFAULT_COS)
//            .creationType(DEFAULT_CREATION_TYPE);
//        return createSubscriber;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static CreateSubscriber createUpdatedEntity(EntityManager em) {
//        CreateSubscriber createSubscriber = new CreateSubscriber()
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .statut(UPDATED_STATUT)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .backendResp(UPDATED_BACKEND_RESP)
//            .cos(UPDATED_COS)
//            .creationType(UPDATED_CREATION_TYPE);
//        return createSubscriber;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        createSubscriber = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createCreateSubscriber() throws Exception {
//        int databaseSizeBeforeCreate = createSubscriberRepository.findAll().size();
//
//        // Create the CreateSubscriber
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(createSubscriber);
//        restCreateSubscriberMockMvc.perform(post("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the CreateSubscriber in the database
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeCreate + 1);
//        CreateSubscriber testCreateSubscriber = createSubscriberList.get(createSubscriberList.size() - 1);
//        assertThat(testCreateSubscriber.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testCreateSubscriber.getImsi()).isEqualTo(DEFAULT_IMSI);
//        assertThat(testCreateSubscriber.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//        assertThat(testCreateSubscriber.getStatut()).isEqualTo(DEFAULT_STATUT);
//        assertThat(testCreateSubscriber.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
//        assertThat(testCreateSubscriber.getBackendResp()).isEqualTo(DEFAULT_BACKEND_RESP);
//        assertThat(testCreateSubscriber.getCos()).isEqualTo(DEFAULT_COS);
//        assertThat(testCreateSubscriber.getCreationType()).isEqualTo(DEFAULT_CREATION_TYPE);
//    }
//
//    @Test
//    @Transactional
//    public void createCreateSubscriberWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = createSubscriberRepository.findAll().size();
//
//        // Create the CreateSubscriber with an existing ID
//        createSubscriber.setId(1L);
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(createSubscriber);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restCreateSubscriberMockMvc.perform(post("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the CreateSubscriber in the database
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = createSubscriberRepository.findAll().size();
//        // set the field null
//        createSubscriber.setMsisdn(null);
//
//        // Create the CreateSubscriber, which fails.
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(createSubscriber);
//
//        restCreateSubscriberMockMvc.perform(post("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkImsiIsRequired() throws Exception {
//        int databaseSizeBeforeTest = createSubscriberRepository.findAll().size();
//        // set the field null
//        createSubscriber.setImsi(null);
//
//        // Create the CreateSubscriber, which fails.
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(createSubscriber);
//
//        restCreateSubscriberMockMvc.perform(post("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllCreateSubscribers() throws Exception {
//        // Initialize the database
//        createSubscriberRepository.saveAndFlush(createSubscriber);
//
//        // Get all the createSubscriberList
//        restCreateSubscriberMockMvc.perform(get("/api/create-subscribers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(createSubscriber.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
//            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
//            .andExpect(jsonPath("$.[*].backendResp").value(hasItem(DEFAULT_BACKEND_RESP.toString())))
//            .andExpect(jsonPath("$.[*].cos").value(hasItem(DEFAULT_COS.toString())))
//            .andExpect(jsonPath("$.[*].creationType").value(hasItem(DEFAULT_CREATION_TYPE.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getCreateSubscriber() throws Exception {
//        // Initialize the database
//        createSubscriberRepository.saveAndFlush(createSubscriber);
//
//        // Get the createSubscriber
//        restCreateSubscriberMockMvc.perform(get("/api/create-subscribers/{id}", createSubscriber.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(createSubscriber.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
//            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()))
//            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()))
//            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
//            .andExpect(jsonPath("$.backendResp").value(DEFAULT_BACKEND_RESP.toString()))
//            .andExpect(jsonPath("$.cos").value(DEFAULT_COS.toString()))
//            .andExpect(jsonPath("$.creationType").value(DEFAULT_CREATION_TYPE.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingCreateSubscriber() throws Exception {
//        // Get the createSubscriber
//        restCreateSubscriberMockMvc.perform(get("/api/create-subscribers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateCreateSubscriber() throws Exception {
//        // Initialize the database
//        createSubscriberRepository.saveAndFlush(createSubscriber);
//
//        int databaseSizeBeforeUpdate = createSubscriberRepository.findAll().size();
//
//        // Update the createSubscriber
//        CreateSubscriber updatedCreateSubscriber = createSubscriberRepository.findById(createSubscriber.getId()).get();
//        // Disconnect from session so that the updates on updatedCreateSubscriber are not directly saved in db
//        em.detach(updatedCreateSubscriber);
//        updatedCreateSubscriber
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .statut(UPDATED_STATUT)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .backendResp(UPDATED_BACKEND_RESP)
//            .cos(UPDATED_COS)
//            .creationType(UPDATED_CREATION_TYPE);
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(updatedCreateSubscriber);
//
//        restCreateSubscriberMockMvc.perform(put("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the CreateSubscriber in the database
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeUpdate);
//        CreateSubscriber testCreateSubscriber = createSubscriberList.get(createSubscriberList.size() - 1);
//        assertThat(testCreateSubscriber.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testCreateSubscriber.getImsi()).isEqualTo(UPDATED_IMSI);
//        assertThat(testCreateSubscriber.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
//        assertThat(testCreateSubscriber.getStatut()).isEqualTo(UPDATED_STATUT);
//        assertThat(testCreateSubscriber.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
//        assertThat(testCreateSubscriber.getBackendResp()).isEqualTo(UPDATED_BACKEND_RESP);
//        assertThat(testCreateSubscriber.getCos()).isEqualTo(UPDATED_COS);
//        assertThat(testCreateSubscriber.getCreationType()).isEqualTo(UPDATED_CREATION_TYPE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingCreateSubscriber() throws Exception {
//        int databaseSizeBeforeUpdate = createSubscriberRepository.findAll().size();
//
//        // Create the CreateSubscriber
//        CreateSubscriberDTO createSubscriberDTO = createSubscriberMapper.toDto(createSubscriber);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restCreateSubscriberMockMvc.perform(put("/api/create-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(createSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the CreateSubscriber in the database
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteCreateSubscriber() throws Exception {
//        // Initialize the database
//        createSubscriberRepository.saveAndFlush(createSubscriber);
//
//        int databaseSizeBeforeDelete = createSubscriberRepository.findAll().size();
//
//        // Delete the createSubscriber
//        restCreateSubscriberMockMvc.perform(delete("/api/create-subscribers/{id}", createSubscriber.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<CreateSubscriber> createSubscriberList = createSubscriberRepository.findAll();
//        assertThat(createSubscriberList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(CreateSubscriber.class);
//        CreateSubscriber createSubscriber1 = new CreateSubscriber();
//        createSubscriber1.setId(1L);
//        CreateSubscriber createSubscriber2 = new CreateSubscriber();
//        createSubscriber2.setId(createSubscriber1.getId());
//        assertThat(createSubscriber1).isEqualTo(createSubscriber2);
//        createSubscriber2.setId(2L);
//        assertThat(createSubscriber1).isNotEqualTo(createSubscriber2);
//        createSubscriber1.setId(null);
//        assertThat(createSubscriber1).isNotEqualTo(createSubscriber2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(CreateSubscriberDTO.class);
//        CreateSubscriberDTO createSubscriberDTO1 = new CreateSubscriberDTO();
//        createSubscriberDTO1.setId(1L);
//        CreateSubscriberDTO createSubscriberDTO2 = new CreateSubscriberDTO();
//        assertThat(createSubscriberDTO1).isNotEqualTo(createSubscriberDTO2);
//        createSubscriberDTO2.setId(createSubscriberDTO1.getId());
//        assertThat(createSubscriberDTO1).isEqualTo(createSubscriberDTO2);
//        createSubscriberDTO2.setId(2L);
//        assertThat(createSubscriberDTO1).isNotEqualTo(createSubscriberDTO2);
//        createSubscriberDTO1.setId(null);
//        assertThat(createSubscriberDTO1).isNotEqualTo(createSubscriberDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(createSubscriberMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(createSubscriberMapper.fromId(null)).isNull();
//    }
//}
