//package sn.sentel.tigocare.web.rest;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.RoamingService;
//import sn.sentel.tigocare.repository.RoamingServiceRepository;
//import sn.sentel.tigocare.service.RoamingServiceService;
//import sn.sentel.tigocare.service.dto.RoamingServiceDTO;
//import sn.sentel.tigocare.service.mapper.RoamingServiceMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//
///**
// * Integration tests for the {@Link RoamingServiceResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class RoamingServiceResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final RoamingService DEFAULT_SERVICES = RoamingService.VOICE_DATA;
//    private static final RoamingService UPDATED_SERVICES = RoamingService.VOICE_NODATA;
//
//    @Autowired
//    private RoamingServiceRepository roamingServiceRepository;
//
//    @Autowired
//    private RoamingServiceMapper roamingServiceMapper;
//
//    @Autowired
//    private RoamingServiceService roamingServiceService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restRoamingServiceMockMvc;
//
//    private RoamingService roamingService;
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RoamingService createEntity(EntityManager em) {
//        RoamingService roamingService = new RoamingService()
//            .msisdn(DEFAULT_MSISDN)
//            .services(DEFAULT_SERVICES);
//        return roamingService;
//    }
//
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RoamingService createUpdatedEntity(EntityManager em) {
//        RoamingService roamingService = new RoamingService()
//            .msisdn(UPDATED_MSISDN)
//            .services(UPDATED_SERVICES);
//        return roamingService;
//    }
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final RoamingServiceResource roamingServiceResource = new RoamingServiceResource(this.roamingServiceService);
//        this.restRoamingServiceMockMvc = MockMvcBuilders.standaloneSetup(roamingServiceResource)
//            .setCustomArgumentResolvers(this.pageableArgumentResolver)
//            .setControllerAdvice(this.exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(this.jacksonMessageConverter)
//            .setValidator(this.validator).build();
//    }
//
//    @BeforeEach
//    public void initTest() {
//        this.roamingService = createEntity(this.em);
//    }
//
//    @Test
//    @Transactional
//    public void createRoamingService() throws Exception {
//        int databaseSizeBeforeCreate = this.roamingServiceRepository.findAll().size();
//
//        // Create the RoamingServiceEnum
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(this.roamingService);
//        this.restRoamingServiceMockMvc.perform(post("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the RoamingServiceEnum in the database
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeCreate + 1);
//        RoamingService testRoamingService = roamingServiceList.get(roamingServiceList.size() - 1);
//        assertThat(testRoamingService.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testRoamingService.getServices()).isEqualTo(DEFAULT_SERVICES);
//    }
//
//    @Test
//    @Transactional
//    public void createRoamingServiceWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = this.roamingServiceRepository.findAll().size();
//
//        // Create the RoamingServiceEnum with an existing ID
//        this.roamingService.setId(1L);
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(this.roamingService);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        this.restRoamingServiceMockMvc.perform(post("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RoamingServiceEnum in the database
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = this.roamingServiceRepository.findAll().size();
//        // set the field null
//        this.roamingService.setMsisdn(null);
//
//        // Create the RoamingServiceEnum, which fails.
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(this.roamingService);
//
//        this.restRoamingServiceMockMvc.perform(post("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkServicesIsRequired() throws Exception {
//        int databaseSizeBeforeTest = this.roamingServiceRepository.findAll().size();
//        // set the field null
//        this.roamingService.setServices(null);
//
//        // Create the RoamingServiceEnum, which fails.
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(this.roamingService);
//
//        this.restRoamingServiceMockMvc.perform(post("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllRoamingServices() throws Exception {
//        // Initialize the database
//        this.roamingServiceRepository.saveAndFlush(this.roamingService);
//
//        // Get all the roamingServiceList
//        this.restRoamingServiceMockMvc.perform(get("/api/roaming-services?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(this.roamingService.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].services").value(hasItem(DEFAULT_SERVICES.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getRoamingService() throws Exception {
//        // Initialize the database
//        this.roamingServiceRepository.saveAndFlush(this.roamingService);
//
//        // Get the roamingService
//        this.restRoamingServiceMockMvc.perform(get("/api/roaming-services/{id}", this.roamingService.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(this.roamingService.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.services").value(DEFAULT_SERVICES.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingRoamingService() throws Exception {
//        // Get the roamingService
//        this.restRoamingServiceMockMvc.perform(get("/api/roaming-services/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateRoamingService() throws Exception {
//        // Initialize the database
//        this.roamingServiceRepository.saveAndFlush(this.roamingService);
//
//        int databaseSizeBeforeUpdate = this.roamingServiceRepository.findAll().size();
//
//        // Update the roamingService
//        RoamingService updatedRoamingService = this.roamingServiceRepository.findById(this.roamingService.getId()).get();
//        // Disconnect from session so that the updates on updatedRoamingService are not directly saved in db
//        this.em.detach(updatedRoamingService);
//        updatedRoamingService
//            .msisdn(UPDATED_MSISDN)
//            .services(UPDATED_SERVICES);
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(updatedRoamingService);
//
//        this.restRoamingServiceMockMvc.perform(put("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the RoamingServiceEnum in the database
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeUpdate);
//        RoamingService testRoamingService = roamingServiceList.get(roamingServiceList.size() - 1);
//        assertThat(testRoamingService.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testRoamingService.getServices()).isEqualTo(UPDATED_SERVICES);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingRoamingService() throws Exception {
//        int databaseSizeBeforeUpdate = this.roamingServiceRepository.findAll().size();
//
//        // Create the RoamingServiceEnum
//        RoamingServiceDTO roamingServiceDTO = this.roamingServiceMapper.toDto(this.roamingService);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        this.restRoamingServiceMockMvc.perform(put("/api/roaming-services")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(roamingServiceDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RoamingServiceEnum in the database
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteRoamingService() throws Exception {
//        // Initialize the database
//        this.roamingServiceRepository.saveAndFlush(this.roamingService);
//
//        int databaseSizeBeforeDelete = this.roamingServiceRepository.findAll().size();
//
//        // Delete the roamingService
//        this.restRoamingServiceMockMvc.perform(delete("/api/roaming-services/{id}", this.roamingService.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<RoamingService> roamingServiceList = this.roamingServiceRepository.findAll();
//        assertThat(roamingServiceList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(RoamingService.class);
//        RoamingService roamingService1 = new RoamingService();
//        roamingService1.setId(1L);
//        RoamingService roamingService2 = new RoamingService();
//        roamingService2.setId(roamingService1.getId());
//        assertThat(roamingService1).isEqualTo(roamingService2);
//        roamingService2.setId(2L);
//        assertThat(roamingService1).isNotEqualTo(roamingService2);
//        roamingService1.setId(null);
//        assertThat(roamingService1).isNotEqualTo(roamingService2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(RoamingServiceDTO.class);
//        RoamingServiceDTO roamingServiceDTO1 = new RoamingServiceDTO();
//        roamingServiceDTO1.setId(1L);
//        RoamingServiceDTO roamingServiceDTO2 = new RoamingServiceDTO();
//        assertThat(roamingServiceDTO1).isNotEqualTo(roamingServiceDTO2);
//        roamingServiceDTO2.setId(roamingServiceDTO1.getId());
//        assertThat(roamingServiceDTO1).isEqualTo(roamingServiceDTO2);
//        roamingServiceDTO2.setId(2L);
//        assertThat(roamingServiceDTO1).isNotEqualTo(roamingServiceDTO2);
//        roamingServiceDTO1.setId(null);
//        assertThat(roamingServiceDTO1).isNotEqualTo(roamingServiceDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(this.roamingServiceMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(this.roamingServiceMapper.fromId(null)).isNull();
//    }
//}
