//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.DeleteSubscriber;
//import sn.sentel.tigocare.repository.DeleteSubscriberRepository;
//import sn.sentel.tigocare.service.DeleteSubscriberService;
//import sn.sentel.tigocare.service.dto.DeleteSubscriberDTO;
//import sn.sentel.tigocare.service.mapper.DeleteSubscriberMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Integration tests for the {@Link DeleteSubscriberResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class DeleteSubscriberResourceIT {
//
//    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_IMSI = "BBBBBBBBBB";
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final Boolean DEFAULT_DELETE_ON_AUC = false;
//    private static final Boolean UPDATED_DELETE_ON_AUC = true;
//
//    private static final Boolean DEFAULT_DELETE_ON_HLR = false;
//    private static final Boolean UPDATED_DELETE_ON_HLR = true;
//
//    private static final Boolean DEFAULT_DELETE_ON_HSS = false;
//    private static final Boolean UPDATED_DELETE_ON_HSS = true;
//
//    private static final Boolean DEFAULT_DELETE_ON_CBS = false;
//    private static final Boolean UPDATED_DELETE_ON_CBS = true;
//
//    private static final String DEFAULT_AUC_DELETED_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_AUC_DELETED_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_HLR_DELETED_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_HLR_DELETED_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_HSS_DELETED_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_HSS_DELETED_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_CBS_DELETED_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_CBS_DELETED_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";
//
//    @Autowired
//    private DeleteSubscriberRepository deleteSubscriberRepository;
//
//    @Autowired
//    private DeleteSubscriberMapper deleteSubscriberMapper;
//
//    @Autowired
//    private DeleteSubscriberService deleteSubscriberService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restDeleteSubscriberMockMvc;
//
//    private DeleteSubscriber deleteSubscriber;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final DeleteSubscriberResource deleteSubscriberResource = new DeleteSubscriberResource(deleteSubscriberService);
//        this.restDeleteSubscriberMockMvc = MockMvcBuilders.standaloneSetup(deleteSubscriberResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static DeleteSubscriber createEntity(EntityManager em) {
//        DeleteSubscriber deleteSubscriber = new DeleteSubscriber()
//            .imsi(DEFAULT_IMSI)
//            .msisdn(DEFAULT_MSISDN)
//            .deleteOnAuc(DEFAULT_DELETE_ON_AUC)
//            .deleteOnHlr(DEFAULT_DELETE_ON_HLR)
//            .deleteOnHss(DEFAULT_DELETE_ON_HSS)
//            .deleteOnCbs(DEFAULT_DELETE_ON_CBS)
//            .aucDeletedStatus(DEFAULT_AUC_DELETED_STATUS)
//            .hlrDeletedStatus(DEFAULT_HLR_DELETED_STATUS)
//            .hssDeletedStatus(DEFAULT_HSS_DELETED_STATUS)
//            .cbsDeletedStatus(DEFAULT_CBS_DELETED_STATUS)
//            .errorMessage(DEFAULT_ERROR_MESSAGE);
//        return deleteSubscriber;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static DeleteSubscriber createUpdatedEntity(EntityManager em) {
//        DeleteSubscriber deleteSubscriber = new DeleteSubscriber()
//            .imsi(UPDATED_IMSI)
//            .msisdn(UPDATED_MSISDN)
//            .deleteOnAuc(UPDATED_DELETE_ON_AUC)
//            .deleteOnHlr(UPDATED_DELETE_ON_HLR)
//            .deleteOnHss(UPDATED_DELETE_ON_HSS)
//            .deleteOnCbs(UPDATED_DELETE_ON_CBS)
//            .aucDeletedStatus(UPDATED_AUC_DELETED_STATUS)
//            .hlrDeletedStatus(UPDATED_HLR_DELETED_STATUS)
//            .hssDeletedStatus(UPDATED_HSS_DELETED_STATUS)
//            .cbsDeletedStatus(UPDATED_CBS_DELETED_STATUS)
//            .errorMessage(UPDATED_ERROR_MESSAGE);
//        return deleteSubscriber;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        deleteSubscriber = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createDeleteSubscriber() throws Exception {
//        int databaseSizeBeforeCreate = deleteSubscriberRepository.findAll().size();
//
//        // Create the DeleteSubscriber
//        DeleteSubscriberDTO deleteSubscriberDTO = deleteSubscriberMapper.toDto(deleteSubscriber);
//        restDeleteSubscriberMockMvc.perform(post("/api/delete-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleteSubscriberDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the DeleteSubscriber in the database
//        List<DeleteSubscriber> deleteSubscriberList = deleteSubscriberRepository.findAll();
//        assertThat(deleteSubscriberList).hasSize(databaseSizeBeforeCreate + 1);
//        DeleteSubscriber testDeleteSubscriber = deleteSubscriberList.get(deleteSubscriberList.size() - 1);
//        assertThat(testDeleteSubscriber.getImsi()).isEqualTo(DEFAULT_IMSI);
//        assertThat(testDeleteSubscriber.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testDeleteSubscriber.isDeleteOnAuc()).isEqualTo(DEFAULT_DELETE_ON_AUC);
//        assertThat(testDeleteSubscriber.isDeleteOnHlr()).isEqualTo(DEFAULT_DELETE_ON_HLR);
//        assertThat(testDeleteSubscriber.isDeleteOnHss()).isEqualTo(DEFAULT_DELETE_ON_HSS);
//        assertThat(testDeleteSubscriber.isDeleteOnCbs()).isEqualTo(DEFAULT_DELETE_ON_CBS);
//        assertThat(testDeleteSubscriber.getAucDeletedStatus()).isEqualTo(DEFAULT_AUC_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getHlrDeletedStatus()).isEqualTo(DEFAULT_HLR_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getHssDeletedStatus()).isEqualTo(DEFAULT_HSS_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getCbsDeletedStatus()).isEqualTo(DEFAULT_CBS_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void createDeleteSubscriberWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = deleteSubscriberRepository.findAll().size();
//
//        // Create the DeleteSubscriber with an existing ID
//        deleteSubscriber.setId(1L);
//        DeleteSubscriberDTO deleteSubscriberDTO = deleteSubscriberMapper.toDto(deleteSubscriber);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restDeleteSubscriberMockMvc.perform(post("/api/delete-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleteSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the DeleteSubscriber in the database
//        List<DeleteSubscriber> deleteSubscriberList = deleteSubscriberRepository.findAll();
//        assertThat(deleteSubscriberList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void getAllDeleteSubscribers() throws Exception {
//        // Initialize the database
//        deleteSubscriberRepository.saveAndFlush(deleteSubscriber);
//
//        // Get all the deleteSubscriberList
//        restDeleteSubscriberMockMvc.perform(get("/api/delete-subscribers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(deleteSubscriber.getId().intValue())))
//            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].deleteOnAuc").value(hasItem(DEFAULT_DELETE_ON_AUC.booleanValue())))
//            .andExpect(jsonPath("$.[*].deleteOnHlr").value(hasItem(DEFAULT_DELETE_ON_HLR.booleanValue())))
//            .andExpect(jsonPath("$.[*].deleteOnHss").value(hasItem(DEFAULT_DELETE_ON_HSS.booleanValue())))
//            .andExpect(jsonPath("$.[*].deleteOnCbs").value(hasItem(DEFAULT_DELETE_ON_CBS.booleanValue())))
//            .andExpect(jsonPath("$.[*].aucDeletedStatus").value(hasItem(DEFAULT_AUC_DELETED_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].hlrDeletedStatus").value(hasItem(DEFAULT_HLR_DELETED_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].hssDeletedStatus").value(hasItem(DEFAULT_HSS_DELETED_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].cbsDeletedStatus").value(hasItem(DEFAULT_CBS_DELETED_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getDeleteSubscriber() throws Exception {
//        // Initialize the database
//        deleteSubscriberRepository.saveAndFlush(deleteSubscriber);
//
//        // Get the deleteSubscriber
//        restDeleteSubscriberMockMvc.perform(get("/api/delete-subscribers/{id}", deleteSubscriber.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(deleteSubscriber.getId().intValue()))
//            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.deleteOnAuc").value(DEFAULT_DELETE_ON_AUC.booleanValue()))
//            .andExpect(jsonPath("$.deleteOnHlr").value(DEFAULT_DELETE_ON_HLR.booleanValue()))
//            .andExpect(jsonPath("$.deleteOnHss").value(DEFAULT_DELETE_ON_HSS.booleanValue()))
//            .andExpect(jsonPath("$.deleteOnCbs").value(DEFAULT_DELETE_ON_CBS.booleanValue()))
//            .andExpect(jsonPath("$.aucDeletedStatus").value(DEFAULT_AUC_DELETED_STATUS.toString()))
//            .andExpect(jsonPath("$.hlrDeletedStatus").value(DEFAULT_HLR_DELETED_STATUS.toString()))
//            .andExpect(jsonPath("$.hssDeletedStatus").value(DEFAULT_HSS_DELETED_STATUS.toString()))
//            .andExpect(jsonPath("$.cbsDeletedStatus").value(DEFAULT_CBS_DELETED_STATUS.toString()))
//            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingDeleteSubscriber() throws Exception {
//        // Get the deleteSubscriber
//        restDeleteSubscriberMockMvc.perform(get("/api/delete-subscribers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateDeleteSubscriber() throws Exception {
//        // Initialize the database
//        deleteSubscriberRepository.saveAndFlush(deleteSubscriber);
//
//        int databaseSizeBeforeUpdate = deleteSubscriberRepository.findAll().size();
//
//        // Update the deleteSubscriber
//        DeleteSubscriber updatedDeleteSubscriber = deleteSubscriberRepository.findById(deleteSubscriber.getId()).get();
//        // Disconnect from session so that the updates on updatedDeleteSubscriber are not directly saved in db
//        em.detach(updatedDeleteSubscriber);
//        updatedDeleteSubscriber
//            .imsi(UPDATED_IMSI)
//            .msisdn(UPDATED_MSISDN)
//            .deleteOnAuc(UPDATED_DELETE_ON_AUC)
//            .deleteOnHlr(UPDATED_DELETE_ON_HLR)
//            .deleteOnHss(UPDATED_DELETE_ON_HSS)
//            .deleteOnCbs(UPDATED_DELETE_ON_CBS)
//            .aucDeletedStatus(UPDATED_AUC_DELETED_STATUS)
//            .hlrDeletedStatus(UPDATED_HLR_DELETED_STATUS)
//            .hssDeletedStatus(UPDATED_HSS_DELETED_STATUS)
//            .cbsDeletedStatus(UPDATED_CBS_DELETED_STATUS)
//            .errorMessage(UPDATED_ERROR_MESSAGE);
//        DeleteSubscriberDTO deleteSubscriberDTO = deleteSubscriberMapper.toDto(updatedDeleteSubscriber);
//
//        restDeleteSubscriberMockMvc.perform(put("/api/delete-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleteSubscriberDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the DeleteSubscriber in the database
//        List<DeleteSubscriber> deleteSubscriberList = deleteSubscriberRepository.findAll();
//        assertThat(deleteSubscriberList).hasSize(databaseSizeBeforeUpdate);
//        DeleteSubscriber testDeleteSubscriber = deleteSubscriberList.get(deleteSubscriberList.size() - 1);
//        assertThat(testDeleteSubscriber.getImsi()).isEqualTo(UPDATED_IMSI);
//        assertThat(testDeleteSubscriber.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testDeleteSubscriber.isDeleteOnAuc()).isEqualTo(UPDATED_DELETE_ON_AUC);
//        assertThat(testDeleteSubscriber.isDeleteOnHlr()).isEqualTo(UPDATED_DELETE_ON_HLR);
//        assertThat(testDeleteSubscriber.isDeleteOnHss()).isEqualTo(UPDATED_DELETE_ON_HSS);
//        assertThat(testDeleteSubscriber.isDeleteOnCbs()).isEqualTo(UPDATED_DELETE_ON_CBS);
//        assertThat(testDeleteSubscriber.getAucDeletedStatus()).isEqualTo(UPDATED_AUC_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getHlrDeletedStatus()).isEqualTo(UPDATED_HLR_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getHssDeletedStatus()).isEqualTo(UPDATED_HSS_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getCbsDeletedStatus()).isEqualTo(UPDATED_CBS_DELETED_STATUS);
//        assertThat(testDeleteSubscriber.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingDeleteSubscriber() throws Exception {
//        int databaseSizeBeforeUpdate = deleteSubscriberRepository.findAll().size();
//
//        // Create the DeleteSubscriber
//        DeleteSubscriberDTO deleteSubscriberDTO = deleteSubscriberMapper.toDto(deleteSubscriber);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restDeleteSubscriberMockMvc.perform(put("/api/delete-subscribers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(deleteSubscriberDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the DeleteSubscriber in the database
//        List<DeleteSubscriber> deleteSubscriberList = deleteSubscriberRepository.findAll();
//        assertThat(deleteSubscriberList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteDeleteSubscriber() throws Exception {
//        // Initialize the database
//        deleteSubscriberRepository.saveAndFlush(deleteSubscriber);
//
//        int databaseSizeBeforeDelete = deleteSubscriberRepository.findAll().size();
//
//        // Delete the deleteSubscriber
//        restDeleteSubscriberMockMvc.perform(delete("/api/delete-subscribers/{id}", deleteSubscriber.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<DeleteSubscriber> deleteSubscriberList = deleteSubscriberRepository.findAll();
//        assertThat(deleteSubscriberList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(DeleteSubscriber.class);
//        DeleteSubscriber deleteSubscriber1 = new DeleteSubscriber();
//        deleteSubscriber1.setId(1L);
//        DeleteSubscriber deleteSubscriber2 = new DeleteSubscriber();
//        deleteSubscriber2.setId(deleteSubscriber1.getId());
//        assertThat(deleteSubscriber1).isEqualTo(deleteSubscriber2);
//        deleteSubscriber2.setId(2L);
//        assertThat(deleteSubscriber1).isNotEqualTo(deleteSubscriber2);
//        deleteSubscriber1.setId(null);
//        assertThat(deleteSubscriber1).isNotEqualTo(deleteSubscriber2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(DeleteSubscriberDTO.class);
//        DeleteSubscriberDTO deleteSubscriberDTO1 = new DeleteSubscriberDTO();
//        deleteSubscriberDTO1.setId(1L);
//        DeleteSubscriberDTO deleteSubscriberDTO2 = new DeleteSubscriberDTO();
//        assertThat(deleteSubscriberDTO1).isNotEqualTo(deleteSubscriberDTO2);
//        deleteSubscriberDTO2.setId(deleteSubscriberDTO1.getId());
//        assertThat(deleteSubscriberDTO1).isEqualTo(deleteSubscriberDTO2);
//        deleteSubscriberDTO2.setId(2L);
//        assertThat(deleteSubscriberDTO1).isNotEqualTo(deleteSubscriberDTO2);
//        deleteSubscriberDTO1.setId(null);
//        assertThat(deleteSubscriberDTO1).isNotEqualTo(deleteSubscriberDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(deleteSubscriberMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(deleteSubscriberMapper.fromId(null)).isNull();
//    }
//}
