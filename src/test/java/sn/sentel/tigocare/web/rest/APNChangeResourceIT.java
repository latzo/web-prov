//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.APNChange;
//import sn.sentel.tigocare.repository.APNChangeRepository;
//import sn.sentel.tigocare.service.APNChangeService;
//import sn.sentel.tigocare.service.dto.APNChangeDTO;
//import sn.sentel.tigocare.service.mapper.APNChangeMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.sentel.tigocare.domain.enumeration.OperationEnum;
///**
// * Integration tests for the {@Link APNChangeResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class APNChangeResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
//    private static final String UPDATED_IMSI = "BBBBBBBBBB";
//
//    private static final OperationEnum DEFAULT_OPERATION = OperationEnum.ACTIVATE;
//    private static final OperationEnum UPDATED_OPERATION = OperationEnum.DEACTIVATE;
//
//    private static final String DEFAULT_APN = "AAAAAAAAAA";
//    private static final String UPDATED_APN = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROFIL = "AAAAAAAAAA";
//    private static final String UPDATED_PROFIL = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
//    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
//    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";
//
//    @Autowired
//    private APNChangeRepository aPNChangeRepository;
//
//    @Autowired
//    private APNChangeMapper aPNChangeMapper;
//
//    @Autowired
//    private APNChangeService aPNChangeService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restAPNChangeMockMvc;
//
//    private APNChange aPNChange;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final APNChangeResource aPNChangeResource = new APNChangeResource(aPNChangeService);
//        this.restAPNChangeMockMvc = MockMvcBuilders.standaloneSetup(aPNChangeResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static APNChange createEntity(EntityManager em) {
//        APNChange aPNChange = new APNChange()
//            .msisdn(DEFAULT_MSISDN)
//            .imsi(DEFAULT_IMSI)
//            .operation(DEFAULT_OPERATION)
//            .apn(DEFAULT_APN)
//            .profil(DEFAULT_PROFIL)
//            .processingStatus(DEFAULT_PROCESSING_STATUS)
//            .transactionId(DEFAULT_TRANSACTION_ID)
//            .createdBy(DEFAULT_CREATED_BY);
//        return aPNChange;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static APNChange createUpdatedEntity(EntityManager em) {
//        APNChange aPNChange = new APNChange()
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .operation(UPDATED_OPERATION)
//            .apn(UPDATED_APN)
//            .profil(UPDATED_PROFIL)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .createdBy(UPDATED_CREATED_BY);
//        return aPNChange;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        aPNChange = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createAPNChange() throws Exception {
//        int databaseSizeBeforeCreate = aPNChangeRepository.findAll().size();
//
//        // Create the APNChange
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the APNChange in the database
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeCreate + 1);
//        APNChange testAPNChange = aPNChangeList.get(aPNChangeList.size() - 1);
//        assertThat(testAPNChange.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testAPNChange.getImsi()).isEqualTo(DEFAULT_IMSI);
//        assertThat(testAPNChange.getOperation()).isEqualTo(DEFAULT_OPERATION);
//        assertThat(testAPNChange.getApn()).isEqualTo(DEFAULT_APN);
//        assertThat(testAPNChange.getProfil()).isEqualTo(DEFAULT_PROFIL);
//        assertThat(testAPNChange.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//        assertThat(testAPNChange.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
//        assertThat(testAPNChange.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
//    }
//
//    @Test
//    @Transactional
//    public void createAPNChangeWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = aPNChangeRepository.findAll().size();
//
//        // Create the APNChange with an existing ID
//        aPNChange.setId(1L);
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the APNChange in the database
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = aPNChangeRepository.findAll().size();
//        // set the field null
//        aPNChange.setMsisdn(null);
//
//        // Create the APNChange, which fails.
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkImsiIsRequired() throws Exception {
//        int databaseSizeBeforeTest = aPNChangeRepository.findAll().size();
//        // set the field null
//        aPNChange.setImsi(null);
//
//        // Create the APNChange, which fails.
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkOperationIsRequired() throws Exception {
//        int databaseSizeBeforeTest = aPNChangeRepository.findAll().size();
//        // set the field null
//        aPNChange.setOperation(null);
//
//        // Create the APNChange, which fails.
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkApnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = aPNChangeRepository.findAll().size();
//        // set the field null
//        aPNChange.setApn(null);
//
//        // Create the APNChange, which fails.
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkProfilIsRequired() throws Exception {
//        int databaseSizeBeforeTest = aPNChangeRepository.findAll().size();
//        // set the field null
//        aPNChange.setProfil(null);
//
//        // Create the APNChange, which fails.
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        restAPNChangeMockMvc.perform(post("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllAPNChanges() throws Exception {
//        // Initialize the database
//        aPNChangeRepository.saveAndFlush(aPNChange);
//
//        // Get all the aPNChangeList
//        restAPNChangeMockMvc.perform(get("/api/apn-changes?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(aPNChange.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
//            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
//            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION.toString())))
//            .andExpect(jsonPath("$.[*].apn").value(hasItem(DEFAULT_APN.toString())))
//            .andExpect(jsonPath("$.[*].profil").value(hasItem(DEFAULT_PROFIL.toString())))
//            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
//            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getAPNChange() throws Exception {
//        // Initialize the database
//        aPNChangeRepository.saveAndFlush(aPNChange);
//
//        // Get the aPNChange
//        restAPNChangeMockMvc.perform(get("/api/apn-changes/{id}", aPNChange.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(aPNChange.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
//            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
//            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION.toString()))
//            .andExpect(jsonPath("$.apn").value(DEFAULT_APN.toString()))
//            .andExpect(jsonPath("$.profil").value(DEFAULT_PROFIL.toString()))
//            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()))
//            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
//            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingAPNChange() throws Exception {
//        // Get the aPNChange
//        restAPNChangeMockMvc.perform(get("/api/apn-changes/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateAPNChange() throws Exception {
//        // Initialize the database
//        aPNChangeRepository.saveAndFlush(aPNChange);
//
//        int databaseSizeBeforeUpdate = aPNChangeRepository.findAll().size();
//
//        // Update the aPNChange
//        APNChange updatedAPNChange = aPNChangeRepository.findById(aPNChange.getId()).get();
//        // Disconnect from session so that the updates on updatedAPNChange are not directly saved in db
//        em.detach(updatedAPNChange);
//        updatedAPNChange
//            .msisdn(UPDATED_MSISDN)
//            .imsi(UPDATED_IMSI)
//            .operation(UPDATED_OPERATION)
//            .apn(UPDATED_APN)
//            .profil(UPDATED_PROFIL)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .createdBy(UPDATED_CREATED_BY);
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(updatedAPNChange);
//
//        restAPNChangeMockMvc.perform(put("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the APNChange in the database
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeUpdate);
//        APNChange testAPNChange = aPNChangeList.get(aPNChangeList.size() - 1);
//        assertThat(testAPNChange.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testAPNChange.getImsi()).isEqualTo(UPDATED_IMSI);
//        assertThat(testAPNChange.getOperation()).isEqualTo(UPDATED_OPERATION);
//        assertThat(testAPNChange.getApn()).isEqualTo(UPDATED_APN);
//        assertThat(testAPNChange.getProfil()).isEqualTo(UPDATED_PROFIL);
//        assertThat(testAPNChange.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
//        assertThat(testAPNChange.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
//        assertThat(testAPNChange.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingAPNChange() throws Exception {
//        int databaseSizeBeforeUpdate = aPNChangeRepository.findAll().size();
//
//        // Create the APNChange
//        APNChangeDTO aPNChangeDTO = aPNChangeMapper.toDto(aPNChange);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restAPNChangeMockMvc.perform(put("/api/apn-changes")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(aPNChangeDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the APNChange in the database
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteAPNChange() throws Exception {
//        // Initialize the database
//        aPNChangeRepository.saveAndFlush(aPNChange);
//
//        int databaseSizeBeforeDelete = aPNChangeRepository.findAll().size();
//
//        // Delete the aPNChange
//        restAPNChangeMockMvc.perform(delete("/api/apn-changes/{id}", aPNChange.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<APNChange> aPNChangeList = aPNChangeRepository.findAll();
//        assertThat(aPNChangeList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(APNChange.class);
//        APNChange aPNChange1 = new APNChange();
//        aPNChange1.setId(1L);
//        APNChange aPNChange2 = new APNChange();
//        aPNChange2.setId(aPNChange1.getId());
//        assertThat(aPNChange1).isEqualTo(aPNChange2);
//        aPNChange2.setId(2L);
//        assertThat(aPNChange1).isNotEqualTo(aPNChange2);
//        aPNChange1.setId(null);
//        assertThat(aPNChange1).isNotEqualTo(aPNChange2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(APNChangeDTO.class);
//        APNChangeDTO aPNChangeDTO1 = new APNChangeDTO();
//        aPNChangeDTO1.setId(1L);
//        APNChangeDTO aPNChangeDTO2 = new APNChangeDTO();
//        assertThat(aPNChangeDTO1).isNotEqualTo(aPNChangeDTO2);
//        aPNChangeDTO2.setId(aPNChangeDTO1.getId());
//        assertThat(aPNChangeDTO1).isEqualTo(aPNChangeDTO2);
//        aPNChangeDTO2.setId(2L);
//        assertThat(aPNChangeDTO1).isNotEqualTo(aPNChangeDTO2);
//        aPNChangeDTO1.setId(null);
//        assertThat(aPNChangeDTO1).isNotEqualTo(aPNChangeDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(aPNChangeMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(aPNChangeMapper.fromId(null)).isNull();
//    }
//}
