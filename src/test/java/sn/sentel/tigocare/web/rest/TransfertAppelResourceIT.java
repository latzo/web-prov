//package sn.sentel.tigocare.web.rest;
//
//import sn.sentel.tigocare.TigocareApp;
//import sn.sentel.tigocare.domain.TransfertAppel;
//import sn.sentel.tigocare.repository.TransfertAppelRepository;
//import sn.sentel.tigocare.service.TransfertAppelService;
//import sn.sentel.tigocare.service.dto.TransfertAppelDTO;
//import sn.sentel.tigocare.service.mapper.TransfertAppelMapper;
//import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.sentel.tigocare.domain.enumeration.OperationEnum;
///**
// * Integration tests for the {@link TransfertAppelResource} REST controller.
// */
//@SpringBootTest(classes = TigocareApp.class)
//public class TransfertAppelResourceIT {
//
//    private static final String DEFAULT_MSISDN_SOURCE = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN_SOURCE = "BBBBBBBBBB";
//
//    private static final String DEFAULT_MSISDN_DEST = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN_DEST = "BBBBBBBBBB";
//
//    private static final OperationEnum DEFAULT_OPERATION = OperationEnum.ACTIVATE;
//    private static final OperationEnum UPDATED_OPERATION = OperationEnum.DEACTIVATE;
//
//    private static final String DEFAULT_PROCESSING_STATUS = "AAAAAAAAAA";
//    private static final String UPDATED_PROCESSING_STATUS = "BBBBBBBBBB";
//
//    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
//    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_BACKEND_RESP = "AAAAAAAAAA";
//    private static final String UPDATED_BACKEND_RESP = "BBBBBBBBBB";
//
//    @Autowired
//    private TransfertAppelRepository transfertAppelRepository;
//
//    @Autowired
//    private TransfertAppelMapper transfertAppelMapper;
//
//    @Autowired
//    private TransfertAppelService transfertAppelService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restTransfertAppelMockMvc;
//
//    private TransfertAppel transfertAppel;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final TransfertAppelResource transfertAppelResource = new TransfertAppelResource(transfertAppelService);
//        this.restTransfertAppelMockMvc = MockMvcBuilders.standaloneSetup(transfertAppelResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static TransfertAppel createEntity(EntityManager em) {
//        TransfertAppel transfertAppel = new TransfertAppel()
//            .msisdnSource(DEFAULT_MSISDN_SOURCE)
//            .msisdnDest(DEFAULT_MSISDN_DEST)
//            .operation(DEFAULT_OPERATION)
//            .processingStatus(DEFAULT_PROCESSING_STATUS)
//            .transactionId(DEFAULT_TRANSACTION_ID)
//            .backendResp(DEFAULT_BACKEND_RESP);
//        return transfertAppel;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static TransfertAppel createUpdatedEntity(EntityManager em) {
//        TransfertAppel transfertAppel = new TransfertAppel()
//            .msisdnSource(UPDATED_MSISDN_SOURCE)
//            .msisdnDest(UPDATED_MSISDN_DEST)
//            .operation(UPDATED_OPERATION)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .backendResp(UPDATED_BACKEND_RESP);
//        return transfertAppel;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        transfertAppel = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createTransfertAppel() throws Exception {
//        int databaseSizeBeforeCreate = transfertAppelRepository.findAll().size();
//
//        // Create the TransfertAppel
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(transfertAppel);
//        restTransfertAppelMockMvc.perform(post("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the TransfertAppel in the database
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeCreate + 1);
//        TransfertAppel testTransfertAppel = transfertAppelList.get(transfertAppelList.size() - 1);
//        assertThat(testTransfertAppel.getMsisdnSource()).isEqualTo(DEFAULT_MSISDN_SOURCE);
//        assertThat(testTransfertAppel.getMsisdnDest()).isEqualTo(DEFAULT_MSISDN_DEST);
//        assertThat(testTransfertAppel.getOperation()).isEqualTo(DEFAULT_OPERATION);
//        assertThat(testTransfertAppel.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
//        assertThat(testTransfertAppel.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
//        assertThat(testTransfertAppel.getBackendResp()).isEqualTo(DEFAULT_BACKEND_RESP);
//    }
//
//    @Test
//    @Transactional
//    public void createTransfertAppelWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = transfertAppelRepository.findAll().size();
//
//        // Create the TransfertAppel with an existing ID
//        transfertAppel.setId(1L);
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(transfertAppel);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restTransfertAppelMockMvc.perform(post("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the TransfertAppel in the database
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnSourceIsRequired() throws Exception {
//        int databaseSizeBeforeTest = transfertAppelRepository.findAll().size();
//        // set the field null
//        transfertAppel.setMsisdnSource(null);
//
//        // Create the TransfertAppel, which fails.
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(transfertAppel);
//
//        restTransfertAppelMockMvc.perform(post("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkOperationIsRequired() throws Exception {
//        int databaseSizeBeforeTest = transfertAppelRepository.findAll().size();
//        // set the field null
//        transfertAppel.setOperation(null);
//
//        // Create the TransfertAppel, which fails.
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(transfertAppel);
//
//        restTransfertAppelMockMvc.perform(post("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllTransfertAppels() throws Exception {
//        // Initialize the database
//        transfertAppelRepository.saveAndFlush(transfertAppel);
//
//        // Get all the transfertAppelList
//        restTransfertAppelMockMvc.perform(get("/api/transfert-appels?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(transfertAppel.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdnSource").value(hasItem(DEFAULT_MSISDN_SOURCE.toString())))
//            .andExpect(jsonPath("$.[*].msisdnDest").value(hasItem(DEFAULT_MSISDN_DEST.toString())))
//            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION.toString())))
//            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
//            .andExpect(jsonPath("$.[*].backendResp").value(hasItem(DEFAULT_BACKEND_RESP.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getTransfertAppel() throws Exception {
//        // Initialize the database
//        transfertAppelRepository.saveAndFlush(transfertAppel);
//
//        // Get the transfertAppel
//        restTransfertAppelMockMvc.perform(get("/api/transfert-appels/{id}", transfertAppel.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(transfertAppel.getId().intValue()))
//            .andExpect(jsonPath("$.msisdnSource").value(DEFAULT_MSISDN_SOURCE.toString()))
//            .andExpect(jsonPath("$.msisdnDest").value(DEFAULT_MSISDN_DEST.toString()))
//            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION.toString()))
//            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()))
//            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
//            .andExpect(jsonPath("$.backendResp").value(DEFAULT_BACKEND_RESP.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingTransfertAppel() throws Exception {
//        // Get the transfertAppel
//        restTransfertAppelMockMvc.perform(get("/api/transfert-appels/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateTransfertAppel() throws Exception {
//        // Initialize the database
//        transfertAppelRepository.saveAndFlush(transfertAppel);
//
//        int databaseSizeBeforeUpdate = transfertAppelRepository.findAll().size();
//
//        // Update the transfertAppel
//        TransfertAppel updatedTransfertAppel = transfertAppelRepository.findById(transfertAppel.getId()).get();
//        // Disconnect from session so that the updates on updatedTransfertAppel are not directly saved in db
//        em.detach(updatedTransfertAppel);
//        updatedTransfertAppel
//            .msisdnSource(UPDATED_MSISDN_SOURCE)
//            .msisdnDest(UPDATED_MSISDN_DEST)
//            .operation(UPDATED_OPERATION)
//            .processingStatus(UPDATED_PROCESSING_STATUS)
//            .transactionId(UPDATED_TRANSACTION_ID)
//            .backendResp(UPDATED_BACKEND_RESP);
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(updatedTransfertAppel);
//
//        restTransfertAppelMockMvc.perform(put("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the TransfertAppel in the database
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeUpdate);
//        TransfertAppel testTransfertAppel = transfertAppelList.get(transfertAppelList.size() - 1);
//        assertThat(testTransfertAppel.getMsisdnSource()).isEqualTo(UPDATED_MSISDN_SOURCE);
//        assertThat(testTransfertAppel.getMsisdnDest()).isEqualTo(UPDATED_MSISDN_DEST);
//        assertThat(testTransfertAppel.getOperation()).isEqualTo(UPDATED_OPERATION);
//        assertThat(testTransfertAppel.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
//        assertThat(testTransfertAppel.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
//        assertThat(testTransfertAppel.getBackendResp()).isEqualTo(UPDATED_BACKEND_RESP);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingTransfertAppel() throws Exception {
//        int databaseSizeBeforeUpdate = transfertAppelRepository.findAll().size();
//
//        // Create the TransfertAppel
//        TransfertAppelDTO transfertAppelDTO = transfertAppelMapper.toDto(transfertAppel);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restTransfertAppelMockMvc.perform(put("/api/transfert-appels")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(transfertAppelDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the TransfertAppel in the database
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteTransfertAppel() throws Exception {
//        // Initialize the database
//        transfertAppelRepository.saveAndFlush(transfertAppel);
//
//        int databaseSizeBeforeDelete = transfertAppelRepository.findAll().size();
//
//        // Delete the transfertAppel
//        restTransfertAppelMockMvc.perform(delete("/api/transfert-appels/{id}", transfertAppel.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<TransfertAppel> transfertAppelList = transfertAppelRepository.findAll();
//        assertThat(transfertAppelList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(TransfertAppel.class);
//        TransfertAppel transfertAppel1 = new TransfertAppel();
//        transfertAppel1.setId(1L);
//        TransfertAppel transfertAppel2 = new TransfertAppel();
//        transfertAppel2.setId(transfertAppel1.getId());
//        assertThat(transfertAppel1).isEqualTo(transfertAppel2);
//        transfertAppel2.setId(2L);
//        assertThat(transfertAppel1).isNotEqualTo(transfertAppel2);
//        transfertAppel1.setId(null);
//        assertThat(transfertAppel1).isNotEqualTo(transfertAppel2);
//    }
//
//    @Test
//    @Transactional
//    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(TransfertAppelDTO.class);
//        TransfertAppelDTO transfertAppelDTO1 = new TransfertAppelDTO();
//        transfertAppelDTO1.setId(1L);
//        TransfertAppelDTO transfertAppelDTO2 = new TransfertAppelDTO();
//        assertThat(transfertAppelDTO1).isNotEqualTo(transfertAppelDTO2);
//        transfertAppelDTO2.setId(transfertAppelDTO1.getId());
//        assertThat(transfertAppelDTO1).isEqualTo(transfertAppelDTO2);
//        transfertAppelDTO2.setId(2L);
//        assertThat(transfertAppelDTO1).isNotEqualTo(transfertAppelDTO2);
//        transfertAppelDTO1.setId(null);
//        assertThat(transfertAppelDTO1).isNotEqualTo(transfertAppelDTO2);
//    }
//
//    @Test
//    @Transactional
//    public void testEntityFromId() {
//        assertThat(transfertAppelMapper.fromId(42L).getId()).isEqualTo(42);
//        assertThat(transfertAppelMapper.fromId(null)).isNull();
//    }
//}
