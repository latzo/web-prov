package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.ResetLocation;
import sn.sentel.tigocare.repository.ResetLocationRepository;
import sn.sentel.tigocare.service.ResetLocationService;
import sn.sentel.tigocare.service.dto.ResetLocationDTO;
import sn.sentel.tigocare.service.mapper.ResetLocationMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ResetLocationResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class ResetLocationResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    @Autowired
    private ResetLocationRepository resetLocationRepository;

    @Autowired
    private ResetLocationMapper resetLocationMapper;

    @Autowired
    private ResetLocationService resetLocationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restResetLocationMockMvc;

    private ResetLocation resetLocation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResetLocationResource resetLocationResource = new ResetLocationResource(resetLocationService);
        this.restResetLocationMockMvc = MockMvcBuilders.standaloneSetup(resetLocationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResetLocation createEntity(EntityManager em) {
        ResetLocation resetLocation = new ResetLocation()
            .msisdn(DEFAULT_MSISDN);
        return resetLocation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResetLocation createUpdatedEntity(EntityManager em) {
        ResetLocation resetLocation = new ResetLocation()
            .msisdn(UPDATED_MSISDN);
        return resetLocation;
    }

    @BeforeEach
    public void initTest() {
        resetLocation = createEntity(em);
    }

    @Test
    @Transactional
    public void createResetLocation() throws Exception {
        int databaseSizeBeforeCreate = resetLocationRepository.findAll().size();

        // Create the ResetLocation
        ResetLocationDTO resetLocationDTO = resetLocationMapper.toDto(resetLocation);
        restResetLocationMockMvc.perform(post("/api/reset-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resetLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the ResetLocation in the database
        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeCreate + 1);
        ResetLocation testResetLocation = resetLocationList.get(resetLocationList.size() - 1);
        assertThat(testResetLocation.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
    }

    @Test
    @Transactional
    public void createResetLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resetLocationRepository.findAll().size();

        // Create the ResetLocation with an existing ID
        resetLocation.setId(1L);
        ResetLocationDTO resetLocationDTO = resetLocationMapper.toDto(resetLocation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResetLocationMockMvc.perform(post("/api/reset-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resetLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResetLocation in the database
        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMsisdnIsRequired() throws Exception {
        int databaseSizeBeforeTest = resetLocationRepository.findAll().size();
        // set the field null
        resetLocation.setMsisdn(null);

        // Create the ResetLocation, which fails.
        ResetLocationDTO resetLocationDTO = resetLocationMapper.toDto(resetLocation);

        restResetLocationMockMvc.perform(post("/api/reset-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resetLocationDTO)))
            .andExpect(status().isBadRequest());

        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResetLocations() throws Exception {
        // Initialize the database
        resetLocationRepository.saveAndFlush(resetLocation);

        // Get all the resetLocationList
        restResetLocationMockMvc.perform(get("/api/reset-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resetLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())));
    }
    
    @Test
    @Transactional
    public void getResetLocation() throws Exception {
        // Initialize the database
        resetLocationRepository.saveAndFlush(resetLocation);

        // Get the resetLocation
        restResetLocationMockMvc.perform(get("/api/reset-locations/{id}", resetLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resetLocation.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResetLocation() throws Exception {
        // Get the resetLocation
        restResetLocationMockMvc.perform(get("/api/reset-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResetLocation() throws Exception {
        // Initialize the database
        resetLocationRepository.saveAndFlush(resetLocation);

        int databaseSizeBeforeUpdate = resetLocationRepository.findAll().size();

        // Update the resetLocation
        ResetLocation updatedResetLocation = resetLocationRepository.findById(resetLocation.getId()).get();
        // Disconnect from session so that the updates on updatedResetLocation are not directly saved in db
        em.detach(updatedResetLocation);
        updatedResetLocation
            .msisdn(UPDATED_MSISDN);
        ResetLocationDTO resetLocationDTO = resetLocationMapper.toDto(updatedResetLocation);

        restResetLocationMockMvc.perform(put("/api/reset-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resetLocationDTO)))
            .andExpect(status().isOk());

        // Validate the ResetLocation in the database
        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeUpdate);
        ResetLocation testResetLocation = resetLocationList.get(resetLocationList.size() - 1);
        assertThat(testResetLocation.getMsisdn()).isEqualTo(UPDATED_MSISDN);
    }

    @Test
    @Transactional
    public void updateNonExistingResetLocation() throws Exception {
        int databaseSizeBeforeUpdate = resetLocationRepository.findAll().size();

        // Create the ResetLocation
        ResetLocationDTO resetLocationDTO = resetLocationMapper.toDto(resetLocation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResetLocationMockMvc.perform(put("/api/reset-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resetLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResetLocation in the database
        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResetLocation() throws Exception {
        // Initialize the database
        resetLocationRepository.saveAndFlush(resetLocation);

        int databaseSizeBeforeDelete = resetLocationRepository.findAll().size();

        // Delete the resetLocation
        restResetLocationMockMvc.perform(delete("/api/reset-locations/{id}", resetLocation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ResetLocation> resetLocationList = resetLocationRepository.findAll();
        assertThat(resetLocationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResetLocation.class);
        ResetLocation resetLocation1 = new ResetLocation();
        resetLocation1.setId(1L);
        ResetLocation resetLocation2 = new ResetLocation();
        resetLocation2.setId(resetLocation1.getId());
        assertThat(resetLocation1).isEqualTo(resetLocation2);
        resetLocation2.setId(2L);
        assertThat(resetLocation1).isNotEqualTo(resetLocation2);
        resetLocation1.setId(null);
        assertThat(resetLocation1).isNotEqualTo(resetLocation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResetLocationDTO.class);
        ResetLocationDTO resetLocationDTO1 = new ResetLocationDTO();
        resetLocationDTO1.setId(1L);
        ResetLocationDTO resetLocationDTO2 = new ResetLocationDTO();
        assertThat(resetLocationDTO1).isNotEqualTo(resetLocationDTO2);
        resetLocationDTO2.setId(resetLocationDTO1.getId());
        assertThat(resetLocationDTO1).isEqualTo(resetLocationDTO2);
        resetLocationDTO2.setId(2L);
        assertThat(resetLocationDTO1).isNotEqualTo(resetLocationDTO2);
        resetLocationDTO1.setId(null);
        assertThat(resetLocationDTO1).isNotEqualTo(resetLocationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(resetLocationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(resetLocationMapper.fromId(null)).isNull();
    }
}
