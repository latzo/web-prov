package sn.sentel.tigocare.web.rest;

import sn.sentel.tigocare.TigocareApp;
import sn.sentel.tigocare.domain.Roaming4g;
import sn.sentel.tigocare.repository.Roaming4gRepository;
import sn.sentel.tigocare.service.Roaming4gService;
import sn.sentel.tigocare.service.dto.Roaming4gDTO;
import sn.sentel.tigocare.service.mapper.Roaming4gMapper;
import sn.sentel.tigocare.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static sn.sentel.tigocare.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.sentel.tigocare.domain.enumeration.Roaming4gEnum;
import sn.sentel.tigocare.domain.enumeration.ProcessingStatus;
/**
 * Integration tests for the {@link Roaming4gResource} REST controller.
 */
@SpringBootTest(classes = TigocareApp.class)
public class Roaming4gResourceIT {

    private static final String DEFAULT_IMSI = "AAAAAAAAAA";
    private static final String UPDATED_IMSI = "BBBBBBBBBB";

    private static final Roaming4gEnum DEFAULT_SERVICES = Roaming4gEnum.F4G_ROAMING;
    private static final Roaming4gEnum UPDATED_SERVICES = Roaming4gEnum.NO_4G_ROAMING;

    private static final ProcessingStatus DEFAULT_PROCESSING_STATUS = ProcessingStatus.SUBMITTED;
    private static final ProcessingStatus UPDATED_PROCESSING_STATUS = ProcessingStatus.SUCCEEDED;

    @Autowired
    private Roaming4gRepository roaming4gRepository;

    @Autowired
    private Roaming4gMapper roaming4gMapper;

    @Autowired
    private Roaming4gService roaming4gService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRoaming4gMockMvc;

    private Roaming4g roaming4g;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Roaming4gResource roaming4gResource = new Roaming4gResource(roaming4gService);
        this.restRoaming4gMockMvc = MockMvcBuilders.standaloneSetup(roaming4gResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Roaming4g createEntity(EntityManager em) {
        Roaming4g roaming4g = new Roaming4g()
            .imsi(DEFAULT_IMSI)
            .services(DEFAULT_SERVICES)
            .processingStatus(DEFAULT_PROCESSING_STATUS);
        return roaming4g;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Roaming4g createUpdatedEntity(EntityManager em) {
        Roaming4g roaming4g = new Roaming4g()
            .imsi(UPDATED_IMSI)
            .services(UPDATED_SERVICES)
            .processingStatus(UPDATED_PROCESSING_STATUS);
        return roaming4g;
    }

    @BeforeEach
    public void initTest() {
        roaming4g = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoaming4g() throws Exception {
        int databaseSizeBeforeCreate = roaming4gRepository.findAll().size();

        // Create the Roaming4g
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(roaming4g);
        restRoaming4gMockMvc.perform(post("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isCreated());

        // Validate the Roaming4g in the database
        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeCreate + 1);
        Roaming4g testRoaming4g = roaming4gList.get(roaming4gList.size() - 1);
        assertThat(testRoaming4g.getImsi()).isEqualTo(DEFAULT_IMSI);
        assertThat(testRoaming4g.getServices()).isEqualTo(DEFAULT_SERVICES);
        assertThat(testRoaming4g.getProcessingStatus()).isEqualTo(DEFAULT_PROCESSING_STATUS);
    }

    @Test
    @Transactional
    public void createRoaming4gWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roaming4gRepository.findAll().size();

        // Create the Roaming4g with an existing ID
        roaming4g.setId(1L);
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(roaming4g);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoaming4gMockMvc.perform(post("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Roaming4g in the database
        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkImsiIsRequired() throws Exception {
        int databaseSizeBeforeTest = roaming4gRepository.findAll().size();
        // set the field null
        roaming4g.setImsi(null);

        // Create the Roaming4g, which fails.
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(roaming4g);

        restRoaming4gMockMvc.perform(post("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isBadRequest());

        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkServicesIsRequired() throws Exception {
        int databaseSizeBeforeTest = roaming4gRepository.findAll().size();
        // set the field null
        roaming4g.setServices(null);

        // Create the Roaming4g, which fails.
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(roaming4g);

        restRoaming4gMockMvc.perform(post("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isBadRequest());

        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRoaming4gs() throws Exception {
        // Initialize the database
        roaming4gRepository.saveAndFlush(roaming4g);

        // Get all the roaming4gList
        restRoaming4gMockMvc.perform(get("/api/roaming-4-gs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roaming4g.getId().intValue())))
            .andExpect(jsonPath("$.[*].imsi").value(hasItem(DEFAULT_IMSI.toString())))
            .andExpect(jsonPath("$.[*].services").value(hasItem(DEFAULT_SERVICES.toString())))
            .andExpect(jsonPath("$.[*].processingStatus").value(hasItem(DEFAULT_PROCESSING_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getRoaming4g() throws Exception {
        // Initialize the database
        roaming4gRepository.saveAndFlush(roaming4g);

        // Get the roaming4g
        restRoaming4gMockMvc.perform(get("/api/roaming-4-gs/{id}", roaming4g.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(roaming4g.getId().intValue()))
            .andExpect(jsonPath("$.imsi").value(DEFAULT_IMSI.toString()))
            .andExpect(jsonPath("$.services").value(DEFAULT_SERVICES.toString()))
            .andExpect(jsonPath("$.processingStatus").value(DEFAULT_PROCESSING_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRoaming4g() throws Exception {
        // Get the roaming4g
        restRoaming4gMockMvc.perform(get("/api/roaming-4-gs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoaming4g() throws Exception {
        // Initialize the database
        roaming4gRepository.saveAndFlush(roaming4g);

        int databaseSizeBeforeUpdate = roaming4gRepository.findAll().size();

        // Update the roaming4g
        Roaming4g updatedRoaming4g = roaming4gRepository.findById(roaming4g.getId()).get();
        // Disconnect from session so that the updates on updatedRoaming4g are not directly saved in db
        em.detach(updatedRoaming4g);
        updatedRoaming4g
            .imsi(UPDATED_IMSI)
            .services(UPDATED_SERVICES)
            .processingStatus(UPDATED_PROCESSING_STATUS);
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(updatedRoaming4g);

        restRoaming4gMockMvc.perform(put("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isOk());

        // Validate the Roaming4g in the database
        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeUpdate);
        Roaming4g testRoaming4g = roaming4gList.get(roaming4gList.size() - 1);
        assertThat(testRoaming4g.getImsi()).isEqualTo(UPDATED_IMSI);
        assertThat(testRoaming4g.getServices()).isEqualTo(UPDATED_SERVICES);
        assertThat(testRoaming4g.getProcessingStatus()).isEqualTo(UPDATED_PROCESSING_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingRoaming4g() throws Exception {
        int databaseSizeBeforeUpdate = roaming4gRepository.findAll().size();

        // Create the Roaming4g
        Roaming4gDTO roaming4gDTO = roaming4gMapper.toDto(roaming4g);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoaming4gMockMvc.perform(put("/api/roaming-4-gs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roaming4gDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Roaming4g in the database
        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRoaming4g() throws Exception {
        // Initialize the database
        roaming4gRepository.saveAndFlush(roaming4g);

        int databaseSizeBeforeDelete = roaming4gRepository.findAll().size();

        // Delete the roaming4g
        restRoaming4gMockMvc.perform(delete("/api/roaming-4-gs/{id}", roaming4g.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Roaming4g> roaming4gList = roaming4gRepository.findAll();
        assertThat(roaming4gList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Roaming4g.class);
        Roaming4g roaming4g1 = new Roaming4g();
        roaming4g1.setId(1L);
        Roaming4g roaming4g2 = new Roaming4g();
        roaming4g2.setId(roaming4g1.getId());
        assertThat(roaming4g1).isEqualTo(roaming4g2);
        roaming4g2.setId(2L);
        assertThat(roaming4g1).isNotEqualTo(roaming4g2);
        roaming4g1.setId(null);
        assertThat(roaming4g1).isNotEqualTo(roaming4g2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(Roaming4gDTO.class);
        Roaming4gDTO roaming4gDTO1 = new Roaming4gDTO();
        roaming4gDTO1.setId(1L);
        Roaming4gDTO roaming4gDTO2 = new Roaming4gDTO();
        assertThat(roaming4gDTO1).isNotEqualTo(roaming4gDTO2);
        roaming4gDTO2.setId(roaming4gDTO1.getId());
        assertThat(roaming4gDTO1).isEqualTo(roaming4gDTO2);
        roaming4gDTO2.setId(2L);
        assertThat(roaming4gDTO1).isNotEqualTo(roaming4gDTO2);
        roaming4gDTO1.setId(null);
        assertThat(roaming4gDTO1).isNotEqualTo(roaming4gDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(roaming4gMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(roaming4gMapper.fromId(null)).isNull();
    }
}
