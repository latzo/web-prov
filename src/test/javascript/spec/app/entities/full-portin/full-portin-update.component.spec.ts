/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { FullPortinUpdateComponent } from 'app/entities/full-portin/full-portin-update.component';
import { FullPortinService } from 'app/entities/full-portin/full-portin.service';
import { CreateSubscriber } from 'app/shared/model/create-subscriber.model';

describe('Component Tests', () => {
  describe('FullPortin Management Update Component', () => {
    let comp: FullPortinUpdateComponent;
    let fixture: ComponentFixture<FullPortinUpdateComponent>;
    let service: FullPortinService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [FullPortinUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FullPortinUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FullPortinUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FullPortinService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CreateSubscriber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CreateSubscriber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
