/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { FullPortinDetailComponent } from 'app/entities/full-portin/full-portin-detail.component';
import { CreateSubscriber } from 'app/shared/model/create-subscriber.model';

describe('Component Tests', () => {
  describe('FullPortin Management Detail Component', () => {
    let comp: FullPortinDetailComponent;
    let fixture: ComponentFixture<FullPortinDetailComponent>;
    const route = ({ data: of({ fullPortin: new CreateSubscriber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [FullPortinDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FullPortinDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FullPortinDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fullPortin).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
