/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { FullPortinDeleteDialogComponent } from 'app/entities/full-portin/full-portin-delete-dialog.component';
import { FullPortinService } from 'app/entities/full-portin/full-portin.service';

describe('Component Tests', () => {
  describe('FullPortin Management Delete Component', () => {
    let comp: FullPortinDeleteDialogComponent;
    let fixture: ComponentFixture<FullPortinDeleteDialogComponent>;
    let service: FullPortinService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [FullPortinDeleteDialogComponent]
      })
        .overrideTemplate(FullPortinDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FullPortinDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FullPortinService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
