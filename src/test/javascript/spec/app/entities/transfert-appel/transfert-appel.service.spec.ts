/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { TransfertAppelService } from 'app/entities/transfert-appel/transfert-appel.service';
import { ITransfertAppel, TransfertAppel, OperationEnum } from 'app/shared/model/transfert-appel.model';

describe('Service Tests', () => {
  describe('TransfertAppel Service', () => {
    let injector: TestBed;
    let service: TransfertAppelService;
    let httpMock: HttpTestingController;
    let elemDefault: ITransfertAppel;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(TransfertAppelService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new TransfertAppel(0, 'AAAAAAA', 'AAAAAAA', OperationEnum.ACTIVATE, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a TransfertAppel', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new TransfertAppel(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a TransfertAppel', async () => {
        const returnedFromService = Object.assign(
          {
            msisdnSource: 'BBBBBB',
            msisdnDest: 'BBBBBB',
            operation: 'BBBBBB',
            processingStatus: 'BBBBBB',
            transactionId: 'BBBBBB',
            backendResp: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of TransfertAppel', async () => {
        const returnedFromService = Object.assign(
          {
            msisdnSource: 'BBBBBB',
            msisdnDest: 'BBBBBB',
            operation: 'BBBBBB',
            processingStatus: 'BBBBBB',
            transactionId: 'BBBBBB',
            backendResp: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TransfertAppel', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
