/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { TransfertAppelDeleteDialogComponent } from 'app/entities/transfert-appel/transfert-appel-delete-dialog.component';
import { TransfertAppelService } from 'app/entities/transfert-appel/transfert-appel.service';

describe('Component Tests', () => {
  describe('TransfertAppel Management Delete Component', () => {
    let comp: TransfertAppelDeleteDialogComponent;
    let fixture: ComponentFixture<TransfertAppelDeleteDialogComponent>;
    let service: TransfertAppelService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [TransfertAppelDeleteDialogComponent]
      })
        .overrideTemplate(TransfertAppelDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransfertAppelDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransfertAppelService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
