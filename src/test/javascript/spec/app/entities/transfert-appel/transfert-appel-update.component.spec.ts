/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { TransfertAppelUpdateComponent } from 'app/entities/transfert-appel/transfert-appel-update.component';
import { TransfertAppelService } from 'app/entities/transfert-appel/transfert-appel.service';
import { TransfertAppel } from 'app/shared/model/transfert-appel.model';

describe('Component Tests', () => {
  describe('TransfertAppel Management Update Component', () => {
    let comp: TransfertAppelUpdateComponent;
    let fixture: ComponentFixture<TransfertAppelUpdateComponent>;
    let service: TransfertAppelService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [TransfertAppelUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TransfertAppelUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TransfertAppelUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransfertAppelService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransfertAppel(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransfertAppel();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
