/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { TransfertAppelDetailComponent } from 'app/entities/transfert-appel/transfert-appel-detail.component';
import { TransfertAppel } from 'app/shared/model/transfert-appel.model';

describe('Component Tests', () => {
  describe('TransfertAppel Management Detail Component', () => {
    let comp: TransfertAppelDetailComponent;
    let fixture: ComponentFixture<TransfertAppelDetailComponent>;
    const route = ({ data: of({ transfertAppel: new TransfertAppel(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [TransfertAppelDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TransfertAppelDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransfertAppelDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.transfertAppel).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
