/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { MnpPortingUpdateComponent } from 'app/entities/mnp-porting/mnp-porting-update.component';
import { MnpPortingService } from 'app/entities/mnp-porting/mnp-porting.service';
import { MnpPorting } from 'app/shared/model/mnp-porting.model';

describe('Component Tests', () => {
  describe('MnpPorting Management Update Component', () => {
    let comp: MnpPortingUpdateComponent;
    let fixture: ComponentFixture<MnpPortingUpdateComponent>;
    let service: MnpPortingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [MnpPortingUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MnpPortingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MnpPortingUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MnpPortingService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MnpPorting(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MnpPorting();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
