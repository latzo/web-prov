/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { MnpPortingDetailComponent } from 'app/entities/mnp-porting/mnp-porting-detail.component';
import { MnpPorting } from 'app/shared/model/mnp-porting.model';

describe('Component Tests', () => {
  describe('MnpPorting Management Detail Component', () => {
    let comp: MnpPortingDetailComponent;
    let fixture: ComponentFixture<MnpPortingDetailComponent>;
    const route = ({ data: of({ mnpPorting: new MnpPorting(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [MnpPortingDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MnpPortingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MnpPortingDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mnpPorting).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
