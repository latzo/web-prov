/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { MnpPortingDeleteDialogComponent } from 'app/entities/mnp-porting/mnp-porting-delete-dialog.component';
import { MnpPortingService } from 'app/entities/mnp-porting/mnp-porting.service';

describe('Component Tests', () => {
  describe('MnpPorting Management Delete Component', () => {
    let comp: MnpPortingDeleteDialogComponent;
    let fixture: ComponentFixture<MnpPortingDeleteDialogComponent>;
    let service: MnpPortingService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [MnpPortingDeleteDialogComponent]
      })
        .overrideTemplate(MnpPortingDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MnpPortingDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MnpPortingService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
