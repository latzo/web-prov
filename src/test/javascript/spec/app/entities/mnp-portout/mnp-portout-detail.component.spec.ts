/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { MnpPortoutDetailComponent } from 'app/entities/mnp-portout/mnp-portout-detail.component';
import { MnpPortout } from 'app/shared/model/mnp-portout.model';

describe('Component Tests', () => {
  describe('MnpPortout Management Detail Component', () => {
    let comp: MnpPortoutDetailComponent;
    let fixture: ComponentFixture<MnpPortoutDetailComponent>;
    const route = ({ data: of({ mnpPortout: new MnpPortout(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [MnpPortoutDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MnpPortoutDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MnpPortoutDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mnpPortout).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
