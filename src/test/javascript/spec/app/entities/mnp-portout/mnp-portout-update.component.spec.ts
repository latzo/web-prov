/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { MnpPortoutUpdateComponent } from 'app/entities/mnp-portout/mnp-portout-update.component';
import { MnpPortoutService } from 'app/entities/mnp-portout/mnp-portout.service';
import { MnpPortout } from 'app/shared/model/mnp-portout.model';

describe('Component Tests', () => {
  describe('MnpPortout Management Update Component', () => {
    let comp: MnpPortoutUpdateComponent;
    let fixture: ComponentFixture<MnpPortoutUpdateComponent>;
    let service: MnpPortoutService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [MnpPortoutUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MnpPortoutUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MnpPortoutUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MnpPortoutService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MnpPortout(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MnpPortout();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
