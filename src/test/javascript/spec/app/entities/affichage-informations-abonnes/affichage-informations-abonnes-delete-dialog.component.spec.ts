/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { AffichageInformationsAbonnesDeleteDialogComponent } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes-delete-dialog.component';
import { AffichageInformationsAbonnesService } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes.service';

describe('Component Tests', () => {
  describe('AffichageInformationsAbonnes Management Delete Component', () => {
    let comp: AffichageInformationsAbonnesDeleteDialogComponent;
    let fixture: ComponentFixture<AffichageInformationsAbonnesDeleteDialogComponent>;
    let service: AffichageInformationsAbonnesService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [AffichageInformationsAbonnesDeleteDialogComponent]
      })
        .overrideTemplate(AffichageInformationsAbonnesDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AffichageInformationsAbonnesDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AffichageInformationsAbonnesService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
