/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { AffichageInformationsAbonnesUpdateComponent } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes-update.component';
import { AffichageInformationsAbonnesService } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes.service';
import { AffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';

describe('Component Tests', () => {
  describe('AffichageInformationsAbonnes Management Update Component', () => {
    let comp: AffichageInformationsAbonnesUpdateComponent;
    let fixture: ComponentFixture<AffichageInformationsAbonnesUpdateComponent>;
    let service: AffichageInformationsAbonnesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [AffichageInformationsAbonnesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AffichageInformationsAbonnesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AffichageInformationsAbonnesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AffichageInformationsAbonnesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AffichageInformationsAbonnes(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AffichageInformationsAbonnes();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
