/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { AffichageInformationsAbonnesDetailComponent } from 'app/entities/affichage-informations-abonnes/affichage-informations-abonnes-detail.component';
import { AffichageInformationsAbonnes } from 'app/shared/model/affichage-informations-abonnes.model';

describe('Component Tests', () => {
  describe('AffichageInformationsAbonnes Management Detail Component', () => {
    let comp: AffichageInformationsAbonnesDetailComponent;
    let fixture: ComponentFixture<AffichageInformationsAbonnesDetailComponent>;
    const route = ({ data: of({ affichageInformationsAbonnes: new AffichageInformationsAbonnes(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [AffichageInformationsAbonnesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AffichageInformationsAbonnesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AffichageInformationsAbonnesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.affichageInformationsAbonnes).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
