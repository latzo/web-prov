/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { CreateSubscriberDetailComponent } from 'app/entities/create-subscriber/create-subscriber-detail.component';
import { CreateSubscriber } from 'app/shared/model/create-subscriber.model';

describe('Component Tests', () => {
  describe('CreateSubscriber Management Detail Component', () => {
    let comp: CreateSubscriberDetailComponent;
    let fixture: ComponentFixture<CreateSubscriberDetailComponent>;
    const route = ({ data: of({ createSubscriber: new CreateSubscriber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [CreateSubscriberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CreateSubscriberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CreateSubscriberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.createSubscriber).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
