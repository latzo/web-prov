/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { CreateSubscriberUpdateComponent } from 'app/entities/create-subscriber/create-subscriber-update.component';
import { CreateSubscriberService } from 'app/entities/create-subscriber/create-subscriber.service';
import { CreateSubscriber } from 'app/shared/model/create-subscriber.model';

describe('Component Tests', () => {
  describe('CreateSubscriber Management Update Component', () => {
    let comp: CreateSubscriberUpdateComponent;
    let fixture: ComponentFixture<CreateSubscriberUpdateComponent>;
    let service: CreateSubscriberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [CreateSubscriberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CreateSubscriberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CreateSubscriberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CreateSubscriberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CreateSubscriber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CreateSubscriber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
