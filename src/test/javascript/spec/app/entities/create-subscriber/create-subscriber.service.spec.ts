/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { CreateSubscriberService } from 'app/entities/create-subscriber/create-subscriber.service';
import { ICreateSubscriber, CreateSubscriber, SIMCreationType } from 'app/shared/model/create-subscriber.model';

describe('Service Tests', () => {
  describe('CreateSubscriber Service', () => {
    let injector: TestBed;
    let service: CreateSubscriberService;
    let httpMock: HttpTestingController;
    let elemDefault: ICreateSubscriber;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(CreateSubscriberService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new CreateSubscriber(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        SIMCreationType.IN
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a CreateSubscriber', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new CreateSubscriber(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a CreateSubscriber', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            processingStatus: 'BBBBBB',
            statut: 'BBBBBB',
            transactionId: 'BBBBBB',
            backendResp: 'BBBBBB',
            cos: 'BBBBBB',
            creationType: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of CreateSubscriber', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            processingStatus: 'BBBBBB',
            statut: 'BBBBBB',
            transactionId: 'BBBBBB',
            backendResp: 'BBBBBB',
            cos: 'BBBBBB',
            creationType: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CreateSubscriber', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
