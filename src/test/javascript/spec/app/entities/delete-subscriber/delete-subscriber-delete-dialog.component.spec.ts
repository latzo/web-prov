/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { DeleteSubscriberDeleteDialogComponent } from 'app/entities/delete-subscriber/delete-subscriber-delete-dialog.component';
import { DeleteSubscriberService } from 'app/entities/delete-subscriber/delete-subscriber.service';

describe('Component Tests', () => {
  describe('DeleteSubscriber Management Delete Component', () => {
    let comp: DeleteSubscriberDeleteDialogComponent;
    let fixture: ComponentFixture<DeleteSubscriberDeleteDialogComponent>;
    let service: DeleteSubscriberService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [DeleteSubscriberDeleteDialogComponent]
      })
        .overrideTemplate(DeleteSubscriberDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeleteSubscriberDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeleteSubscriberService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
