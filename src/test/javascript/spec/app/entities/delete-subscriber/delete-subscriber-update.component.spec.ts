/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { DeleteSubscriberUpdateComponent } from 'app/entities/delete-subscriber/delete-subscriber-update.component';
import { DeleteSubscriberService } from 'app/entities/delete-subscriber/delete-subscriber.service';
import { DeleteSubscriber } from 'app/shared/model/delete-subscriber.model';

describe('Component Tests', () => {
  describe('DeleteSubscriber Management Update Component', () => {
    let comp: DeleteSubscriberUpdateComponent;
    let fixture: ComponentFixture<DeleteSubscriberUpdateComponent>;
    let service: DeleteSubscriberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [DeleteSubscriberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DeleteSubscriberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeleteSubscriberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeleteSubscriberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DeleteSubscriber(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DeleteSubscriber();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
