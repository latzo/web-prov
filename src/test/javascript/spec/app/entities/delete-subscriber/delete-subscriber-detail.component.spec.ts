/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { DeleteSubscriberDetailComponent } from 'app/entities/delete-subscriber/delete-subscriber-detail.component';
import { DeleteSubscriber } from 'app/shared/model/delete-subscriber.model';

describe('Component Tests', () => {
  describe('DeleteSubscriber Management Detail Component', () => {
    let comp: DeleteSubscriberDetailComponent;
    let fixture: ComponentFixture<DeleteSubscriberDetailComponent>;
    const route = ({ data: of({ deleteSubscriber: new DeleteSubscriber(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [DeleteSubscriberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DeleteSubscriberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeleteSubscriberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.deleteSubscriber).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
