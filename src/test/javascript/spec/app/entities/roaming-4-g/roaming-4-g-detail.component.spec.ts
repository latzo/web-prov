import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { Roaming4gDetailComponent } from 'app/entities/roaming-4-g/roaming-4-g-detail.component';
import { Roaming4g } from 'app/shared/model/roaming-4-g.model';

describe('Component Tests', () => {
  describe('Roaming4g Management Detail Component', () => {
    let comp: Roaming4gDetailComponent;
    let fixture: ComponentFixture<Roaming4gDetailComponent>;
    const route = ({ data: of({ roaming4g: new Roaming4g(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [Roaming4gDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(Roaming4gDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Roaming4gDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.roaming4g).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
