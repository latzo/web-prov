import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { Roaming4gUpdateComponent } from 'app/entities/roaming-4-g/roaming-4-g-update.component';
import { Roaming4gService } from 'app/entities/roaming-4-g/roaming-4-g.service';
import { Roaming4g } from 'app/shared/model/roaming-4-g.model';

describe('Component Tests', () => {
  describe('Roaming4g Management Update Component', () => {
    let comp: Roaming4gUpdateComponent;
    let fixture: ComponentFixture<Roaming4gUpdateComponent>;
    let service: Roaming4gService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [Roaming4gUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(Roaming4gUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(Roaming4gUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Roaming4gService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Roaming4g(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Roaming4g();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
