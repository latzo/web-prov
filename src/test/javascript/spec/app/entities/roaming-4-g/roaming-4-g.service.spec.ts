import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import { Roaming4gService } from 'app/entities/roaming-4-g/roaming-4-g.service';
import { IRoaming4g, Roaming4g } from 'app/shared/model/roaming-4-g.model';
import { Roaming4gEnum } from 'app/shared/model/enumerations/roaming-4-g-enum.model';
import { ProcessingStatus } from 'app/shared/model/enumerations/processing-status.model';

describe('Service Tests', () => {
  describe('Roaming4g Service', () => {
    let injector: TestBed;
    let service: Roaming4gService;
    let httpMock: HttpTestingController;
    let elemDefault: IRoaming4g;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(Roaming4gService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Roaming4g(0, 'AAAAAAA', Roaming4gEnum.F4G_ROAMING, ProcessingStatus.SUBMITTED);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Roaming4g', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new Roaming4g(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Roaming4g', () => {
        const returnedFromService = Object.assign(
          {
            imsi: 'BBBBBB',
            services: 'BBBBBB',
            processingStatus: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Roaming4g', () => {
        const returnedFromService = Object.assign(
          {
            imsi: 'BBBBBB',
            services: 'BBBBBB',
            processingStatus: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Roaming4g', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
