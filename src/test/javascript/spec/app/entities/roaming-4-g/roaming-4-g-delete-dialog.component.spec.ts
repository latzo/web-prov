import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { Roaming4gDeleteDialogComponent } from 'app/entities/roaming-4-g/roaming-4-g-delete-dialog.component';
import { Roaming4gService } from 'app/entities/roaming-4-g/roaming-4-g.service';

describe('Component Tests', () => {
  describe('Roaming4g Management Delete Component', () => {
    let comp: Roaming4gDeleteDialogComponent;
    let fixture: ComponentFixture<Roaming4gDeleteDialogComponent>;
    let service: Roaming4gService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [Roaming4gDeleteDialogComponent]
      })
        .overrideTemplate(Roaming4gDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(Roaming4gDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(Roaming4gService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
