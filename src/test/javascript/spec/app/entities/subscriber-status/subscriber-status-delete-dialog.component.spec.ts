/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { SubscriberStatusDeleteDialogComponent } from 'app/entities/subscriber-status/subscriber-status-delete-dialog.component';
import { SubscriberStatusService } from 'app/entities/subscriber-status/subscriber-status.service';

describe('Component Tests', () => {
  describe('SubscriberStatus Management Delete Component', () => {
    let comp: SubscriberStatusDeleteDialogComponent;
    let fixture: ComponentFixture<SubscriberStatusDeleteDialogComponent>;
    let service: SubscriberStatusService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [SubscriberStatusDeleteDialogComponent]
      })
        .overrideTemplate(SubscriberStatusDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubscriberStatusDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubscriberStatusService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
