/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { SimSwapUpdateComponent } from 'app/entities/sim-swap/sim-swap-update.component';
import { SimSwapService } from 'app/entities/sim-swap/sim-swap.service';
import { SimSwap } from 'app/shared/model/sim-swap.model';

describe('Component Tests', () => {
  describe('SimSwap Management Update Component', () => {
    let comp: SimSwapUpdateComponent;
    let fixture: ComponentFixture<SimSwapUpdateComponent>;
    let service: SimSwapService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [SimSwapUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SimSwapUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SimSwapUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SimSwapService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SimSwap(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SimSwap();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
