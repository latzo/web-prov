/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { SimSwapDeleteDialogComponent } from 'app/entities/sim-swap/sim-swap-delete-dialog.component';
import { SimSwapService } from 'app/entities/sim-swap/sim-swap.service';

describe('Component Tests', () => {
  describe('SimSwap Management Delete Component', () => {
    let comp: SimSwapDeleteDialogComponent;
    let fixture: ComponentFixture<SimSwapDeleteDialogComponent>;
    let service: SimSwapService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [SimSwapDeleteDialogComponent]
      })
        .overrideTemplate(SimSwapDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SimSwapDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SimSwapService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
