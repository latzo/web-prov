/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { SimSwapDetailComponent } from 'app/entities/sim-swap/sim-swap-detail.component';
import { SimSwap } from 'app/shared/model/sim-swap.model';

describe('Component Tests', () => {
  describe('SimSwap Management Detail Component', () => {
    let comp: SimSwapDetailComponent;
    let fixture: ComponentFixture<SimSwapDetailComponent>;
    const route = ({ data: of({ simSwap: new SimSwap(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [SimSwapDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SimSwapDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SimSwapDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.simSwap).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
