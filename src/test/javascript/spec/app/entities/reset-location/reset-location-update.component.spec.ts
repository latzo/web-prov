/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ResetLocationUpdateComponent } from 'app/entities/reset-location/reset-location-update.component';
import { ResetLocationService } from 'app/entities/reset-location/reset-location.service';
import { ResetLocation } from 'app/shared/model/reset-location.model';

describe('Component Tests', () => {
  describe('ResetLocation Management Update Component', () => {
    let comp: ResetLocationUpdateComponent;
    let fixture: ComponentFixture<ResetLocationUpdateComponent>;
    let service: ResetLocationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ResetLocationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ResetLocationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ResetLocationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResetLocationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResetLocation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResetLocation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
