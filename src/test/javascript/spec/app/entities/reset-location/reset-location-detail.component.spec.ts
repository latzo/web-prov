/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ResetLocationDetailComponent } from 'app/entities/reset-location/reset-location-detail.component';
import { ResetLocation } from 'app/shared/model/reset-location.model';

describe('Component Tests', () => {
  describe('ResetLocation Management Detail Component', () => {
    let comp: ResetLocationDetailComponent;
    let fixture: ComponentFixture<ResetLocationDetailComponent>;
    const route = ({ data: of({ resetLocation: new ResetLocation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ResetLocationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ResetLocationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ResetLocationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.resetLocation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
