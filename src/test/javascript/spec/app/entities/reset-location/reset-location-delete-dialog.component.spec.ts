/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { ResetLocationDeleteDialogComponent } from 'app/entities/reset-location/reset-location-delete-dialog.component';
import { ResetLocationService } from 'app/entities/reset-location/reset-location.service';

describe('Component Tests', () => {
  describe('ResetLocation Management Delete Component', () => {
    let comp: ResetLocationDeleteDialogComponent;
    let fixture: ComponentFixture<ResetLocationDeleteDialogComponent>;
    let service: ResetLocationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ResetLocationDeleteDialogComponent]
      })
        .overrideTemplate(ResetLocationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ResetLocationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResetLocationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
