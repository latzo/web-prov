/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ServiceChangeDetailComponent } from 'app/entities/service-change/service-change-detail.component';
import { ServiceChange } from 'app/shared/model/service-change.model';

describe('Component Tests', () => {
  describe('ServiceChange Management Detail Component', () => {
    let comp: ServiceChangeDetailComponent;
    let fixture: ComponentFixture<ServiceChangeDetailComponent>;
    const route = ({ data: of({ serviceChange: new ServiceChange(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ServiceChangeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ServiceChangeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ServiceChangeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.serviceChange).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
