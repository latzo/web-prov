/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ServiceChangeUpdateComponent } from 'app/entities/service-change/service-change-update.component';
import { ServiceChangeService } from 'app/entities/service-change/service-change.service';
import { ServiceChange } from 'app/shared/model/service-change.model';

describe('Component Tests', () => {
  describe('ServiceChange Management Update Component', () => {
    let comp: ServiceChangeUpdateComponent;
    let fixture: ComponentFixture<ServiceChangeUpdateComponent>;
    let service: ServiceChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ServiceChangeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ServiceChangeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ServiceChangeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ServiceChangeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ServiceChange(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ServiceChange();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
