/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { ServiceChangeDeleteDialogComponent } from 'app/entities/service-change/service-change-delete-dialog.component';
import { ServiceChangeService } from 'app/entities/service-change/service-change.service';

describe('Component Tests', () => {
  describe('ServiceChange Management Delete Component', () => {
    let comp: ServiceChangeDeleteDialogComponent;
    let fixture: ComponentFixture<ServiceChangeDeleteDialogComponent>;
    let service: ServiceChangeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ServiceChangeDeleteDialogComponent]
      })
        .overrideTemplate(ServiceChangeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ServiceChangeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ServiceChangeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
