/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { RoamingServiceDetailComponent } from 'app/entities/roaming-service/roaming-service-detail.component';
import { RoamingService } from 'app/shared/model/roaming-service.model';

describe('Component Tests', () => {
  describe('RoamingServiceEnum Management Detail Component', () => {
    let comp: RoamingServiceDetailComponent;
    let fixture: ComponentFixture<RoamingServiceDetailComponent>;
    const route = ({ data: of({ roamingService: new RoamingService(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [RoamingServiceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RoamingServiceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RoamingServiceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.roamingService).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
