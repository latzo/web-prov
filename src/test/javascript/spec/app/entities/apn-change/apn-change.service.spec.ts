/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { APNChangeService } from 'app/entities/apn-change/apn-change.service';
import { IAPNChange, APNChange, OperationEnum } from 'app/shared/model/apn-change.model';

describe('Service Tests', () => {
  describe('APNChange Service', () => {
    let injector: TestBed;
    let service: APNChangeService;
    let httpMock: HttpTestingController;
    let elemDefault: IAPNChange;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(APNChangeService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new APNChange(0, 'AAAAAAA', 'AAAAAAA', OperationEnum.ACTIVATE, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a APNChange', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new APNChange(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a APNChange', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            operation: 'BBBBBB',
            apn: 'BBBBBB',
            profil: 'BBBBBB',
            processingStatus: 'BBBBBB',
            transactionId: 'BBBBBB',
            createdBy: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of APNChange', async () => {
        const returnedFromService = Object.assign(
          {
            msisdn: 'BBBBBB',
            imsi: 'BBBBBB',
            operation: 'BBBBBB',
            apn: 'BBBBBB',
            profil: 'BBBBBB',
            processingStatus: 'BBBBBB',
            transactionId: 'BBBBBB',
            createdBy: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a APNChange', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
