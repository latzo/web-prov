/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { APNChangeUpdateComponent } from 'app/entities/apn-change/apn-change-update.component';
import { APNChangeService } from 'app/entities/apn-change/apn-change.service';
import { APNChange } from 'app/shared/model/apn-change.model';

describe('Component Tests', () => {
  describe('APNChange Management Update Component', () => {
    let comp: APNChangeUpdateComponent;
    let fixture: ComponentFixture<APNChangeUpdateComponent>;
    let service: APNChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [APNChangeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(APNChangeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(APNChangeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(APNChangeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new APNChange(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new APNChange();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
