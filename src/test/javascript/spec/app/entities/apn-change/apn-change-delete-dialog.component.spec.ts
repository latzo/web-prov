/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { APNChangeDeleteDialogComponent } from 'app/entities/apn-change/apn-change-delete-dialog.component';
import { APNChangeService } from 'app/entities/apn-change/apn-change.service';

describe('Component Tests', () => {
  describe('APNChange Management Delete Component', () => {
    let comp: APNChangeDeleteDialogComponent;
    let fixture: ComponentFixture<APNChangeDeleteDialogComponent>;
    let service: APNChangeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [APNChangeDeleteDialogComponent]
      })
        .overrideTemplate(APNChangeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(APNChangeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(APNChangeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
