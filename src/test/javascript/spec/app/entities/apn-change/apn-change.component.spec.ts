/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TigocareTestModule } from '../../../test.module';
import { APNChangeComponent } from 'app/entities/apn-change/apn-change.component';
import { APNChangeService } from 'app/entities/apn-change/apn-change.service';
import { APNChange } from 'app/shared/model/apn-change.model';

describe('Component Tests', () => {
  describe('APNChange Management Component', () => {
    let comp: APNChangeComponent;
    let fixture: ComponentFixture<APNChangeComponent>;
    let service: APNChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [APNChangeComponent],
        providers: []
      })
        .overrideTemplate(APNChangeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(APNChangeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(APNChangeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new APNChange(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.aPNChanges[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
