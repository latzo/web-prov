/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { APNChangeDetailComponent } from 'app/entities/apn-change/apn-change-detail.component';
import { APNChange } from 'app/shared/model/apn-change.model';

describe('Component Tests', () => {
  describe('APNChange Management Detail Component', () => {
    let comp: APNChangeDetailComponent;
    let fixture: ComponentFixture<APNChangeDetailComponent>;
    const route = ({ data: of({ aPNChange: new APNChange(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [APNChangeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(APNChangeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(APNChangeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.aPNChange).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
