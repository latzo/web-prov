import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ArchivageUpdateComponent } from 'app/entities/archivage/archivage-update.component';
import { ArchivageService } from 'app/entities/archivage/archivage.service';
import { Archivage } from 'app/shared/model/archivage.model';

describe('Component Tests', () => {
  describe('Archivage Management Update Component', () => {
    let comp: ArchivageUpdateComponent;
    let fixture: ComponentFixture<ArchivageUpdateComponent>;
    let service: ArchivageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ArchivageUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ArchivageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ArchivageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ArchivageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Archivage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Archivage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
