import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { TigocareTestModule } from '../../../test.module';
import { ArchivageDeleteDialogComponent } from 'app/entities/archivage/archivage-delete-dialog.component';
import { ArchivageService } from 'app/entities/archivage/archivage.service';

describe('Component Tests', () => {
  describe('Archivage Management Delete Component', () => {
    let comp: ArchivageDeleteDialogComponent;
    let fixture: ComponentFixture<ArchivageDeleteDialogComponent>;
    let service: ArchivageService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ArchivageDeleteDialogComponent]
      })
        .overrideTemplate(ArchivageDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ArchivageDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ArchivageService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
