import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TigocareTestModule } from '../../../test.module';
import { ArchivageComponent } from 'app/entities/archivage/archivage.component';
import { ArchivageService } from 'app/entities/archivage/archivage.service';
import { Archivage } from 'app/shared/model/archivage.model';

describe('Component Tests', () => {
  describe('Archivage Management Component', () => {
    let comp: ArchivageComponent;
    let fixture: ComponentFixture<ArchivageComponent>;
    let service: ArchivageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ArchivageComponent],
        providers: []
      })
        .overrideTemplate(ArchivageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ArchivageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ArchivageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Archivage(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.archivages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
