import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TigocareTestModule } from '../../../test.module';
import { ArchivageDetailComponent } from 'app/entities/archivage/archivage-detail.component';
import { Archivage } from 'app/shared/model/archivage.model';

describe('Component Tests', () => {
  describe('Archivage Management Detail Component', () => {
    let comp: ArchivageDetailComponent;
    let fixture: ComponentFixture<ArchivageDetailComponent>;
    const route = ({ data: of({ archivage: new Archivage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TigocareTestModule],
        declarations: [ArchivageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ArchivageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ArchivageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.archivage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
